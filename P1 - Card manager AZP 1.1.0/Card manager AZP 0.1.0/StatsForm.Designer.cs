﻿namespace Card_manager_AZP
{
    partial class StatsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StatsForm));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.comboBoxPerson = new System.Windows.Forms.ComboBox();
            this.buttonFilter = new System.Windows.Forms.Button();
            this.platnostDoTime = new System.Windows.Forms.DateTimePicker();
            this.platnostOdTime = new System.Windows.Forms.DateTimePicker();
            this.platnostOdDate = new System.Windows.Forms.DateTimePicker();
            this.platnostDoDate = new System.Windows.Forms.DateTimePicker();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.time = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cardId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.userClass = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.eventType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cashChange = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.person = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.title = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pocetVydanych = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pocetPrijatych = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.zmenaPkladny = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.buttonDelAll = new System.Windows.Forms.Button();
            this.buttonExportData = new System.Windows.Forms.Button();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.label1 = new System.Windows.Forms.Label();
            this.pokladnaNulovatButton = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.comboBoxPerson);
            this.groupBox1.Controls.Add(this.buttonFilter);
            this.groupBox1.Controls.Add(this.platnostDoTime);
            this.groupBox1.Controls.Add(this.platnostOdTime);
            this.groupBox1.Controls.Add(this.platnostOdDate);
            this.groupBox1.Controls.Add(this.platnostDoDate);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(780, 56);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Filtr dat";
            // 
            // comboBoxPerson
            // 
            this.comboBoxPerson.FormattingEnabled = true;
            this.comboBoxPerson.Location = new System.Drawing.Point(520, 21);
            this.comboBoxPerson.Name = "comboBoxPerson";
            this.comboBoxPerson.Size = new System.Drawing.Size(183, 21);
            this.comboBoxPerson.TabIndex = 73;
            // 
            // buttonFilter
            // 
            this.buttonFilter.Location = new System.Drawing.Point(709, 21);
            this.buttonFilter.Name = "buttonFilter";
            this.buttonFilter.Size = new System.Drawing.Size(65, 23);
            this.buttonFilter.TabIndex = 1;
            this.buttonFilter.Text = "Filtrovat";
            this.buttonFilter.UseVisualStyleBackColor = true;
            this.buttonFilter.Click += new System.EventHandler(this.buttonFilter_Click);
            // 
            // platnostDoTime
            // 
            this.platnostDoTime.CausesValidation = false;
            this.platnostDoTime.CustomFormat = "HH:mm";
            this.platnostDoTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.platnostDoTime.Location = new System.Drawing.Point(456, 21);
            this.platnostDoTime.Name = "platnostDoTime";
            this.platnostDoTime.ShowUpDown = true;
            this.platnostDoTime.Size = new System.Drawing.Size(54, 20);
            this.platnostDoTime.TabIndex = 72;
            this.platnostDoTime.Value = new System.DateTime(2080, 7, 5, 23, 59, 0, 0);
            // 
            // platnostOdTime
            // 
            this.platnostOdTime.CausesValidation = false;
            this.platnostOdTime.CustomFormat = "HH:mm";
            this.platnostOdTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.platnostOdTime.Location = new System.Drawing.Point(201, 21);
            this.platnostOdTime.Name = "platnostOdTime";
            this.platnostOdTime.ShowUpDown = true;
            this.platnostOdTime.Size = new System.Drawing.Size(54, 20);
            this.platnostOdTime.TabIndex = 71;
            this.platnostOdTime.Value = new System.DateTime(2011, 1, 1, 0, 0, 0, 0);
            // 
            // platnostOdDate
            // 
            this.platnostOdDate.Location = new System.Drawing.Point(57, 22);
            this.platnostOdDate.MaxDate = new System.DateTime(2099, 1, 1, 0, 0, 0, 0);
            this.platnostOdDate.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.platnostOdDate.Name = "platnostOdDate";
            this.platnostOdDate.Size = new System.Drawing.Size(138, 20);
            this.platnostOdDate.TabIndex = 67;
            // 
            // platnostDoDate
            // 
            this.platnostDoDate.Location = new System.Drawing.Point(312, 21);
            this.platnostDoDate.MaxDate = new System.DateTime(2099, 1, 1, 0, 0, 0, 0);
            this.platnostDoDate.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.platnostDoDate.Name = "platnostDoDate";
            this.platnostDoDate.Size = new System.Drawing.Size(138, 20);
            this.platnostDoDate.TabIndex = 68;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 25);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(45, 13);
            this.label10.TabIndex = 69;
            this.label10.Text = "Data od";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(261, 25);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(45, 13);
            this.label11.TabIndex = 70;
            this.label11.Text = "Data do";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.time,
            this.cardId,
            this.userClass,
            this.eventType,
            this.cashChange,
            this.person});
            this.dataGridView1.Location = new System.Drawing.Point(12, 74);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(780, 244);
            this.dataGridView1.TabIndex = 1;
            // 
            // time
            // 
            this.time.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.time.FillWeight = 130F;
            this.time.HeaderText = "Čas";
            this.time.Name = "time";
            this.time.ReadOnly = true;
            this.time.Width = 130;
            // 
            // cardId
            // 
            this.cardId.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.cardId.HeaderText = "Číslo karty";
            this.cardId.Name = "cardId";
            this.cardId.ReadOnly = true;
            // 
            // userClass
            // 
            this.userClass.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.userClass.HeaderText = "Skupina";
            this.userClass.Name = "userClass";
            this.userClass.ReadOnly = true;
            // 
            // eventType
            // 
            this.eventType.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.eventType.HeaderText = "Typ události";
            this.eventType.Name = "eventType";
            this.eventType.ReadOnly = true;
            // 
            // cashChange
            // 
            this.cashChange.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.cashChange.HeaderText = "Změna kreditu";
            this.cashChange.Name = "cashChange";
            this.cashChange.ReadOnly = true;
            // 
            // person
            // 
            this.person.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.person.HeaderText = "Provedl";
            this.person.Name = "person";
            this.person.ReadOnly = true;
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.AllowUserToResizeRows = false;
            this.dataGridView2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.title,
            this.pocetVydanych,
            this.pocetPrijatych,
            this.zmenaPkladny});
            this.dataGridView2.Location = new System.Drawing.Point(12, 353);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.RowHeadersVisible = false;
            this.dataGridView2.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridView2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView2.Size = new System.Drawing.Size(780, 200);
            this.dataGridView2.TabIndex = 2;
            // 
            // title
            // 
            this.title.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.title.HeaderText = "";
            this.title.Name = "title";
            this.title.ReadOnly = true;
            this.title.Width = 19;
            // 
            // pocetVydanych
            // 
            this.pocetVydanych.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.pocetVydanych.HeaderText = "Vydaných karet";
            this.pocetVydanych.Name = "pocetVydanych";
            this.pocetVydanych.ReadOnly = true;
            // 
            // pocetPrijatych
            // 
            this.pocetPrijatych.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.pocetPrijatych.HeaderText = "Přijatých karet";
            this.pocetPrijatych.Name = "pocetPrijatych";
            this.pocetPrijatych.ReadOnly = true;
            // 
            // zmenaPkladny
            // 
            this.zmenaPkladny.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.zmenaPkladny.HeaderText = "Změna pokladny";
            this.zmenaPkladny.Name = "zmenaPkladny";
            this.zmenaPkladny.ReadOnly = true;
            // 
            // buttonDelAll
            // 
            this.buttonDelAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonDelAll.Enabled = false;
            this.buttonDelAll.Location = new System.Drawing.Point(532, 324);
            this.buttonDelAll.Name = "buttonDelAll";
            this.buttonDelAll.Size = new System.Drawing.Size(110, 23);
            this.buttonDelAll.TabIndex = 3;
            this.buttonDelAll.Text = "Vymazat data";
            this.buttonDelAll.UseVisualStyleBackColor = true;
            this.buttonDelAll.Click += new System.EventHandler(this.buttonDelAll_Click);
            // 
            // buttonExportData
            // 
            this.buttonExportData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonExportData.Location = new System.Drawing.Point(648, 324);
            this.buttonExportData.Name = "buttonExportData";
            this.buttonExportData.Size = new System.Drawing.Size(144, 23);
            this.buttonExportData.TabIndex = 4;
            this.buttonExportData.Text = "Export vybraných dat";
            this.buttonExportData.UseVisualStyleBackColor = true;
            this.buttonExportData.Click += new System.EventHandler(this.buttonExportData_Click);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.Filter = "CM AZP log|*.csv";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 337);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(132, 13);
            this.label1.TabIndex = 73;
            this.label1.Text = "Souhrny pro vybraná data:";
            // 
            // pokladnaNulovatButton
            // 
            this.pokladnaNulovatButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pokladnaNulovatButton.Enabled = false;
            this.pokladnaNulovatButton.Location = new System.Drawing.Point(413, 324);
            this.pokladnaNulovatButton.Name = "pokladnaNulovatButton";
            this.pokladnaNulovatButton.Size = new System.Drawing.Size(113, 23);
            this.pokladnaNulovatButton.TabIndex = 74;
            this.pokladnaNulovatButton.Text = "Nulovat pokladnu";
            this.pokladnaNulovatButton.UseVisualStyleBackColor = true;
            this.pokladnaNulovatButton.Click += new System.EventHandler(this.pokladnaNulovatButton_Click);
            // 
            // StatsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(804, 562);
            this.Controls.Add(this.pokladnaNulovatButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonExportData);
            this.Controls.Add(this.buttonDelAll);
            this.Controls.Add(this.dataGridView2);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(820, 600);
            this.Name = "StatsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Card Manager - Statistiky";
            this.Load += new System.EventHandler(this.StatsForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button buttonFilter;
        private System.Windows.Forms.DateTimePicker platnostDoTime;
        private System.Windows.Forms.DateTimePicker platnostOdTime;
        private System.Windows.Forms.DateTimePicker platnostOdDate;
        private System.Windows.Forms.DateTimePicker platnostDoDate;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.Button buttonDelAll;
        private System.Windows.Forms.Button buttonExportData;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button pokladnaNulovatButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn time;
        private System.Windows.Forms.DataGridViewTextBoxColumn cardId;
        private System.Windows.Forms.DataGridViewTextBoxColumn userClass;
        private System.Windows.Forms.DataGridViewTextBoxColumn eventType;
        private System.Windows.Forms.DataGridViewTextBoxColumn cashChange;
        private System.Windows.Forms.DataGridViewTextBoxColumn person;
        private System.Windows.Forms.ComboBox comboBoxPerson;
        private System.Windows.Forms.DataGridViewTextBoxColumn title;
        private System.Windows.Forms.DataGridViewTextBoxColumn pocetVydanych;
        private System.Windows.Forms.DataGridViewTextBoxColumn pocetPrijatych;
        private System.Windows.Forms.DataGridViewTextBoxColumn zmenaPkladny;
    }
}