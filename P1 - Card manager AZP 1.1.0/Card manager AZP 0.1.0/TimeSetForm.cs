﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MFCardAZP;
using RFID_Reader;

namespace Card_manager_AZP
{
    public partial class TimeSetForm : Form
    {
        public TimeSetForm()
        {
            InitializeComponent();
        }

        private void button16_Click(object sender, EventArgs e)
        {
                        TDetekovanaKarta karta;
            try
            {
                lock (Program.hlavniOkno.zamek1)
                {
                    MainForm.Ctecka.SignalizaceWork();
                    karta = MainForm.Ctecka.DetekceKarty();

                    if (karta.TypKarty == TRFIDTagy.MifareClass1kB || karta.TypKarty == TRFIDTagy.MifareClass4kB)
                    {
                        CardMf1AZPv1 servisCard = new CardMf1AZPv1(ref MainForm.Ctecka);
                        TTerminalSets sets = new TTerminalSets();
                        TSetCardEnables setsEnables = new TSetCardEnables();
                        TMfCard_Head hlavickaServiceCard = servisCard.ReadCardHeader();
                        if (hlavickaServiceCard.cardType == 34)
                        {
                            setsEnables.APID = 0xff;
                            setsEnables.TerminalID = 0xff;
                            setsEnables.writeTime = 1;
                            sets.time = new DateTime(platnostOdDate.Value.Year, platnostOdDate.Value.Month, platnostOdDate.Value.Day, platnostOdTime.Value.Hour, platnostOdTime.Value.Minute, 0);
                            servisCard.ServisCardWrite(sets, setsEnables, MainForm.NastaveniAplikace.servisKey);
                            MainForm.Ctecka.SignalizaceOK();
                        }
                        else
                        {
                            MainForm.Ctecka.SignalizaceError();
                            MessageBox.Show("Přiložená karta není \"Servisní časová karta\"", "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        MainForm.Ctecka.SignalizaceError();
                        if (karta.TypKarty != TRFIDTagy.Zadna)
                        {
                            MessageBox.Show("Nepodporovaný typ karty!", "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        else
                        {
                            MessageBox.Show("Karta nenalezena.", "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }

            }
            catch (ExChybaKomunikaceSKartou ex)
            {
                MainForm.Ctecka.SignalizaceError();
                MessageBox.Show(ex.Time.ToString() + " " + ex.Message, "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (ExChybaKomunikaceMfCardAzpV1 ex)
            {
                MainForm.Ctecka.SignalizaceError();
                MessageBox.Show(ex.Time.ToString() + " " + ex.Message, "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                MainForm.Ctecka.SignalizaceIdle();
            }
            Close();
        }

    }
}
