﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Card_manager_AZP
{

    

    /// <summary>
    /// Typ operace s kartou
    /// </summary>
    public enum t_typUdalosti { distribution, withdrawal, cashChange };

    /// <summary>
    /// Kompletní záznam operace s kartou
    /// </summary>
    public class cardEvent
    {
        public int id;
        public DateTime time;
        public int cardNum;
        
        public ushort userClass;
        public t_typUdalosti typUdalosti;
        public int cashChange;
        public int user;

        public cardEvent(int id, DateTime time, int cardNum, ushort userClass, t_typUdalosti typUdalosti, int cashChange, int user)
        {
            this.id = id;
            this.time = time;
            this.cardNum = cardNum;
            this.userClass = userClass;
            this.typUdalosti = typUdalosti;
            this.cashChange = cashChange;
            this.user = user;
        }
    }



    /// <summary>
    /// Třída pro načítání/ukládání záznamů manipulace s kartou
    /// </summary>
    static public class CardEventFile
    {

        /// <summary>
        /// nuluje soubor s událostmi
        /// </summary>
        /// <param name="fileName">jméno souboru</param>
        public static void DelAll(int userId)
        {
            SqlTransaction myTran = null; //vlakno transakce
            string connectionString = "Data source=" + MainForm.NastaveniAplikace.MSSQLServer + ";User ID=" + MainForm.NastaveniAplikace.MSSQLLogin + ";Password=" + MainForm.NastaveniAplikace.MSSQLPassword + ";Connect Timeout=1;Database=CMData";
            SqlConnection conn = new SqlConnection(connectionString);
            try
            {
                conn.Open();
                myTran = conn.BeginTransaction();
                //smazani vsech udalosti pro vybraneho uzivatele nebo pro vsechny uzivatele - 0 vsechny
                SqlCommand CmdDel = (userId == 0) ? new SqlCommand("DELETE FROM [CMData].[dbo].[eventsList]", conn) : new SqlCommand("DELETE FROM [CMData].[dbo].[eventsList] WHERE [userId] = " + userId.ToString(), conn);
                CmdDel.Transaction = myTran;
                CmdDel.ExecuteNonQuery();
                myTran.Commit(); //transakce bude dokoncena
            }
            catch
            {
                try
                {
                    myTran.Rollback();
                }
                catch { }
                // MessageBox.Show("Během ukládání transakce do databáze došlo k chybě.", "Chyba spojení s databází.", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                conn.Close();
            }
        }

        /// <summary>
        /// Načte ze souboru vešekéré události
        /// </summary>
        /// <param name="fileName">jméno souboru</param>
        /// <returns>Seznam všech událostí</returns>
        public static List<cardEvent> ReadAllEvents(int userId, DateTime from, DateTime to)
        {
            
            List<cardEvent> SeznamUdalostiBuff = new List<cardEvent>();
            //nacteni vybranych udalosti z databaze
            string connectionString = "Data source=" + MainForm.NastaveniAplikace.MSSQLServer + ";User ID=" + MainForm.NastaveniAplikace.MSSQLLogin + ";Password=" + MainForm.NastaveniAplikace.MSSQLPassword + ";Connect Timeout=1;Database=CMData";
            SqlConnection conn = new SqlConnection(connectionString);
            try
            {
                conn.Open();
                SqlCommand CmdSelect;
                if (userId != 0)
                {
                    CmdSelect = new SqlCommand(@"SET LANGUAGE BRITISH; SELECT [id],[eventTime],[cardNum],[userClass],[userId],[eventType],[cashChange] FROM [CMData].[dbo].[eventsList] WHERE [userId] = " + userId.ToString() + " AND [eventTime] >= '" + from + "' AND [eventTime] <= '" + to + "'", conn);
                }
                else
                {
                    CmdSelect = new SqlCommand(@"SET LANGUAGE BRITISH; SELECT [id],[eventTime],[cardNum],[userClass],[userId],[eventType],[cashChange] FROM [CMData].[dbo].[eventsList] WHERE [eventTime] >= '" + from + "' AND [eventTime] <= '" + to + "'", conn);
                }

                SqlDataReader dataReader;
                dataReader = CmdSelect.ExecuteReader();

                if (dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        SeznamUdalostiBuff.Add(new cardEvent(dataReader.GetInt32(0), dataReader.GetDateTime(1), dataReader.GetInt32(2), (ushort)dataReader.GetByte(3), (t_typUdalosti)dataReader.GetByte(5), dataReader.GetInt32(6), dataReader.GetInt16(4)));
                    }
                }
                dataReader.Close();
            }
            catch
            {
            }
            finally
            {
                conn.Close();
            }
            return SeznamUdalostiBuff;
        }
    }
}
