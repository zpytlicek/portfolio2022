﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Security.Cryptography;
using System.Data.SqlClient;

namespace Card_manager_AZP
{

    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            InitializeComponent();
        }

        public class ComboboxItem
        {
            public string Text { get; set; }
            public object Value { get; set; }

            public override string ToString()
            {
                return Text;
            }
        }

        private void LoginForm_Load(object sender, EventArgs e)
        {
            //nacteni seznamu uzivatelů z databáze
            string connectionString = "Data source=" + MainForm.NastaveniAplikace.MSSQLServer + ";User ID=" + MainForm.NastaveniAplikace.MSSQLLogin + ";Password=" + MainForm.NastaveniAplikace.MSSQLPassword + ";Connect Timeout=1;Database=CMData";
            SqlConnection conn = new SqlConnection(connectionString);
            try
            {
                conn.Open();
                SqlCommand CmdSelectPermanetka = new SqlCommand(@"SELECT [id],[nickname] FROM [CMData].[dbo].[users]", conn);
                // string dataReader = CmdAddARow.ExecuteScalar().ToString();
                SqlDataReader dataReader;
                dataReader = CmdSelectPermanetka.ExecuteReader();

                login.Items.Clear(); //vymazani stavajiciho seznamu

                while (dataReader.Read())
                {
                    ComboboxItem item = new ComboboxItem();
                    item.Text = dataReader.GetString(1);
                    item.Value = dataReader.GetInt32(0);
                    login.Items.Add(item);

                }
                if (login.Items.Count > 0) login.SelectedIndex = 0;
                dataReader.Close();
            }
            catch 
            {
             //   MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string heslo = MainForm.PasswordEncryptor(password.Text);
            //nacteni informaci o aktualne vybranem uzivateli
            string connectionString = "Data source=" + MainForm.NastaveniAplikace.MSSQLServer + ";User ID=" + MainForm.NastaveniAplikace.MSSQLLogin + ";Password=" + MainForm.NastaveniAplikace.MSSQLPassword + ";Connect Timeout=1;Database=CMData";
            SqlConnection conn = new SqlConnection(connectionString);
            try
            {
                conn.Open();

                //kontrola zda toto uzivatelske jmeno existuje
                SqlCommand CmdSelectPermanetka = new SqlCommand(@"SELECT [nickname],[password] FROM [CMData].[dbo].[users] WHERE [id] = " + ((ComboboxItem)login.SelectedItem).Value, conn);

                SqlDataReader dataReader;
                dataReader = CmdSelectPermanetka.ExecuteReader();
                if (dataReader.HasRows)
                {
                    dataReader.Read();
                    //overeni hesla
                    if (dataReader.GetString(1) == heslo)
                    {
                        //heslo je zadáno dobře
                        Program.hlavniOkno.UserId = (int)((ComboboxItem)login.SelectedItem).Value;
                        Program.hlavniOkno.UserName = dataReader.GetString(0);
                    }
                    else
                    {
                        //heslo bylo zadáno špatně
                        MessageBox.Show("Nesprávné heslo.","Chybné přihlášení.",MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
                dataReader.Close();
            }
            catch
            {
                MessageBox.Show("Příhlášení se nezdařilo. Zkontrolujte spojení s databází.", "Chybné přihlášení.", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                conn.Close();
            }
            Program.hlavniOkno.UserInfoRefresh();
            Close();
        }

        private void password_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)System.Windows.Forms.Keys.Enter)
            {
                button1_Click(null, null);
            }
        }
    }
}
