﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RFID_Reader;


namespace MFCardAZP
{

    /// <summary>
    /// Knihovna pro ovládání karet MF Classic 1 a 4kB, RFID Systém AZP v1
    /// Ver 0.1.1
    /// 
    /// Poslední úprava: 11.12.2012 - ZP 
    /// 
    /// Ver 0.1.1 (11.12.2012) - doplnena fce. CheckValueBlock - kontrola zda byl spravne inicializovan value block
    ///                          doplnena fce. CheckBackup - Kontroluje zda je posledni zaznam v poradku, pripadne ho obnovuje
    ///                          opravena fce. RecordAddNew - opraveno pridavani zaznamu na konec karty s prepisem
    /// </summary>

    /// <summary>
    /// recorBlocku AZP v1 (64b)
    /// </summary>
    public struct dt_recordAzp
    {
        public DateTime datum;
        public byte apiId,tId;
        public int credit;
    }


    /// <summary>
    /// Datovy typ s proměnnými které se mají programovat přes servisní kartu
    /// </summary>
    [Serializable]
    public class TSetCardEnables
    {
        public byte writeUsersSets = 0;
        public byte writeBlockedCards = 0;
        public byte writeProductionNum = 0;
        public byte writeUseCounter = 0;
        public byte writeTerminalId = 0;
        public byte writeCustomerKeys = 0;
        public byte writeHwRegisters = 0;
        public byte writeUsersRegisters = 0;
        public byte writeTime = 0;
        public byte APID = 0xff;
        public byte TerminalID = 0xff;
    }

    /// <summary>
    /// UserSet u1/4 - Datový typ pro nastaveni jedne casove zony pro uzivatele - doba od HH:MM do HH:MM + tarif za tuto zonu
    /// </summary>
    [Serializable]
    public struct TUserDayZone
    {
        public byte startHour;
        public byte startMinute;
        public byte endHour;
        public byte endMinute;
        public ushort tarif;
    }

    /// <summary>
    /// UserSet u2/4 - Datový typ pro nastaveni jednoho dne uzivatele
    /// </summary>
    [Serializable]
    public struct TUserSetDay
    {
        public TUserDayZone UserDayZone1;
        public TUserDayZone UserDayZone2;
        public TUserDayZone UserDayZone3;
    }

    /// <summary>
    /// Datový typ pro hlavičku na kartě AZP / základní informace o kartě
    /// </summary>
    public struct TMfCard_Head
    {
        public byte cardType; // typ karty
        public int cardNum; // unikátní číslo karty
        public DateTime validFrom; //platnost od
        public DateTime validTo; //platnost do
        public byte personClass; //číslo skupiny
        public byte overWriteData; //číslo skupiny
        public int allowDebit; // dovolený debet
        public int pMin, pStart, pActual; //pointery pro záznam proběhlých událostí
    }

    /// <summary>
    /// UserSet u3/4 - Datovy typ pro ulozeni nastaveni uzivatele
    /// </summary>
    [Serializable]
    public class TUserSets {
        public TUserSetDay[] userDay = new TUserSetDay[7];
        public ushort[] uSet = new ushort[10];
    }

    /// <summary>
    /// UserSet u4/4 - Datovy typ pro ulozeni nastaveni vsech uzivatelu
    /// </summary>
    [Serializable]
    public class TUsersSets
    {
        public TUserSets[] user = new TUserSets[7] { new TUserSets(), new TUserSets(), new TUserSets(), new TUserSets(), new TUserSets(), new TUserSets(), new TUserSets() };
    }

    /// <summary>
    /// Datovy typ seznam blokovanych karet
    /// </summary>
    [Serializable]
    public class TBlockedCards
    {
        public uint[] CardNumber = new uint[20];
    }

    /// <summary>
    /// Datovy typ pro nastavení terminalu
    /// </summary>
    [Serializable]
    public class TTerminalSets
    {
        //**variabilní promenné**//
        public TUsersSets usersSets = new TUsersSets(); //nastaveni uzivatelskych skupin
        public TBlockedCards blockedCards = new TBlockedCards(); //seznam blokovanych karet
        public uint productionNum = 0; // vyrobni cislo
        public uint useCounter = 0; // pocet pouziti ctecky
        public byte terminalId = 0; //0 - 127
        public byte[] customerKey = new byte[8]; //heslo pro uživatelské karty
        public byte[] customerServisKey = new byte[8]; //heslo pro servisní nastavení
        public ushort[] hwRegisters = new ushort[20]; //nastavení HW - nepřístupné pro uživatele
        public ushort[] usersRegisters = new ushort[20]; //nastavení aplikace
        public DateTime time = DateTime.Now; //čas pro nastavení

        //**HW promenné**//
        /// <summary>
        /// Číslo aplikace 0x00 - 0xff / 0xff pro všechny aplikace
        /// </summary>
        public byte apiId; //0 - 255
        //verze firmware terminalu
        public byte verMajor; //0 - 255
        public byte verMinor; //0 - 255
        public byte verRevize; //0 - 255
        //datum programovaní ctecky
        public byte programmingYear; //0 - 99
        public byte programmingMonth; //0 - 12
        public byte programmingDay; //0 - 31
    }



    public class ExChybaKomunikaceMfCardAzpV1 : Exception
    {
        // Při vyvolání vyjímky se uloží i čas kdy k vyjímce došlo
        private DateTime time;
        // Funkce která vyjímku vyvolala
        private String Fce;
        private int ErrorNum;

        public ExChybaKomunikaceMfCardAzpV1(string message)
            : base(message)
        {
            time = DateTime.Now;
            Fce = "";
            ErrorNum = 0;
        }

        public ExChybaKomunikaceMfCardAzpV1(string message, int errorNum)
            : base(message)
        {
            time = DateTime.Now;
            Fce = "";
            ErrorNum = errorNum;
        }

        public ExChybaKomunikaceMfCardAzpV1(string message, string vyvolalaFce, int errorNum)
            : base(message)
        {
            time = DateTime.Now;
            Fce = vyvolalaFce;
            ErrorNum = errorNum;
        }

        public ExChybaKomunikaceMfCardAzpV1(string message, string vyvolalaFce)
            : base(message)
        {
            time = DateTime.Now;
            Fce = vyvolalaFce;
            ErrorNum = 0;
        }

        public String VyvolalaFce
        {
            get { return Fce; }
        }

        public int JakeErrorNum
        {
            get { return ErrorNum; }
        }

        public DateTime Time
        {
            get { return time; }
        }
    }

    /// <summary>
    /// Třída s nastavením aplikace (HW Klíč)
    /// </summary>
    class HWKeyAZPv1
    {
        public bool[] PovoleneAPID = new bool[256]; //pole kde je vyznaceno 1 == polovene APID, 0 == zakazane APID, index pole odpovida APID
        public char[] CustomerName = new char[16]; //poznamka na karte, jmeno zakaznika
        public byte[] CustomerUserKey = new byte[6]; //klíč pro uživatelské karty
        public byte[] CustomerServisKey = new byte[6]; //servisní klíč aplikace
        public DateTime ReleaseDate = DateTime.Now; //datum přidělení
    }

    /// <summary>
    /// Třída pro obsluhu dat na kartě MF Classic 1k,4k dle formátování AZP vre 1.0.0
    /// Ver 0.1.0
    /// 
    /// Poslední úprava: 3.04.2012 - ZP 
    /// </summary>
    /// <remarks>
    /// Pozn.: Dodelat fce pro nastaveni, nacteni casu ctecky. 
    /// 
    /// </remarks>
    class CardMf1AZPv1
    {
        TTerminalSets terminalSets = new TTerminalSets();
        
        private RFIDReader PcCtecka;
        /// <summary>
        /// Inicializace tridy pro AZP karty MF1 v1
        /// </summary>
        /// <param name="ctecka">reference na tridu ctekcky</param>
        public CardMf1AZPv1(ref RFID_Reader.RFIDReader ctecka){
            PcCtecka = ctecka;
            
        }

        byte[] HeaderKeyAZP = new byte[6] { 0x19, 0x92, 0xae, 0xb0, 0x20, 0x12 }; //univerzalni klic pro hlavicky karet AZP
        //byte[] HeaderKeyAZP = new byte[6] { 0xff, 0xff, 0xff, 0xff, 0xff, 0xff };


        /// <summary>
        /// Nastavení všech klíčů na kartě AZP formatovani ver 1.
        /// </summary>
        /// <param name="oldHeaderKey">Současný klíč hlavičkového sektoru</param>
        /// <param name="newHeaderKey">Nový klíč hlavičkového sektoru</param>
        /// <param name="oldCardKey">Současný klíč pro sektory</param>
        /// <param name="newCardKey">Nový klíč pro sektory</param>
        public void SetKeysMfCardAzpV1(byte[] oldHeaderKey, byte[] newHeaderKey, byte[] oldCardKey, byte[] newCardKey)
        {
                TDetekovanaKarta karta;
                karta = PcCtecka.DetekceKarty();
                if (karta.TypKarty == TRFIDTagy.MifareClass1kB || karta.TypKarty == TRFIDTagy.MifareClass4kB){
                    //karta je mifare classic, lze ji tedy formatovat

                    //nastavení klíče header sektrou
                    try
                    {
                        //autorizace karty (header sektor - sektor 1)
                        PcCtecka.MifareClassicAuthentication(7, oldHeaderKey);
                    }
                    catch (ExChybaKomunikaceSKartou)
                    {
                        //pokud se nepodari autorizace s prvnim key, vyzkousi se druhy, zda vse probehlo ok
                        karta = PcCtecka.DetekceKarty();
                        PcCtecka.MifareClassicAuthentication(7, newHeaderKey);
                    }
                    PcCtecka.MifareClassicWriteTrailer(7, PcCtecka.MFClassicTrailerSectorGernerator(newHeaderKey, new byte[6] { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }, 0x00, TMFAccessMode.PouzeKeyA));

                    //formatovani sektoru 0
                    try
                    {
                        //autorizace karty (header sektor - sektor 1)
                        PcCtecka.MifareClassicAuthentication(3, oldCardKey);
                    }
                    catch (ExChybaKomunikaceSKartou)
                    {
                        //pokud se nepodari autorizace s prvnim key, vyzkousi se druhy, zda vse probehlo ok
                        karta = PcCtecka.DetekceKarty();
                        PcCtecka.MifareClassicAuthentication(3, newCardKey);
                    }
                    PcCtecka.MifareClassicWriteTrailer(3, PcCtecka.MFClassicTrailerSectorGernerator(newCardKey, new byte[6] { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }, 0x00, TMFAccessMode.PouzeKeyA));

                    //formatovani zbyvajicich trailer sektorů dle velikosti karty
                    byte maxSec = 255;
                    if (karta.TypKarty == TRFIDTagy.MifareClass1kB)
                    {
                        maxSec = 63;
                    }
    
                    for (int i = 8; i <= maxSec; i++)
                    {
                        //overuje zda se jedná o trailer sector
                        if (PcCtecka.IsBlockTrailer((byte)i) == 1) {
                            //format sektoru ktery vyhovel testu na trailer
                            try
                            {
                                //autorizace karty (header sektor - sektor 1)
                                PcCtecka.MifareClassicAuthentication((byte)i, oldCardKey);
                            }
                            catch (ExChybaKomunikaceSKartou)
                            {
                                //pokud se nepodari autorizace s prvnim key, vyzkousi se druhy, zda vse probehlo ok
                                karta = PcCtecka.DetekceKarty();
                                PcCtecka.MifareClassicAuthentication((byte)i, newCardKey);
                            }
                            PcCtecka.MifareClassicWriteTrailer((byte)i, PcCtecka.MFClassicTrailerSectorGernerator(newCardKey, new byte[6] { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }, 0x00, TMFAccessMode.PouzeKeyA));
                        }
                    }


                } else {
                    throw new ExChybaKomunikaceMfCardAzpV1("Formátovaná karta není typu MifareClassic.", "");
                }
        }

        /// <summary>
        /// recordPointerInc - Inkrementace posledniho zaznamu o 1 dle zrozlozeni bloku na karte
        /// </summary>
        /// <param name="pActual">ukazatel na posledni zaznam v poly zaznamu (pokud je 0 - dojde k inicializaci pole na hodnote pMin, je treba udelat v PC, terminaly nezabezpeci nulovani!)</param>
        /// <param name="pMin">minimalni hodnota pole zaznamu</param>
        /// <param name="pStart">hodnota urcujici zacatek pole zaznamu</param>
        /// <param name="overWriteRecordEnable">urcuje zda je povolen prepis karty, 0 - prepis neni dovolen, 1 - prepis je dovolen</param>
        /// <param name="cardType">typ karty dle typu TRFIDTagy</param>
        /// <returns>inkrementovany pointer (rotace pres hodnoty od 1 do velikosti karty)</returns>
        public int RecordPointerInc(int pActual, int pMin, int pStart, byte overWriteRecordEnable, TRFIDTagy cardType)
        {
            //stanoveni maximalniho pointeru dle velikosti karty
            //maximalni pointer + 1, tedy pointer ktery je mimo kapacitu karty
            int pMax;
            int pActualInc;
            if (cardType == TRFIDTagy.MifareClass1kB)
            {
                pMax = 126; //posledni trailsector je v blocku 63
            }
            else
            {
                pMax = 510; //posledni trailsector je v blocku 255
            }

            if (pActual != 0)
            {
                //prvnotni inkrementace
                pActualInc = ++pActual;
                //kontrola zda se nejedn� o konec pole, pokud ano tak se nastav� na zacatek pole
                if (pActualInc == pMax) pActualInc = pMin;
                //pokud se jedna o trailer block, tak se vraci incPointer posunuty o dalsi dva 
                if (PcCtecka.IsBlockTrailer((byte)(pActualInc / 2)) != 0)
                {
                    pActualInc += 2;
                }
                if (overWriteRecordEnable == 0 && pActualInc == pStart)
                {
                    //stejn� record jako prvni zaznam v poly recordu
                    return (0); //karta je plna
                }
                else
                {
                    return (pActualInc);
                }
            }
            else
            {
                //aktualni pointer byl nastaven na nulu, jedna se tedy o prvn� inicializaci pole, hodnota zaznamu bude nastavena na pMin
                return pMin;
            }
        }

        /// <summary>
        /// Převod typu DayOfWeek na číslo dne v týdnu po = 1, ne = 7
        /// </summary>
        /// <param name="dayOfWekk">typu DayOfWeek</param>
        /// <returns>číslo dne v týdnu po = 1, ne = 7</returns>
        int DayOfWeekToInt(DayOfWeek dayOfWekk){
            switch (dayOfWekk){
                case DayOfWeek.Monday:
                    return (1);
                case DayOfWeek.Tuesday:
                    return (2);
                case DayOfWeek.Wednesday:
                    return (3);
                case DayOfWeek.Thursday:
                    return (4);
                case DayOfWeek.Friday:
                    return (5);
                case DayOfWeek.Saturday:
                    return (6);
                default: return (7);

            }
        }

        /// <summary>
        /// Uložení servisního nastavení na kartu Mifare4kB ver.0.1.0
        /// </summary>
        /// <param name="nastaveni">TTerminalSets - nastaveni terminalu ver.0.1.0</param>
        /// <param name="writeEnables">TSetCardEnables - určuje která data bude karta nastavovat</param>
        /// <param name="carKey">byte[8] - klíč pro přístup k sektorům</param>
        public void ServisCardWrite(TTerminalSets nastaveni, TSetCardEnables writeEnables, byte[] carKey)
        {
            byte[] dataBuff = new byte[16];

            //zapsání bloku 1 - nastavení registrů které se mají zapsat
            PcCtecka.MifareClassicAuthentication(1, carKey);
            dataBuff[0] = writeEnables.writeUsersSets;
            dataBuff[1] = writeEnables.writeBlockedCards;
            dataBuff[2] = writeEnables.writeProductionNum;
            dataBuff[3] = writeEnables.writeUseCounter;
            dataBuff[4] = writeEnables.writeTerminalId;
            dataBuff[5] = writeEnables.writeCustomerKeys;
            dataBuff[6] = writeEnables.writeHwRegisters;
            dataBuff[7] = writeEnables.writeUsersRegisters;
            dataBuff[8] = writeEnables.writeTime;
            dataBuff[14] = writeEnables.APID;
            dataBuff[15] = writeEnables.TerminalID;
            PcCtecka.MifareClassicWrite(1, dataBuff);

            //zápis bloku 2 - vseobecné nastavení ApiID,Ver,ProgramingTime,ProductionNum,UseCounter,TerminalId
            if (writeEnables.writeProductionNum != 0 || writeEnables.writeUseCounter != 0 || writeEnables.writeTerminalId != 0)
            {
                //nulovani vsech registru
                for (int i = 0; i < 16; i++)
                {
                    dataBuff[i] = 0;
                }
                //zapis prislusnych dat
                dataBuff[7] = (byte)((nastaveni.productionNum >> 24) & 0xff);
                dataBuff[8] = (byte)((nastaveni.productionNum >> 16) & 0xff);
                dataBuff[9] = (byte)((nastaveni.productionNum >> 8) & 0xff);
                dataBuff[10] = (byte)(nastaveni.productionNum & 0xff);
                dataBuff[11] = (byte)((nastaveni.useCounter >> 24) & 0xff);
                dataBuff[12] = (byte)((nastaveni.useCounter >> 16) & 0xff);
                dataBuff[13] = (byte)((nastaveni.useCounter >> 8) & 0xff);
                dataBuff[14] = (byte)(nastaveni.useCounter & 0xff);
                dataBuff[15] = (byte)nastaveni.terminalId;
                PcCtecka.MifareClassicWrite(2, dataBuff);
            }

            //zápis bloku 8 - nastavení času
            if (writeEnables.writeTime != 0)
            {
                //nulovani vsech registru
                for (int i = 0; i < 16; i++)
                {
                    dataBuff[i] = 0;
                }
                //zapis prislusnych dat (čas)
                dataBuff[0] = (byte)((nastaveni.time.Year % 100) & 0xff);
                dataBuff[1] = (byte)(nastaveni.time.Month & 0xff);
                dataBuff[2] = (byte)(nastaveni.time.Day & 0xff);
                dataBuff[3] = (byte)(nastaveni.time.Hour & 0xff);
                dataBuff[4] = (byte)(nastaveni.time.Minute & 0xff);
                dataBuff[5] = (byte)(nastaveni.time.Second & 0xff);
                dataBuff[6] = (byte)(DayOfWeekToInt(nastaveni.time.DayOfWeek) & 0xff);
                //autorizace a ulozeni dat
                PcCtecka.MifareClassicAuthentication(8, carKey);
                PcCtecka.MifareClassicWrite(8, dataBuff);
            }

            //zápis bloku 9 - customersKeys
            if (writeEnables.writeCustomerKeys != 0)
            {
                //nulovani vsech registru
                for (int i = 0; i < 16; i++)
                {
                    dataBuff[i] = 0;
                }
                //zapis prislusnych klíčů
                for (int i = 0; i < 8; i++)
                {
                    dataBuff[i] = nastaveni.customerKey[i];
                }
                for (int i = 0; i < 8; i++)
                {
                    dataBuff[i+8] = nastaveni.customerServisKey[i];
                }
                //autorizace a ulozeni dat
                PcCtecka.MifareClassicAuthentication(9, carKey);
                PcCtecka.MifareClassicWrite(9, dataBuff);
            }

            //zápis bloku 12 - 14 - HWRegisters
            if (writeEnables.writeHwRegisters != 0)
            {
                int HWRegisterPointer = 0;
                //zapis HWRegistru 0-7
                for (int i = 0; i < 16; i++)
                {
                    dataBuff[i] = (byte)((nastaveni.hwRegisters[HWRegisterPointer] >> 8) & 0xff);
                    i++;
                    dataBuff[i] = (byte)(nastaveni.hwRegisters[HWRegisterPointer] & 0xff);
                    HWRegisterPointer++;
                }
                //autorizace a ulozeni dat
                PcCtecka.MifareClassicAuthentication(12, carKey);
                PcCtecka.MifareClassicWrite(12, dataBuff);

                //zapis HWRegistru 8-15
                for (int i = 0; i < 16; i++)
                {
                    dataBuff[i] = (byte)((nastaveni.hwRegisters[HWRegisterPointer] >> 8) & 0xff);
                    i++;
                    dataBuff[i] = (byte)(nastaveni.hwRegisters[HWRegisterPointer] & 0xff);
                    HWRegisterPointer++;
                }
                //autorizace a ulozeni dat
                PcCtecka.MifareClassicAuthentication(13, carKey);
                PcCtecka.MifareClassicWrite(13, dataBuff);

                //nulovani vsech registru
                for (int i = 0; i < 16; i++)
                {
                    dataBuff[i] = 0;
                }

                //zapis HWRegistru 16-19
                for (int i = 0; i < 8; i++)
                {
                    dataBuff[i] = (byte)((nastaveni.hwRegisters[HWRegisterPointer] >> 8) & 0xff);
                    i++;
                    dataBuff[i] = (byte)(nastaveni.hwRegisters[HWRegisterPointer] & 0xff);
                    HWRegisterPointer++;
                }
                //autorizace a ulozeni dat
                PcCtecka.MifareClassicAuthentication(14, carKey);
                PcCtecka.MifareClassicWrite(14, dataBuff);
            }

            //zápis bloku 16 - 18 - UsersRegisters
            if (writeEnables.writeUsersRegisters != 0)
            {
                int UsersRegisterPointer = 0;
                //zapis UsersRegisters 0-7
                for (int i = 0; i < 16; i++)
                {
                    dataBuff[i] = (byte)((nastaveni.usersRegisters[UsersRegisterPointer] >> 8) & 0xff);
                    i++;
                    dataBuff[i] = (byte)(nastaveni.usersRegisters[UsersRegisterPointer] & 0xff);
                    UsersRegisterPointer++;
                }
                //autorizace a ulozeni dat
                PcCtecka.MifareClassicAuthentication(16, carKey);
                PcCtecka.MifareClassicWrite(16, dataBuff);

                //zapis UsersRegisters 8-15
                for (int i = 0; i < 16; i++)
                {
                    dataBuff[i] = (byte)((nastaveni.usersRegisters[UsersRegisterPointer] >> 8) & 0xff);
                    i++;
                    dataBuff[i] = (byte)(nastaveni.usersRegisters[UsersRegisterPointer] & 0xff);
                    UsersRegisterPointer++;
                }
                //autorizace a ulozeni dat
                PcCtecka.MifareClassicAuthentication(17, carKey);
                PcCtecka.MifareClassicWrite(17, dataBuff);

                //nulovani vsech registru
                for (int i = 0; i < 16; i++)
                {
                    dataBuff[i] = 0;
                }

                //zapis UsersRegisters 16-19
                for (int i = 0; i < 8; i++)
                {
                    dataBuff[i] = (byte)((nastaveni.usersRegisters[UsersRegisterPointer] >> 8) & 0xff);
                    i++;
                    dataBuff[i] = (byte)(nastaveni.usersRegisters[UsersRegisterPointer] & 0xff);
                    UsersRegisterPointer++;
                }
                //autorizace a ulozeni dat
                PcCtecka.MifareClassicAuthentication(18, carKey);
                PcCtecka.MifareClassicWrite(18, dataBuff);
            }

            //zápis bloku 20 - 22 a 24 - 25 - BlockedCards 0 - 19
            if (writeEnables.writeBlockedCards != 0)
            {
                int BlockedCardsPointer = 0;
                byte[] listOfBlockNum = new byte[5] { 20, 21, 22, 24, 25 };

                for (int blockIndex = 0; blockIndex < 5; blockIndex++) //kazdy run zapise jeden blok po 4 kartach
                {
                    //zapis BlockedCards 0-19
                    
                    for (int i = 0; i < 16; i++)
                    {
                        dataBuff[i] = (byte)((nastaveni.blockedCards.CardNumber[BlockedCardsPointer] >> 24) & 0xff);
                        i++;
                        dataBuff[i] = (byte)((nastaveni.blockedCards.CardNumber[BlockedCardsPointer] >> 16) & 0xff);
                        i++;
                        dataBuff[i] = (byte)((nastaveni.blockedCards.CardNumber[BlockedCardsPointer] >> 8) & 0xff);
                        i++;
                        dataBuff[i] = (byte)(nastaveni.blockedCards.CardNumber[BlockedCardsPointer] & 0xff);
                        BlockedCardsPointer++;
                    }
                    //autorizace a ulozeni dat
                    PcCtecka.MifareClassicAuthentication(listOfBlockNum[blockIndex], carKey);
                    PcCtecka.MifareClassicWrite(listOfBlockNum[blockIndex], dataBuff);
                }

            }
            
            //ulozeni nastaveni uzivatelskych skupin 0-7

            if (writeEnables.writeUsersSets != 0)
            {
                //int BlockedCardsPointer = 0;
                int bytePointer;


                byte[] sectorsStartList = new byte[7] { 128, 144, 160, 176, 192, 208, 224 };

                for (int userClassIndex = 0; userClassIndex < 7; userClassIndex++) //kazdy run zapise jeden sektro s daty uzivatelske skupiny
                {
                    //autorizace prislusneho sektoru
                    PcCtecka.MifareClassicAuthentication((byte)(sectorsStartList[userClassIndex]), carKey);
                    //ulozeni bloku dne 1-7
                    for (int denIndex = 0; denIndex < 7; denIndex++)
                    {
                        // hodiny
                        dataBuff[0] = (byte)(nastaveni.usersSets.user[userClassIndex].userDay[denIndex].UserDayZone1.startHour & 0xff);
                        dataBuff[1] = (byte)(nastaveni.usersSets.user[userClassIndex].userDay[denIndex].UserDayZone1.endHour & 0xff);
                        dataBuff[2] = (byte)(nastaveni.usersSets.user[userClassIndex].userDay[denIndex].UserDayZone2.startHour & 0xff);
                        dataBuff[3] = (byte)(nastaveni.usersSets.user[userClassIndex].userDay[denIndex].UserDayZone2.endHour & 0xff);
                        dataBuff[4] = (byte)(nastaveni.usersSets.user[userClassIndex].userDay[denIndex].UserDayZone3.startHour & 0xff);
                        dataBuff[5] = (byte)(nastaveni.usersSets.user[userClassIndex].userDay[denIndex].UserDayZone3.endHour & 0xff);

                        // minuty
                        dataBuff[6] = (byte)(nastaveni.usersSets.user[userClassIndex].userDay[denIndex].UserDayZone1.startMinute & 0xff);
                        dataBuff[7] = (byte)(nastaveni.usersSets.user[userClassIndex].userDay[denIndex].UserDayZone1.endMinute & 0xff);
                        dataBuff[8] = (byte)(nastaveni.usersSets.user[userClassIndex].userDay[denIndex].UserDayZone2.startMinute & 0xff);
                        dataBuff[9] = (byte)(nastaveni.usersSets.user[userClassIndex].userDay[denIndex].UserDayZone2.endMinute & 0xff);
                        dataBuff[10] = (byte)(nastaveni.usersSets.user[userClassIndex].userDay[denIndex].UserDayZone3.startMinute & 0xff);
                        dataBuff[11] = (byte)(nastaveni.usersSets.user[userClassIndex].userDay[denIndex].UserDayZone3.endMinute & 0xff);

                        //tarifForZone 1 a 2 pro den v ramci smyčky
                        dataBuff[12] = (byte)((nastaveni.usersSets.user[userClassIndex].userDay[denIndex].UserDayZone1.tarif >> 8) & 0xff);
                        dataBuff[13] = (byte)(nastaveni.usersSets.user[userClassIndex].userDay[denIndex].UserDayZone1.tarif & 0xff);
                        dataBuff[14] = (byte)((nastaveni.usersSets.user[userClassIndex].userDay[denIndex].UserDayZone2.tarif >> 8) & 0xff);
                        dataBuff[15] = (byte)(nastaveni.usersSets.user[userClassIndex].userDay[denIndex].UserDayZone2.tarif & 0xff);

                        //

                        PcCtecka.MifareClassicWrite((byte)(sectorsStartList[userClassIndex] + denIndex), dataBuff);
                    }
                    //ulouzeni tarifForZone 3 pro dny 1 - 7 
                    bytePointer = 0;
                    for (int denIndex = 0; denIndex < 7; denIndex++)
                    {
                        dataBuff[bytePointer] = (byte)((nastaveni.usersSets.user[userClassIndex].userDay[denIndex].UserDayZone3.tarif >> 8) & 0xff);
                        bytePointer++;
                        dataBuff[bytePointer] = (byte)(nastaveni.usersSets.user[userClassIndex].userDay[denIndex].UserDayZone3.tarif & 0xff);
                        bytePointer++;
                    }
                    dataBuff[14] = 0x00;
                    dataBuff[15] = 0x00;
                    PcCtecka.MifareClassicAuthentication((byte)(sectorsStartList[userClassIndex] + 7), carKey);
                    PcCtecka.MifareClassicWrite((byte)(sectorsStartList[userClassIndex] + 7), dataBuff);

                    //ulozeni uSet0 - 7
                    bytePointer = 0;
                    for (int setIndex = 0; setIndex < 8; setIndex++)
                    {
                        dataBuff[bytePointer] = (byte)((nastaveni.usersSets.user[userClassIndex].uSet[setIndex] >> 8) & 0xff);
                        bytePointer++;
                        dataBuff[bytePointer] = (byte)(nastaveni.usersSets.user[userClassIndex].uSet[setIndex] & 0xff);
                        bytePointer++;
                    }
                    PcCtecka.MifareClassicAuthentication((byte)(sectorsStartList[userClassIndex] + 8), carKey);
                    PcCtecka.MifareClassicWrite((byte)(sectorsStartList[userClassIndex] + 8), dataBuff);

                    //ulozeni uSet8 - 9
                    //nulovani vsech registru
                    for (int i = 0; i < 16; i++)
                    {
                        dataBuff[i] = 0;
                    }
                    dataBuff[0] = (byte)((nastaveni.usersSets.user[userClassIndex].uSet[8] >> 8) & 0xff);
                    dataBuff[1] = (byte)(nastaveni.usersSets.user[userClassIndex].uSet[8] & 0xff);
                    dataBuff[2] = (byte)((nastaveni.usersSets.user[userClassIndex].uSet[9] >> 8) & 0xff);
                    dataBuff[3] = (byte)(nastaveni.usersSets.user[userClassIndex].uSet[9] & 0xff);
                    PcCtecka.MifareClassicAuthentication((byte)(sectorsStartList[userClassIndex] + 9), carKey);
                    PcCtecka.MifareClassicWrite((byte)(sectorsStartList[userClassIndex] + 9), dataBuff);
                }

            }
        }

        /// <summary>
        /// Načtení servisního nastavení z karty Mifare4kB ver.0.1.0
        /// </summary>
        /// <param name="nastaveni">TTerminalSets - nastaveni terminalu ver.0.1.0</param>
        /// <param name="carKey">byte[8] - klíč pro přístup k sektorům servisní karty</param>
        public void ServisCardRead(ref TTerminalSets nastaveni, byte[] carKey)
        {
            uint numberBuff;
            byte[] dataBuff;
            //** čtení bloku 2 - vseobecné nastavení ApiID,Ver,ProgramingTime,ProductionNum,UseCounter,TerminalId **//
            //autorizace a načtení sektoru do bufferu
            PcCtecka.MifareClassicAuthentication(2, carKey);
            dataBuff = PcCtecka.MifareClassicRead(2);
            //čtení jednotlivých dat
            nastaveni.apiId = dataBuff[0]; //ApiId
            //verze firmware
            nastaveni.verMajor = dataBuff[1]; //verMajor
            nastaveni.verMinor = dataBuff[2]; //verMinor
            nastaveni.verRevize = dataBuff[3]; //verRevize
            //datum naprogramovani
            nastaveni.programmingYear = dataBuff[4]; //programmingYear
            nastaveni.programmingMonth = dataBuff[5]; //programmingMonth
            nastaveni.programmingDay = dataBuff[6]; //programmingDay
            //productionNum
            numberBuff = dataBuff[7];
            numberBuff = ((numberBuff << 8) & 0xffffff00) | dataBuff[8];
            numberBuff = ((numberBuff << 8) & 0xffffff00) | dataBuff[9];
            numberBuff = ((numberBuff << 8) & 0xffffff00) | dataBuff[10];
            nastaveni.productionNum = numberBuff;
            //userCounter
            numberBuff = dataBuff[11];
            numberBuff = ((numberBuff << 8) & 0xffffff00) | dataBuff[12];
            numberBuff = ((numberBuff << 8) & 0xffffff00) | dataBuff[13];
            numberBuff = ((numberBuff << 8) & 0xffffff00) | dataBuff[14];
            nastaveni.useCounter = numberBuff;
            //terminalId
            nastaveni.terminalId = dataBuff[15]; //terminalId

            //** čtení bloku 8 - nastavení času **//
            PcCtecka.MifareClassicAuthentication(8, carKey);//autorizace a načtení sektoru do bufferu
            dataBuff = PcCtecka.MifareClassicRead(8);
            //nacteni datumu z jednotlivych sektoru
            try
            {
                nastaveni.time = new DateTime(dataBuff[0] + 2000, dataBuff[1], dataBuff[2], dataBuff[3], dataBuff[4], dataBuff[5]);
            }
            catch(ArgumentOutOfRangeException) {
                nastaveni.time = new DateTime(1999,1,1);
            }

            //** čtení bloku 9 - nastavené klíče **//
            PcCtecka.MifareClassicAuthentication(9, carKey);//autorizace a načtení sektoru do bufferu
            dataBuff = PcCtecka.MifareClassicRead(9);
            for (int i = 0; i < 8; i++)
            {
                nastaveni.customerKey[i] = dataBuff[i];
            }
            for (int i = 0; i < 8; i++)
            {
                nastaveni.customerServisKey[i] = dataBuff[i+8];
            }

            //** čtení HWRegistrů z bloků 12 - 14 **//
            //blok 12 - registry 0 - 7
            PcCtecka.MifareClassicAuthentication(12, carKey);//autorizace a načtení sektoru do bufferu
            dataBuff = PcCtecka.MifareClassicRead(12);
            int registerNum = 0;
            for (int i = 0; i < 16; i++)
            {
                nastaveni.hwRegisters[registerNum] = dataBuff[i];
                i++;
                nastaveni.hwRegisters[registerNum] = (ushort)(((nastaveni.hwRegisters[registerNum] << 8) & 0xff00) ^ dataBuff[i]);
                registerNum++;
            }
            //blok 13 - registry 8 - 15
            PcCtecka.MifareClassicAuthentication(13, carKey);//autorizace a načtení sektoru do bufferu
            dataBuff = PcCtecka.MifareClassicRead(13);
            for (int i = 0; i < 16; i++)
            {
                nastaveni.hwRegisters[registerNum] = dataBuff[i];
                i++;
                nastaveni.hwRegisters[registerNum] = (ushort)(((nastaveni.hwRegisters[registerNum] << 8) & 0xff00) ^ dataBuff[i]);
                registerNum++;
            }
            //blok 14 - registry 16 - 19
            PcCtecka.MifareClassicAuthentication(14, carKey);//autorizace a načtení sektoru do bufferu
            dataBuff = PcCtecka.MifareClassicRead(14);
            for (int i = 0; i < 8; i++)
            {
                nastaveni.hwRegisters[registerNum] = dataBuff[i];
                i++;
                nastaveni.hwRegisters[registerNum] = (ushort)(((nastaveni.hwRegisters[registerNum] << 8) & 0xff00) ^ dataBuff[i]);
                registerNum++;
            }

            //** čtení HWRegistrů z bloků 16 - 18 **//
            //blok 16 - registry 0 - 7
            PcCtecka.MifareClassicAuthentication(16, carKey);//autorizace a načtení sektoru do bufferu
            dataBuff = PcCtecka.MifareClassicRead(16);
            registerNum = 0;
            for (int i = 0; i < 16; i++)
            {
                nastaveni.usersRegisters[registerNum] = dataBuff[i];
                i++;
                nastaveni.usersRegisters[registerNum] = (ushort)(((nastaveni.usersRegisters[registerNum] << 8) & 0xff00) ^ dataBuff[i]);
                registerNum++;
            }
            //blok 17 - registry 8 - 15
            PcCtecka.MifareClassicAuthentication(17, carKey);//autorizace a načtení sektoru do bufferu
            dataBuff = PcCtecka.MifareClassicRead(17);
            for (int i = 0; i < 16; i++)
            {
                nastaveni.usersRegisters[registerNum] = dataBuff[i];
                i++;
                nastaveni.usersRegisters[registerNum] = (ushort)(((nastaveni.usersRegisters[registerNum] << 8) & 0xff00) ^ dataBuff[i]);
                registerNum++;
            }
            //blok 18 - registry 16 - 19
            PcCtecka.MifareClassicAuthentication(18, carKey);//autorizace a načtení sektoru do bufferu
            dataBuff = PcCtecka.MifareClassicRead(18);
            for (int i = 0; i < 8; i++)
            {
                nastaveni.usersRegisters[registerNum] = dataBuff[i];
                i++;
                nastaveni.usersRegisters[registerNum] = (ushort)(((nastaveni.usersRegisters[registerNum] << 8) & 0xff00) ^ dataBuff[i]);
                registerNum++;
            }

            //** čtení seznamu blokovaných karet - bloky 20 - 22 a 24 - 25 - BlockedCards 0 - 19 **//

            int BlockedCardsPointer = 0;
            byte[] listOfBlockNum = new byte[5] { 20, 21, 22, 24, 25 };

            for (int blockIndex = 0; blockIndex < 5; blockIndex++) //kazdy run cte jeden blok po 4 kartach
            {
                //cteni BlockedCards 0-19
                PcCtecka.MifareClassicAuthentication(listOfBlockNum[blockIndex], carKey);//autorizace a načtení sektoru do bufferu
                dataBuff = PcCtecka.MifareClassicRead(listOfBlockNum[blockIndex]);
                //nacteni sady 4 karet
                for (int i = 0; i < 16; i++)
                {
                    numberBuff = dataBuff[i];
                    i++;
                    numberBuff = ((numberBuff << 8) & 0xffffff00) | dataBuff[i];
                    i++;
                    numberBuff = ((numberBuff << 8) & 0xffffff00) | dataBuff[i];
                    i++;
                    numberBuff = ((numberBuff << 8) & 0xffffff00) | dataBuff[i];
                    nastaveni.blockedCards.CardNumber[BlockedCardsPointer] = numberBuff;
                    BlockedCardsPointer++;
                }
            }

            //** čtení nastavení uživatelských skupin 1-7 **//

            //int BlockedCardsPointer = 0;
            int bytePointer;


            byte[] sectorsStartList = new byte[7] { 128, 144, 160, 176, 192, 208, 224 };

            for (int userClassIndex = 0; userClassIndex < 7; userClassIndex++) //kazdy run zapise jeden sektro s daty uzivatelske skupiny
            {
                //autorizace prislusneho sektoru uzivatele (1-7)
                PcCtecka.MifareClassicAuthentication((byte)(sectorsStartList[userClassIndex]), carKey);
                
                //cteni bloku dne 1-7
                for (int denIndex = 0; denIndex < 7; denIndex++)
                {
                    //nacteni dat bloku
                    dataBuff = PcCtecka.MifareClassicRead((byte)(sectorsStartList[userClassIndex] + denIndex));
                    // hodiny
                    nastaveni.usersSets.user[userClassIndex].userDay[denIndex].UserDayZone1.startHour = dataBuff[0];
                    nastaveni.usersSets.user[userClassIndex].userDay[denIndex].UserDayZone1.endHour = dataBuff[1];
                    nastaveni.usersSets.user[userClassIndex].userDay[denIndex].UserDayZone2.startHour = dataBuff[2];
                    nastaveni.usersSets.user[userClassIndex].userDay[denIndex].UserDayZone2.endHour = dataBuff[3];
                    nastaveni.usersSets.user[userClassIndex].userDay[denIndex].UserDayZone3.startHour = dataBuff[4];
                    nastaveni.usersSets.user[userClassIndex].userDay[denIndex].UserDayZone3.endHour = dataBuff[5];

                    // minuty
                    nastaveni.usersSets.user[userClassIndex].userDay[denIndex].UserDayZone1.startMinute = dataBuff[6];
                    nastaveni.usersSets.user[userClassIndex].userDay[denIndex].UserDayZone1.endMinute = dataBuff[7];
                    nastaveni.usersSets.user[userClassIndex].userDay[denIndex].UserDayZone2.startMinute = dataBuff[8];
                    nastaveni.usersSets.user[userClassIndex].userDay[denIndex].UserDayZone2.endMinute = dataBuff[9];
                    nastaveni.usersSets.user[userClassIndex].userDay[denIndex].UserDayZone3.startMinute = dataBuff[10];
                    nastaveni.usersSets.user[userClassIndex].userDay[denIndex].UserDayZone3.endMinute = dataBuff[11];

                    //tarifForZone 1 a 2 pro den v ramci smyčky
                    nastaveni.usersSets.user[userClassIndex].userDay[denIndex].UserDayZone1.tarif = dataBuff[12];
                    nastaveni.usersSets.user[userClassIndex].userDay[denIndex].UserDayZone1.tarif = (ushort)(((nastaveni.usersSets.user[userClassIndex].userDay[denIndex].UserDayZone1.tarif << 8) & 0xff00) ^ dataBuff[13]);
                    nastaveni.usersSets.user[userClassIndex].userDay[denIndex].UserDayZone2.tarif = dataBuff[14];
                    nastaveni.usersSets.user[userClassIndex].userDay[denIndex].UserDayZone2.tarif = (ushort)(((nastaveni.usersSets.user[userClassIndex].userDay[denIndex].UserDayZone2.tarif << 8) & 0xff00) ^ dataBuff[15]);
                }

                //nacteni tarifForZone 3 pro dny 1 - 7
                bytePointer = 0;
                dataBuff = PcCtecka.MifareClassicRead((byte)(sectorsStartList[userClassIndex] + 7));

                for (int denIndex = 0; denIndex < 7; denIndex++)
                {
                    nastaveni.usersSets.user[userClassIndex].userDay[denIndex].UserDayZone3.tarif = dataBuff[bytePointer];
                    bytePointer++;
                    nastaveni.usersSets.user[userClassIndex].userDay[denIndex].UserDayZone3.tarif = (ushort)(((nastaveni.usersSets.user[userClassIndex].userDay[denIndex].UserDayZone3.tarif << 8) & 0xff00) ^ dataBuff[bytePointer]);
                    bytePointer++;
                }

                //nacteni uSet0 - 7
                bytePointer = 0;
                dataBuff = PcCtecka.MifareClassicRead((byte)(sectorsStartList[userClassIndex] + 8));

                for (int setIndex = 0; setIndex < 8; setIndex++)
                {
                    nastaveni.usersSets.user[userClassIndex].uSet[setIndex] = dataBuff[bytePointer];
                    bytePointer++;
                    nastaveni.usersSets.user[userClassIndex].uSet[setIndex] = (ushort)(((nastaveni.usersSets.user[userClassIndex].uSet[setIndex] << 8) & 0xff00) ^ dataBuff[bytePointer]);
                    bytePointer++;
                }

                dataBuff = PcCtecka.MifareClassicRead((byte)(sectorsStartList[userClassIndex] + 9));
                //nacteni uSet8 - 9
                nastaveni.usersSets.user[userClassIndex].uSet[8] = dataBuff[0];
                nastaveni.usersSets.user[userClassIndex].uSet[8] = (ushort)(((nastaveni.usersSets.user[userClassIndex].uSet[8] << 8) & 0xff00) ^ dataBuff[1]);
                nastaveni.usersSets.user[userClassIndex].uSet[9] = dataBuff[2];
                nastaveni.usersSets.user[userClassIndex].uSet[9] = (ushort)(((nastaveni.usersSets.user[userClassIndex].uSet[9] << 8) & 0xff00) ^ dataBuff[3]);
            }

        }

        /// <summary>
        /// Zápis hlavičky na MifareClassic AZP v1, pokud je cardNum == 0 potom se udaje v trailer sectoru nenastavuji
        /// </summary>
        /// <param name="header">Data pro header, pokud je pStart,pActual, !! pokud je cardNum == 0 potom se tyto info nenastavují !!</param>
        public void WriteCardHeader(TMfCard_Head header)
        {
            byte[] blocBuff = new byte[16];
            try
            {
                //autorizace karty
                PcCtecka.MifareClassicAuthentication(4, HeaderKeyAZP);
                //inicializace pointru pro zaznam dat - výchozí hodnota
                if (header.pStart != 0)
                {
                    PcCtecka.MifareClassicValueWrite(4, header.pStart);
                }

                //inicializace pointru pro log - ukazatel na aktualni hodnotu
                /* opraveno, nezapisovala se nulovací hodnota
                if (header.pActual != 0)
                {
                    PcCtecka.MifareClassicValueWrite(5, header.pActual);
                }*/
                PcCtecka.MifareClassicValueWrite(5, header.pActual);

                //platnost karty od - parsovani do AZP header formatu
                int monthBuff, dayBuff;
                blocBuff[0] = (byte)(header.validFrom.Year % 100);
                monthBuff = (header.validFrom.Month << 4) & 0xf0;
                dayBuff = (header.validFrom.Day >> 2) & 0x0f;
                blocBuff[1] = (byte)(monthBuff ^ dayBuff);
                dayBuff = (header.validFrom.Day << 6) & 0xc0;
                blocBuff[2] = (byte)(dayBuff ^ header.validFrom.Hour);

                //platnost karty do - parsovani do AZP header formatu
                blocBuff[3] = (byte)(header.validTo.Year % 100);
                monthBuff = (header.validTo.Month << 4) & 0xf0;
                dayBuff = (header.validTo.Day >> 2) & 0x0f;
                blocBuff[4] = (byte)(monthBuff ^ dayBuff);
                dayBuff = (header.validTo.Day << 6) & 0xc0;
                blocBuff[5] = (byte)(dayBuff ^ header.validTo.Hour);

                //dovoleny debet
                blocBuff[6] = (byte)((header.allowDebit >> 24) & 0xff);
                blocBuff[7] = (byte)((header.allowDebit >> 16) & 0xff);
                blocBuff[8] = (byte)((header.allowDebit >> 8) & 0xff);
                blocBuff[9] = (byte)(header.allowDebit & 0xff);

                //pMin (od jakého bloku zacina zaznam udalosti)
                blocBuff[10] = (byte)((header.pMin >> 24) & 0xff);
                blocBuff[11] = (byte)((header.pMin >> 16) & 0xff);
                blocBuff[12] = (byte)((header.pMin >> 8) & 0xff);
                blocBuff[13] = (byte)(header.pMin & 0xff);

                //nastaveni prepisu dat
                if (header.overWriteData != 0)
                {
                    blocBuff[14] = 0x80;
                }
                else
                {
                    blocBuff[14] = 0x00;
                }

                //uzivatelska skupina
                blocBuff[14] ^= header.personClass;
                blocBuff[15] = 0x00;

                PcCtecka.MifareClassicWrite(6, blocBuff);

                //číslo a typ karty
                if (header.cardNum != 0){
                    byte[] dataKeyB = new byte[6];
                    dataKeyB[0] = 0x00; //nevyuzito
                    dataKeyB[1] = header.cardType;
                    dataKeyB[2] = (byte)((header.cardNum >> 24) & 0xff);
                    dataKeyB[3] = (byte)((header.cardNum >> 16) & 0xff);
                    dataKeyB[4] = (byte)((header.cardNum >> 8) & 0xff);
                    dataKeyB[5] = (byte)(header.cardNum & 0xff);
                    //zapis do trailler sektoru
                    PcCtecka.MifareClassicWriteTrailer(7, PcCtecka.MFClassicTrailerSectorGernerator(HeaderKeyAZP, dataKeyB, 0x00, TMFAccessMode.PouzeKeyA));
                }
            }
            catch
            {
                throw new ExChybaKomunikaceMfCardAzpV1("Chyba komunikaci s kartou. Nelze zapsat hlavičku karty.", "");
            }

        }

        /// <summary>
        /// Přidání nového záznamu o události
        /// </summary>
        /// <param name="record">dt_recordAzp - záznam</param>
        /// <param name="cardKey">klíč por zákaznickou kartu</param>
        /// <returns><para>0 - Vše v pořadku</para>
        ///          <para>1 - karta je plna</para></returns>
        public int RecordAddNew(dt_recordAzp record, byte[] cardKey)
        {
            TMfCard_Head hlavickaKarty;
            try
            {
                TDetekovanaKarta karta = PcCtecka.DetekceKarty();
                hlavickaKarty = ReadCardHeader();
                int pActualNew = RecordPointerInc(hlavickaKarty.pActual, hlavickaKarty.pMin, hlavickaKarty.pStart, hlavickaKarty.overWriteData, karta.TypKarty);
                //kontrola zda neni karta plna, pokud je karta plna vrati se error 1
                if (pActualNew == 0) return(1);
                
                //kontrola zda neni treba posunout prvni zaznam pro uvolneni posledniho (cyklický přepis) a zaroven jestli se nejedna o prazdnou kartu
                if (pActualNew == hlavickaKarty.pStart && hlavickaKarty.pActual != 0)
                {
                    int pStartNew = hlavickaKarty.pStart;
                    //posun pStart o dva zaznamy
                    pStartNew = RecordPointerInc(pStartNew, hlavickaKarty.pMin, hlavickaKarty.pStart, 1, karta.TypKarty);
                    pStartNew = RecordPointerInc(pStartNew, hlavickaKarty.pMin, hlavickaKarty.pStart, 1, karta.TypKarty);
                    //autorizace karty head - pro posunuti pointeru na pStart
                    PcCtecka.MifareClassicAuthentication(4, HeaderKeyAZP);
                    PcCtecka.MifareClassicValueWrite(4, pStartNew);
                }

                //vlozeni zaznamu
                recordWrite(pActualNew, record, cardKey);
                //autorizace karty head - pro posunuti pointeru na pActual
                PcCtecka.MifareClassicAuthentication(5, HeaderKeyAZP);
                PcCtecka.MifareClassicValueWrite(5, pActualNew);

            }
            catch
            {
                throw new ExChybaKomunikaceMfCardAzpV1("Chyba komunikaci s kartou. Nepodařilo se přidat záznam.", "");
            }
            return (0);
        }

        /// <summary>
        /// recordWrite - zapis udalosti v systemu karet AZP
        /// </summary>
        /// <param name="recordPointer">Absolutni adresa na kterou se ma udalost zapsat</param>
        /// <param name="record">Událost</param>
        /// <param name="cardKey">Klič karty pro blok s udalostmi</param>
        private void recordWrite(int recordPointer, dt_recordAzp record, byte[] cardKey){

            byte[] blockData;
            int cisloByte = 0;

            //autorizace karty
            PcCtecka.MifareClassicAuthentication((byte)(recordPointer/2), cardKey);
            //nacteni aktualnich dat v blocku, pro pripad ze jsou zde data v prvni pulce bloku
            blockData = PcCtecka.MifareClassicRead((byte)(recordPointer / 2));

            //detekce zda se jedna o prvni nebo druhy byte
            if ((recordPointer % 2) != 0){
                //jedna se o druhy zaznam v bloku
                cisloByte = 8;
            }
            blockData[cisloByte] = (byte)(((record.datum.Year - 2000) << 4) ^ record.datum.Month);
            blockData[cisloByte + 1] = (byte)((record.datum.Day << 3) ^ ((record.datum.Hour >> 2) & 0x07));
            blockData[cisloByte + 2] = (byte)(((record.datum.Hour & 0x03) << 6) ^ (record.datum.Minute));
            blockData[cisloByte + 3] = record.apiId;
            blockData[cisloByte + 4] = (byte)(record.tId << 1);
            if (record.credit < 0)
            {
                blockData[cisloByte + 4] = (byte)(blockData[cisloByte + 4] ^ 0x01);
                record.credit = record.credit * -1;
            }
            blockData[cisloByte + 5] = (byte)((record.credit & 0x00ff0000) >> 16);
            blockData[cisloByte + 6] = (byte)((record.credit & 0x0000ff00) >> 8);
            blockData[cisloByte + 7] = (byte)((record.credit & 0x000000ff));
            PcCtecka.MifareClassicWrite((byte)(recordPointer / 2), blockData);

        }

        /// <summary>
        /// Kontroluje zda je posledni zaznam v poradku, pripadne ho obnovuje
        /// </summary>
        /// <param name="header">aktualni hlavicka</param>
        /// <param name="lastRecord">posledni zaznam</param>
        /// <param name="cardKey">zakaznicky klic</param>
        public void CheckBackup(ref TMfCard_Head header,ref dt_recordAzp lastRecord, byte[] cardKey)
        {
            short restoreNeeded = 0; //priznak zda bude treba obnovit zaznam
            try
            {
                //kontrola pStart - pokud je pStart rovny nule, doslo ke smazani a je treba obnovit zaznam
                if (header.pStart == 0)
                {
                    restoreNeeded = 1;
                }

                //kontrola pActual - pokud je pActual rovny nule je treba zkontrolovat zda je zaznam hodnoty v poradku, pokud nebyl ulozen jsou v bloku same 00, jinak korektni 0
                if (header.pActual == 0 && restoreNeeded == 0)
                {
                    if (CheckValueBlock(5, new byte[6] { 0x19, 0x92, 0xae, 0xb0, 0x20, 0x12 }) != 0) restoreNeeded = 1; //kontrola bloku 5 (hodnota pActual) zda jsou same 00 nebo hodnota
                }

                //pokud je nastaven aktualni zaznam, kontroluje se jeho relevance 
                if (header.pActual != 0 && restoreNeeded == 0)
                {
                    if (lastRecord.datum.Year == 1999 && lastRecord.apiId == 0 && lastRecord.tId == 0 & lastRecord.credit == 0) restoreNeeded = 1; //pokud nabiva zaznam techto hodnot je treba obnovit cely zaznam
                }
            }
            catch (ExChybaKomunikaceSKartou)
            {
                throw new ExChybaKomunikaceMfCardAzpV1("Chyba komunikaci s kartou. Nelze obnovit poslední záznam.", "");
            }
            catch (ExChybaKomunikaceMfCardAzpV1)
            {
                throw new ExChybaKomunikaceMfCardAzpV1("Chyba komunikaci s kartou. Nelze obnovit poslední záznam.", "");
            }

            //pokud je potreba dojde k obnoveni posledniho zaznamu
            if (restoreNeeded != 0)
            {
                try
                {
                    int backupPStart, backupPActual;

                    byte[] DataBuff;

                    //nacteni pStart a pActual z backup bloku
                    PcCtecka.MifareClassicAuthentication(9, cardKey);
                    DataBuff = PcCtecka.MifareClassicRead(9);

                    //parsovani nactenych dat pStart
                    backupPStart = DataBuff[0];
                    backupPStart = (backupPStart << 8) | DataBuff[1];
                    backupPStart = (backupPStart << 8) | DataBuff[2];
                    backupPStart = (backupPStart << 8) | DataBuff[3];

                    //parsovani nactenych dat pActual
                    backupPActual = DataBuff[4];
                    backupPActual = (backupPActual << 8) | DataBuff[5];
                    backupPActual = (backupPActual << 8) | DataBuff[6];
                    backupPActual = (backupPActual << 8) | DataBuff[7];

                    //obnoveni hlavicky
                    header.pStart = backupPStart;
                    header.pActual = backupPActual;
                    WriteCardHeader(header);

                    //nacteni dat posledniho zaznamu z backupu
                    PcCtecka.MifareClassicAuthentication(10, cardKey);
                    DataBuff = PcCtecka.MifareClassicRead(10);
                    //ulozeni dat posledniho zaznamu z backupu
                    PcCtecka.MifareClassicAuthentication((byte)(backupPActual / 2), cardKey);
                    PcCtecka.MifareClassicWrite((byte)(backupPActual / 2), DataBuff);
                    //znovu nacteni posledniho zaznamu
                    lastRecord = RecordRead(header.pActual, cardKey);
                }
                catch
                {
                    throw new ExChybaKomunikaceMfCardAzpV1("Chyba komunikaci s kartou. Nelze obnovit poslední záznam.", "");
                }

            }
        }

        /// <summary>
        /// RecordRead - nactení udalosti ze zakaznické karty v systemu karet AZP
        /// </summary>
        /// <param name="RecordNum">Číslo čteného záznamu</param>
        /// <param name="cardKey">6B klíč pro přístup k oblasti záznamu</param>
        /// <returns>Přečtený záznam ve formě dt_recordAzp</returns>
        public dt_recordAzp RecordRead(int RecordNum, byte[] cardKey)
        {
            try
            {
                int year, month, day, hour, minute, credit;
                dt_recordAzp zaznamBuff;
                zaznamBuff.credit = 0;
                byte[] blockData;
                byte blockNum = (byte)(RecordNum / 2);
                PcCtecka.MifareClassicAuthentication(blockNum, cardKey);
                blockData = PcCtecka.MifareClassicRead(blockNum);
                //posun byte podle toho zda se jedna o prvni polovinu nebo druhou polovinu byte
                int order = RecordNum % 2;
                int cisloByte = 0;
                if (order != 0) cisloByte = 8; //pokud se jedna od druhou polovinu bloku

                //parsovani recordu (64b) dle recordAzp
                year = 2000 + ((blockData[cisloByte] >> 4) & 0x0f);
                month = blockData[cisloByte] & 0x0f;
                day = (blockData[cisloByte + 1] >> 3) & 0x1f;
                hour = ((blockData[cisloByte + 1] << 2) & 0x1c) ^ ((blockData[cisloByte + 2] >> 6) & 0x03);
                minute = (blockData[cisloByte + 2] & 0x3f);
                zaznamBuff.apiId = (byte)(blockData[cisloByte + 3] & 0xff);
                zaznamBuff.tId = (byte)((blockData[cisloByte + 4] >> 1) & 0x7f);
                credit = 0;
                credit = blockData[cisloByte + 5];
                credit = credit << 8;
                credit ^= blockData[cisloByte + 6];
                credit = credit << 8;
                credit ^= blockData[cisloByte + 7];
                if ((blockData[cisloByte + 4] & 0x01) != 0x00) credit *= -1;
                zaznamBuff.credit = credit;
                try
                {
                    zaznamBuff.datum = new DateTime(year, month, day, hour, minute, 0);
                }
                catch
                {
                    zaznamBuff.datum = new DateTime(1999, 1, 1, 00, 00, 0);
                }
                return (zaznamBuff);
            }
            catch
            {
                throw new ExChybaKomunikaceMfCardAzpV1("Chyba komunikaci s kartou. Nelze načíst záznam.", "");
            }
        }


        public void RecordDelAll()
        {

        }

        /// <summary>
        /// Načtení HWKey karty 
        /// </summary>
        /// <param name="hWKeyCard">HWKeyAZPv1 pointer pro načtení HW key karty</param>
        public void HwKeyAZPRead(ref HWKeyAZPv1 hWKeyCard)
        {
            byte[] DataBuff;
            try
            {
                //autorizace karty
                PcCtecka.MifareClassicAuthentication(8, PcCtecka.HWKeyCardKey);
                DataBuff = PcCtecka.MifareClassicRead(8);
                //parsovani dat customer user key + customer servis key + den vystaveni (block 8 - sektor 2)
                for (int i = 0; i < 6; i++)  hWKeyCard.CustomerUserKey[i] = DataBuff[i];
                for (int i = 0; i < 6; i++) hWKeyCard.CustomerServisKey[i] = DataBuff[i + 6];
                hWKeyCard.ReleaseDate = new DateTime(((DataBuff[14] << 8) | DataBuff[15]), DataBuff[13], DataBuff[12]);
                //nacteni a parsovani dat poznamky (block 9 a 10 - sektor 2)
                DataBuff = PcCtecka.MifareClassicRead(9);
                int pointer = 0;
                for (int i = 0; i < 8; i++)
                {
                    hWKeyCard.CustomerName[i] = (char)((DataBuff[pointer] << 8) | DataBuff[pointer + 1]);
                    pointer += 2;
                }
                //parsovani druhe pulky poznamky
                pointer = 0;
                DataBuff = PcCtecka.MifareClassicRead(10);
                for (int i = 8; i < 16; i++)
                {
                    hWKeyCard.CustomerName[i] = (char)((DataBuff[pointer] << 8) | DataBuff[pointer + 1]);
                    pointer += 2;
                }
                //parsovani dat pro seznam povolenych aplikací (block 12,13 / sektor 3)
                //parsovani aplikaci 0-127
                pointer = 0;
                PcCtecka.MifareClassicAuthentication(12, PcCtecka.HWKeyCardKey);
                DataBuff = PcCtecka.MifareClassicRead(12);
                for (int i = 0; i < 16; i++)
                {
                    hWKeyCard.PovoleneAPID[pointer] = ((DataBuff[i] & 0x80) != 0) ? true : false;
                    hWKeyCard.PovoleneAPID[pointer + 1] = ((DataBuff[i] & 0x40) != 0) ? true : false;
                    hWKeyCard.PovoleneAPID[pointer + 2] = ((DataBuff[i] & 0x20) != 0) ? true : false;
                    hWKeyCard.PovoleneAPID[pointer + 3] = ((DataBuff[i] & 0x10) != 0) ? true : false;
                    hWKeyCard.PovoleneAPID[pointer + 4] = ((DataBuff[i] & 0x08) != 0) ? true : false;
                    hWKeyCard.PovoleneAPID[pointer + 5] = ((DataBuff[i] & 0x04) != 0) ? true : false;
                    hWKeyCard.PovoleneAPID[pointer + 6] = ((DataBuff[i] & 0x02) != 0) ? true : false;
                    hWKeyCard.PovoleneAPID[pointer + 7] = ((DataBuff[i] & 0x01) != 0) ? true : false;
                    pointer += 8;
                }
                //parsovani aplikaci 128-255
                DataBuff = PcCtecka.MifareClassicRead(13);
                for (int i = 0; i < 16; i++)
                {
                    hWKeyCard.PovoleneAPID[pointer] = ((DataBuff[i] & 0x80) != 0) ? true : false;
                    hWKeyCard.PovoleneAPID[pointer + 1] = ((DataBuff[i] & 0x40) != 0) ? true : false;
                    hWKeyCard.PovoleneAPID[pointer + 2] = ((DataBuff[i] & 0x20) != 0) ? true : false;
                    hWKeyCard.PovoleneAPID[pointer + 3] = ((DataBuff[i] & 0x10) != 0) ? true : false;
                    hWKeyCard.PovoleneAPID[pointer + 4] = ((DataBuff[i] & 0x08) != 0) ? true : false;
                    hWKeyCard.PovoleneAPID[pointer + 5] = ((DataBuff[i] & 0x04) != 0) ? true : false;
                    hWKeyCard.PovoleneAPID[pointer + 6] = ((DataBuff[i] & 0x02) != 0) ? true : false;
                    hWKeyCard.PovoleneAPID[pointer + 7] = ((DataBuff[i] & 0x01) != 0) ? true : false;
                    pointer += 8;
                }

            }
            catch
            {
                throw new ExChybaKomunikaceMfCardAzpV1("Chyba komunikaci s kartou. Došlo k chybě při čtení HW klíče.", "");
            }
        }

        /// <summary>
        /// CheckValueBlock - kontrola zda byl spravne inicializovan value block
        /// </summary>
        /// <param name="blockNum">číslo bloku</param>
        /// <param name="cardKey">6B klíč pro přístup k oblasti záznamu</param>
        /// <returns><para>0 - value block je pravdepodobne OK</para>
        /// <para>1 - value block nebyl inicializovan</para></returns>
        public int CheckValueBlock(byte blockNum, byte[] cardKey)
        {
            try
            {
                dt_recordAzp zaznamBuff;
                zaznamBuff.credit = 0;
                byte[] blockData;

                PcCtecka.MifareClassicAuthentication(blockNum, cardKey);
                blockData = PcCtecka.MifareClassicRead(blockNum);

                for (int i = 0; i < 16; i++)
                {
                    if (blockData[i] != 0x00) return 0;
                }
                return 1;
            }
            catch
            {
                throw new ExChybaKomunikaceMfCardAzpV1("Chyba komunikaci s kartou. Nelze načíst záznam.", "");
            }
        }

        /// <summary>
        /// Zápis dat na HW klíč pro PC aplikaci
        /// </summary>
        /// <param name="hWKeyCard">HWKeyAZPv1 - data se základním nastavením PC aplikace</param>
        public void HwKeyAZPWrite(HWKeyAZPv1 hWKeyCard)
        {
            byte[] DataBuff = new byte[16];
            try
            {
                //autorizace karty
                PcCtecka.MifareClassicAuthentication(8, PcCtecka.HWKeyCardKey);
                //příprava dat customer user key + customer servis key + den vystaveni (block 8 - sektor 2)
                for (int i = 0; i < 6; i++) DataBuff[i] = hWKeyCard.CustomerUserKey[i];
                for (int i = 0; i < 6; i++) DataBuff[i+6] = hWKeyCard.CustomerServisKey[i];
                DataBuff[12] = (byte)hWKeyCard.ReleaseDate.Day;
                DataBuff[13] = (byte)hWKeyCard.ReleaseDate.Month;
                DataBuff[14] = (byte)((hWKeyCard.ReleaseDate.Year >> 8) & 0xff);
                DataBuff[15] = (byte)((hWKeyCard.ReleaseDate.Year) & 0xff);
                PcCtecka.MifareClassicWrite(8, DataBuff);

                //příprava dat poznamky (block 9 a 10 - sektor 2)
                int pointer = 0;
                for (int i = 0; i < 8; i++)
                {
                    DataBuff[pointer] = (byte)((hWKeyCard.CustomerName[i] >> 8) & 0xff);
                    pointer++; 
                    DataBuff[pointer] = (byte)((hWKeyCard.CustomerName[i]) & 0xff);
                    pointer++;
                }
                PcCtecka.MifareClassicWrite(9, DataBuff);
                //druha pulka poznamky
                pointer = 0;
                for (int i = 8; i < 16; i++)
                {
                    DataBuff[pointer] = (byte)((hWKeyCard.CustomerName[i] >> 8) & 0xff);
                    pointer++;
                    DataBuff[pointer] = (byte)((hWKeyCard.CustomerName[i]) & 0xff);
                    pointer++;
                }
                PcCtecka.MifareClassicWrite(10, DataBuff);

                //připrava dat pro seznam povolenych aplikací (block 12,13 / sektor 3)
                pointer = 0;
                byte byteBuff;
                //pridani aplikaci 0-127
                for (int i = 0; i < 16; i++)
                {
                    byteBuff = 0;
                    if (hWKeyCard.PovoleneAPID[pointer] == true) byteBuff |= 0x80;
                    if (hWKeyCard.PovoleneAPID[pointer + 1] == true) byteBuff |= 0x40;
                    if (hWKeyCard.PovoleneAPID[pointer + 2] == true) byteBuff |= 0x20;
                    if (hWKeyCard.PovoleneAPID[pointer + 3] == true) byteBuff |= 0x10;
                    if (hWKeyCard.PovoleneAPID[pointer + 4] == true) byteBuff |= 0x08;
                    if (hWKeyCard.PovoleneAPID[pointer + 5] == true) byteBuff |= 0x04;
                    if (hWKeyCard.PovoleneAPID[pointer + 6] == true) byteBuff |= 0x02;
                    if (hWKeyCard.PovoleneAPID[pointer + 7] == true) byteBuff |= 0x01;
                    pointer += 8;
                    DataBuff[i] = byteBuff;
                }
                PcCtecka.MifareClassicAuthentication(12, PcCtecka.HWKeyCardKey);
                PcCtecka.MifareClassicWrite(12, DataBuff);
                //pridani aplikaci 128-255
                for (int i = 0; i < 16; i++)
                {
                    byteBuff = 0;
                    if (hWKeyCard.PovoleneAPID[pointer] == true) byteBuff |= 0x80;
                    if (hWKeyCard.PovoleneAPID[pointer + 1] == true) byteBuff |= 0x40;
                    if (hWKeyCard.PovoleneAPID[pointer + 2] == true) byteBuff |= 0x20;
                    if (hWKeyCard.PovoleneAPID[pointer + 3] == true) byteBuff |= 0x10;
                    if (hWKeyCard.PovoleneAPID[pointer + 4] == true) byteBuff |= 0x08;
                    if (hWKeyCard.PovoleneAPID[pointer + 5] == true) byteBuff |= 0x04;
                    if (hWKeyCard.PovoleneAPID[pointer + 6] == true) byteBuff |= 0x02;
                    if (hWKeyCard.PovoleneAPID[pointer + 7] == true) byteBuff |= 0x01;
                    pointer += 8;
                    DataBuff[i] = byteBuff;
                }
                PcCtecka.MifareClassicWrite(13, DataBuff);
            }
            catch
            {
                throw new ExChybaKomunikaceMfCardAzpV1("Chyba komunikaci s kartou. Došlo k chybě při zápisu.", "");
            }
        }
      
        /// <summary>
        /// Načtení hlavičky MifareClassic AZP v1
        /// </summary>
        /// <returns>Načtená hlavička AZP v1</returns>
        public TMfCard_Head ReadCardHeader()
        {
            TMfCard_Head headerBuff = new TMfCard_Head();
            byte[] readDataBuff;
            try
            {
                //autorizace karty
                PcCtecka.MifareClassicAuthentication(4, HeaderKeyAZP);
                //inicializace pointru pro zaznam dat - výchozí hodnota
                headerBuff.pStart = PcCtecka.MifareClassicValueRead(4);
                headerBuff.pActual = PcCtecka.MifareClassicValueRead(5);
                readDataBuff = PcCtecka.MifareClassicRead(6);
                //datum od
                int yearBuff, monthBuff, dayBuff, hourBuff;
                yearBuff = readDataBuff[0];
                monthBuff = (readDataBuff[1] >> 4) & 0x0f;
                dayBuff = (readDataBuff[1] << 2) & 0x3c;
                dayBuff ^= ((readDataBuff[2] >> 6) & 0x03);
                hourBuff = (readDataBuff[2] & 0x3f);
                headerBuff.validFrom = new DateTime(2000 + yearBuff, monthBuff, dayBuff, hourBuff, 0, 0);
                //datum do
                yearBuff = readDataBuff[3];
                monthBuff = (readDataBuff[4] >> 4) & 0x0f;
                dayBuff = (readDataBuff[4] << 2) & 0x3c;
                dayBuff ^= ((readDataBuff[5] >> 6) & 0x03);
                hourBuff = (readDataBuff[5] & 0x3f);
                headerBuff.validTo = new DateTime(2000 + yearBuff, monthBuff, dayBuff, hourBuff, 0, 0);
                
                //dovoleny debet
                headerBuff.allowDebit = readDataBuff[6];
                headerBuff.allowDebit <<= 8;
                headerBuff.allowDebit ^= readDataBuff[7];
                headerBuff.allowDebit <<= 8;
                headerBuff.allowDebit ^= readDataBuff[8];
                headerBuff.allowDebit <<= 8;
                headerBuff.allowDebit ^= readDataBuff[9];

                //pMin (od jakého bloku zacina zaznam udalosti)
                headerBuff.pMin = readDataBuff[10];
                headerBuff.pMin <<= 8;
                headerBuff.pMin ^= readDataBuff[11];
                headerBuff.pMin <<= 8;
                headerBuff.pMin ^= readDataBuff[12];
                headerBuff.pMin <<= 8;
                headerBuff.pMin ^= readDataBuff[13];

                //nastaveni prepisu dat
                if ((readDataBuff[14] & 0x80) == 0)
                {
                    headerBuff.overWriteData = 0;
                }
                else
                {
                    headerBuff.overWriteData = 1;
                }

                //uzivatelska skupina
                headerBuff.personClass = (byte)(readDataBuff[14] & 0x7f);

                //číslo a typ karty
                readDataBuff = PcCtecka.MifareClassicRead(7);

                headerBuff.cardType = readDataBuff[11];

                headerBuff.cardNum = readDataBuff[12];
                headerBuff.cardNum <<= 8;
                headerBuff.cardNum ^= readDataBuff[13];
                headerBuff.cardNum <<= 8;
                headerBuff.cardNum ^= readDataBuff[14];
                headerBuff.cardNum <<= 8;
                headerBuff.cardNum ^= readDataBuff[15];

                return (headerBuff);
            }
            catch
            {
                throw new ExChybaKomunikaceMfCardAzpV1("Chyba komunikaci s kartou. Nelze zapsat hlavičku karty.", "");
            }
            
        }

    }
}
