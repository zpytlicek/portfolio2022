﻿using System;
using System.Runtime.InteropServices;

namespace RFID_Reader
{
    /// <summary>
    /// Třída pro ovládání USB RFID čtečky SL500
    /// Ver 0.1.0
    /// 
    /// Poslední úprava: 26.05.2012 - ZP 
    /// 
    /// </summary>

    /// <summary>
    /// Typ pro rozpoznani RFID karet
    /// </summary>
    public enum TRFIDTagy { Zadna, MifareUltralight, MifareClass1kB, MifareClass4kB, MifareDesfire4Kb, NeznamaKarta };

    /// <summary>
    /// Typ pro nastavení přístupových práv karty
    /// PouzeKeyA - stejné jako transportní nastavení
    /// KeyAiB - Key A pouze pro čtení, B pro čtení i zápis - použiji jen pro hodnotu peněženky
    /// </summary>
    public enum TMFAccessMode { PouzeKeyA, KeyAiB };

    /// <summary>
    /// Základní informace o detekované karte
    /// </summary>
    public struct TDetekovanaKarta
    {
        public TRFIDTagy TypKarty; //typ karty
        public byte[] UID; // unikatní ID karty
        public ushort TypKapacity; //id typu kapacity
    }
   
    /// <summary>
    /// Všeobecná chyba komunikace se čtečkou
    /// </summary>
    public class ExChybaKomunikaceSeCteckou : Exception
    {
        // Při vyvolání vyjímky se uloží i čas kdy k vyjímce došlo
        private DateTime time;
        private int ErrorNum;

        public ExChybaKomunikaceSeCteckou(string message)
            : base(message)
        {
            time = DateTime.Now; 
            ErrorNum = 0;
        }

        public ExChybaKomunikaceSeCteckou(string message, int errorNum)
            : base(message)
        {
            time = DateTime.Now;
            ErrorNum = errorNum;
        }

        public int JakeErrorNum
        {
            get { return ErrorNum; }
        }

        public DateTime Time
        {
            get { return time; }
        }
    }

    /// <summary>
    /// Všeobecná chyba komunikace s kartou
    /// </summary>
    public class ExChybaKomunikaceSKartou : Exception
    {
        // Při vyvolání vyjímky se uloží i čas kdy k vyjímce došlo
        private DateTime time;
        // Funkce která vyjímku vyvolala
        private String Fce;
        private int ErrorNum;

        public ExChybaKomunikaceSKartou(string message)
            : base(message)
        {
            time = DateTime.Now;
            Fce = "";
            ErrorNum = 0;
        }

        public ExChybaKomunikaceSKartou(string message, int errorNum)
            : base(message)
        {
            time = DateTime.Now;
            Fce = "";
            ErrorNum = errorNum;
        }

        public ExChybaKomunikaceSKartou(string message, string vyvolalaFce, int errorNum)
            : base(message)
        {
            time = DateTime.Now;
            Fce = vyvolalaFce;
            ErrorNum = errorNum;
        }

        public ExChybaKomunikaceSKartou(string message, string vyvolalaFce)
            : base(message)
        {
            time = DateTime.Now;
            Fce = vyvolalaFce;
            ErrorNum = 0;
        }

        public String VyvolalaFce
        {
            get { return Fce; }
        }

        public int JakeErrorNum
        {
            get { return ErrorNum; }
        }

        public DateTime Time
        {
            get { return time; }
        }
    }

    /// <summary>
    /// Knihovna pro komunikaci s RFID čtečkou SL500
    /// </summary>
    public class RFIDReader
    {
        // SYSTEM FUNCTION
        /// <summary>
        /// Úspání
        /// </summary>
        /// <param name="dwMilliseconds">Čas uspání v milisekundách</param>
        [DllImport("kernel32.dll")]
        static extern void Sleep(int dwMilliseconds);

        /// <summary>
        /// Get DLL Version
        /// </summary>
        /// <param name="pVer">DLL version</param>
        /// <returns></returns>
        [DllImport("MasterRD.dll")]
        static extern int lib_ver(ref uint pVer);

        /// <summary>
        /// Connect
        /// </summary>
        /// <param name="port">serial port number</param>
        /// <param name="baud">communication baud rate, 4800 - 115200 bps</param>
        /// <returns>Return 0 on success</returns>
        [DllImport("MasterRD.dll")]
        static extern int rf_init_com(int port, int baud);

        /// <summary>
        /// Disconnect
        /// </summary>
        /// <returns>0 on success</returns>
        [DllImport("MasterRD.dll")]
        static extern int rf_ClosePort();

        /// <summary>
        /// Get Device Type
        /// </summary>
        /// <param name="icdev">[IN] Device ID</param>
        /// <param name="pVersion">[OUT] response information</param>
        /// <param name="pLen">[OUT] length of response information</param>
        /// <returns>0 on success</returns>
        [DllImport("MasterRD.dll")]
        static extern int rf_get_model(ushort icdev, IntPtr pVersion, ref int pLen);

        /// <summary>
        /// Designate Device ID
        /// </summary>
        /// <param name="icdev">[IN] Device ID</param>
        /// <returns>0 on success</returns>
        [DllImport("MasterRD.dll")]
        static extern int rf_init_device_number(ushort icdev);

        /// <summary>
        /// Read Device ID
        /// </summary>
        /// <param name="icdev">[OUT] response Device ID</param>
        /// <returns>0 on success</returns>
        [DllImport("MasterRD.dll")]
        static extern int rf_get_device_number(ref ushort icdev);

        /// <summary>
        /// Set Reader contactless working mode
        /// </summary>
        /// <param name="icdev">[IN] Device ID</param>
        /// <param name="type">[IN] reader working mode
        /// <para>this function in not effective to the readers only support single protocol</para>
        /// <para>type='A': set SL500 into ISO14443A mode</para>
        /// <para>type='B': set ISO14443B mode</para>
        /// <para>type='r': set AT88RF020 card mode</para>
        /// <para>type='l': set ISO15693 mode</para>
        /// </param>
        /// <returns>0 on success</returns>
        [DllImport("MasterRD.dll")]
        static extern int rf_init_type(ushort icdev, char type);

        /// <summary>
        /// Manage RF Transmittal
        /// </summary>
        /// <param name="icdev">[IN] Device ID</param>
        /// <param name="mode">[IN] transmittal state
        /// <para>mode = 0: turn off RF transmittal</para>
        /// <para>mode = 1: turn on RF transmittal</para>
        /// </param>
        /// <returns>0 on success</returns>
        [DllImport("MasterRD.dll")]
        static extern int rf_antenna_sta(ushort icdev, char mode);
        
        /// <summary>
        /// Manage LED
        /// </summary>
        /// <param name="icdev">[IN] Device ID</param>
        /// <param name="color">[IN] 0 = off
        ///               <para>     1 = red</para>
        ///               <para>     2 = green</para>
        ///               <para>     3 = yellow</para>
        /// </param>
        /// <returns>0 on success</returns>
        [DllImport("MasterRD.dll")]
        static extern int rf_light(ushort icdev, char color);

        /// <summary>
        /// beep
        /// </summary>
        /// <param name="icdev">Device ID</param>
        /// <param name="msec">beep time, unit 10 msec</param>
        /// <returns>0 on success</returns>
        [DllImport("MasterRD.dll")]
        static extern int rf_beep(ushort icdev, char msec);

        // DES FUNCTION
        /// <summary>
        /// DES_Encrypt
        /// </summary>
        /// <param name="pSzOut">[OUT] ciphertext, bytes length equal to plaintext</param>
        /// <param name="pSzIn">[IN] plaintext</param>
        /// <param name="inlen">[IN] length of plaintext, integer times of 8 bytes (každý byte zvlášť)</param>
        /// <param name="pKey">[IN] encrypt key</param>
        /// <param name="keylen">[IN] length of key, 8 bytes for single DES, 16 bytes for triple DES</param>
        /// <returns>0 on success</returns>
        /// <example><code>
        ///public string Encode()
        ///{
        ///    IntPtr dataBuff = Marshal.AllocHGlobal(1);
        ///    string result = "";
        ///    byte[] text = new byte[8] {0x11, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77};
        ///    byte[] key = new byte[8] {0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77};
        ///
        ///    des_encrypt(dataBuff, ref text[0], 8, ref key[0], 8);
        ///
        ///    byte[] bytesData = new byte[8];
        ///    for (int j = 0; j &lt; 8; j++)
        ///    {
        ///        bytesData[j] = Marshal.ReadByte(dataBuff, j);
        ///    }
        ///
        ///    result = BitConverter.ToString(bytesData);
        ///    return result;
        ///}</code></example>
        [DllImport("MasterRD.dll")]
        static extern int des_encrypt(IntPtr pSzOut, ref byte pSzIn, ushort inlen, ref byte pKey, short keylen);
        
        /// <summary>
        /// DES_Decrypt
        /// </summary>
        /// <param name="pSzOut">[OUT] plaintext, bytes length equal to ciphertext</param>
        /// <param name="pSzIn">[IN] ciphertext</param>
        /// <param name="inlen">[IN] length of ciphertext, integer times of 8 bytes</param>
        /// <param name="pKey">[IN] encrypt key</param>
        /// <param name="keylen">[IN] length of key, 8 bytes for single DES, 16 bytes for triple DES</param>
        /// <returns>0 on success</returns>
        [DllImport("MasterRD.dll")]
        static extern int des_decrypt(IntPtr pSzOut, ref byte pSzIn, ushort inlen, ref byte pKey, short keylen);

        //Ultra Light
        /// <summary>
        /// ReqA - (zjisteni typu kart)
        /// </summary>
        /// <param name="icdev">Device ID</param>
        /// <param name="model">REQ MODE
        /// <para>Annotation: mode = 0x26: REQ_STD</para>
        /// <para>mode = 0x52: REQ_ALL</para> 
        /// </param>
        /// <param name="pTagType">response data, chip type code
        /// </param>
        /// <returns>0 on success</returns>
        /// <remarks>2 - 4kB Mifare classic
        ///<para>4 - 1kB Mifare classic</para>
        ///<para>386 - 4 kB Desfire</para></remarks>
        [DllImport("MasterRD.dll")]
        static extern int rf_request(ushort icdev, byte model, ref ushort pTagType);

        /// <summary>
        /// Select UltraLight
        /// </summary>
        /// <param name="icdev">[IN] device ID</param>
        /// <param name="pSnr">[OUT] response data, card unique serial number</param>
        /// <param name="pLen">[OUT] length of response data</param>
        /// <returns>0 on success</returns>
        [DllImport("MasterRD.dll")]
        static extern int rf_ul_select(ushort icdev, IntPtr pSnr, ref int pLen);

        /// <summary>
        /// MifareOne read
        /// <para>Annotation: this function is also applicable for UltraLight card. Every page of UltraLight card has 4 bytes. After calling this function, return data of 4 consecutive pages.</para>
        /// </summary>
        /// <param name="icdev">[IN] device ID</param>
        /// <param name="block">[IN] block absolute address</param>
        /// <param name="pData">[OUT] response data from card</param>
        /// <param name="pLen">[OUT] length of response data</param>
        /// <returns>0 on success</returns>
        [DllImport("MasterRD.dll")]
        static extern int rf_M1_read(ushort icdev, byte block, IntPtr pData, ref int pLen);

        /// <summary>
        /// UltraLight Write
        /// </summary>
        /// <param name="icdev">[IN] - Device ID</param>
        /// <param name="page">[IN] - UltraLight card page address, 0 - 0x0F</param>
        /// <param name="pData">[IN] - written data, 4 bytes</param>
        /// <returns>0 on success</returns>
        [DllImport("MasterRD.dll")]
        static extern int rf_write(ushort icdev, byte page, ref byte pData);

        /// <summary>
        /// Authenticate Key for Ultralight C
        /// </summary>
        /// <param name="icdev">[IN] Communication device identifier</param>
        /// <param name="pKey">[IN] Key, 16 bytes</param>
        /// <returns>return 0 on success</returns>
        [DllImport("MasterRD.dll")]
        static extern int rf_UC_authentication(ushort icdev, ref byte pKey);

        /// <summary>
        /// Update key of Ultralight C
        /// </summary>
        /// <param name="icdev">Communication device identifier</param>
        /// <param name="pKey">Key, 16 bytes</param>
        /// <returns>0 on success</returns>
        [DllImport("MasterRD.dll")]
        static extern int rf_UC_changekey(ushort icdev, ref byte pKey);

        /// <summary>
        /// TYPE_A card HALT
        /// </summary>
        /// <param name="icdev">Communication device identifier</param>
        /// <returns>0 on success</returns>
        [DllImport("MasterRD.dll")]
        static extern int rf_halt(ushort icdev);

        /***************************************/
        /************ Mifare Class *************/
        /***************************************/

        /// <summary>
        /// Mifare card Anticollision
        /// </summary>
        /// <param name="icdev">[IN] Device ID</param>
        /// <param name="bcnt">[IN] must be 4</param>
        /// <param name="pData">[OUT] response data from card, unique serial number</param>
        /// <param name="pLen">[OUT] length of response data</param>
        /// <returns>return 0 if successful</returns>
        [DllImport("MasterRD.dll")]
        static extern int rf_anticoll(ushort icdev, byte bcnt, IntPtr pData, ref int pLen);

        /// <summary>
        /// Mifare card Selectting
        /// <para>Annotation: card will be on active estate after received this commnad, onlz one TYPE_A card on active estate at the same influence range at same time.</para>
        /// </summary>
        /// <param name="icdev">[IN] Device ID</param>
        /// <param name="pSnr">[IN] card unique serial number</param>
        /// <param name="snrLen">[IN] length of pSnr</param>
        /// <param name="pSize">[OUT] response data from card, capacitz code</param>
        /// <returns>0 on success</returns>
        [DllImport("MasterRD.dll")]
        static extern int rf_select(ushort icdev, IntPtr pSnr, int snrLen, ref ushort pSize);

        /// <summary>
        /// Mifare STD Authentify
        /// </summary>
        /// <param name="icdev">[IN] Device ID</param>
        /// <param name="model"><para>[IN] key validate mode</para>
        /// <para>model = 0x60: use Key A</para>
        /// <para>model = 0x61: use Key B</para>
        /// </param>
        /// <param name="block">[IN] block absolute address</param>
        /// <param name="pKey">[IN] 6 bytes password</param>
        /// <returns>0 on success</returns>
        [DllImport("MasterRD.dll")]
        static extern int rf_M1_authentication2(ushort icdev, byte model, byte block, byte[] pKey);

        /// <summary>
        /// MifareOne Read
        /// </summary>
        /// <param name="icdev">[IN] Device ID</param>
        /// <param name="bloc">[IN] block absolute address</param>
        /// <param name="pData">[OUT] response data from card</param>
        /// <param name="pLen">[OUT] length of response data</param>
        /// <returns>0 on success</returns>
        [DllImport("MasterRD.dll")]
        static extern int rf_M1_read(ushort icdev, byte bloc, IntPtr pData, ref byte pLen);

        /// <summary>
        /// Mifare_Std Write
        /// </summary>
        /// <param name="icdev">[IN] Device ID</param>
        /// <param name="bloc">[IN] block absolute address 0 - 255</param>
        /// <param name="data">[IN] written data, 16 bytes</param>
        /// <returns>0 on success</returns>
        [DllImport("MasterRD.dll")]
        static extern int rf_M1_write(ushort icdev, byte bloc, byte[] data);

        /// <summary>
        /// Mifare_Std card Initialize Value
        /// </summary>
        /// <param name="icdev">[IN] Device ID</param>
        /// <param name="bloc">[IN] block absolute address 0 - 255</param>
        /// <param name="value">initialize purse value HEX format, low byte in former</param>
        /// <returns>0 on success</returns>
        [DllImport("MasterRD.dll")]
        static extern int rf_M1_initval(ushort icdev, byte bloc, int value);

        /// <summary>
        /// Mifare_Std Read Value
        /// </summary>
        /// <param name="icdev">[IN] Device ID<</param>
        /// <param name="bloc">[IN] block absolute address 0 - 255</param>
        /// <param name="pValue">resonse purse value HEX format, low byte in former</param>
        /// <returns>0 on success</returns>
        [DllImport("MasterRD.dll")]
        static extern int rf_M1_readval(ushort icdev, byte bloc, ref int pValue);

        /// <summary>
        /// Mifare purse increment
        /// </summary>
        /// <param name="icdev">[IN] Device ID<</param>
        /// <param name="bloc">[IN] block absolute address 0 - 255</param>
        /// <param name="value">increase purse value HEX format, low byte in former</param>
        /// <returns>0 on success</returns>
        [DllImport("MasterRD.dll")]
        static extern int rf_M1_increment(ushort icdev, byte bloc, int value);

        /// <summary>
        /// Mifare purse decrement
        /// </summary>
        /// <param name="icdev">[IN] Device ID<</param>
        /// <param name="bloc">[IN] block absolute address 0 - 255</param>
        /// <param name="value">decrease purse value HEX format, low byte in former</param>
        /// <returns>0 on success</returns>
        [DllImport("MasterRD.dll")]
        static extern int rf_M1_decrement(ushort icdev, byte bloc, int value); 


        /// <summary>
        /// Desfire card Selectting
        /// </summary>
        /// <param name="icdev">[IN] Device ID</param>
        /// <param name="model"><para>[IN] ReqA mode</para>
        /// <para>mode = 0x26: REQ_STD</para>
        /// <para>mode = 0x52: REQ_ALL</para>
        /// </param>
        /// <param name="pData"><para>[OUT] response data from card</para>
        /// <para>7 byte CSN + n byte RATS according to ISO1443-4 protocol</para>
        ///</param>
        /// <param name="pMsgLg">[OUT] length of response data</param>
        /// <returns>0 on success</returns>
        [DllImport("MasterRD.dll")] //DESFire Reset
        static extern int rf_DESFire_rst(ushort icdev, byte model, IntPtr pData, ref int pMsgLg);

        //nedořešeno
        [DllImport("MasterRD.dll")] //DESFire data commutting
        static extern int rf_cos_command(ushort icdev, byte[] pCommnad, byte cmdLen, IntPtr pData, ref int pMsgLg);
        
        /// <summary>
        /// Identifikační číslo připojené čtečky
        /// </summary>
        private ushort DeviceId = 0;

        /// <summary>
        /// Typové označení připojené čtečky
        /// </summary>
        public string TypCtecky = "";

        /// <summary>
        /// Verze dll knihovny
        /// </summary>
        public string VerzeDll = "";

        /// <summary>
        /// Nastavení zvuku čtečky (ON/OFF)
        /// </summary>
        public bool SoundOnOff = false;

        /// <summary>
        /// Indikuje stav připojení
        /// </summary>
        public bool StavPripojeni = false;

        /// <summary>
        /// Číslo Virtuálního com portu pod kterým je připojena čtečka
        /// </summary>
        private int cisloPortu = 1;
        
        /// <summary>
        /// Nastavená přenosová rychlost čtečky
        /// </summary>
        private int prenosovaRychlost = 115200;

        private void beepBAD()
        {
           // rf_light(DeviceId, (char)1); //red
            rf_beep(DeviceId, (char)12);
            Sleep(100);
            //rf_light(DeviceId, (char)0); //red
            Sleep(100);
            //rf_light(DeviceId, (char)1); //red
            rf_beep(DeviceId, (char)12);
            Sleep(100);
            //rf_light(DeviceId, (char)0); //red
            Sleep(100);
            //rf_light(DeviceId, (char)1); //red
            rf_beep(DeviceId, (char)12);
            Sleep(100);
            //rf_light(DeviceId, (char)0); //red
            Sleep(100);
        }

        private void beepOK()
        {
            rf_beep(DeviceId, (char)10);
            Sleep(450);
        }

        public void SignalizaceIdle()
        {
            rf_light(DeviceId, (char)1); //red
        }

        public void SignalizaceWork()
        {
            rf_light(DeviceId, (char)0); //yellow
        }

        public void SignalizaceOK()
        {
            rf_light(DeviceId, (char)2); //green
            if (SoundOnOff == true) beepOK();
            Sleep(400);
            //rf_light(DeviceId, (char)0); //red
            //Sleep(200);
            rf_light(DeviceId, (char)1); //red
        }

        public void SignalizaceError()
        {
            //rf_light(DeviceId, (char)1); //red
            if (SoundOnOff == true) beepBAD();
            rf_light(DeviceId, (char)1); //red
        }

        /// <summary>
        /// Připojí čtečku karet
        /// <para>Připojená čtečka má vypnutou antenu.</para>
        /// </summary>
        /// <param name="port">Číslo virtůálního COM portu</param>
        /// <param name="baud">Přenosová rychlost 4800 - 115200 bps</param>
        public void Pripojit(int port, int baud)
        {
            cisloPortu = port;
            prenosovaRychlost = baud;
            int error = 0;
            if (StavPripojeni == true) Odpojit();
            if (StavPripojeni == false)
            {
                int lenght = 0;
                IntPtr dataBuff = Marshal.AllocHGlobal(1024); //alokace paměti pro datový pointer z funkce dll knihovny čtečky
                error += rf_init_com(cisloPortu, prenosovaRychlost); //připojeni dle portu a rychlosti spojení
                error += rf_get_device_number(ref DeviceId); // zjisteni id připojené čtečky
                error += AntennaStat((char)0); // vypnuti anteny
                error += rf_init_type(DeviceId, 'A');  // nastavení typu komunikace A = ISO14443A - Karty Mifare
                error += rf_get_model(DeviceId, dataBuff, ref lenght); //zjištění modelu čtečky

                if (error == 0)
                {
                    TypCtecky = "";
                    for (int j = 0; j < lenght; j++)
                    {
                        TypCtecky += Convert.ToChar(Marshal.ReadByte(dataBuff, j)); // převod dat z pameni vyznacene pointrem na string
                        TypCtecky = TypCtecky.Replace("\0", "");
                    }
                }

                uint libVer = 0; // zjisteni verze dll knihovny
                error += lib_ver(ref libVer);
                if (error == 0) VerzeDll = libVer.ToString();
                // nastaveni stavu pripojeni
                if (error == 0) { StavPripojeni = true; }
                else { StavPripojeni = false; }
            }
            if (error != 0){
                throw new ExChybaKomunikaceSeCteckou("Nepodařilo se prvotní připojení čtečky.");
            }
        }
        /// <summary>
        /// Připojí čtečku karet
        /// <para>Připojená čtečka má vypnutou antenu.</para>
        /// </summary>
        /// <param name="port">Číslo virtůálního COM portu</param>
        public void Pripojit(int port)
        {
            // pretizena fce pripojeni pouze s portem
            Pripojit(port, 115200);
        }

        /// <summary>
        /// klíč pro kartu HWKey
        /// </summary>
        public byte[] HWKeyCardKey = new byte[6] { 0x5e, 0x2f, 0x4f, 0x2b, 0x31, 0x55 };

        /// <summary>
        /// Odpojí čtečku karet
        /// </summary>
        public void Odpojit()
        {
            int error = 0;
            if (StavPripojeni == true)
            {
                error += rf_ClosePort(); //přikaz pro odpojeni čtečky
                DeviceId = 0;
                TypCtecky = "";
                VerzeDll = "";
                StavPripojeni = false;
            }
            if (error != 0)
            {
                // v případě že se nezdaří uzavřít port
                throw new ExChybaKomunikaceSeCteckou("Nepodařilo se odpojit čtečku.");
            }
        }

        /// <summary>
        /// Zjistí typ přiložené karty
        /// </summary>
        /// <returns>Typ přiložené karty</returns>
        private TRFIDTagy ZjistiTypKarty()
        {
            if(StavPripojeni == true){
                // provede se pokud je ctecka pripojena
                // zapnuti anteny
                if (AntennaStat((char)1) != 0)
                {
                    throw new ExChybaKomunikaceSeCteckou("Problém při zapínání antény čtečky.");
                }

                // reference pro funkci zjištující typ karty
                ushort typKarty = 0;
                int error = 0;

                error += AntennaStat((char)0); // vypnuti anteny
                error += rf_init_type(DeviceId, 'A');  // nastavení typu komunikace A = ISO14443A - Karty Mifare
                error += AntennaStat((char)1); // zapnuti anteny
                error += request(ref typKarty);
                if (error == 0 || error == 20)
                {
                    // vrácení typu karty dle vrácené hodnoty funkcí "request"
                    switch (typKarty)
                    {
                        case 0:
                            return (TRFIDTagy.Zadna);
                        case 4:
                            return (TRFIDTagy.MifareClass1kB);
                        case 2:
                            return (TRFIDTagy.MifareClass4kB);
                        case 836:
                            return (TRFIDTagy.MifareDesfire4Kb);
                        default :
                            return (TRFIDTagy.NeznamaKarta);
                    }
                }
                else
                {
                    throw new ExChybaKomunikaceSKartou("Chyba při identifikaci karty.", "FcRequest - cislo chyby " + error);
                }
            } else {
               //pokud neni ctecka pripojena, nebo neni pritomna karta vrati 0
                return (TRFIDTagy.Zadna);
            }
        }

        /// <summary>
        /// Detekce přiložené karty
        /// </summary>
        /// <returns>Základní informace o detekované karte</returns>
        public TDetekovanaKarta DetekceKarty()
        {
            TDetekovanaKarta karta; // promena pro vraceni s info o prilozene karte
            // zjisteni typu karty
            try
            {
                karta.TypKarty = ZjistiTypKarty();
            }
            catch (ExChybaKomunikaceSKartou ex)
            {
                throw new ExChybaKomunikaceSKartou("Chyba při identifikaci karty.", ex.VyvolalaFce);
            }
            if (karta.TypKarty == TRFIDTagy.MifareDesfire4Kb || karta.TypKarty == TRFIDTagy.MifareClass1kB || karta.TypKarty == TRFIDTagy.MifareClass4kB)
            {
                //** Obsluha typu karty kterou používáme **//
                ushort typKapacity = 0;
                int error = 0;
                IntPtr dataBuff = Marshal.AllocHGlobal(1024); //alokování paměti pro příchozí data z dll
                int dataLen = 0;
                byte[] SerNum;

                switch (karta.TypKarty) {
                    case TRFIDTagy.MifareDesfire4Kb: //aktivace a identifikace karty DesFire
                        error += rf_DESFire_rst(DeviceId,0x26, dataBuff, ref dataLen);
                        SerNum = new byte[dataLen]; //inicializace pole pro uložení příchozích dat a pro předání
                        for (int j = 0; j < dataLen; j++)
                        {
                            SerNum[j] = Marshal.ReadByte(dataBuff, j); //přenos přijatých bytů do pole
                        }           
                        karta.TypKapacity = typKapacity;
                        karta.UID = SerNum;
                        break;

                    case TRFIDTagy.MifareClass1kB:
                    case TRFIDTagy.MifareClass4kB: //aktivace a identifikace karty MifareClass
                        error += rf_anticoll(DeviceId, 0x04, dataBuff, ref dataLen);
                        SerNum = new byte[dataLen]; //inicializace pole pro uložení příchozích dat a pro předání
                        for (int j = 0; j < dataLen; j++)
                        {
                            SerNum[j] = Marshal.ReadByte(dataBuff, j); //přenos přijatých bytů do pole
                        }           
                        // byte[] serNumber = UniqueSerialNumber;
                        error = rf_select(DeviceId, dataBuff, SerNum.Length, ref typKapacity);
                        karta.TypKapacity = typKapacity;
                        karta.UID = SerNum;
                        break;
                    default:
                        karta.UID = new byte[0];
                        karta.TypKapacity = 0;
                        break;
                }
                if (error != 0)
                {
                    throw new ExChybaKomunikaceSKartou("Chyba při identifikaci karty.", "rf_select/rf_DESFire_rst - cislo chyby " + error);
                    
                }
            }
            else
            {
                //** Obsluha typu karty kterou NEpoužíváme **//
                karta.UID = new byte[0];
                karta.TypKapacity = 0;
            }
            return karta;
        }

        /// <summary>
        /// Autorizace pro blok sektoru na kartě MifareClassic
        /// </summary>
        /// <param name="block">Absolutní adresa bloku</param>
        /// <param name="targetKey">vybíraný klíč 'A', nebo 'B', defaultně 'A'</param>
        /// <param name="key">IN byte[6] pole s hodnotou klíče</param>
        /// <returns><para>true - správná autorizace</para>
        /// <para>false - autorizace se nezdařila</para></returns>
        public bool MifareClassicAuthentication(byte block, char targetKey, byte[] key)
        {
            int error = 0;
            bool stavAuthenticace;
            byte keyMode; // proměná pro výběr klíč fce pro autentifikaci s následnou inicializací dle vstupních parametrů metody
            if (targetKey == 'B' || targetKey == 'b')
            {
                keyMode = 0x61;
            } 
            else 
            {
                keyMode = 0x60;
            }
            error = rf_M1_authentication2(DeviceId, keyMode, block, key);
            switch (error)
            {
                case 0:
                    stavAuthenticace = true;
                    break;
                default:
                    stavAuthenticace = false;
                    throw new ExChybaKomunikaceSKartou("Chyba při ověřování klíče karty MifareClassic.", "Rf_M1_authentication - cislo chyby " + error.ToString());
            }
            return (stavAuthenticace);
        }

        /// <summary>
        /// Autorizace pro blok sektoru na kartě MifareClassic klíčem A
        /// </summary>
        /// <param name="block">Absolutní adresa bloku</param>
        /// <param name="key">IN byte[6] pole s hodnotou klíče</param>
        /// <returns><para>true - správná autorizace</para>
        /// <para>false - autorizace se nezdařila</para></returns>
        public bool MifareClassicAuthentication(byte block, byte[] key)
        {
            return (MifareClassicAuthentication(block, 'A', key));
        }

        /// <summary>
        /// Čtení dat z karty MifareClassic - přečte vybraný blok dat - nutná autorizace
        /// </summary>
        /// <param name="block">Absolutní adresa čteného bloku dat. 0..255 (dle velikosti karty)</param>
        /// <returns>16B přečtených dat</returns>
        public byte[] MifareClassicRead(byte block)
        {
            int error = 0;
            IntPtr dataBuff = Marshal.AllocHGlobal(1024); //alokování paměti pro příchozí data z dll
            int dataLen = 0;

            error += rf_M1_read(DeviceId, block, dataBuff, ref dataLen);
            byte[] readData = new byte[dataLen]; //inicializace pole pro uložení příchozích dat a pro předání
            for (int j = 0; j < dataLen; j++)
            {
                readData[j] = Marshal.ReadByte(dataBuff, j); //přenos přijatých bytů do pole
            }
            if (error != 0)
            {
                throw new ExChybaKomunikaceSKartou("Chyba komunikaci s kartou. Nelze číst data.", "rf_M1_read - cislo chyby " + error.ToString());
            }
            return readData;
        }


        /// <summary>
        /// Inicializace hodnoty value bloku na kartě MifareClassic
        /// </summary>
        /// <param name="block">Absolutní adresa bloku dat. 0..255 (dle velikosti karty)</param>
        /// <param name="value">Hodnota pro inicializace</param>
        public void MifareClassicValueWrite(byte block, int value)
        {
            int error = 0;
            
            error += rf_M1_initval(DeviceId, block, value);
            if (error != 0)
            {
                throw new ExChybaKomunikaceSKartou("Chyba komunikaci s kartou. Nelze zapisovat data.", "rf_M1_initval - cislo chyby " + error.ToString());
            }
        }

        /// <summary>
        /// Přečtení hodnoty value bloku na kartě MifareClassic
        /// </summary>
        /// <param name="block">Absolutní adresa bloku dat. 0..255 (dle velikosti karty)</param>
        /// <returns>Hodnota value bloku</returns>
        public int MifareClassicValueRead(byte block)
        {
            int error = 0;
            int value = 0;
            error += rf_M1_readval(DeviceId, block, ref value);
            if (error != 0)
            {
                throw new ExChybaKomunikaceSKartou("Chyba komunikaci s kartou. Nelze zapisovat data.", "rf_M1_readval - cislo chyby " + error.ToString());
            }
            else
            {
                return(value);
            }
        }

        /// <summary>
        /// Inkrementace value bloku
        /// </summary>
        /// <param name="block">Absolutní adresa bloku dat. 0..255 (dle velikosti karty)</param>
        /// <param name="value">Hodnota o kterou inkrementovat</param>
        public void MifareClassicValueInc(byte block, int value)
        {
            int error = 0;
            error += rf_M1_increment(DeviceId, block, value);
            if (error != 0)
            {
                throw new ExChybaKomunikaceSKartou("Chyba komunikaci s kartou. Nelze zapisovat data.", "rf_M1_increment - cislo chyby " + error.ToString());
            }
        }

        /// <summary>
        /// Dekrementace value bloku
        /// </summary>
        /// <param name="block">Absolutní adresa bloku dat. 0..255 (dle velikosti karty)</param>
        /// <param name="value">Hodnota o kterou dekrementovat</param>
        public void MifareClassicValueDec(byte block, int value)
        {
            int error = 0;
            error += rf_M1_decrement(DeviceId, block, value);
            if (error != 0)
            {
                throw new ExChybaKomunikaceSKartou("Chyba komunikaci s kartou. Nelze zapisovat data.", "rf_M1_decrement - cislo chyby " + error.ToString());
            }
        }

        /// <summary>
        /// IsBlockTrailer - zjistí z císla 0 - 255 jestli se jedná o trailer sector
        /// </summary>
        /// <param name="blockNum">císlo bloku 0 - 255</param>
        /// <returns>
        /// <para>0 - není sector trailer</para>
        /// <para>1 - je sector trailer</para>
        ///</returns>
        public byte IsBlockTrailer(byte blockNum)
        {
            byte IsTrailer = 0;
            if ((blockNum & 0x0080) == 0)
            {
                // sektor je v rozmezí sektoru 0 - 31 (block 0b0xxxxxx)
                if ((blockNum & 0x0003) == 0x03) IsTrailer = 1;
            }
            else
            {
                // sektor je v rozmezí sektoru 32 - 39 (block 0b1xxxxxx)
                if ((blockNum & 0x000f) == 0x0f) IsTrailer = 1;
            }
            return (IsTrailer);
        }

        /// <summary>
        /// IsOutOfMemory - zjisti zda je blok v ramci pameti dane karty
        /// </summary>
        /// <param name="blockNum">cislo bloku 0 - 255</param>
        /// <param name="cardType">typ karty dle typu TRFIDTagy</param>
        /// <returns><para>0 - je v ramci pameti</para>
        /// <para>1 - neni v ramci pamei, nebo se jedna o jiny typ karty</para></returns>
        private byte IsOutOfMemory(int blockNum, TRFIDTagy cardType){
            switch (cardType){
                case TRFIDTagy.MifareClass1kB:
                    if (blockNum < 64){
                        return (0);
                    } else {
                         return (1);
                    }
                case TRFIDTagy.MifareClass4kB:
                     return (0);
                default:
                    return (1);
            }
        }

        /// <summary>
        /// SectorNum - zjisti z cisla 0 - 255 cislo sektoru na karte Mifare
        /// </summary>
        /// <param name="blockNum">cislo bloku 0 - 255</param>
        /// <returns>cislo sektoru 0 - 39</returns>
        byte SectorNum(int blockNum)
        {
            int sNum = 0;
            if ((blockNum & 0x0080) == 0)
            {
                // sektor je v rozmez� sektor? 0 - 31 (block 0b0xxxxxx)
                sNum = (blockNum & 0x007c) >> 2;
            }
            else
            {
                // sektor je v rozmez� sektor? 32 - 39 (block 0b1xxxxxx)
                sNum = ((blockNum & 0x00f0) >> 4) + 24;
            }
            return ((byte)sNum);
        }


        /// <summary>
        /// Zápis dat na kartu MifareClassic včetně sektoru trailer - zapíše 16B dat do určitého bloku - nutná autorizace
        /// </summary>
        /// <param name="block">Absolutní adresa čteného bloku dat. 0..255 (dle velikosti karty)</param>
        /// <param name="data">16B zapisovaných dat</param>
        public void MifareClassicWriteTrailer(byte block, byte[] data)
        {
            int error = 0;

            if (data.Length != 16)
            {
                throw new ExChybaKomunikaceSKartou("Chyba komunikaci s kartou. Nelze zapisovat data.", "Chyba v délce vstupního řetězce.");
            }

            error += rf_M1_write(DeviceId, block, data);
            if (error != 0)
            {
                throw new ExChybaKomunikaceSKartou("Chyba komunikaci s kartou. Nelze zapisovat data.", "rf_M1_write - cislo chyby " + error.ToString());
            }
        }

        /// <summary>
        /// Zápis dat na kartu MifareClassic - zapíše 16B dat do určitého bloku - nutná autorizace
        /// </summary>
        /// <param name="block">Absolutní adresa čteného bloku dat. 0..255 (dle velikosti karty)</param>
        /// <param name="data">16B zapisovaných dat</param>
        public void MifareClassicWrite(byte block, byte[] data)
        {
            int error = 0;

            if (IsBlockTrailer(block) != 0)
            {
                throw new ExChybaKomunikaceSKartou("Chyba komunikaci s kartou. Nelze zapisovat data.", "Pokus o zápis do sektoru s klíčem.");
            }

            if (data.Length != 16)
            {
                throw new ExChybaKomunikaceSKartou("Chyba komunikaci s kartou. Nelze zapisovat data.", "Chyba v délce vstupního řetězce.");
            }

            error += rf_M1_write(DeviceId, block, data);
            if (error != 0)
            {
                throw new ExChybaKomunikaceSKartou("Chyba komunikaci s kartou. Nelze zapisovat data.", "rf_M1_write - cislo chyby " + error.ToString());
            }
        }


        /// <summary>
        /// Generátor TrailerSectoru (nastavení přístupových práv pro kartu MifareClassic)
        /// </summary>
        /// <param name="KeyA">přístupový klíč A</param>
        /// <param name="KeyB">volitelný přístupový klíč B (lze použít pro data)</param>
        /// <param name="B9">1 datový byte z Access bytů</param>
        /// <param name="AccessMode"><para>keyAiB - </para>
        /// <para>pouzeKeyA - transportní nastavení karty - vše lze pouze s klíčem A, defaultně nastavený jako FF..FF</para></param>
        /// <returns>vrací data pro block + keyA, keyB + trailerSector</returns>
        public byte[] MFClassicTrailerSectorGernerator(byte[] KeyA, byte[] KeyB, byte B9, TMFAccessMode AccessMode)
        {
            byte[] TrailerSectorBytes = new byte[16];
            byte[] AccessByteSet;
            //kontrolo délky keyA a keyB
            if (KeyA.Length != 6 || KeyB.Length != 6)
            {
                throw new ExChybaKomunikaceSKartou("Chyba komunikaci s kartou. Neplatná délka keyA nebo keyB.", "MCClassicTrailerSectorGenerator - cislo chyby 0x00000");                
            }
            switch (AccessMode)
            {    
                case TMFAccessMode.KeyAiB:
                    //KeyAiB - Key A pouze pro čtení, B pro čtení i zápis - použiji jen pro hodnotu peněženky
                    AccessByteSet = new byte[3] { 0x08, 0x77, 0x8f };
                    break;
                case TMFAccessMode.PouzeKeyA:
                default:
                    // PouzeKeyA - stejné jako transportní nastavení
                    AccessByteSet = new byte[3] { 0xff, 0x07, 0x80 };
                    break;
            }
            KeyA.CopyTo(TrailerSectorBytes, 0);
            AccessByteSet.CopyTo(TrailerSectorBytes, 6);
            TrailerSectorBytes[9] = B9;
            KeyB.CopyTo(TrailerSectorBytes, 10);
            return TrailerSectorBytes;
        }

        /// <summary>
        /// Funkce na generování 3B access bytu pro karty MifareClassic 
        /// </summary>
        /// <param name="c10">jednotlive parametry accesbyte nastavené dle datasheetu MifareClassic</param>
        /// <param name="c20"></param>
        /// <param name="c30"></param>
        /// <param name="c11"></param>
        /// <param name="c21"></param>
        /// <param name="c31"></param>
        /// <param name="c12"></param>
        /// <param name="c22"></param>
        /// <param name="c32"></param>
        /// <param name="c13"></param>
        /// <param name="c23"></param>
        /// <param name="c33"></param>
        /// <remarks><para>pozn.:</para>
        /// <para>varianta nastavení transportní konfigurace pouze A dle datasheetu k MifareDesfire</para>
        /// <para>= MF_makeAccessBytes(false, false, false, false, false, false, false, false, false, false, false, true);</para>
        /// <para>varianta nastavení A key pro reda a B key pro read/write dle datasheetu k MifareDesfire</para>
        /// <para>= MF_makeAccessBytes(true, true, false, true, true, false, true, true, false, false, true, true);</para></remarks>
        /// <returns>3B access byte</returns>
        private byte[] MF_makeAccessBytes(bool c10, bool c20, bool c30, bool c11, bool c21, bool c31, bool c12, bool c22, bool c32, bool c13, bool c23, bool c33)
        {

            byte[] newAccessByte = new byte[3] {0x00, 0x00, 0x00};

            //kopilace prvního bytu bit po bitu
            if (c23 == false) newAccessByte[0] |= 0x01; //kompilace prvního access bitu
            newAccessByte[0] <<= 1; //posun o jedno doleva
            if (c22 == false) newAccessByte[0] |= 0x01; //kompilace prvního access bitu
            newAccessByte[0] <<= 1; //posun o jedno doleva
            if (c21 == false) newAccessByte[0] |= 0x01; //kompilace prvního access bitu
            newAccessByte[0] <<= 1; //posun o jedno doleva
            if (c20 == false) newAccessByte[0] |= 0x01; //kompilace prvního access bitu
            newAccessByte[0] <<= 1; //posun o jedno doleva
            if (c13 == false) newAccessByte[0] |= 0x01; //kompilace prvního access bitu
            newAccessByte[0] <<= 1; //posun o jedno doleva
            if (c12 == false) newAccessByte[0] |= 0x01; //kompilace prvního access bitu
            newAccessByte[0] <<= 1; //posun o jedno doleva
            if (c11 == false) newAccessByte[0] |= 0x01; //kompilace prvního access bitu
            newAccessByte[0] <<= 1; //posun o jedno doleva
            if (c10 == false) newAccessByte[0] |= 0x01; //kompilace prvního access bitu

            //kopilace druhého bytu bit po bitu
            if (c13 == true) newAccessByte[1] |= 0x01; //kompilace prvního access bitu
            newAccessByte[1] <<= 1; //posun o jedno doleva
            if (c12 == true) newAccessByte[1] |= 0x01; //kompilace prvního access bitu
            newAccessByte[1] <<= 1; //posun o jedno doleva
            if (c11 == true) newAccessByte[1] |= 0x01; //kompilace prvního access bitu
            newAccessByte[1] <<= 1; //posun o jedno doleva
            if (c10 == true) newAccessByte[1] |= 0x01; //kompilace prvního access bitu
            newAccessByte[1] <<= 1; //posun o jedno doleva
            if (c33 == false) newAccessByte[1] |= 0x01; //kompilace prvního access bitu
            newAccessByte[1] <<= 1; //posun o jedno doleva
            if (c32 == false) newAccessByte[1] |= 0x01; //kompilace prvního access bitu
            newAccessByte[1] <<= 1; //posun o jedno doleva
            if (c31 == false) newAccessByte[1] |= 0x01; //kompilace prvního access bitu
            newAccessByte[1] <<= 1; //posun o jedno doleva
            if (c30 == false) newAccessByte[1] |= 0x01; //kompilace prvního access bitu

            //kopilace druhého bytu bit po bitu
            if (c33 == true) newAccessByte[2] |= 0x01; //kompilace prvního access bitu
            newAccessByte[2] <<= 1; //posun o jedno doleva
            if (c32 == true) newAccessByte[2] |= 0x01; //kompilace prvního access bitu
            newAccessByte[2] <<= 1; //posun o jedno doleva
            if (c31 == true) newAccessByte[2] |= 0x01; //kompilace prvního access bitu
            newAccessByte[2] <<= 1; //posun o jedno doleva
            if (c30 == true) newAccessByte[2] |= 0x01; //kompilace prvního access bitu
            newAccessByte[2] <<= 1; //posun o jedno doleva
            if (c23 == true) newAccessByte[2] |= 0x01; //kompilace prvního access bitu
            newAccessByte[2] <<= 1; //posun o jedno doleva
            if (c22 == true) newAccessByte[2] |= 0x01; //kompilace prvního access bitu
            newAccessByte[2] <<= 1; //posun o jedno doleva
            if (c21 == true) newAccessByte[2] |= 0x01; //kompilace prvního access bitu
            newAccessByte[2] <<= 1; //posun o jedno doleva
            if (c20 == true) newAccessByte[2] |= 0x01; //kompilace prvního access bitu

            return newAccessByte;

        }


        /// <summary>
        /// Ukončí aktivní kartu. 
        /// </summary>
        public void Halt() {
            int error = 0;
            error = rf_halt(DeviceId);
            if (error != 0)
            {
                throw new ExChybaKomunikaceSKartou("Nepodařilo se ukončit komunikaci s kartou.", "FcRequest - cislo chyby " + error.ToString());
            }
        }

        /// <summary>
        /// Anticoll - MifareClass card Anticollision - vybere jednu kartu a zjistí její seriové číslo
        /// </summary>
        /// <returns>Seriové číslo MifareClass karty. Pole bude inicializováno dle délky odpovědi</returns>
        private byte[] Anticoll() {
            int error = 0;
            IntPtr dataBuff = Marshal.AllocHGlobal(1024); //alokování paměti pro příchozí data z dll
            int dataLen = 0;
            error += rf_anticoll(DeviceId, 0x04, dataBuff, ref dataLen);
            byte[] SerNum = new byte[dataLen]; //inicializace pole pro uložení příchozích dat a pro předání
            for (int j = 0; j < dataLen; j++)
            {
                SerNum[j] = Marshal.ReadByte(dataBuff, j); //přenos přijatých bytů do pole
            }
            if (error != 0)
            {
                throw new ExChybaKomunikaceSKartou("Chyba komunikaci s kartou.", "FcRf_anticoll - cislo chyby " + error.ToString());
            }
            return SerNum;
        }

        /// <summary>
        /// Inicializuje a zjistí typ přiložené karty
        ///<para>2 - 4kB Mifare classic</para>
        ///<para>4 - 1kB Mifare classic</para>
        ///<para>836 - 4 kB Desfire, Desfire EV1</para>
        /// </summary>
        /// <param name="ChipTypeCode">Typ přiložené karty: (reference pro návratovou hodnotu)</param>
        /// <returns>0 - pokud je vše v pořádku</returns>
        private int request(ref ushort ChipTypeCode)
        {
            int error = 0;
            error += rf_request(DeviceId, 0x26, ref ChipTypeCode);
            return error;
        }

        /// <summary>
        /// GetModel - zjistí model čtecího zařízení
        /// </summary>
        /// <param name="SerNum">Sériové číslo čtečky karet: reference na pole znaků</param>
        /// <returns>0 - pokud je vše v pořádku</returns>
        private int GetModel(ref byte[] SerNum)
        {
            int error = 0;
            IntPtr dataBuff = Marshal.AllocHGlobal(1024); //alokování paměti pro příchozí data z dll
            int dataLen = 0;
            error += rf_get_model(DeviceId, dataBuff, ref dataLen);
            SerNum = new byte[dataLen]; //inicializace pole pro uložení příchozích dat a pro předání
            for (int j = 0; j < dataLen; j++)
            {
                SerNum[j] = Marshal.ReadByte(dataBuff, j); //přenos přijatých bytů do pole
            }
            return error;
        }

        /// <summary>
        /// Nastavení antény čtečky ON/OFF
        /// </summary>
        /// <param name="mode">Nastavený mod
        /// <para>mode = 0: turn off RF transmittal</para>
        /// <para>mode = 1: turn on RF transmittal</para>
        /// </param>
        /// <returns>0 - pokud je vše v pořádku</returns>
        private int AntennaStat(char mode)
        {
            int error = 0;
            rf_antenna_sta(DeviceId, mode);
            return error;
        }



/************** Další nedořešené fce  ***********/

        /*
private int MFSelecting()
{
    ushort typKapacity = 0;
    int error = 0;

    IntPtr dataBuff = Marshal.AllocHGlobal(1024); //alokování paměti pro příchozí data z dll
    int dataLen = 0;
    error += rf_anticoll(DeviceId, 0x04, dataBuff, ref dataLen);
            
    byte[] SerNum = new byte[dataLen]; //inicializace pole pro uložení příchozích dat a pro předání
    for (int j = 0; j < dataLen; j++)
    {
        SerNum[j] = Marshal.ReadByte(dataBuff, j); //přenos přijatých bytů do pole
    }           

   // byte[] serNumber = UniqueSerialNumber;
    error = rf_select(DeviceId, dataBuff, SerNum.Length, ref typKapacity);
    return error;
}
*/

        private string Encode()
        {
            IntPtr dataBuff = Marshal.AllocHGlobal(1024);
            int error = 0;
            string result = "";
            byte[] text = new byte[8] { 0x11, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77 };
            byte[] key = new byte[8] { 0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77 };

            error += des_encrypt(dataBuff, ref text[0], 8, ref key[0], 8);

            for (int j = 0; j < 8; j++)
            {
                text[j] = Marshal.ReadByte(dataBuff, j);
                //  message += Convert.ToChar(bytesData[j]);
            }
            result = BitConverter.ToString(text);

            error += des_decrypt(dataBuff, ref text[0], 8, ref key[0], 8);
            byte[] bytesData = new byte[8];
            for (int j = 0; j < 8; j++)
            {
                bytesData[j] = Marshal.ReadByte(dataBuff, j);
                //  message += Convert.ToChar(bytesData[j]);
            }
            result += " a ";
            result += BitConverter.ToString(bytesData);
            result += " a " + error.ToString();
            return result;
        } 
        /*public string request()
        {  
            
            IntPtr dataBuff = Marshal.AllocHGlobal(1024);
            //ushort TagType = 0;
            string result = "";
            int lenght = 0;
            
            rf_ul_select(deviceId, dataBuff, ref lenght);
            
            byte[] bytesData = new byte[lenght];

            for (int j = 0; j < bytesData.Length; j++)
            {
                bytesData[j] = Marshal.ReadByte(dataBuffer, j);
                result += Convert.ToChar(bytesData[j]);
            }
            //result = TagType.ToString();
            return result;
        }*/
        /*
        public static byte[] FromHex(string hex) {
            hex = hex.Replace("-", ""); 
            byte[] raw = new byte[hex.Length / 2]; 
            for (int i = 0; i < raw.Length; i++) {
                raw[i] = Convert.ToByte(hex.Substring(i * 2, 2), 16);
            } 
            return raw; 
        } 
        */

        /*
        public int SetPort(int port)
        {
            int error = 0;
            int lenght = 0;
            
            error += rf_init_com(port, 115200);
            error += rf_get_device_number(ref deviceId);
            rf_get_model(deviceId, dataBuffer, ref lenght);
            byte[] bytesData = new byte[lenght];

            for (int j = 0; j < bytesData.Length; j++)
            {
                bytesData[j] = Marshal.ReadByte(dataBuffer, j);
                message += Convert.ToChar(bytesData[j]);
            }
             message = message.ToString();
             return error + deviceId;
        }
        */

        /*
        private IntPtr dataBuffer = Marshal.AllocHGlobal(1);

        public int SetPort(int port)
        {
            
            int error = 0;
            int lenght = 0;
            error += rf_init_com(port, 115200);
            ushort deviceId = 0;
            error += rf_get_device_number(ref deviceId);
            rf_get_model(deviceId, dataBuffer, ref lenght);

            byte[] bytesData = new byte[lenght];

            for (int j = 0; j < bytesData.Length; j++)
            {
                bytesData[j] = Marshal.ReadByte(dataBuffer, j);
                message += Convert.ToChar(bytesData[j]);
            }

            
             message = message.ToString();
             return error + deviceId;
//            rf_get_device_number
        }
        */
    }
}
