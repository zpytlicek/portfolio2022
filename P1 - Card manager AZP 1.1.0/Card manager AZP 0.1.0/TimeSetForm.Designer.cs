﻿namespace Card_manager_AZP
{
    partial class TimeSetForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TimeSetForm));
            this.button16 = new System.Windows.Forms.Button();
            this.platnostOdTime = new System.Windows.Forms.DateTimePicker();
            this.platnostOdDate = new System.Windows.Forms.DateTimePicker();
            this.label10 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button16
            // 
            this.button16.Location = new System.Drawing.Point(225, 44);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(103, 23);
            this.button16.TabIndex = 66;
            this.button16.Text = "Uložit na kartu";
            this.button16.UseVisualStyleBackColor = true;
            this.button16.Click += new System.EventHandler(this.button16_Click);
            // 
            // platnostOdTime
            // 
            this.platnostOdTime.CustomFormat = "HH:mm";
            this.platnostOdTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.platnostOdTime.Location = new System.Drawing.Point(274, 18);
            this.platnostOdTime.Name = "platnostOdTime";
            this.platnostOdTime.ShowUpDown = true;
            this.platnostOdTime.Size = new System.Drawing.Size(54, 20);
            this.platnostOdTime.TabIndex = 69;
            // 
            // platnostOdDate
            // 
            this.platnostOdDate.Location = new System.Drawing.Point(99, 18);
            this.platnostOdDate.MaxDate = new System.DateTime(2099, 1, 1, 0, 0, 0, 0);
            this.platnostOdDate.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.platnostOdDate.Name = "platnostOdDate";
            this.platnostOdDate.Size = new System.Drawing.Size(169, 20);
            this.platnostOdDate.TabIndex = 67;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(7, 20);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(90, 13);
            this.label10.TabIndex = 68;
            this.label10.Text = "Nastavovaný čas";
            // 
            // TimeSetForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(340, 79);
            this.Controls.Add(this.button16);
            this.Controls.Add(this.platnostOdTime);
            this.Controls.Add(this.platnostOdDate);
            this.Controls.Add(this.label10);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(356, 117);
            this.MinimumSize = new System.Drawing.Size(356, 117);
            this.Name = "TimeSetForm";
            this.Text = "Nastavení času";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.DateTimePicker platnostOdTime;
        private System.Windows.Forms.DateTimePicker platnostOdDate;
        private System.Windows.Forms.Label label10;
    }
}