﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Card_manager_AZP
{
    public partial class UserForm : Form
    {
        private int userId = 0;
        public UserForm(int userId)
        {
            this.userId = userId;
            InitializeComponent();

            //pokud byl vybrany uzivatel pro zmenu
            if (userId != 0)
            {
                //zamena tlacitka pro ulozeni zmen
                button1.Visible = false;
                button3.Visible = true;
                //nacteni informaci o aktualne vybranem uzivateli
                string connectionString = "Data source=" + MainForm.NastaveniAplikace.MSSQLServer + ";User ID=" + MainForm.NastaveniAplikace.MSSQLLogin + ";Password=" + MainForm.NastaveniAplikace.MSSQLPassword + ";Connect Timeout=1;Database=CMData";
                SqlConnection conn = new SqlConnection(connectionString);
                try
                {
                    conn.Open();

                    //kontrola zda toto uzivatelske jmeno existuje
                    SqlCommand CmdSelectPermanetka = new SqlCommand(@"SELECT [nickname],[password] FROM [CMData].[dbo].[users] WHERE [id] = " + userId.ToString(), conn);

                    SqlDataReader dataReader;
                    dataReader = CmdSelectPermanetka.ExecuteReader();
                    if (dataReader.HasRows){
                        dataReader.Read();
                        name.Text = dataReader.GetString(0);
                       // password.Text = dataReader.GetString(1);
                    }
                    dataReader.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    conn.Close();
                }

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //přidání uživatele

            string connectionString = "Data source=" + MainForm.NastaveniAplikace.MSSQLServer + ";User ID=" + MainForm.NastaveniAplikace.MSSQLLogin + ";Password=" + MainForm.NastaveniAplikace.MSSQLPassword + ";Connect Timeout=1;Database=CMData";
            SqlConnection conn = new SqlConnection(connectionString);
            
            try
            {
                conn.Open();

                //kontrola zda toto uzivatelske jmeno existuje
                SqlCommand CmdSelectPermanetka = new SqlCommand(@"SELECT [id] FROM [CMData].[dbo].[users] WHERE [nickname] = '" + name.Text + "'", conn);
                object test = CmdSelectPermanetka.ExecuteScalar();
                if (test != null)
                {
                    MessageBox.Show("Toto uživatelské jméno již existuje. Zadejte jiné.","Pozor!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    conn.Close();
                    return;
                }
                //Založení nového uživatele
                SqlCommand CmdInsertUser = new SqlCommand(@"INSERT INTO [CMData].[dbo].[users] ([nickname],[password]) VALUES ('"+name.Text+"','"+MainForm.PasswordEncryptor(password.Text)+"')",conn);
                CmdInsertUser.ExecuteNonQuery();
                conn.Close();
                this.Close();

                //  SqlCommand CmdSelectPermanetka = new SqlCommand(@"DELETE FROM [CMData].[dbo].[users] WHERE [id] = " + ((ComboboxItem)comboBoxLoginList.SelectedItem).Value.ToString(), conn);
             //   CmdSelectPermanetka.ExecuteNonQuery(); //provedeni osdstraneni
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }

        private void UserForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            ((SystemForm)MainForm.SubForm).UlozeniNastaveniDBaNacteniLoginu(); //nacteni seznamu loginu v suboknu
            this.Dispose();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //uložení změn uživatele

            string connectionString = "Data source=" + MainForm.NastaveniAplikace.MSSQLServer + ";User ID=" + MainForm.NastaveniAplikace.MSSQLLogin + ";Password=" + MainForm.NastaveniAplikace.MSSQLPassword + ";Connect Timeout=1;Database=CMData";
            SqlConnection conn = new SqlConnection(connectionString);

            try
            {
                conn.Open();

                //kontrola zda toto uzivatelske jmeno existuje
                SqlCommand CmdSelectPermanetka = new SqlCommand(@"SELECT [id] FROM [CMData].[dbo].[users] WHERE [nickname] = '" + name.Text + "' AND [id] <> "+ userId, conn);
                object test = CmdSelectPermanetka.ExecuteScalar();
                if (test != null)
                {
                    MessageBox.Show("Toto uživatelské jméno již existuje. Zadejte jiné.", "Pozor!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    conn.Close();
                    return;
                }
                //Uložení změn pro vybraného uživatele

                SqlCommand CmdInsertUser = new SqlCommand(@"UPDATE [CMData].[dbo].[users] SET [nickname] = '" + name.Text + "',[password] = '" + MainForm.PasswordEncryptor(password.Text) + "' WHERE [id] = " + userId, conn);
                CmdInsertUser.ExecuteNonQuery();
                conn.Close();
                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }
    }
}
