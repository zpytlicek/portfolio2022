﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MFCardAZP;
using System.Globalization;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace RFID_AZP_Servis_0._1._0
{
    public static class HexFce{
        /// <summary>
        /// Převod HexStringu na Byte Array
        /// </summary>
        /// <param name="hexString">("aa" -> 0xaa)</param>
        /// <returns>("aa" -> 0xaa)</returns>
        public static byte[] ConvertHexStringToByteArray(string hexString)
        {
            if (hexString.Length % 2 != 0)
            {
                throw new ArgumentException(String.Format(CultureInfo.InvariantCulture, "The binary key cannot have an odd number of digits: {0}", hexString));
            }

            byte[] HexAsBytes = new byte[hexString.Length / 2];
            try
            {
                for (int index = 0; index < HexAsBytes.Length; index++)
                {
                    string byteValue = hexString.Substring(index * 2, 2);
                    HexAsBytes[index] = byte.Parse(byteValue, NumberStyles.HexNumber, CultureInfo.InvariantCulture);
                }
                return HexAsBytes;
            }
            catch (FormatException)
            {
                throw new FormatException();
            }

        } 

    }

   /* public static class zaznam
    {
        void save(zaznamNastaveni so)
    }*/

    [Serializable]
    public class zaznamNastaveni
    {
        public TTerminalSets sets = new TTerminalSets();
        public TSetCardEnables setsEnables = new TSetCardEnables();
        public int verzeZaznamu = 1;
        public string note;

        public void iniAllValue()
        {
            note = "";

            //nastaveni enables
            setsEnables.APID = 0xff;
            setsEnables.TerminalID = 0xff;
            setsEnables.writeBlockedCards = 0;
            setsEnables.writeCustomerKeys = 0;
            setsEnables.writeHwRegisters = 0;
            setsEnables.writeProductionNum = 0;
            setsEnables.writeTerminalId = 0;
            setsEnables.writeTime = 0;
            setsEnables.writeUseCounter = 0;
            setsEnables.writeUsersRegisters = 0;
            setsEnables.writeUsersSets = 0;

            //nastaveni sets
            for (int i = 0; i < 20; i++) sets.blockedCards.CardNumber[i] = 0;
            for (int i = 0; i < 6; i++) sets.customerKey[i] = 0x00;
            for (int i = 0; i < 6; i++) sets.customerServisKey[i] = 0x00;
            for (int i = 0; i < 20; i++) sets.hwRegisters[i] = 0x00;
            sets.productionNum = 0;
            sets.programmingDay = 0;
            sets.programmingMonth = 0;
            sets.programmingYear = 0;
            sets.terminalId = 0;
            sets.useCounter = 0;
            for (int i = 0; i < 20; i++) sets.usersRegisters[i] = 0x00;
            for (int classIndex = 0; classIndex < 7; classIndex++)
            {
                for (int i = 0; i < 10; i++) sets.usersSets.user[classIndex].uSet[i] = 0;
                for (int dayIndex = 0; dayIndex < 7; dayIndex++)
                {
                    //zona 1
                    sets.usersSets.user[classIndex].userDay[dayIndex].UserDayZone1.startHour = 0;
                    sets.usersSets.user[classIndex].userDay[dayIndex].UserDayZone1.startMinute = 0;
                    sets.usersSets.user[classIndex].userDay[dayIndex].UserDayZone1.endHour = 0;
                    sets.usersSets.user[classIndex].userDay[dayIndex].UserDayZone1.endMinute = 0;
                    sets.usersSets.user[classIndex].userDay[dayIndex].UserDayZone1.tarif = 0;
                    //zona 2
                    sets.usersSets.user[classIndex].userDay[dayIndex].UserDayZone2.startHour = 0;
                    sets.usersSets.user[classIndex].userDay[dayIndex].UserDayZone2.startMinute = 0;
                    sets.usersSets.user[classIndex].userDay[dayIndex].UserDayZone2.endHour = 0;
                    sets.usersSets.user[classIndex].userDay[dayIndex].UserDayZone2.endMinute = 0;
                    sets.usersSets.user[classIndex].userDay[dayIndex].UserDayZone2.tarif = 0;
                    //zona 3
                    sets.usersSets.user[classIndex].userDay[dayIndex].UserDayZone3.startHour = 0;
                    sets.usersSets.user[classIndex].userDay[dayIndex].UserDayZone3.startMinute = 0;
                    sets.usersSets.user[classIndex].userDay[dayIndex].UserDayZone3.endHour = 0;
                    sets.usersSets.user[classIndex].userDay[dayIndex].UserDayZone3.endMinute = 0;
                    sets.usersSets.user[classIndex].userDay[dayIndex].UserDayZone3.tarif = 0;
                }
            }


            //  sets. .usersRegisters
        }

        public void save(string fileName, zaznamNastaveni data){
            FileStream fs = null;
            try
            {
                fs = File.Create(fileName); //vytvori nebo prepise soubor
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(fs, data);
            }
            finally
            {
                if (fs != null) fs.Close();
            }
        }

        public zaznamNastaveni load(string fileName)
        {
            zaznamNastaveni dataBuff;
            FileStream fs = null;
            try
            {
                fs = File.Open(fileName, FileMode.Open);
                BinaryFormatter bf = new BinaryFormatter();
                dataBuff = ((zaznamNastaveni)bf.Deserialize(fs));
            }
            catch
            {
                throw new Exception("Nepodařilo se načíst soubor.");
            }
            finally
            {
                if (fs != null) fs.Close();
            }
            return (dataBuff);
        }
    }
}
