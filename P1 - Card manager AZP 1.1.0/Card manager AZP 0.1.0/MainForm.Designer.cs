﻿namespace Card_manager_AZP
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.pokladanLabel = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.buttonExportUdalosti = new System.Windows.Forms.Button();
            this.deactivationButton = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.buttonReadRecords = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.newCreditLabel = new System.Windows.Forms.Label();
            this.StartCreditLabel = new System.Windows.Forms.Label();
            this.releaseButton = new System.Windows.Forms.Button();
            this.newCredit = new System.Windows.Forms.NumericUpDown();
            this.textBoxNote = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.buttonSaveChanges = new System.Windows.Forms.Button();
            this.actualCredit = new System.Windows.Forms.Label();
            this.classComboBox = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.platnostDoTime = new System.Windows.Forms.DateTimePicker();
            this.platnostOdTime = new System.Windows.Forms.DateTimePicker();
            this.platnostOdDate = new System.Windows.Forms.DateTimePicker();
            this.label13 = new System.Windows.Forms.Label();
            this.platnostDoDate = new System.Windows.Forms.DateTimePicker();
            this.debetNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.startCredit = new System.Windows.Forms.NumericUpDown();
            this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripDropDownButton2 = new System.Windows.Forms.ToolStripDropDownButton();
            this.systémToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripDropDownButton4 = new System.Windows.Forms.ToolStripDropDownButton();
            this.systémToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.správaTerminálůToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.statsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.oProgramuToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.readerToolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.cardIco = new System.Windows.Forms.ToolStripStatusLabel();
            this.CardLoadingProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.CardLoadingStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.label5 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.pokladnaLockImage = new System.Windows.Forms.PictureBox();
            this.toolStripDropDownButton3 = new System.Windows.Forms.ToolStripDropDownButton();
            this.oProgramuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.správaTerminálůToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.loginButton = new System.Windows.Forms.Button();
            this.login = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.newCredit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.debetNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.startCredit)).BeginInit();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pokladnaLockImage)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pokladanLabel
            // 
            this.pokladanLabel.AutoSize = true;
            this.pokladanLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pokladanLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.pokladanLabel.Location = new System.Drawing.Point(33, 0);
            this.pokladanLabel.Name = "pokladanLabel";
            this.pokladanLabel.Size = new System.Drawing.Size(151, 30);
            this.pokladanLabel.TabIndex = 5;
            this.pokladanLabel.Text = "Stav pokladny : 0 Kč";
            this.pokladanLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Location = new System.Drawing.Point(5, 33);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(804, 404);
            this.tabControl1.TabIndex = 4;
            this.tabControl1.Visible = false;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.buttonExportUdalosti);
            this.tabPage1.Controls.Add(this.deactivationButton);
            this.tabPage1.Controls.Add(this.dataGridView1);
            this.tabPage1.Controls.Add(this.buttonReadRecords);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(796, 378);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Výdej karet";
            // 
            // buttonExportUdalosti
            // 
            this.buttonExportUdalosti.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonExportUdalosti.Enabled = false;
            this.buttonExportUdalosti.Location = new System.Drawing.Point(6, 349);
            this.buttonExportUdalosti.Name = "buttonExportUdalosti";
            this.buttonExportUdalosti.Size = new System.Drawing.Size(171, 23);
            this.buttonExportUdalosti.TabIndex = 8;
            this.buttonExportUdalosti.Text = "Export načtených událostí";
            this.buttonExportUdalosti.UseVisualStyleBackColor = true;
            this.buttonExportUdalosti.Visible = false;
            this.buttonExportUdalosti.Click += new System.EventHandler(this.buttonExportUdalosti_Click);
            // 
            // deactivationButton
            // 
            this.deactivationButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.deactivationButton.Location = new System.Drawing.Point(563, 349);
            this.deactivationButton.Name = "deactivationButton";
            this.deactivationButton.Size = new System.Drawing.Size(113, 23);
            this.deactivationButton.TabIndex = 74;
            this.deactivationButton.Text = "Deaktivovat kartu";
            this.deactivationButton.UseVisualStyleBackColor = true;
            this.deactivationButton.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle17.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle17.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle17;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column6,
            this.Column1,
            this.Column3,
            this.Column4,
            this.Column5});
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle23.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle23.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle23.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle23.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle23;
            this.dataGridView1.Location = new System.Drawing.Point(6, 134);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle24.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle24.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle24.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle24.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle24.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle24;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(782, 209);
            this.dataGridView1.TabIndex = 73;
            // 
            // Column6
            // 
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Column6.DefaultCellStyle = dataGridViewCellStyle18;
            this.Column6.HeaderText = "#";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Width = 50;
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Column1.DefaultCellStyle = dataGridViewCellStyle19;
            this.Column1.HeaderText = "Čas události";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 89;
            // 
            // Column3
            // 
            this.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.Column3.DefaultCellStyle = dataGridViewCellStyle20;
            this.Column3.HeaderText = "Terminál";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // Column4
            // 
            this.Column4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Column4.DefaultCellStyle = dataGridViewCellStyle21;
            this.Column4.HeaderText = "Změna kreditu";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            // 
            // Column5
            // 
            this.Column5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Column5.DefaultCellStyle = dataGridViewCellStyle22;
            this.Column5.HeaderText = "Konečný stav";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 97;
            // 
            // buttonReadRecords
            // 
            this.buttonReadRecords.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonReadRecords.Location = new System.Drawing.Point(682, 349);
            this.buttonReadRecords.Name = "buttonReadRecords";
            this.buttonReadRecords.Size = new System.Drawing.Size(106, 23);
            this.buttonReadRecords.TabIndex = 72;
            this.buttonReadRecords.Text = "Načíst události";
            this.buttonReadRecords.UseVisualStyleBackColor = true;
            this.buttonReadRecords.Click += new System.EventHandler(this.button4_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.newCreditLabel);
            this.groupBox1.Controls.Add(this.StartCreditLabel);
            this.groupBox1.Controls.Add(this.releaseButton);
            this.groupBox1.Controls.Add(this.newCredit);
            this.groupBox1.Controls.Add(this.textBoxNote);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.buttonSaveChanges);
            this.groupBox1.Controls.Add(this.actualCredit);
            this.groupBox1.Controls.Add(this.classComboBox);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.platnostDoTime);
            this.groupBox1.Controls.Add(this.platnostOdTime);
            this.groupBox1.Controls.Add(this.platnostOdDate);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.platnostDoDate);
            this.groupBox1.Controls.Add(this.debetNumericUpDown);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.startCredit);
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(782, 117);
            this.groupBox1.TabIndex = 69;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Základní nastavení karty č.";
            // 
            // newCreditLabel
            // 
            this.newCreditLabel.AutoSize = true;
            this.newCreditLabel.Location = new System.Drawing.Point(307, 88);
            this.newCreditLabel.Name = "newCreditLabel";
            this.newCreditLabel.Size = new System.Drawing.Size(75, 13);
            this.newCreditLabel.TabIndex = 73;
            this.newCreditLabel.Text = "Změna kreditu";
            // 
            // StartCreditLabel
            // 
            this.StartCreditLabel.AutoSize = true;
            this.StartCreditLabel.Location = new System.Drawing.Point(300, 88);
            this.StartCreditLabel.Name = "StartCreditLabel";
            this.StartCreditLabel.Size = new System.Drawing.Size(86, 13);
            this.StartCreditLabel.TabIndex = 72;
            this.StartCreditLabel.Text = "Počáteční kredit";
            // 
            // releaseButton
            // 
            this.releaseButton.Location = new System.Drawing.Point(503, 83);
            this.releaseButton.Name = "releaseButton";
            this.releaseButton.Size = new System.Drawing.Size(261, 22);
            this.releaseButton.TabIndex = 71;
            this.releaseButton.Text = "Vydat kartu";
            this.releaseButton.UseVisualStyleBackColor = true;
            this.releaseButton.Click += new System.EventHandler(this.button3_Click);
            // 
            // newCredit
            // 
            this.newCredit.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.newCredit.Location = new System.Drawing.Point(388, 86);
            this.newCredit.Maximum = new decimal(new int[] {
            50000,
            0,
            0,
            0});
            this.newCredit.Minimum = new decimal(new int[] {
            50000,
            0,
            0,
            -2147483648});
            this.newCredit.Name = "newCredit";
            this.newCredit.Size = new System.Drawing.Size(90, 20);
            this.newCredit.TabIndex = 70;
            this.newCredit.ValueChanged += new System.EventHandler(this.zmenaDatNastaveniKarty);
            this.newCredit.Enter += new System.EventHandler(this.zmenaDatNastaveniKarty);
            // 
            // textBoxNote
            // 
            this.textBoxNote.Location = new System.Drawing.Point(70, 84);
            this.textBoxNote.Name = "textBoxNote";
            this.textBoxNote.Size = new System.Drawing.Size(215, 20);
            this.textBoxNote.TabIndex = 70;
            this.textBoxNote.TextChanged += new System.EventHandler(this.zmenaDatNastaveniKarty);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 85);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 13);
            this.label4.TabIndex = 69;
            this.label4.Text = "Poznámka";
            // 
            // buttonSaveChanges
            // 
            this.buttonSaveChanges.Enabled = false;
            this.buttonSaveChanges.Location = new System.Drawing.Point(503, 82);
            this.buttonSaveChanges.Name = "buttonSaveChanges";
            this.buttonSaveChanges.Size = new System.Drawing.Size(261, 24);
            this.buttonSaveChanges.TabIndex = 58;
            this.buttonSaveChanges.Text = "Uložit změny";
            this.buttonSaveChanges.UseVisualStyleBackColor = true;
            this.buttonSaveChanges.Click += new System.EventHandler(this.button16_Click);
            // 
            // actualCredit
            // 
            this.actualCredit.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(238)));
            this.actualCredit.Location = new System.Drawing.Point(84, 27);
            this.actualCredit.Name = "actualCredit";
            this.actualCredit.Size = new System.Drawing.Size(87, 23);
            this.actualCredit.TabIndex = 69;
            this.actualCredit.Text = "-";
            this.actualCredit.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // classComboBox
            // 
            this.classComboBox.DisplayMember = "1";
            this.classComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.classComboBox.Items.AddRange(new object[] {
            "Skupina 1",
            "Skupina 2",
            "Skupina 3",
            "Skupina 4",
            "Skupina 5",
            "Skupina 6",
            "Skupina 7"});
            this.classComboBox.Location = new System.Drawing.Point(237, 26);
            this.classComboBox.Name = "classComboBox";
            this.classComboBox.Size = new System.Drawing.Size(241, 21);
            this.classComboBox.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(6, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 20);
            this.label3.TabIndex = 68;
            this.label3.Text = "Zůstatek";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(185, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Skupina";
            // 
            // platnostDoTime
            // 
            this.platnostDoTime.CausesValidation = false;
            this.platnostDoTime.CustomFormat = "HH:00";
            this.platnostDoTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.platnostDoTime.Location = new System.Drawing.Point(710, 56);
            this.platnostDoTime.Name = "platnostDoTime";
            this.platnostDoTime.ShowUpDown = true;
            this.platnostDoTime.Size = new System.Drawing.Size(54, 20);
            this.platnostDoTime.TabIndex = 66;
            this.platnostDoTime.ValueChanged += new System.EventHandler(this.zmenaDatNastaveniKarty);
            this.platnostDoTime.Enter += new System.EventHandler(this.zmenaDatNastaveniKarty);
            // 
            // platnostOdTime
            // 
            this.platnostOdTime.CausesValidation = false;
            this.platnostOdTime.CustomFormat = "HH:00";
            this.platnostOdTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.platnostOdTime.Location = new System.Drawing.Point(710, 30);
            this.platnostOdTime.Name = "platnostOdTime";
            this.platnostOdTime.ShowUpDown = true;
            this.platnostOdTime.Size = new System.Drawing.Size(54, 20);
            this.platnostOdTime.TabIndex = 65;
            this.platnostOdTime.ValueChanged += new System.EventHandler(this.zmenaDatNastaveniKarty);
            this.platnostOdTime.Enter += new System.EventHandler(this.zmenaDatNastaveniKarty);
            // 
            // platnostOdDate
            // 
            this.platnostOdDate.Location = new System.Drawing.Point(566, 30);
            this.platnostOdDate.MaxDate = new System.DateTime(2099, 1, 1, 0, 0, 0, 0);
            this.platnostOdDate.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.platnostOdDate.Name = "platnostOdDate";
            this.platnostOdDate.Size = new System.Drawing.Size(138, 20);
            this.platnostOdDate.TabIndex = 59;
            this.platnostOdDate.ValueChanged += new System.EventHandler(this.zmenaDatNastaveniKarty);
            this.platnostOdDate.Enter += new System.EventHandler(this.zmenaDatNastaveniKarty);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(301, 62);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(81, 13);
            this.label13.TabIndex = 64;
            this.label13.Text = "Povolený debet";
            // 
            // platnostDoDate
            // 
            this.platnostDoDate.Location = new System.Drawing.Point(566, 56);
            this.platnostDoDate.MaxDate = new System.DateTime(2099, 1, 1, 0, 0, 0, 0);
            this.platnostDoDate.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.platnostDoDate.Name = "platnostDoDate";
            this.platnostDoDate.Size = new System.Drawing.Size(138, 20);
            this.platnostDoDate.TabIndex = 60;
            this.platnostDoDate.ValueChanged += new System.EventHandler(this.zmenaDatNastaveniKarty);
            this.platnostDoDate.Enter += new System.EventHandler(this.zmenaDatNastaveniKarty);
            // 
            // debetNumericUpDown
            // 
            this.debetNumericUpDown.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.debetNumericUpDown.Location = new System.Drawing.Point(388, 60);
            this.debetNumericUpDown.Maximum = new decimal(new int[] {
            50000,
            0,
            0,
            0});
            this.debetNumericUpDown.Name = "debetNumericUpDown";
            this.debetNumericUpDown.Size = new System.Drawing.Size(90, 20);
            this.debetNumericUpDown.TabIndex = 63;
            this.debetNumericUpDown.ValueChanged += new System.EventHandler(this.zmenaDatNastaveniKarty);
            this.debetNumericUpDown.Enter += new System.EventHandler(this.zmenaDatNastaveniKarty);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(500, 32);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(60, 13);
            this.label10.TabIndex = 61;
            this.label10.Text = "Platnost od";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(500, 60);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(60, 13);
            this.label11.TabIndex = 62;
            this.label11.Text = "Platnost do";
            // 
            // startCredit
            // 
            this.startCredit.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.startCredit.Location = new System.Drawing.Point(388, 86);
            this.startCredit.Maximum = new decimal(new int[] {
            50000,
            0,
            0,
            0});
            this.startCredit.Name = "startCredit";
            this.startCredit.Size = new System.Drawing.Size(90, 20);
            this.startCredit.TabIndex = 71;
            // 
            // toolStripDropDownButton1
            // 
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.Size = new System.Drawing.Size(23, 23);
            // 
            // toolStripDropDownButton2
            // 
            this.toolStripDropDownButton2.Name = "toolStripDropDownButton2";
            this.toolStripDropDownButton2.Size = new System.Drawing.Size(23, 23);
            // 
            // systémToolStripMenuItem
            // 
            this.systémToolStripMenuItem.Name = "systémToolStripMenuItem";
            this.systémToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.systémToolStripMenuItem.Text = "Systém";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripDropDownButton4,
            this.readerToolStripStatusLabel,
            this.cardIco,
            this.CardLoadingProgressBar,
            this.CardLoadingStatusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 440);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.statusStrip1.Size = new System.Drawing.Size(809, 22);
            this.statusStrip1.SizingGrip = false;
            this.statusStrip1.TabIndex = 6;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripDropDownButton4
            // 
            this.toolStripDropDownButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripDropDownButton4.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.systémToolStripMenuItem1,
            this.správaTerminálůToolStripMenuItem1,
            this.toolStripMenuItem1,
            this.statsToolStripMenuItem1,
            this.oProgramuToolStripMenuItem1});
            this.toolStripDropDownButton4.Image = global::Card_manager_AZP.Properties.Resources.advanced;
            this.toolStripDropDownButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton4.Name = "toolStripDropDownButton4";
            this.toolStripDropDownButton4.Size = new System.Drawing.Size(29, 20);
            this.toolStripDropDownButton4.Text = "toolStripDropDownButton4";
            // 
            // systémToolStripMenuItem1
            // 
            this.systémToolStripMenuItem1.Image = global::Card_manager_AZP.Properties.Resources.lin_agt_wrench;
            this.systémToolStripMenuItem1.Name = "systémToolStripMenuItem1";
            this.systémToolStripMenuItem1.Size = new System.Drawing.Size(163, 22);
            this.systémToolStripMenuItem1.Text = "Systém";
            this.systémToolStripMenuItem1.Click += new System.EventHandler(this.systémToolStripMenuItem1_Click);
            // 
            // správaTerminálůToolStripMenuItem1
            // 
            this.správaTerminálůToolStripMenuItem1.Image = global::Card_manager_AZP.Properties.Resources.view_choose;
            this.správaTerminálůToolStripMenuItem1.Name = "správaTerminálůToolStripMenuItem1";
            this.správaTerminálůToolStripMenuItem1.Size = new System.Drawing.Size(163, 22);
            this.správaTerminálůToolStripMenuItem1.Text = "Správa terminálů";
            this.správaTerminálůToolStripMenuItem1.Click += new System.EventHandler(this.správaTerminálůToolStripMenuItem1_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Image = global::Card_manager_AZP.Properties.Resources.card;
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(163, 22);
            this.toolStripMenuItem1.Text = "Správa karet";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // statsToolStripMenuItem1
            // 
            this.statsToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("statsToolStripMenuItem1.Image")));
            this.statsToolStripMenuItem1.Name = "statsToolStripMenuItem1";
            this.statsToolStripMenuItem1.Size = new System.Drawing.Size(163, 22);
            this.statsToolStripMenuItem1.Text = "Statistiky";
            this.statsToolStripMenuItem1.Click += new System.EventHandler(this.statsToolStripMenuItem1_Click);
            // 
            // oProgramuToolStripMenuItem1
            // 
            this.oProgramuToolStripMenuItem1.Image = global::Card_manager_AZP.Properties.Resources.info;
            this.oProgramuToolStripMenuItem1.Name = "oProgramuToolStripMenuItem1";
            this.oProgramuToolStripMenuItem1.Size = new System.Drawing.Size(163, 22);
            this.oProgramuToolStripMenuItem1.Text = "O programu";
            this.oProgramuToolStripMenuItem1.Click += new System.EventHandler(this.oProgramuToolStripMenuItem1_Click);
            // 
            // readerToolStripStatusLabel
            // 
            this.readerToolStripStatusLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.readerToolStripStatusLabel.Image = global::Card_manager_AZP.Properties.Resources.usb;
            this.readerToolStripStatusLabel.Name = "readerToolStripStatusLabel";
            this.readerToolStripStatusLabel.Size = new System.Drawing.Size(76, 17);
            this.readerToolStripStatusLabel.Text = "Odpojeno";
            // 
            // cardIco
            // 
            this.cardIco.Image = global::Card_manager_AZP.Properties.Resources.card;
            this.cardIco.Name = "cardIco";
            this.cardIco.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cardIco.Size = new System.Drawing.Size(16, 17);
            this.cardIco.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            // 
            // CardLoadingProgressBar
            // 
            this.CardLoadingProgressBar.Name = "CardLoadingProgressBar";
            this.CardLoadingProgressBar.Size = new System.Drawing.Size(100, 16);
            this.CardLoadingProgressBar.Step = 1;
            this.CardLoadingProgressBar.Visible = false;
            // 
            // CardLoadingStatusLabel
            // 
            this.CardLoadingStatusLabel.Name = "CardLoadingStatusLabel";
            this.CardLoadingStatusLabel.Size = new System.Drawing.Size(24, 17);
            this.CardLoadingStatusLabel.Text = "0/0";
            this.CardLoadingStatusLabel.Visible = false;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.label5.Location = new System.Drawing.Point(12, 33);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(785, 394);
            this.label5.TabIndex = 7;
            this.label5.Text = "vložte kartu";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.Filter = "CM AZP log|*.csv";
            // 
            // pokladnaLockImage
            // 
            this.pokladnaLockImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pokladnaLockImage.Image = global::Card_manager_AZP.Properties.Resources.cashLock;
            this.pokladnaLockImage.Location = new System.Drawing.Point(3, 3);
            this.pokladnaLockImage.Name = "pokladnaLockImage";
            this.pokladnaLockImage.Size = new System.Drawing.Size(24, 24);
            this.pokladnaLockImage.TabIndex = 0;
            this.pokladnaLockImage.TabStop = false;
            // 
            // toolStripDropDownButton3
            // 
            this.toolStripDropDownButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripDropDownButton3.Image = global::Card_manager_AZP.Properties.Resources.advanced;
            this.toolStripDropDownButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton3.Name = "toolStripDropDownButton3";
            this.toolStripDropDownButton3.Size = new System.Drawing.Size(29, 20);
            this.toolStripDropDownButton3.Text = "toolStripDropDownButton3";
            // 
            // oProgramuToolStripMenuItem
            // 
            this.oProgramuToolStripMenuItem.Image = global::Card_manager_AZP.Properties.Resources.info;
            this.oProgramuToolStripMenuItem.Name = "oProgramuToolStripMenuItem";
            this.oProgramuToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.oProgramuToolStripMenuItem.Text = "O programu";
            // 
            // správaTerminálůToolStripMenuItem
            // 
            this.správaTerminálůToolStripMenuItem.Image = global::Card_manager_AZP.Properties.Resources.view_choose;
            this.správaTerminálůToolStripMenuItem.Name = "správaTerminálůToolStripMenuItem";
            this.správaTerminálůToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.správaTerminálůToolStripMenuItem.Text = "Správa terminálů";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 6;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel1.Controls.Add(this.pokladnaLockImage, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.pokladanLabel, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.loginButton, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.login, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.pictureBox1, 3, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(809, 30);
            this.tableLayoutPanel1.TabIndex = 8;
            // 
            // loginButton
            // 
            this.loginButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.loginButton.Location = new System.Drawing.Point(732, 3);
            this.loginButton.Name = "loginButton";
            this.loginButton.Size = new System.Drawing.Size(74, 24);
            this.loginButton.TabIndex = 9;
            this.loginButton.Text = "Přihlásit";
            this.loginButton.UseVisualStyleBackColor = true;
            this.loginButton.Click += new System.EventHandler(this.button1_Click_2);
            // 
            // login
            // 
            this.login.AutoSize = true;
            this.login.Dock = System.Windows.Forms.DockStyle.Fill;
            this.login.Font = new System.Drawing.Font("Arial", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.login.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.login.Location = new System.Drawing.Point(685, 0);
            this.login.Name = "login";
            this.login.Size = new System.Drawing.Size(41, 30);
            this.login.TabIndex = 8;
            this.login.Text = "login";
            this.login.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.pictureBox1.Image = global::Card_manager_AZP.Properties.Resources.FastIcon_Users_lnx_Icons_64X64_user_png_64x64;
            this.pictureBox1.Location = new System.Drawing.Point(655, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(24, 24);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(809, 462);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.label5);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(825, 500);
            this.Name = "MainForm";
            this.Text = "Card Manager AZP 1.1.0";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.newCredit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.debetNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.startCredit)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pokladnaLockImage)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pokladnaLockImage;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton2;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton3;
        private System.Windows.Forms.ToolStripMenuItem systémToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem oProgramuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem správaTerminálůToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton4;
        private System.Windows.Forms.ToolStripMenuItem správaTerminálůToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem oProgramuToolStripMenuItem1;
        public System.Windows.Forms.ToolStripMenuItem systémToolStripMenuItem1;
        public System.Windows.Forms.ToolStripStatusLabel readerToolStripStatusLabel;
        private System.Windows.Forms.ToolStripStatusLabel cardIco;
        public System.Windows.Forms.Label pokladanLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox classComboBox;
        private System.Windows.Forms.DateTimePicker platnostDoTime;
        private System.Windows.Forms.DateTimePicker platnostOdTime;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.NumericUpDown debetNumericUpDown;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DateTimePicker platnostDoDate;
        private System.Windows.Forms.DateTimePicker platnostOdDate;
        private System.Windows.Forms.Button buttonSaveChanges;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button buttonReadRecords;
        private System.Windows.Forms.Button releaseButton;
        private System.Windows.Forms.NumericUpDown newCredit;
        private System.Windows.Forms.Label actualCredit;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBoxNote;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label StartCreditLabel;
        private System.Windows.Forms.NumericUpDown startCredit;
        private System.Windows.Forms.ToolStripMenuItem statsToolStripMenuItem1;
        private System.Windows.Forms.Button deactivationButton;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label newCreditLabel;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ToolStripProgressBar CardLoadingProgressBar;
        private System.Windows.Forms.ToolStripStatusLabel CardLoadingStatusLabel;
        private System.Windows.Forms.Button buttonExportUdalosti;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        public System.Windows.Forms.Label login;
        private System.Windows.Forms.Button loginButton;
    }
}

