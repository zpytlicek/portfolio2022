﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Card_manager_AZP
{
    public partial class CardsForm : Form
    {

        public DataTable gridViewTable1 = new DataTable("vended cards");

        public CardsForm()
        {
            InitializeComponent();

            dataGridView1.Rows.Clear();
            //    nacteniDataGridu();
            //DataRow radek = new DataRow();
            //Nastaveni tabulky s daty
            gridViewTable1.Columns.Add("č.karty");
            gridViewTable1.Columns.Add("vydáno");
            gridViewTable1.Columns.Add("vydal");
            gridViewTable1.Columns.Add("poznámka");
            //dt.Columns.Add("id", System.Type.GetType("System.Int32"));

            //propojeni tabulky dat a gridView
            dataGridView1.DataSource = gridViewTable1;
            dataGridView1.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            dataGridView1.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            dataGridView1.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            dataGridView1.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dataGridView1.Columns[0].ReadOnly = true;
            dataGridView1.Columns[1].ReadOnly = true;
            dataGridView1.Columns[2].ReadOnly = true;
            dataGridView1.Columns[3].ReadOnly = true;
            dataGridView1.Columns[0].SortMode = DataGridViewColumnSortMode.Automatic;
            dataGridView1.Columns[1].SortMode = DataGridViewColumnSortMode.Automatic;
            dataGridView1.Columns[2].SortMode = DataGridViewColumnSortMode.Automatic;
            dataGridView1.Columns[3].SortMode = DataGridViewColumnSortMode.Automatic;

            //nacteni seznamu z databaze
            string connectionString = "Data source=" + MainForm.NastaveniAplikace.MSSQLServer + ";User ID=" + MainForm.NastaveniAplikace.MSSQLLogin + ";Password=" + MainForm.NastaveniAplikace.MSSQLPassword + ";Connect Timeout=1;Database=CMData";
            SqlConnection conn = new SqlConnection(connectionString);
            try
            {
                conn.Open();
                SqlCommand CmdSelect = new SqlCommand(@"SET LANGUAGE BRITISH; SELECT [cardNum],[vendetTime],[userName],[note] FROM [CMData].[dbo].[vendetCardList]", conn);

                SqlDataReader dataReader;
                dataReader = CmdSelect.ExecuteReader();

                DataRow dr;
                if (dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        dr = gridViewTable1.NewRow();
                        dr[0] = dataReader.GetInt32(0).ToString();
                        dr[1] = dataReader.GetDateTime(1).ToString("dd.MM.yyyy HH:mm");
                        dr[2] = dataReader.GetString(2);
                        dr[3] = dataReader.GetString(3);
                        gridViewTable1.Rows.Add(dr);
                    }
                }
                dataReader.Close();
            }
            catch
            {
                MessageBox.Show("Nepodařilo se spojit s databází.","Problém s databází",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
            finally
            {
                conn.Close();
            }
        }

        private void dataGridView1_KeyDown(object sender, KeyEventArgs e)
        {
            //pokud je prilozeni hw klic dojde po stisknuti DEL k odstraneni prislusne karty s DB
            if (e.KeyCode == Keys.Delete)
            {
                if (MainForm.HwKeyExpose == true)
                {
                    //odstraneni vybrane karty s DB
                    string connectionString = "Data source=" + MainForm.NastaveniAplikace.MSSQLServer + ";User ID=" + MainForm.NastaveniAplikace.MSSQLLogin + ";Password=" + MainForm.NastaveniAplikace.MSSQLPassword + ";Connect Timeout=1;Database=CMData";
                    SqlConnection conn = new SqlConnection(connectionString);
                    try
                    {
                        conn.Open();
                        SqlCommand CmdDel = new SqlCommand(@"DELETE FROM [CMData].[dbo].[vendetCardList] WHERE [cardNum] = " + dataGridView1.SelectedRows[0].Cells[0].Value.ToString(), conn);

                        CmdDel.ExecuteNonQuery();
                        gridViewTable1.Rows.RemoveAt(dataGridView1.SelectedRows[0].Index);
                    }
                    catch
                    {
                    }
                    finally
                    {
                        conn.Close();
                    }
                }
            }
        }

    }
}
