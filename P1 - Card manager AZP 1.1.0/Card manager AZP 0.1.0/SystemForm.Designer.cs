﻿namespace Card_manager_AZP
{
    partial class SystemForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SystemForm));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.readerAutomaticCheckBox = new System.Windows.Forms.CheckBox();
            this.readerConectButton = new System.Windows.Forms.Button();
            this.readerLabel = new System.Windows.Forms.Label();
            this.readerSoundCheckBox = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.readerPortComboBox = new System.Windows.Forms.ComboBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.button3 = new System.Windows.Forms.Button();
            this.textBoxSqlPassword = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxSqlLogin = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxSqlServerName = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.buttonUserPassword = new System.Windows.Forms.Button();
            this.buttonUserAdd = new System.Windows.Forms.Button();
            this.buttonUserDel = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxLoginList = new System.Windows.Forms.ComboBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.terminalId = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.terminalId)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.readerAutomaticCheckBox);
            this.groupBox1.Controls.Add(this.readerConectButton);
            this.groupBox1.Controls.Add(this.readerLabel);
            this.groupBox1.Controls.Add(this.readerSoundCheckBox);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.readerPortComboBox);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(751, 51);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Nastavení PC čtečky";
            // 
            // readerAutomaticCheckBox
            // 
            this.readerAutomaticCheckBox.AutoSize = true;
            this.readerAutomaticCheckBox.Location = new System.Drawing.Point(304, 21);
            this.readerAutomaticCheckBox.Name = "readerAutomaticCheckBox";
            this.readerAutomaticCheckBox.Size = new System.Drawing.Size(117, 17);
            this.readerAutomaticCheckBox.TabIndex = 4;
            this.readerAutomaticCheckBox.Text = "připojit automaticky";
            this.readerAutomaticCheckBox.UseVisualStyleBackColor = true;
            this.readerAutomaticCheckBox.CheckedChanged += new System.EventHandler(this.readerPortComboBox_SelectedIndexChanged);
            // 
            // readerConectButton
            // 
            this.readerConectButton.Location = new System.Drawing.Point(158, 17);
            this.readerConectButton.Name = "readerConectButton";
            this.readerConectButton.Size = new System.Drawing.Size(85, 23);
            this.readerConectButton.TabIndex = 4;
            this.readerConectButton.Text = "Připojit";
            this.readerConectButton.UseVisualStyleBackColor = true;
            this.readerConectButton.Click += new System.EventHandler(this.readerConectButton_Click);
            // 
            // readerLabel
            // 
            this.readerLabel.Location = new System.Drawing.Point(427, 22);
            this.readerLabel.Name = "readerLabel";
            this.readerLabel.Size = new System.Drawing.Size(282, 17);
            this.readerLabel.TabIndex = 3;
            this.readerLabel.Text = "Čtečka:";
            // 
            // readerSoundCheckBox
            // 
            this.readerSoundCheckBox.AutoSize = true;
            this.readerSoundCheckBox.Location = new System.Drawing.Point(249, 22);
            this.readerSoundCheckBox.Name = "readerSoundCheckBox";
            this.readerSoundCheckBox.Size = new System.Drawing.Size(49, 17);
            this.readerSoundCheckBox.TabIndex = 3;
            this.readerSoundCheckBox.Text = "zvuk";
            this.readerSoundCheckBox.UseVisualStyleBackColor = true;
            this.readerSoundCheckBox.CheckedChanged += new System.EventHandler(this.readerPortComboBox_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Rozhraní";
            // 
            // readerPortComboBox
            // 
            this.readerPortComboBox.FormattingEnabled = true;
            this.readerPortComboBox.Items.AddRange(new object[] {
            "COM 1",
            "COM 2",
            "COM 3",
            "COM 4",
            "COM 5",
            "COM 6",
            "COM 7",
            "COM 8",
            "COM 9",
            "COM 10",
            "COM 11",
            "COM 12",
            "COM 13"});
            this.readerPortComboBox.Location = new System.Drawing.Point(64, 19);
            this.readerPortComboBox.Name = "readerPortComboBox";
            this.readerPortComboBox.Size = new System.Drawing.Size(88, 21);
            this.readerPortComboBox.TabIndex = 0;
            this.readerPortComboBox.SelectedIndexChanged += new System.EventHandler(this.readerPortComboBox_SelectedIndexChanged);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column6,
            this.Column1});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.Location = new System.Drawing.Point(11, 19);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridView1.Size = new System.Drawing.Size(451, 177);
            this.dataGridView1.TabIndex = 74;
            // 
            // Column6
            // 
            this.Column6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
            this.Column6.HeaderText = "#";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column6.Width = 5;
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column1.HeaderText = "Zobrazený název";
            this.Column1.Name = "Column1";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dataGridView1);
            this.groupBox2.Location = new System.Drawing.Point(295, 69);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(468, 204);
            this.groupBox2.TabIndex = 75;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Názvy skupin";
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(688, 326);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 76;
            this.button1.Text = "Uložit změny";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.button3);
            this.groupBox4.Controls.Add(this.textBoxSqlPassword);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.textBoxSqlLogin);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Controls.Add(this.textBoxSqlServerName);
            this.groupBox4.Location = new System.Drawing.Point(12, 69);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(277, 139);
            this.groupBox4.TabIndex = 79;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Databáze pro ukládání dat";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(122, 102);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(135, 23);
            this.button3.TabIndex = 6;
            this.button3.Text = "Ověřit spojení";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // textBoxSqlPassword
            // 
            this.textBoxSqlPassword.Enabled = false;
            this.textBoxSqlPassword.Location = new System.Drawing.Point(105, 76);
            this.textBoxSqlPassword.Name = "textBoxSqlPassword";
            this.textBoxSqlPassword.PasswordChar = '*';
            this.textBoxSqlPassword.Size = new System.Drawing.Size(152, 20);
            this.textBoxSqlPassword.TabIndex = 5;
            this.textBoxSqlPassword.Text = "lipno";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(11, 79);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(88, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Přístupové heslo";
            // 
            // textBoxSqlLogin
            // 
            this.textBoxSqlLogin.Enabled = false;
            this.textBoxSqlLogin.Location = new System.Drawing.Point(105, 50);
            this.textBoxSqlLogin.Name = "textBoxSqlLogin";
            this.textBoxSqlLogin.Size = new System.Drawing.Size(152, 20);
            this.textBoxSqlLogin.TabIndex = 3;
            this.textBoxSqlLogin.Text = "sa";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 53);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Uživatelské jméno";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Název serveru";
            // 
            // textBoxSqlServerName
            // 
            this.textBoxSqlServerName.Enabled = false;
            this.textBoxSqlServerName.Location = new System.Drawing.Point(105, 24);
            this.textBoxSqlServerName.Name = "textBoxSqlServerName";
            this.textBoxSqlServerName.Size = new System.Drawing.Size(152, 20);
            this.textBoxSqlServerName.TabIndex = 0;
            this.textBoxSqlServerName.Text = "localhost\\DB_Lipno";
            this.textBoxSqlServerName.WordWrap = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.buttonUserPassword);
            this.groupBox3.Controls.Add(this.buttonUserAdd);
            this.groupBox3.Controls.Add(this.buttonUserDel);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.comboBoxLoginList);
            this.groupBox3.Location = new System.Drawing.Point(12, 214);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(277, 86);
            this.groupBox3.TabIndex = 80;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Správa uživatelských účtů";
            // 
            // buttonUserPassword
            // 
            this.buttonUserPassword.Enabled = false;
            this.buttonUserPassword.Location = new System.Drawing.Point(105, 52);
            this.buttonUserPassword.Name = "buttonUserPassword";
            this.buttonUserPassword.Size = new System.Drawing.Size(73, 23);
            this.buttonUserPassword.TabIndex = 4;
            this.buttonUserPassword.Text = "Upravit";
            this.buttonUserPassword.UseVisualStyleBackColor = true;
            this.buttonUserPassword.Click += new System.EventHandler(this.buttonUserPassword_Click);
            // 
            // buttonUserAdd
            // 
            this.buttonUserAdd.Enabled = false;
            this.buttonUserAdd.Location = new System.Drawing.Point(184, 52);
            this.buttonUserAdd.Name = "buttonUserAdd";
            this.buttonUserAdd.Size = new System.Drawing.Size(87, 23);
            this.buttonUserAdd.TabIndex = 3;
            this.buttonUserAdd.Text = "Přidat účet";
            this.buttonUserAdd.UseVisualStyleBackColor = true;
            this.buttonUserAdd.Click += new System.EventHandler(this.buttonUserAdd_Click);
            // 
            // buttonUserDel
            // 
            this.buttonUserDel.Enabled = false;
            this.buttonUserDel.Location = new System.Drawing.Point(31, 52);
            this.buttonUserDel.Name = "buttonUserDel";
            this.buttonUserDel.Size = new System.Drawing.Size(68, 23);
            this.buttonUserDel.TabIndex = 2;
            this.buttonUserDel.Text = "Odstranit";
            this.buttonUserDel.UseVisualStyleBackColor = true;
            this.buttonUserDel.Click += new System.EventHandler(this.buttonUserDel_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Vybraný uživatel";
            // 
            // comboBoxLoginList
            // 
            this.comboBoxLoginList.Enabled = false;
            this.comboBoxLoginList.FormattingEnabled = true;
            this.comboBoxLoginList.Location = new System.Drawing.Point(96, 22);
            this.comboBoxLoginList.Name = "comboBoxLoginList";
            this.comboBoxLoginList.Size = new System.Drawing.Size(175, 21);
            this.comboBoxLoginList.TabIndex = 0;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label6);
            this.groupBox5.Controls.Add(this.terminalId);
            this.groupBox5.Location = new System.Drawing.Point(12, 308);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(277, 42);
            this.groupBox5.TabIndex = 81;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Identifikace aplikace";
            // 
            // terminalId
            // 
            this.terminalId.Enabled = false;
            this.terminalId.Location = new System.Drawing.Point(145, 15);
            this.terminalId.Maximum = new decimal(new int[] {
            124,
            0,
            0,
            0});
            this.terminalId.Name = "terminalId";
            this.terminalId.Size = new System.Drawing.Size(54, 20);
            this.terminalId.TabIndex = 0;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 18);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(133, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "Číslo pokladního terminálu";
            // 
            // SystemForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(775, 357);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SystemForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Základní nastavení systému";
            this.TopMost = true;
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.terminalId)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox readerAutomaticCheckBox;
        private System.Windows.Forms.Button readerConectButton;
        private System.Windows.Forms.Label readerLabel;
        private System.Windows.Forms.CheckBox readerSoundCheckBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox readerPortComboBox;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox textBoxSqlPassword;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxSqlLogin;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxSqlServerName;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button buttonUserPassword;
        private System.Windows.Forms.Button buttonUserAdd;
        private System.Windows.Forms.Button buttonUserDel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBoxLoginList;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown terminalId;
    }
}