﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using MFCardAZP;
using RFID_Reader;
using System.Security.AccessControl;
using System.Security.Cryptography;
using System.Data.SqlClient;

namespace Card_manager_AZP
{
    public partial class MainForm : Form
    {
        //Globalni promenne nastaveni aplikace
        public static AplicationSettings NastaveniAplikace = new AplicationSettings(); //globalni nastaveni aplikace
        public static bool HwKeyExpose = false; //indikator, zda je prilozena karta klíč
        private string MainFormTitle; //titulek hlavniho okna (aktivovano / neaktivovano)
        public static int Pokladna = 0; //aktualni stav pokladny
        public Microsoft.Win32.RegistryKey RegisterKeyForAccount;
        public string DirectoryUser = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\Card_Manager_AZP_User\\";
        public string DirectoryAll = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + "\\Card_Manager_AZP_Common\\";

        public static cTermianlList TerminalList = new cTermianlList();

        //Globalni tridy pro praci s kartou
        public static RFIDReader Ctecka = new RFIDReader(); //HW komunikace se čtečkou
        CardMf1AZPv1 ActualMFCard = new CardMf1AZPv1(ref Ctecka); //funkce pro komunikaci s kartou ve formatu AZP
        TMfCard_Head ActualMFCardHead; //hlavička aktualne přiložene karty
        public static TDetekovanaKarta ActiveCard; //základní informace o přiložené karte (typ, kapacita, uid)
        public int ActualCardCredit = 0; //aktualni stav kreditu na karte
        
        private Thread vlaknoCteniKarty; //Vlakna pro multitasking - interkativni nacitani karty
        private Thread vlaknoNacitaniUdalosti; //Vlakna pro multitasking - nacitani seznamu udalosti

        //Lokalni promene formulare práce s kartou
        private DateTime platnostOdBuff;
        private DateTime platnostDoBuff;

        //seznam skupin a terminalu
        private DateTime DatumAktualnihoNastaveni; //priznak jake datum nastaveni ma aktualni datova sada nastaveni aplikace
        public string[] classNames = new string[7] { "Skupina 1", "Skupina 2", "Skupina 3", "Skupina 4", "Skupina 5", "Skupina 6", "Skupina 7" };

        //aktuálně přihlášený uživatel
        public string UserName = "-"; //nickname aktuálně přihlášeného uživatele
        public int UserId = 0; //id aktuálně přihlášeného uživatele
        public int UserCredit = 0; //pokladna aktuálního uživatele

        //ukazatel na sub okno
        public static Form SubForm = null;

        public MainForm()
        {
            InitializeComponent();
            //ActiveCard.UID = null; //nastaveni nulove karty (bude nastavene po uspesne detekci)

            //overeni existence adresaru aplikace pro uzivatel, pripadne jejich zalozeni
            //adresar pro spolecna data - sets (nastaveni programu), terminals names, users class names, card personalization data
            if (!Directory.Exists(DirectoryAll))
            {
                try
                {
                    Directory.CreateDirectory(DirectoryAll);
                    //pridani prav umoznujicich pristup pro vsechny
                    DirectorySecurity dSecurity = Directory.GetAccessControl(DirectoryAll);
                    dSecurity.AddAccessRule(new FileSystemAccessRule(@"Everyone", FileSystemRights.FullControl, AccessControlType.Allow));
                    Directory.SetAccessControl(DirectoryAll, dSecurity);
                }
                catch (System.UnauthorizedAccessException ex)
                {
                    MessageBox.Show(ex.Message);
                    Close();
                }
                catch
                {
                    Close();
                }
            }

            //adresar pro uzivatelska data - statistiky
            if (!Directory.Exists(DirectoryUser))
            {
                try { Directory.CreateDirectory(DirectoryUser); }
                catch (System.UnauthorizedAccessException ex)
                {
                    MessageBox.Show(ex.Message);
                    Close();
                }
                catch
                {
                    Close();
                }
            }

            //klíč s údaji o pokladně
            RegisterKeyForAccount = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\RAS20ZP\\RAS20ZP");

            //pokus o nacteni souboru s nastavenim Api
            try
            {
                NastaveniAplikace.nacti(DirectoryAll + "apiSets.ser", ref NastaveniAplikace);
            }
            catch (System.Runtime.Serialization.SerializationException)
            {
                //verze osetreni v pripade ze je ulozena jina verze nastaveni
                NastaveniAplikace = new AplicationSettings();
            }
            catch
            {
                NastaveniAplikace = new AplicationSettings();
            }

            MainFormTitle = this.Text;

            //nastaveni stavu poklady a aktivace aplikace dle systemoveho registru

            //pokud neexistuje registr se stavem pokladny a v nastaveni aplikace je nenastaveno, potom se jedná o neaktivovanou aplikaci
            if (RegisterKeyForAccount == null || NastaveniAplikace.aplicationActivated == false)
            {
                this.Text = MainFormTitle + " (neaktivováno)";
            }
            else
            {
                this.Text = MainFormTitle;
            }


            //*NASTAVENI KOMPONENT*//
            //vychozi nastaveni buferu pro nastaveni doby platnosti pro novou kartu po spusteni aplikace - urychleni vydavani
            platnostOdBuff = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 6, 0, 0);
            platnostDoBuff = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 22, 0, 0);
            platnostOdDate.Value = platnostOdBuff;
            platnostOdTime.Value = platnostOdBuff;
            platnostDoDate.Value = platnostDoBuff;
            platnostDoTime.Value = platnostDoBuff;


            //nastaveni vychoziho stavu programu
            cardIco.Visible = false; //viditelnost ikony prilozene karty
            classComboBox.SelectedIndex = 0; //nastaveni vychozi skupiny na 0

            //nastaveni ctecky
            Ctecka.SoundOnOff = NastaveniAplikace.nastaveniZvukuCtecky; //nastaveni zvuku ctecky
            if (NastaveniAplikace.automaticConnect == true) readerConect(); //automaticke pripojeni ctecky

            //nacteni seznamu terminalu
            
            //Vlakno pro automatickou detekci prilozeni karty
            vlaknoCteniKarty = new Thread(new ThreadStart(cteniKarty)); //vlakno ktere slouzi k automaticke detekci vlozeni karty
            vlaknoCteniKarty.IsBackground = true; //nastaveni na pozadi tak aby vlakno skoncilo spolu s aplikaci
            vlaknoCteniKarty.Start();

            //zobrazeni okna pro prihlaseni pokud byla nastavena komunikace s databazí
            login.Text = UserName;
            pokladanLabel.Text = "Stav pokladny : "+ UserCredit.ToString() +" Kč";

            DatumAktualnihoNastaveni = new DateTime(1900, 1, 1, 1, 1, 0); //nastaveni casu posledniho sestaveni nastaveni databaze tak aby doslo k jeho obnoveni dle platneho z DB
            if (NastaveniAplikace.MSSQLLogin != "" && NastaveniAplikace.MSSQLPassword != "" && NastaveniAplikace.MSSQLServer != "")
            {
                button1_Click_2(loginButton, null);
                //pokus o nacteni skupin
                AplicationSettingRefresh();
            }

            //GENEROVANI STATISTIKY
         /*   string connectionString = "Data source=" + MainForm.NastaveniAplikace.MSSQLServer + ";User ID=" + MainForm.NastaveniAplikace.MSSQLLogin + ";Password=" + MainForm.NastaveniAplikace.MSSQLPassword + ";Connect Timeout=1;Database=CMData";
            SqlConnection conn = new SqlConnection(connectionString);
            try
            {
                conn.Open();
                DateTime starDate = DateTime.Now;
                int skup = 0;
                int userId = 1;
                int eventType = 0;
               

                for (int i = 0; i < 5000; i++)
                {
                    skup++;
                    if (skup == 6) skup = 0;
                    userId++;
                    if (userId == 6) userId = 1;
                    eventType++;
                    if (eventType == 3) eventType = 0;
                    starDate = starDate.AddMinutes(2);
                    SqlCommand CmdAddEvent = new SqlCommand("SET LANGUAGE BRITISH; INSERT INTO [CMData].[dbo].[eventsList] ([eventTime],[cardNum],[userClass],[userId],[eventType],[cashChange]) VALUES ('" + starDate + "'," + (1001).ToString() + "," + skup.ToString() + "," + userId.ToString() + "," + eventType.ToString() + "," + (i/10).ToString() + ")", conn);
                    CmdAddEvent.ExecuteNonQuery();
                }
            }
            catch
            {
            }
            finally
            {
                conn.Close();
            }*/
        }

        
        /// <summary>
        /// Aktualizace nastavení apliakce
        /// </summary>
        public void AplicationSettingRefresh(){
            //kontrola zda je treba aktualizovat z DB
            string connectionString = "Data source=" + MainForm.NastaveniAplikace.MSSQLServer + ";User ID=" + MainForm.NastaveniAplikace.MSSQLLogin + ";Password=" + MainForm.NastaveniAplikace.MSSQLPassword + ";Connect Timeout=1;Database=CMData";
            SqlConnection conn = new SqlConnection(connectionString);
            try
            {
                conn.Open();
                SqlCommand CmdSelectApSettingTime = new SqlCommand(@"SET LANGUAGE BRITISH; SELECT [lastApChange] FROM [CMData].[dbo].[lastApChange]", conn);
                DateTime casPosledniZmenyNastaveni = (DateTime)CmdSelectApSettingTime.ExecuteScalar();
                //porovnani casu posledni aktualizace a casu paltneho nastaveni
                if (casPosledniZmenyNastaveni.Ticks != DatumAktualnihoNastaveni.Ticks)
                {
                    //je treba obnovit jednotlivá nastavení
                    //nacteni nazvu skupin z databáze
                    SqlCommand CmdSelectClassNames = new SqlCommand(@"SELECT [id],[className] FROM [CMData].[dbo].[classNames]" , conn);
                        SqlDataReader dataReader;
                        dataReader = CmdSelectClassNames.ExecuteReader();
                        if (dataReader.HasRows)
                        {
                            while (dataReader.Read())
                            {
                                if (dataReader.GetInt32(0) > 0 && dataReader.GetInt32(0) < 8)
                                {
                                    classNames[dataReader.GetInt32(0)-1] = dataReader.GetString(1); //nacteni jmena skupiny pokud je id skupiny od 1 do 7 (pojistka)
                                }
                            }
                        }
                        dataReader.Close();
                        //upload nactenych nicku skupin
                        classComboBox.Items.Clear();
                        for (int i = 0; i < classNames.Length; i++)
                        {
                            classComboBox.Items.Add(classNames[i]);
                        }
                        //classComboBox.SelectedIndex = 0;
                        conn.Close();

                        //nacteni nicku terminalů z databáze
                        TerminalList.load();    

                        DatumAktualnihoNastaveni = casPosledniZmenyNastaveni; //nacteni probehlo v poradku bude ulozeno jako aktualni
                }
                
            }
            catch
            {
                MessageBox.Show("Nepodařilo se navázat spojení s databází. Bez tohoto spojení nelze vydávat karty.", "Chyba spojení s databází", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                conn.Close();
            }

        }

        /// <summary>
        /// Fce na hashovani hesla
        /// </summary>
            public static string PasswordEncryptor(string text)
            {
                MD5 md5 = new MD5CryptoServiceProvider();

                //compute hash from the bytes of text
                md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(text));

                //get hash result after compute it
                byte[] result = md5.Hash;

                StringBuilder strBuilder = new StringBuilder();
                for (int i = 0; i < result.Length; i++)
                {
                    //change it into 2 hexadecimal digits
                    //for each byte
                    strBuilder.Append(result[i].ToString("x2"));
                }

                return strBuilder.ToString();
            }


        

        public void readerConect()
        {
            if (readerToolStripStatusLabel.Text == "Odpojeno")
            {
                //ctecka neni pripojena, dojde k pripojeni
                try
                {
                    Ctecka.Pripojit(NastaveniAplikace.preddefinovanyPort);
                    readerToolStripStatusLabel.Text = "Připojeno";
                    Ctecka.SignalizaceIdle();
                }
                catch (ExChybaKomunikaceSeCteckou ex)
                {
                    MessageBox.Show(ex.Time.ToString() + " " + ex.Message, "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                //ctecka je pripojena, dojde k odpojeni
                try
                {
                    Ctecka.Odpojit();
                    readerToolStripStatusLabel.Text = "Odpojeno";
                }
                catch (ExChybaKomunikaceSeCteckou ex)
                {
                    MessageBox.Show(ex.Time.ToString() + " " + ex.Message, "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
        }

        //promena zamek1 - simbolicky predstavuje zamek v pripade prace s kartou
        public Object zamek1 = new Object();

        public TDetekovanaKarta NewDetectedCard = new TDetekovanaKarta();

        public void cteniKarty()
        {
            while (true)
            {
                if (Ctecka.StavPripojeni == true)
                {
                    lock (zamek1)
                    {
                        try
                        {
                            NewDetectedCard = Ctecka.DetekceKarty();
                        }
                        catch
                        {
                        }
                    }
                }
            }
        }


        private void UklidZadnaKarta()
        {
            switch (ActualMFCardHead.cardType)
            {
                case 63:
                    //uklid po HW klíč
                    pokladnaLockImage.Image = Properties.Resources.cashLock;
                    HwKeyExpose = false;
                    break;
                case 64:
                    //zakaznicka karta
                    UklidZakaznickaKarta();
                    break;
            }
        }

        private void UklidZakaznickaKarta()
        {
            tabControl1.Visible = false; //zobrazeni panelu pro nastaveni karty
            platnostOdDate.Value = platnostOdBuff;
            platnostOdTime.Value = platnostOdBuff;
            platnostDoDate.Value = platnostDoBuff;
            platnostDoTime.Value = platnostDoBuff;
            textBoxNote.Text = "";
            classComboBox.SelectedIndex = 0;
            debetNumericUpDown.Value = 0;
            dataGridView1.Rows.Clear();
            newCredit.Value = 0;
        }

        private void OblsuhaPrilozeniNoveKarty()
        {
            try
            {
                Ctecka.SignalizaceWork();
                ActualMFCardHead = ActualMFCard.ReadCardHeader();
                //MessageBox.Show(ActualMFCardHead.cardType.ToString());
                switch (ActualMFCardHead.cardType)
                {
                    case 63:
                        //HW klíč
                        PrilozeniHwKeyCard();
                        break;

                    case 64:    
                        //zakaznicka karta
                        AplicationSettingRefresh(); //kontrola zda je treba aktualizovat aplikaci
                        if (UserId != 0) //kontrola zda je uzivatel prihlaseny
                        {
                            NacteniZakaznicekKarty();
                        }
                        break;

                    case 34:
                        //Nastavení času
                        ObsluhaKartyNastaveniCasu();
                        break;

                    case 37:
                        //Nastavení univerzální ceny
                        ObsluhaKartyNastaveniCeny();
                        break;
                }

            }
            catch (ExChybaKomunikaceMfCardAzpV1)
            {
                Ctecka.SignalizaceError();
                //MessageBox.Show("Chyba komunikace s kartou.", "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch
            {
                Ctecka.SignalizaceError();
            }
            finally
            {
                Ctecka.SignalizaceIdle();
            }
        }

        private void ObsluhaKartyNastaveniCeny()
        {
            if (SubForm != null) SubForm.Close();
            SubForm = new CreditSetForm();
            SubForm.Owner = this;
            SubForm.Show();
        }

        private void ObsluhaKartyNastaveniCasu()
        {
            if (SubForm != null) SubForm.Close();
            SubForm = new TimeSetForm();
            SubForm.Owner = this;
            SubForm.Show();
        }

        //funkce pro vypočet a zobrazeni kapacity karty
        private void CardCapacityShow(TMfCard_Head header)
        {
            int velikostKarty=0, aktualniPocetZaznamu=0;
            int procento;
            if (ActualMFCardHead.pActual != 0)
            {
                aktualniPocetZaznamu = 1;
                for (int RecPointer = ActualMFCardHead.pStart; RecPointer != ActualMFCardHead.pActual; RecPointer = ActualMFCard.RecordPointerInc(RecPointer, ActualMFCardHead.pMin, ActualMFCardHead.pActual, 1, NewDetectedCard.TypKarty)) aktualniPocetZaznamu++; //card overwrite musi byt nastaveno na 1 jinak by se mohla vratit 0
            }
            for (int RecPointer = ActualMFCardHead.pMin; RecPointer != 0; RecPointer = ActualMFCard.RecordPointerInc(RecPointer, ActualMFCardHead.pMin, ActualMFCardHead.pMin, 0, NewDetectedCard.TypKarty)) velikostKarty++; //card overwrite musi byt nastaveno na 1 jinak by se mohla vratit 0

            //zobrazeni hodnoty
            procento = (aktualniPocetZaznamu * 100) / velikostKarty;
            cardIco.Text = ("(" + procento.ToString() + " % zaplněno)");
            if (procento == 100 && ActualMFCardHead.overWriteData == 0)
            {
                cardIco.ForeColor = Color.Red;
                //v pripade plne karty bez prepisu neni povoleno pridavat zaznam
                newCredit.Enabled = false;
                debetNumericUpDown.Enabled = false;
            }
            else
            {
                cardIco.ForeColor = SystemColors.ControlText;
                //defaultni nastaveni - povoleno menit kredit
                newCredit.Enabled = true;
                debetNumericUpDown.Enabled = true;
            }
        }

        private void NacteniZakaznicekKarty()
        {
            groupBox1.Text = "Základní nastavení karty č." + ActualMFCardHead.cardNum.ToString(); //zobrazení čísla karty
            
            //vychozi nastaveni datumu pro vydani nove karty (kontrola zda jiz byla karta vydana)
            if (ActualMFCardHead.validFrom.Year == 2000 && ActualMFCardHead.validFrom.Month == 1 && ActualMFCardHead.validFrom.Day == 1)
            {
                //karta je prazdna
                platnostOdDate.Value = platnostOdBuff;
                platnostOdTime.Value = platnostOdBuff;
            }
            else
            {
                platnostOdDate.Value = ActualMFCardHead.validFrom;
                platnostOdTime.Value = ActualMFCardHead.validFrom;
            }

            if (ActualMFCardHead.validTo.Year == 2080 && ActualMFCardHead.validTo.Month == 12 && ActualMFCardHead.validTo.Day == 31)
            {
                //karta je prazdna
                platnostDoDate.Value = platnostDoBuff;
                platnostDoTime.Value = platnostDoBuff;
            }
            else
            {
                platnostDoDate.Value = ActualMFCardHead.validTo;
                platnostDoTime.Value = ActualMFCardHead.validTo;
            }

            classComboBox.SelectedIndex = ActualMFCardHead.personClass;
            debetNumericUpDown.Value = ActualMFCardHead.allowDebit;

            dt_recordAzp posledniZaznam = new dt_recordAzp();
            int novyKredit = 0;

            //nacteni posledniho zaznamu
            if (ActualMFCardHead.pActual != 0)
            {
                posledniZaznam = ActualMFCard.RecordRead(ActualMFCardHead.pActual, NastaveniAplikace.usersKey);
            }

            //kontrola korektrnosti posledniho zaznamu -- backup recovery system
            ActualMFCard.CheckBackup(ref ActualMFCardHead, ref posledniZaznam, NastaveniAplikace.usersKey);

            //obsluha nacteni aktualniho zaznamu
            if (ActualMFCardHead.pActual != 0)
            {
                //povoleni prislusnych tlacitek k manipulaci se starou kartou
                classComboBox.Enabled = false; //pridelit kartu do skupiny lez pouze nazacatku

                posledniZaznam = ActualMFCard.RecordRead(ActualMFCardHead.pActual, NastaveniAplikace.usersKey);
                novyKredit = posledniZaznam.credit;
                newCredit.Value = 0;
                ActualCardCredit = posledniZaznam.credit; //globalni promena s hodnotou kreditu pro manipulaci s pokladnou
                actualCredit.Text = novyKredit.ToString() + " Kč";

                releaseButton.Visible = false; //nastaveni polozek a tlacitek pro prijem karty
                deactivationButton.Visible = true;
                buttonReadRecords.Visible = true;
                newCredit.Visible = true;
                newCreditLabel.Visible = true;
                buttonSaveChanges.Visible = true;
                startCredit.Visible = false;
                StartCreditLabel.Visible = false;
                buttonSaveChanges.Enabled = false;
                //nacteni ulozene poznamky 
                    string connectionString = "Data source=" + MainForm.NastaveniAplikace.MSSQLServer + ";User ID=" + MainForm.NastaveniAplikace.MSSQLLogin + ";Password=" + MainForm.NastaveniAplikace.MSSQLPassword + ";Connect Timeout=1;Database=CMData";
                    SqlConnection conn = new SqlConnection(connectionString);
                    try
                    {
                        conn.Open();
                        SqlCommand CmdSelect = new SqlCommand(@"SELECT [note] FROM [CMData].[dbo].[vendetCardList] WHERE [cardNum] = " + ActualMFCardHead.cardNum.ToString(), conn);
                        object scalar = CmdSelect.ExecuteScalar();
                        if (scalar != null) textBoxNote.Text = scalar.ToString();

                    }
                    catch
                    {
                        textBoxNote.Text = "";
                    }
                    finally
                    {
                        conn.Close();
                    }
            }
            else
            {
                classComboBox.Enabled = true;
                startCredit.Visible = true;
                StartCreditLabel.Visible = true;

                newCredit.Value = 0;
                ActualCardCredit = 0; //globalni promena s hodnotou kreditu pro manipulaci s pokladnou
                actualCredit.Text = "nová karta";

                releaseButton.Visible = true; //nastaveni polozek a tlacitek pro vydej karty
                deactivationButton.Visible = false;
                buttonReadRecords.Visible = false;
                newCredit.Visible = false;
                newCreditLabel.Visible = false;
                buttonSaveChanges.Visible = false;
                startCredit.Visible = true;
                StartCreditLabel.Visible = true;
            }
            
            CardCapacityShow(ActualMFCardHead); //zobrazeni informaci o zaplneni karty

            //zneviditelneni tlacitka pro export
            buttonExportUdalosti.Visible = false;
            buttonExportUdalosti.Enabled = false;

            tabControl1.Visible = true; //zobrazeni panelu pro nastaveni karty



            Ctecka.SignalizaceOK();
        }

        /// <summary>
        /// porovnani dvou poly
        /// </summary>
        /// <param name="a1"></param>
        /// <param name="a2"></param>
        /// <returns></returns>
        bool ArraysEqual(byte[] a1, byte[] a2)
        {
            if (a1.Length != a2.Length) return false;
            for (int i = 0; i < a1.Length; i++) if (a1[i] != a2[i]) return false;
            return true;
        }

        /// <summary>
        /// Funkce pro obsluhu prilozeni HW key karty - aktivace aplikace (zalozeni registru pokladny, statistik) + odemceni aplikace pro upravy
        /// </summary>
        private void PrilozeniHwKeyCard()
        {
            HWKeyAZPv1 klic = new HWKeyAZPv1();
            ActualMFCard.HwKeyAZPRead(ref klic);
            bool messageAplicationActivated = false;
            //kontrola stejneho klice api
            if (!(ArraysEqual(NastaveniAplikace.servisKey, klic.CustomerServisKey) && ArraysEqual(NastaveniAplikace.usersKey, klic.CustomerUserKey)))
            {
                //HW klic a Api klic se lisi (reaktivace) - nove nastaveni klicu a jmena majitele licence
                //nastaveni api dle klíče a ulozeni do trvaleho api nastaveni
                for (int i = 0; i < 6; i++)
                {
                    NastaveniAplikace.servisKey[i] = klic.CustomerServisKey[i];
                    NastaveniAplikace.usersKey[i] = klic.CustomerUserKey[i];
                }
                NastaveniAplikace.customerName = new string(klic.CustomerName); //nastaveni jmena majitele licence 
                NastaveniAplikace.aplicationActivated = true; //znaci prvni aktivaci -> prvni nastaveni klicu
                try
                {
                    MainForm.NastaveniAplikace.uloz(DirectoryAll + "apiSets.ser", MainForm.NastaveniAplikace);
                }
                catch
                {
                    MessageBox.Show("Nepodařilo se uložit nastavení aplikace.", "Pozor!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                messageAplicationActivated = true;
            }

            //kontrola zda je aplikace aktivovana, pokud ne kontroluji se klice jestli jsou stejne, pak pouze aktivace aplikace pro urciteho uzivatele
            RegisterKeyForAccount = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\RAS20ZP\\RAS20ZP");
            if (RegisterKeyForAccount == null)
            {
                try
                {
                    RegisterKeyForAccount = Microsoft.Win32.Registry.CurrentUser.CreateSubKey("SOFTWARE\\RAS20ZP\\RAS20ZP");
                    RegisterKeyForAccount.SetValue("vaz", (int)0);
                    Pokladna = 0;
                    RegisterKeyForAccount.Close();
                    
                    pokladanLabel.Text = "Stav pokladny : " + Pokladna.ToString() + " Kč";
                    Ctecka.SignalizaceOK();
                    messageAplicationActivated = true;
                }
                catch
                {
                    MessageBox.Show("Aplikace se nepodařilo aktivovat.", "Pozor!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    Ctecka.SignalizaceError();
                }
            }
            else
            {
                Ctecka.SignalizaceOK();
            }
            if (messageAplicationActivated == true)
            {
                MessageBox.Show("Aplikace byla úspěšně aktivována.", "Aktivace...", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Text = MainFormTitle;
            }

            //odemceni aplikace pro uzivatele
            pokladnaLockImage.Image = Properties.Resources.cashUnlock; //ikonka se zamkem
            HwKeyExpose = true;
        }



        private void systémToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (SubForm != null) SubForm.Close();
            SubForm = new SystemForm();
            SubForm.Owner = this;
            SubForm.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //obsluha vydani nove karty
            //timer1.Enabled = false;
            //programovani karty

            //nastaveni spojeni s databazi
            SqlTransaction myTran = null; //vlakno transakce
            string connectionString = "Data source=" + MainForm.NastaveniAplikace.MSSQLServer + ";User ID=" + MainForm.NastaveniAplikace.MSSQLLogin + ";Password=" + MainForm.NastaveniAplikace.MSSQLPassword + ";Connect Timeout=1;Database=CMData";
            SqlConnection conn = new SqlConnection(connectionString);

            timer1.Enabled = false;
            try
            {
                lock (zamek1) //zamek zabranujici kolizi pri automaticke detekci karty a komunikaci a beznou komunikaci s kartou
                {
                    //zacatek komunikace s databazi
                    conn.Open();
                    myTran = conn.BeginTransaction();

                    Ctecka.SignalizaceWork();
                    TMfCard_Head headBuff = ActualMFCardHead;
                    headBuff.validFrom = new DateTime(platnostOdDate.Value.Year, platnostOdDate.Value.Month, platnostOdDate.Value.Day, platnostOdTime.Value.Hour, platnostOdTime.Value.Minute, 0);
                    headBuff.validTo = new DateTime(platnostDoDate.Value.Year, platnostDoDate.Value.Month, platnostDoDate.Value.Day, platnostDoTime.Value.Hour, platnostDoTime.Value.Minute, 0);
                    headBuff.personClass = (byte)classComboBox.SelectedIndex;
                    headBuff.allowDebit = (int)debetNumericUpDown.Value;
                    dt_recordAzp zaznam = new dt_recordAzp();
                    zaznam.apiId = 0;
                    zaznam.tId = (byte)NastaveniAplikace.terminalId;
                    zaznam.credit = (int)startCredit.Value;
                    zaznam.datum = DateTime.Now;
                    //pripraveni davky pro ulozeni udalosti do DB
                    //ulozeni udalosti o vydani kary
                    SqlCommand CmdAddEvent = new SqlCommand("SET LANGUAGE BRITISH; INSERT INTO [CMData].[dbo].[eventsList] ([eventTime],[cardNum],[userClass],[userId],[eventType],[cashChange]) VALUES ('" + DateTime.Now + "'," + headBuff.cardNum.ToString() + "," + headBuff.personClass.ToString() + "," + UserId + "," + ((byte)t_typUdalosti.distribution).ToString() + "," + startCredit.Value.ToString() + ")", conn);
                    CmdAddEvent.Transaction = myTran;
                    CmdAddEvent.ExecuteNonQuery();
                    //ulozeni vydane karty
                    SqlCommand CmdAddCard = new SqlCommand("SET LANGUAGE BRITISH; INSERT INTO [CMData].[dbo].[vendetCardList] ([cardNum],[vendetTime],[userName],[note]) VALUES (" + headBuff.cardNum.ToString() + ",'" + DateTime.Now + "','" + UserName + "','" + textBoxNote.Text + "')", conn);
                    CmdAddCard.Transaction = myTran;
                    CmdAddCard.ExecuteNonQuery();
                    //aktualizace pokladny
                    SqlCommand CmdUserCreditUpdate = new SqlCommand("UPDATE [CMData].[dbo].[users] SET [cash] = " + (UserCredit + (int)startCredit.Value).ToString() + " WHERE [id] = " + UserId.ToString(), conn);
                    CmdUserCreditUpdate.Transaction = myTran;
                    CmdUserCreditUpdate.ExecuteNonQuery();

                    //provedeni operaci s kartou
                    ActualMFCard.WriteCardHeader(headBuff);
                    ActualMFCardHead = headBuff;
                    if (headBuff.pActual == 0) //nastavení počátečního kreditu
                    {
                        ActualMFCard.RecordAddNew(zaznam, NastaveniAplikace.usersKey);
                    }
                    ActualMFCardHead = ActualMFCard.ReadCardHeader();

                    //prop obnoveni nactenych dat
                    platnostBuffering(); //ulozeni posledni doby platnosti do mezipameti
                    //ulozeni zaznamu o udalosti

                    myTran.Commit(); //transakce bude dokoncena 
                    OblsuhaPrilozeniNoveKarty();
                }
            }
            catch
            {
                try
                {
                    myTran.Rollback();
                }
                catch { }
                Ctecka.SignalizaceError();
                MessageBox.Show("Chyba komunikace s kartou.", "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                buttonSaveChanges.Enabled = false;
                //timer1.Enabled = true;
                dataGridView1.Rows.Clear();
                conn.Close();
                UserInfoRefresh();
                timer1.Enabled = true;
            }
        }

        private void button16_Click(object sender, EventArgs e)
        {
            if (!(vlaknoNacitaniUdalosti == null || vlaknoNacitaniUdalosti.IsAlive == false))
            {
                MessageBox.Show("Během načítání nelze provádět jiné změny.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            //ulozeni zmen nastaveni platnosti karty a debetu
            lock (zamek1) //zamek zabranujici kolizi pri automaticke detekci karty a komunikaci a beznou komunikaci s kartou
            {
                timer1.Enabled = false;

                //nastaveni spojeni s databazi
                SqlTransaction myTran = null; //vlakno transakce
                string connectionString = "Data source=" + MainForm.NastaveniAplikace.MSSQLServer + ";User ID=" + MainForm.NastaveniAplikace.MSSQLLogin + ";Password=" + MainForm.NastaveniAplikace.MSSQLPassword + ";Connect Timeout=1;Database=CMData";
                SqlConnection conn = new SqlConnection(connectionString);

                try
                {
                    //zacatek komunikace s databazi
                    conn.Open();
                    myTran = conn.BeginTransaction();

                    Ctecka.SignalizaceWork();
                    TMfCard_Head headBuff = ActualMFCardHead;
                    headBuff.validFrom = new DateTime(platnostOdDate.Value.Year, platnostOdDate.Value.Month, platnostOdDate.Value.Day, platnostOdTime.Value.Hour, platnostOdTime.Value.Minute, 0);
                    headBuff.validTo = new DateTime(platnostDoDate.Value.Year, platnostDoDate.Value.Month, platnostDoDate.Value.Day, platnostDoTime.Value.Hour, platnostDoTime.Value.Minute, 0);
                    headBuff.allowDebit = (int)debetNumericUpDown.Value;
                    ActualMFCard.WriteCardHeader(headBuff);
                    ActualMFCardHead = headBuff;
                    //prop obnoveni nactenych dat
                    platnostBuffering(); //ulozeni posledni doby platnosti do mezipameti
                    //ulozeni zmeny kreditu
                    if (newCredit.Value != 0)
                    {
                        dt_recordAzp posledniZazanm, novyZaznam;
                        int novyKredit;
                        if (ActualMFCardHead.pActual != 0)
                        {
                            posledniZazanm = ActualMFCard.RecordRead(ActualMFCardHead.pActual, NastaveniAplikace.usersKey);
                            novyKredit = posledniZazanm.credit + (int)newCredit.Value;
                        }
                        else
                        {
                            novyKredit = (int)newCredit.Value;
                        }
                        novyZaznam.apiId = 0;
                        novyZaznam.credit = novyKredit;
                        novyZaznam.tId = (byte)NastaveniAplikace.terminalId;
                        novyZaznam.datum = DateTime.Now;

                        //pripraveni davky pro ulozeni udalosti do DB
                        //ulozeni udalosti o vydani kary
                        SqlCommand CmdAddEvent = new SqlCommand("SET LANGUAGE BRITISH; INSERT INTO [CMData].[dbo].[eventsList] ([eventTime],[cardNum],[userClass],[userId],[eventType],[cashChange]) VALUES ('" + DateTime.Now + "'," + headBuff.cardNum.ToString() + "," + headBuff.personClass.ToString() + "," + UserId + "," + ((byte)t_typUdalosti.cashChange).ToString() + "," + newCredit.Value.ToString() + ")", conn);
                        CmdAddEvent.Transaction = myTran;
                        CmdAddEvent.ExecuteNonQuery();

                        //aktualizace pokladny
                        SqlCommand CmdUserCreditUpdate = new SqlCommand("UPDATE [CMData].[dbo].[users] SET [cash] = " + (UserCredit + (int)newCredit.Value).ToString() + " WHERE [id] = " + UserId.ToString(), conn);
                        CmdUserCreditUpdate.Transaction = myTran;
                        CmdUserCreditUpdate.ExecuteNonQuery();

                        //aktualizace karty
                        ActualMFCard.RecordAddNew(novyZaznam, NastaveniAplikace.usersKey);
                        actualCredit.Text = novyKredit.ToString() + " Kč";
                        ActualMFCardHead = ActualMFCard.ReadCardHeader();

                        newCredit.Value = 0;
                    }
                    //ulozeni vydane karty
                    SqlCommand CmdAddCard = new SqlCommand("SET LANGUAGE BRITISH; UPDATE [CMData].[dbo].[vendetCardList] SET [note] = '" + textBoxNote.Text + "' WHERE [cardNum] = " + headBuff.cardNum.ToString(), conn);
                    CmdAddCard.Transaction = myTran;
                    CmdAddCard.ExecuteNonQuery();
                    myTran.Commit(); //transakce bude dokoncena 

                    OblsuhaPrilozeniNoveKarty();
                    
                }
                catch 
                {
                    try
                    {
                        myTran.Rollback();
                    }
                    catch { }
                    Ctecka.SignalizaceError();
                    MessageBox.Show("Chyba komunikace s kartou.", "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    buttonSaveChanges.Enabled = false;
                    dataGridView1.Rows.Clear();
                    conn.Close();
                    UserInfoRefresh();
                    timer1.Enabled = true;
                }
            }

        }

        /// <summary>
        /// Vlakno pro nacitani udalosti
        /// </summary>
        private void nacitaniUdalosti()
        {
            Action akce = new Action(() => { dataGridView1.Rows.Clear(); });
            //dataGridView1.Rows.Clear();
            this.Invoke(akce);

            TDetekovanaKarta karta;
            lock (zamek1) //zamek zabranujici kolizi pri automaticke detekci karty a komunikaci a beznou komunikaci s kartou
            {
                int pocetTerminaluNaZacatku = TerminalList.TerminalList.Count;
                try
                {
                    Ctecka.SignalizaceWork();
                    karta = Ctecka.DetekceKarty();

                    //ActualMFCard
                    
                    dt_recordAzp zaznam;

                    if (ActualMFCardHead.pActual != 0)
                    {
                        ////Pocitani celkoveho poctu udalosti a zobrazeni statusbaru///
                        int pocetUdalosti = 0, pocetNactenychUdalsoti = 0;
                        for (int RecPointer = ActualMFCardHead.pStart; RecPointer != ActualMFCardHead.pActual; RecPointer = ActualMFCard.RecordPointerInc(RecPointer, ActualMFCardHead.pMin, ActualMFCardHead.pActual, 1, karta.TypKarty))
                        //card overwrite musi byt nastaveno na 1 jinak by se mohla vratit 0
                        {
                            pocetUdalosti++;
                        }

                        if (!(((NewDetectedCard.TypKarty == TRFIDTagy.MifareClass1kB && pocetUdalosti >= 77) || (NewDetectedCard.TypKarty == TRFIDTagy.MifareClass4kB && pocetUdalosti >= 413)) && ActualMFCardHead.overWriteData != 0)) //kontrola zda neni karta plna, v pripade ze ano je zobrazeno o udalost min
                        {
                            pocetUdalosti++;
                        }

                        
                        akce = new Action(() => { CardLoadingStatusLabel.Visible = true; });
                        this.Invoke(akce);
                        akce = new Action(() => { CardLoadingStatusLabel.Text = pocetUdalosti.ToString() + "/0"; });
                        this.Invoke(akce);
                        akce = new Action(() => { CardLoadingProgressBar.Visible = true; });
                        this.Invoke(akce);
                        akce = new Action(() => { CardLoadingProgressBar.Value = 0; });
                        this.Invoke(akce);
                        akce = new Action(() => { statusStrip1.Refresh(); });
                        this.Invoke(akce);


                        int zmena = 0;
                        for (int RecordPointer = ActualMFCardHead.pStart; RecordPointer != ActualMFCardHead.pActual; RecordPointer = ActualMFCard.RecordPointerInc(RecordPointer, ActualMFCardHead.pMin, ActualMFCardHead.pActual, 1, karta.TypKarty))
                        //card overwrite musi byt nastaveno na 1 jinak by se mohla vratit 0
                        {

                            zaznam = ActualMFCard.RecordRead(RecordPointer, NastaveniAplikace.usersKey);

                            if (!(((NewDetectedCard.TypKarty == TRFIDTagy.MifareClass1kB && pocetUdalosti >= 77) || (NewDetectedCard.TypKarty == TRFIDTagy.MifareClass4kB && pocetUdalosti >= 413)) && ActualMFCardHead.overWriteData != 0 && RecordPointer == ActualMFCardHead.pStart)) //kontrola zda neni karta plna, v pripade ze ano a je povoleny prepis tak se nezobrazi prvni udalost
                            { 
                                akce = new Action(() => { dataGridView1.Rows.Add(); }); this.Invoke(akce);
                                akce = new Action(() => { dataGridView1.Rows[dataGridView1.Rows.Count - 1].Cells[0].Value = dataGridView1.Rows.Count; }); this.Invoke(akce);
                                akce = new Action(() => { dataGridView1.Rows[dataGridView1.Rows.Count - 1].Cells[1].Value = zaznam.datum.ToShortDateString() + " " + zaznam.datum.ToShortTimeString(); }); this.Invoke(akce);
                            }

                            //  dataGridView1.Rows[dataGridView1.Rows.Count - 1].Cells[2].Value = zaznam.tId;
                            if (!(((NewDetectedCard.TypKarty == TRFIDTagy.MifareClass1kB && pocetUdalosti >= 77) || (NewDetectedCard.TypKarty == TRFIDTagy.MifareClass4kB && pocetUdalosti >= 413)) && ActualMFCardHead.overWriteData != 0 && RecordPointer == ActualMFCardHead.pStart)) //kontrola zda neni karta plna, v pripade ze ano a je povoleny prepis tak se nezobrazi prvni udalost
                            {
                                akce = new Action(() => { dataGridView1.Rows[dataGridView1.Rows.Count - 1].Cells[2].Value = TerminalList.terminaGetName(zaznam.apiId, zaznam.tId); }); this.Invoke(akce);
                            }
                            zmena = (RecordPointer != ActualMFCardHead.pStart) ? (zaznam.credit - zmena) : zaznam.credit;

                            //  dataGridView1.Rows[dataGridView1.Rows.Count - 1].Cells[2].Value = zaznam.tId;
                            if (!(((NewDetectedCard.TypKarty == TRFIDTagy.MifareClass1kB && pocetUdalosti >= 77) || (NewDetectedCard.TypKarty == TRFIDTagy.MifareClass4kB && pocetUdalosti >= 413)) && ActualMFCardHead.overWriteData != 0 && RecordPointer == ActualMFCardHead.pStart)) //kontrola zda neni karta plna, v pripade ze ano a je povoleny prepis tak se nezobrazi prvni udalost
                            {
                                akce = new Action(() => { dataGridView1.Rows[dataGridView1.Rows.Count - 1].Cells[4].Value = zaznam.credit; }); this.Invoke(akce);
                                akce = new Action(() => { dataGridView1.Rows[dataGridView1.Rows.Count - 1].Cells[3].Value = zmena; }); this.Invoke(akce);
                            }
                            else
                            {
                                pocetNactenychUdalsoti--; //odecteni nezobrazeneho zaznamu
                            }
                            zmena = zaznam.credit;

                            //prubezne zobrazovani stavu nacitani
                            pocetNactenychUdalsoti++;
                            akce = new Action(() => { CardLoadingStatusLabel.Text = pocetUdalosti.ToString() + "/" + pocetNactenychUdalsoti.ToString(); }); this.Invoke(akce);
                            akce = new Action(() => { CardLoadingProgressBar.Value = (int)(((double)pocetNactenychUdalsoti / (double)pocetUdalosti) * 100); }); this.Invoke(akce);
                            akce = new Action(() => { statusStrip1.Refresh(); }); this.Invoke(akce);

                        }
                        //nacteni aktualniho zaznamu

                        zaznam = ActualMFCard.RecordRead(ActualMFCardHead.pActual, NastaveniAplikace.usersKey);

                        akce = new Action(() => { dataGridView1.Rows.Add(); }); this.BeginInvoke(akce);
                        akce = new Action(() => { dataGridView1.Rows[dataGridView1.Rows.Count - 1].Cells[0].Value = dataGridView1.Rows.Count; }); this.Invoke(akce);
                        akce = new Action(() => { dataGridView1.Rows[dataGridView1.Rows.Count - 1].Cells[1].Value = zaznam.datum.ToShortDateString() + " " + zaznam.datum.ToShortTimeString(); }); this.Invoke(akce);

                        //  dataGridView1.Rows[dataGridView1.Rows.Count - 1].Cells[2].Value = zaznam.tId;
                        akce = new Action(() => { dataGridView1.Rows[dataGridView1.Rows.Count - 1].Cells[2].Value = TerminalList.terminaGetName(zaznam.apiId, zaznam.tId ); }); this.Invoke(akce);

                        zmena = zaznam.credit - zmena;
                        akce = new Action(() => { dataGridView1.Rows[dataGridView1.Rows.Count - 1].Cells[3].Value = zmena; }); this.Invoke(akce);
                        akce = new Action(() => { dataGridView1.Rows[dataGridView1.Rows.Count - 1].Cells[4].Value = zaznam.credit; }); this.Invoke(akce);


                        Ctecka.SignalizaceOK();

                        //TTerminalSets sets = new TTerminalSets();
                        //prubezne zobrazovani stavu nacitani
                        pocetNactenychUdalsoti++;
                        akce = new Action(() => { CardLoadingStatusLabel.Text = pocetUdalosti.ToString() + "/" + pocetNactenychUdalsoti.ToString(); }); this.Invoke(akce);
                        akce = new Action(() => { CardLoadingProgressBar.Value = (int)(((double)pocetNactenychUdalsoti / (double)pocetUdalosti) * 100); }); this.Invoke(akce);
                        akce = new Action(() => { statusStrip1.Refresh(); }); this.Invoke(akce);
                    }
                    else
                    {
                        MessageBox.Show("Na kartě nejsou žádné události.", "Pozor!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    akce = new Action(() => { CardLoadingStatusLabel.Visible = false; }); this.Invoke(akce);
                    akce = new Action(() => { CardLoadingProgressBar.Visible = false; }); this.Invoke(akce);
                    akce = new Action(() => { statusStrip1.Refresh(); }); this.Invoke(akce);
                    akce = new Action(() => { buttonExportUdalosti.Visible = true; }); this.Invoke(akce);
                    akce = new Action(() => { buttonExportUdalosti.Enabled = true; }); this.Invoke(akce);
                    
                }
                catch (ExChybaKomunikaceMfCardAzpV1)
                {

                    Ctecka.SignalizaceError();
                    akce = new Action(() => { CardLoadingStatusLabel.Visible = false; }); this.Invoke(akce);
                    akce = new Action(() => { CardLoadingProgressBar.Visible = false; }); this.Invoke(akce);
                    akce = new Action(() => { statusStrip1.Refresh(); }); this.Invoke(akce);

                    MessageBox.Show("Chyba komunikace s kartou.", "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    Ctecka.SignalizaceIdle();
                    
                    //skryti progressbaru se stavem nacitani
                    //akce = new Action(() => { tabControl1.Enabled = true;}); this.Invoke(akce);
                    //akce = new Action(() => { dataGridView1.Update(); }); this.Invoke(akce);

                }
            }

        }


        private void button4_Click(object sender, EventArgs e)
        {
            //tabControl1.Enabled = false;
            if (vlaknoNacitaniUdalosti == null || vlaknoNacitaniUdalosti.IsAlive == false)
            {
                vlaknoNacitaniUdalosti = new Thread(new ThreadStart(nacitaniUdalosti)); //vlakno na nacitani udalosti z karty
                timer1.Enabled = false;
                vlaknoNacitaniUdalosti.IsBackground = true; //nastaveni vlakna na pozadi, tak aby skoncilo spolu s aplikaci
                vlaknoNacitaniUdalosti.Start();
                timer1.Enabled = true;
            }
        }

/*
        private void zmenaKreditu(int value)
        {
            //programovani karty
            try
            {
                Ctecka.SignalizaceWork();
                dt_recordAzp posledniZazanm, novyZaznam;
                int novyKredit;
                if (ActualMFCardHead.pActual != 0)
                {
                    posledniZazanm = ActualMFCard.RecordRead(ActualMFCardHead.pActual, NastaveniAplikace.usersKey);
                    novyKredit = posledniZazanm.credit + value;
                }
                else
                {
                    novyKredit = value;
                }
                novyZaznam.apiId = 0;
                novyZaznam.credit = novyKredit;
                novyZaznam.tId = 0;
                novyZaznam.datum = DateTime.Now;
                ActualMFCard.RecordAddNew(novyZaznam, NastaveniAplikace.usersKey);
                aktualizacePokladny(value);
                actualCredit.Text = novyKredit.ToString() + " Kč";
                newCredit.Value = 0;
                ActualMFCardHead = ActualMFCard.ReadCardHeader();
            //    cardEvent novaUdalost = new cardEvent((int)Program.eventCount + 1, DateTime.Now, ActualMFCardHead.cardNum, ActualMFCardHead.personClass, t_typUdalosti.cashChange, value);
            //    CardEventFile.AddEvent(novaUdalost, DirectoryUser + "cevents.dat");
                Ctecka.SignalizaceOK();
            }
            catch (ArgumentException)
            {
                Ctecka.SignalizaceError();
                MessageBox.Show("Nesprávný formát vstupních dat.", "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (FormatException)
            {
                Ctecka.SignalizaceError();
                MessageBox.Show("Nesprávný formát vstupních dat.", "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (ExChybaKomunikaceMfCardAzpV1)
            {
                Ctecka.SignalizaceError();
                MessageBox.Show("Chyba komunikace s kartou.", "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
*/

        /// <summary>
        /// Ukladani posledniho nastaveni casu do mezipameti
        /// </summary>
        public void platnostBuffering()
        {
            platnostOdBuff = new DateTime(platnostOdDate.Value.Year, platnostOdDate.Value.Month, platnostOdDate.Value.Day, platnostOdTime.Value.Hour, 0, 0);
            platnostDoBuff = new DateTime(platnostDoDate.Value.Year, platnostDoDate.Value.Month, platnostDoDate.Value.Day, platnostDoTime.Value.Hour, 0, 0);
        }



        private void správaTerminálůToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (SubForm != null) SubForm.Close();
            SubForm = new TerminalListForm();
            SubForm.Owner = this;
            SubForm.Show();
        }

        private void oProgramuToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (SubForm != null) SubForm.Close();
            SubForm = new AboutForm();
            SubForm.Owner = this;
            SubForm.Show();
        }

        private void zmenaDatNastaveniKarty(object sender, EventArgs e)
        {
            if (actualCredit.Text != "nová karta")
            {
                buttonSaveChanges.Enabled = true;
            }
        }


        private void statsToolStripMenuItem1_Click(object sender, EventArgs e)
        {
                if (SubForm != null) SubForm.Close();
                SubForm = new StatsForm();
                SubForm.Owner = this;
                if (SubForm.IsDisposed != true) SubForm.Show();
                
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            if (!(vlaknoNacitaniUdalosti == null || vlaknoNacitaniUdalosti.IsAlive == false))
            {
                MessageBox.Show("Během načítání nelze provádět jiné změny.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
                //obsluha příjmu již vydané karty
                //nastaveni spojeni s databazi
                SqlTransaction myTran = null; //vlakno transakce
                string connectionString = "Data source=" + MainForm.NastaveniAplikace.MSSQLServer + ";User ID=" + MainForm.NastaveniAplikace.MSSQLLogin + ";Password=" + MainForm.NastaveniAplikace.MSSQLPassword + ";Connect Timeout=1;Database=CMData";
                SqlConnection conn = new SqlConnection(connectionString);

                try
                {
                    lock (zamek1) //zamek zabranujici kolizi pri automaticke detekci karty a komunikaci a beznou komunikaci s kartou
                    {
                        conn.Open();
                        myTran = conn.BeginTransaction();

                        timer1.Enabled = false;
                        Ctecka.SignalizaceWork();
                        //pripraveni davky pro ulozeni udalosti do DB
                        //ulozeni udalosti o vydani kary
                        SqlCommand CmdAddEvent = new SqlCommand("SET LANGUAGE BRITISH; INSERT INTO [CMData].[dbo].[eventsList] ([eventTime],[cardNum],[userClass],[userId],[eventType],[cashChange]) VALUES ('" + DateTime.Now + "'," + ActualMFCardHead.cardNum.ToString() + "," + ActualMFCardHead.personClass.ToString() + "," + UserId + "," + ((byte)t_typUdalosti.withdrawal).ToString() + "," + (-ActualCardCredit).ToString() + ")", conn);
                        CmdAddEvent.Transaction = myTran;
                        CmdAddEvent.ExecuteNonQuery();
                        //ulozeni vydane karty
                        SqlCommand CmdDelCard = new SqlCommand("DELETE FROM [CMData].[dbo].[vendetCardList] WHERE [cardNum] = " + ActualMFCardHead.cardNum.ToString(), conn);
                        CmdDelCard.Transaction = myTran;
                        CmdDelCard.ExecuteNonQuery();

                        //Vymazani karty
                        ActualMFCardHead.cardNum = 0; //pokud je cardNum = 0 - nemeni se cislo karty
                        ActualMFCardHead.allowDebit = 0;
                        //ActualMFCardHead.overWriteData = ActualMFCardHead.overWriteData;
                        ActualMFCardHead.personClass = 0;
                        //ActualMFCardHead.pMin = ActualMFCardHead.pMin;
                        ActualMFCardHead.pStart = ActualMFCardHead.pMin;
                        ActualMFCardHead.pActual = 0;
                        DateTime datumOd = new DateTime(2000, 1, 1, 12, 0, 0);
                        ActualMFCardHead.validFrom = datumOd;
                        DateTime datumDo = new DateTime(2080, 12, 31, 12, 0, 0);
                        ActualMFCardHead.validTo = datumDo;
                        ActualMFCard.WriteCardHeader(ActualMFCardHead);
                        //deaktivace antipassbacku
                        Ctecka.MifareClassicAuthentication(8, NastaveniAplikace.usersKey);
                        Ctecka.MifareClassicWrite(8, new byte[16] { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 });

                        //aktualizace pokladny
                        SqlCommand CmdUserCreditUpdate = new SqlCommand("UPDATE [CMData].[dbo].[users] SET [cash] = " + (UserCredit - ActualCardCredit).ToString() + " WHERE [id] = " + UserId.ToString(), conn);
                        CmdUserCreditUpdate.Transaction = myTran;
                        CmdUserCreditUpdate.ExecuteNonQuery();

                        //ulozeni zaznamu o udalosti
                        myTran.Commit(); //transakce bude dokoncena 
                        UklidZakaznickaKarta();
                        OblsuhaPrilozeniNoveKarty();
                    }
                }
                catch 
                {
                    try
                    {
                        myTran.Rollback();
                    }
                    catch { }
                    Ctecka.SignalizaceError();
                    MessageBox.Show("Chyba komunikace s kartou.", "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    dataGridView1.Rows.Clear();
                    Ctecka.SignalizaceIdle();
                    //timer1.Enabled = true;
                    conn.Close();
                    UserInfoRefresh();
                    timer1.Enabled = true;
                }
        }


        static bool PorovnaniPole(byte[] a1, byte[] a2) { if (a1 == a2) return true; if (a1 == null || a2 == null) return false; if (a1.Length != a2.Length) return false; for (int i = 0; i < a1.Length; i++) { if (a1[i] != a2[i]) return false; } return true; }

        private void timer1_Tick(object sender, EventArgs e)
        {

            if ((NewDetectedCard.TypKarty == TRFIDTagy.MifareClass1kB || NewDetectedCard.TypKarty == TRFIDTagy.MifareClass4kB))
            {
                //byl detekovan typ podporovane karty -> dalsi zpracovani
                cardIco.Visible = true; //zobrazeni ikonky o pritomnosti karty
                if (!PorovnaniPole(NewDetectedCard.UID, ActiveCard.UID)) //kontroluje zda se jedna o nove prilozenou kartu / pokud je karta stale stejna -> nedochazi k dalsi detekci
                {
                    ActiveCard.UID = NewDetectedCard.UID;
                    lock (zamek1) //zamek zabranujici kolizi pri automaticke detekci karty a komunikaci a beznou komunikaci s kartou
                    {
                        OblsuhaPrilozeniNoveKarty();
                    }
                }

            }
            else
            {
                //karta neni podporovana -> uklid pokud neni prilozena karta
                cardIco.Visible = false; //skryti ikonky o pritomnosti karty
                cardIco.Text = ""; //vymazani udaje o zaplneni karty
                ActiveCard.UID = null; //odstraneni id aktualni karty
                UklidZadnaKarta();
            }


        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (vlaknoNacitaniUdalosti != null)
            {
                vlaknoNacitaniUdalosti.Abort(); //nasilne ukonceni nacitani udalosti, ostatni vlakna se ukoncuji autoamticky
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
           
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
                if (SubForm != null) SubForm.Close();
                SubForm = new CardsForm();
                SubForm.Owner = this;
                SubForm.Show();
        }

        public static string GetMD5Hash(string input)
        {
            System.Security.Cryptography.MD5CryptoServiceProvider x = new System.Security.Cryptography.MD5CryptoServiceProvider();
            byte[] bs = System.Text.Encoding.UTF8.GetBytes(input);
            bs = x.ComputeHash(bs);
            System.Text.StringBuilder s = new System.Text.StringBuilder();
            foreach (byte b in bs)
            {
                s.Append(b.ToString("x2").ToLower());
            }
            string password = s.ToString();
            return password;
        }

        private void buttonExportUdalosti_Click(object sender, EventArgs e)
        {
            saveFileDialog1.FileName = DateTime.Now.ToString("yyyy_MM_dd_HH_mm") + " výpis karty č." + ActualMFCardHead.cardNum.ToString() + ".csv";
            if (DialogResult.OK == saveFileDialog1.ShowDialog())
            {
                //uloženi datagriedview1 do formatu CSV
                FileStream fs = null;
                StreamWriter sw = null;

                try
                {
                    fs = new FileStream(saveFileDialog1.FileName, FileMode.Create);
                    sw = new StreamWriter(fs, Encoding.Default);
                    sw.WriteLine("Detailní výpis transakcí na kartě č." + ActualMFCardHead.cardNum.ToString());
                    sw.WriteLine("Datum a čas vystavení: " + DateTime.Now.ToString("dd.MM.yyyy HH:mm"));
                    sw.WriteLine("Skupina: " + classComboBox.SelectedItem.ToString());
                    sw.WriteLine("Zůstatek: " + actualCredit.Text);
                    sw.WriteLine("Povolený debet: " + debetNumericUpDown.Value.ToString() + " Kč");
                    sw.WriteLine("Platnost karty: od " + ActualMFCardHead.validFrom.ToString("dd.MM.yyyy HH:mm") + " do "+ ActualMFCardHead.validTo.ToString("dd.MM.yyyy HH:mm"));
                    sw.WriteLine("");
                    sw.WriteLine("");
                    sw.WriteLine("p.č.události; čas události; terminál; změna kreditu; konečný stav");
                    for (int i = 0; i < dataGridView1.RowCount; i++)
                    {
                        sw.WriteLine((i+1).ToString()+";"+ dataGridView1.Rows[i].Cells[1].Value.ToString() +";"+ dataGridView1.Rows[i].Cells[2].Value.ToString()+";"+ dataGridView1.Rows[i].Cells[3].Value.ToString()+";"+ dataGridView1.Rows[i].Cells[4].Value.ToString());
                    }


                    MessageBox.Show("Export dat proběhl úspěšně.", "Export dat", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch
                {
                    MessageBox.Show("Export dat se nezdařil.", "Export dat", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    if (sw != null) sw.Close();
                    if (fs != null) fs.Close();
                }
            }
        }

        private void button1_Click_2(object sender, EventArgs e)
        {
            if (((Button)sender).Text == "Odhlásit")
            {
                //udhlášení aktuálního uživatele
                UserId = 0;
                UserInfoRefresh();
                UklidZakaznickaKarta();
            }
            else
            {
                if (SubForm != null) SubForm.Close();
                SubForm = new LoginForm();
                SubForm.Owner = this;
                SubForm.Show();
            }
        }

        /// <summary>
        /// Načtení informací o konkrétním uživateli a ověření komunikace s DB
        /// </summary>
        public void UserInfoRefresh()
        {
            if (UserId != 0)
            {
                string connectionString = "Data source=" + MainForm.NastaveniAplikace.MSSQLServer + ";User ID=" + MainForm.NastaveniAplikace.MSSQLLogin + ";Password=" + MainForm.NastaveniAplikace.MSSQLPassword + ";Connect Timeout=1;Database=CMData";
                SqlConnection conn = new SqlConnection(connectionString);
                try
                {
                    conn.Open();
                    //kontrola zda toto uzivatelske jmeno existuje
                    SqlCommand CmdSelectUser = new SqlCommand(@"SELECT [nickname],[password],[cash] FROM [CMData].[dbo].[users] WHERE [id] = " + UserId.ToString(), conn);

                    SqlDataReader dataReader;
                    dataReader = CmdSelectUser.ExecuteReader();
                    if (dataReader.HasRows)
                    {
                        dataReader.Read();
                        //overeni hesla
                        UserName = dataReader.GetString(0);
                        UserCredit = dataReader.GetInt32(2);
                    }
                    else
                    {
                        UserId = 0;
                        UserName = "-";
                        UserCredit = 0;
                    }
                    dataReader.Close();
                    //aktualizace aktualniho casu posledni aktivity uzivatele
                    SqlCommand CmdUpdateActivityTime = new SqlCommand("SET LANGUAGE BRITISH; UPDATE [CMData].[dbo].[users] SET [lastActivity] = '" + DateTime.Now + "'", conn);
                    CmdUpdateActivityTime.ExecuteNonQuery();
                }
                catch
                {
                    UserId = 0;
                    UserName = "-";
                    UserCredit = 0;
                }
                finally
                {
                    conn.Close();
                }
            }
            else
            {
                UserName = "-";
                UserCredit = 0;
            }
            loginButton.Text = (UserId == 0) ? "Přihlásit" : "Odhlásit";
            login.Text = UserName;
            pokladanLabel.Text = "Stav pokladny : " + UserCredit.ToString() + " Kč";
        }

    }
}
