﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections;
using System.Threading;
using System.Xml.Serialization;
using System.Xml;
using System.Security.AccessControl;
using System.Data.SqlClient;

namespace Card_manager_AZP
{

    //vyčet možných měn
    [Serializable]
    public enum mena{EUR, CZK};

    /// <summary>
    /// Seznam terminalu
    /// </summary>
    public class cTermianlList
    {
        /// <summary>
        /// Polozka nazvu terminalů
        /// </summary>
        public class tterminalItem
        {
            public tterminalItem(int apilikacniID, int terminalId, string terminalNick)
            {
                ApilikacniID = apilikacniID;
                TerminalId = terminalId;
                TerminalNick = terminalNick;
            }
            public int ApilikacniID = 0;
            public int TerminalId = 0;
            public string TerminalNick = "";
        }

        /// <summary>
        /// vraceni nazvu terminalu
        /// </summary>
        /// <param name="apiId"></param>
        /// <param name="tId"></param>
        public string terminaGetName(int apiId, int tId){
            for (int i = 0; i < TerminalList.Count; i++ )
            {
                if (apiId == TerminalList[i].ApilikacniID && tId == TerminalList[i].TerminalId) return TerminalList[i].TerminalNick;
            }
            newTerminalAdd(apiId,tId);
            return ("neznámý terminál ap" + apiId.ToString() + ", tId" + tId.ToString());
        }

        //!!!!!!!!!! Musi se rucne nacist v nacitani ulozeneho souboru !!!!!!!!!
        public List<tterminalItem> TerminalList = new List<tterminalItem>(); //seznam terminalu

        

        /// <summary>
        /// Pridani noveho terminalu do DB a seznamu
        /// </summary>
        public void newTerminalAdd(int apiId, int tId)
        {
            TerminalList.Add(new tterminalItem(apiId, tId, "neznámý terminál ap" + apiId.ToString() + ", tId" + tId.ToString()));
            //vlozeni terminalu do DB pomoci transakce
            SqlTransaction myTran = null; //vlakno transakce
            string connectionString = "Data source=" + MainForm.NastaveniAplikace.MSSQLServer + ";User ID=" + MainForm.NastaveniAplikace.MSSQLLogin + ";Password=" + MainForm.NastaveniAplikace.MSSQLPassword + ";Connect Timeout=1;Database=CMData";
            SqlConnection conn = new SqlConnection(connectionString);
            try
            {
                conn.Open();
                myTran = conn.BeginTransaction();
                
                //zjisteni existence terminalu v seznamu
                SqlCommand CmdConfirm = new SqlCommand("SELECT [terminalNick] FROM [CMData].[dbo].[terminalNames] WHERE [aplikacniId] = "+apiId.ToString()+" AND [terminalId] = "+tId.ToString(), conn);
                CmdConfirm.Transaction = myTran;
                SqlDataReader dataReader;
                dataReader = CmdConfirm.ExecuteReader();
                if (!dataReader.HasRows)
                {
                    dataReader.Close();
                    //terminal v seznamu neni, bude vlozen jako neznamý terminál
                    SqlCommand CmdAddARow = new SqlCommand("INSERT INTO [CMData].[dbo].[terminalNames] ([aplikacniId],[terminalId],[terminalNick]) VALUES (" + apiId.ToString() + "," + tId.ToString() + ",'" + "neznámý terminál ap" + apiId.ToString() + ", tId" + tId.ToString() + "')", conn);
                    CmdAddARow.Transaction = myTran;
                    CmdAddARow.ExecuteNonQuery();
                    //nastaveni casu posledni aktualizace nastaveni aplikace
                    SqlCommand CmdChangeTime = new SqlCommand(@"SET LANGUAGE BRITISH; UPDATE [CMData].[dbo].[lastApChange] SET [lastApChange] = '" + DateTime.Now + "'", conn);
                    CmdChangeTime.Transaction = myTran;
                    CmdChangeTime.ExecuteNonQuery();
                }
                myTran.Commit(); //transakce bude dokoncena
            }
            catch
            {
                try
                {
                    myTran.Rollback();
                }
                catch { }
               // MessageBox.Show("Během ukládání transakce do databáze došlo k chybě.", "Chyba spojení s databází.", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                conn.Close();
            }
        }

        public void save(DataGridView upravenySeznam)
        {
            load(); //aktualizace seznamu terminalu z databaze pred ulozenim zmen
            SqlTransaction myTran = null; //vlakno transakce
            string connectionString = "Data source=" + MainForm.NastaveniAplikace.MSSQLServer + ";User ID=" + MainForm.NastaveniAplikace.MSSQLLogin + ";Password=" + MainForm.NastaveniAplikace.MSSQLPassword + ";Connect Timeout=1;Database=CMData";
            SqlConnection conn = new SqlConnection(connectionString);
            try
            {
                conn.Open();
                myTran = conn.BeginTransaction();

                bool byloZmeneno = false;

                for (int i = 0; i < TerminalList.Count; i++)
                {
                    //projiti stavajiciho seznamu a jeho uprava na zaklade najitych zmen dle upraveneho datagridu
                    bool nalezeno = false;

                    for (int x = 0; x < upravenySeznam.Rows.Count; x++)
                    {
                        if (int.Parse(upravenySeznam.Rows[x].Cells[0].Value.ToString()) == TerminalList[i].ApilikacniID && int.Parse(upravenySeznam.Rows[x].Cells[1].Value.ToString()) == TerminalList[i].TerminalId)
                        {
                            if (upravenySeznam.Rows[x].Cells[2].Value.ToString() != TerminalList[i].TerminalNick)
                            {
                                //pokud je popis terminalu jiny bude uploadovan do DB
                                SqlCommand CmdUpdate = new SqlCommand("UPDATE [CMData].[dbo].[terminalNames] SET [terminalNick] = '" + upravenySeznam.Rows[x].Cells[2].Value.ToString() + "' WHERE [aplikacniId] = " + upravenySeznam.Rows[x].Cells[0].Value.ToString() + " AND [terminalId] = " + upravenySeznam.Rows[x].Cells[1].Value.ToString(), conn);
                                CmdUpdate.Transaction = myTran;
                                CmdUpdate.ExecuteNonQuery();
                                byloZmeneno = true;
                            }
                            nalezeno = true;
                            x = upravenySeznam.Rows.Count;
                        }
                    }
                    if (nalezeno == false){
                        //tento terminal nebyl nalezen, proto bude ze seznamu snazan
                        SqlCommand CmdDelet = new SqlCommand("DELETE FROM [CMData].[dbo].[terminalNames] WHERE [aplikacniId] = " + TerminalList[i].ApilikacniID.ToString() + " AND [terminalId] = " + TerminalList[i].TerminalId.ToString(), conn);
                        CmdDelet.Transaction = myTran;
                        CmdDelet.ExecuteNonQuery();
                        byloZmeneno = true;
                    }

                }

                if (byloZmeneno == true)
                {
                    //pokud bylo neco zmeneno tak se nastavi cas posledni zmeny nastaveni aplikace
                    //nastaveni casu posledni aktualizace nastaveni aplikace
                    SqlCommand CmdChangeTime = new SqlCommand(@"SET LANGUAGE BRITISH; UPDATE [CMData].[dbo].[lastApChange] SET [lastApChange] = '" + DateTime.Now + "'", conn);
                    CmdChangeTime.Transaction = myTran;
                    CmdChangeTime.ExecuteNonQuery();
                }

                myTran.Commit(); //transakce bude dokoncena    
            }
            catch
            {
                try
                {
                    myTran.Rollback();
                }
                catch { }
               // MessageBox.Show("Během ukládání transakce do databáze došlo k chybě.", "Chyba spojení s databází.", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                conn.Close();
            }
            load(); //aktualizace stavajiciho seznamu terminalu
        }

        public void load()
        {
            //nacteni seznamu uzivatelů z databáze
            string connectionString = "Data source=" + MainForm.NastaveniAplikace.MSSQLServer + ";User ID=" + MainForm.NastaveniAplikace.MSSQLLogin + ";Password=" + MainForm.NastaveniAplikace.MSSQLPassword + ";Connect Timeout=1;Database=CMData";
            SqlConnection conn = new SqlConnection(connectionString);
            try
            {
                conn.Open();
                SqlCommand CmdSelect = new SqlCommand(@"SELECT [aplikacniId],[terminalId],[terminalNick] FROM [CMData].[dbo].[terminalNames]", conn);
                
                SqlDataReader dataReader;
                dataReader = CmdSelect.ExecuteReader();

                TerminalList.Clear(); //vymazani stavajiciho seznamu
                if (dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        TerminalList.Add(new tterminalItem(dataReader.GetInt32(0), dataReader.GetInt32(1), dataReader.GetString(2)));
                    }
                }
                dataReader.Close();
            }
            catch
            {
                //   MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }


    }


    [Serializable]
    public class AplicationSettings
    {
        //nastaveni spojeni pro MS SQL databazi
        public string MSSQLLogin = "";
        public string MSSQLPassword = "";
        public string MSSQLServer = "";
        public int terminalId = 0; //id pro identifikaci pokladni aplikace
        public mena menaAplikace = mena.CZK;
        public int zakladniJednotkyMeny = 1; //pocet desetinych mist 1 = 1, 10 = 0,1, 100 = 0,01
        public int preddefinovanyPort = 1;
        public bool nastaveniZvukuCtecky = false;
        public bool automaticConnect = false;
        public bool aplicationActivated = false;
        //defaultni nastaveni klicu (po zmene uz nelze zmenit) -- pouziva se pro identifikaci neaktivovane aplikace
        public byte[] usersKey = new byte[6] {0xff, 0xff, 0xff, 0xff, 0xff, 0xff};
        public byte[] servisKey = new byte[6] { 0xff, 0xff, 0xff, 0xff, 0xff, 0xff };
        public string customerName = "";

        public void uloz(string soubor, AplicationSettings nastaveni)
        {
            FileStream fs = null;
            try
            {
                fs = File.Create(soubor); //vytvori nebo prepise soubor
                //pridani prav
                FileSecurity fSecurity = File.GetAccessControl(soubor);
                fSecurity.AddAccessRule(new FileSystemAccessRule(@"Everyone", FileSystemRights.FullControl, AccessControlType.Allow));
                File.SetAccessControl(soubor, fSecurity);
                //zapis binarnich dat
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(fs, nastaveni);

            }
            finally
            {
                if (fs != null) fs.Close();
            }
        }

        public void nacti(string soubor, ref AplicationSettings nastaveni)
        {
            FileStream fs = null;
            try
            {
                fs = File.Open(soubor, FileMode.Open);
                BinaryFormatter bf = new BinaryFormatter();
                nastaveni = (AplicationSettings)bf.Deserialize(fs);
            }
            finally
            {
                if (fs != null) fs.Close();
            }
        }
    }

    static class Program
    {
        public static long eventCount = 0;

        public static MainForm hlavniOkno;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            hlavniOkno = new MainForm();
            Application.Run(hlavniOkno);
            
            
        }
    }
}
