﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using RFID_Reader;
using System.Data.SqlClient;

namespace Card_manager_AZP
{
    public partial class SystemForm : Form
    {
        bool ApiInitiationComplete = false;


        public SystemForm()
        {
            InitializeComponent();

            //vychozi nastaveni polozek pro pripojeni ctecky
            readerPortComboBox.SelectedIndex = MainForm.NastaveniAplikace.preddefinovanyPort - 1;
            readerConectButton.Text = (Program.hlavniOkno.readerToolStripStatusLabel.Text == "Odpojeno") ? "Připojit" : "Odpojit";
            readerSoundCheckBox.Checked = (MainForm.NastaveniAplikace.nastaveniZvukuCtecky == true) ? true : false;
            readerAutomaticCheckBox.Checked = (MainForm.NastaveniAplikace.automaticConnect == true) ? true : false;
            if (Program.hlavniOkno.readerToolStripStatusLabel.Text == "Odpojeno")
            {
                readerLabel.Text = "Typ: nepřipojeno";
            }
            else
            {
                readerLabel.Text = "Typ: " + MainForm.Ctecka.TypCtecky + " (ver.DLL " + MainForm.Ctecka.VerzeDll + ")";
            }

            readerConectButton.Text = (Program.hlavniOkno.readerToolStripStatusLabel.Text == "Odpojeno") ? "Připojit" : "Odpojit";

            ApiInitiationComplete = true;
            if (MainForm.HwKeyExpose == true){
                buttonUserAdd.Enabled = true;
                buttonUserDel.Enabled = true;
                buttonUserPassword.Enabled = true;
                comboBoxLoginList.Enabled = true;
                textBoxSqlLogin.Enabled = true;
                textBoxSqlPassword.Enabled = true;
                textBoxSqlServerName.Enabled = true;
                terminalId.Enabled = true;
            }
            terminalId.Value = MainForm.NastaveniAplikace.terminalId;
            textBoxSqlLogin.Text = MainForm.NastaveniAplikace.MSSQLLogin;
            textBoxSqlPassword.Text = MainForm.NastaveniAplikace.MSSQLPassword;
            textBoxSqlServerName.Text = MainForm.NastaveniAplikace.MSSQLServer;


            //vychozi nacteni uzivatelskych jmen
            if (MainForm.NastaveniAplikace.MSSQLLogin != "" && MainForm.NastaveniAplikace.MSSQLPassword != "" && MainForm.NastaveniAplikace.MSSQLServer != "")
            {
                //test spojeni s DB
                string connectionString = "Data source=" + MainForm.NastaveniAplikace.MSSQLServer + ";User ID=" + MainForm.NastaveniAplikace.MSSQLLogin + ";Password=" + MainForm.NastaveniAplikace.MSSQLPassword + ";Connect Timeout=1;Database=CMData";
                SqlConnection conn = new SqlConnection(connectionString);
                try
                {
                    conn.Open();
                    conn.Close();
                    UlozeniNastaveniDBaNacteniLoginu();
                    NacteniAktualnichNazvuSkupin();
                }
                catch 
                {
                    dataGridView1.Enabled = false;
                    MessageBox.Show("Nepodařilo se spojit s databázovým serverem.");
                }
                finally
                {
                    conn.Close();                    
                }
            }
            
        }

        private void readerConectButton_Click(object sender, EventArgs e)
        {
            if (readerConectButton.Text == "Připojit")
            {
                //ctecka neni pripojena, dojde k pripojeni
                try
                {
                    MainForm.Ctecka.Pripojit(MainForm.NastaveniAplikace.preddefinovanyPort);
                    readerConectButton.Text = "Odpojit";
                    Program.hlavniOkno.readerToolStripStatusLabel.Text = "Připojeno";
                    MainForm.Ctecka.SignalizaceIdle();
                    readerLabel.Text = "Typ: " + MainForm.Ctecka.TypCtecky + " (ver.DLL " + MainForm.Ctecka.VerzeDll + ")";
                }
                catch (ExChybaKomunikaceSeCteckou ex)
                {
                    MessageBox.Show(ex.Time.ToString() + " " + ex.Message, "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                //ctecka je pripojena, dojde k odpojeni
                try
                {
                    MainForm.Ctecka.Odpojit();
                    readerConectButton.Text = "Připojit";
                    Program.hlavniOkno.readerToolStripStatusLabel.Text = "Odpojeno";
                    readerLabel.Text = "Typ: nepřipojeno";
                }
                catch (ExChybaKomunikaceSeCteckou ex)
                {
                    MessageBox.Show(ex.Time.ToString() + " " + ex.Message, "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
           

        }

        private void readerPortComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ApiInitiationComplete != false)
            {
                //pokus o ulozeni souboru s nastavenim Api
                try
                {
                    MainForm.NastaveniAplikace.preddefinovanyPort = readerPortComboBox.SelectedIndex + 1;
                    MainForm.NastaveniAplikace.nastaveniZvukuCtecky = readerSoundCheckBox.Checked;
                    MainForm.NastaveniAplikace.automaticConnect = readerAutomaticCheckBox.Checked;
                    MainForm.Ctecka.SoundOnOff = MainForm.NastaveniAplikace.nastaveniZvukuCtecky;
                    MainForm.NastaveniAplikace.uloz(Program.hlavniOkno.DirectoryAll + "apiSets.ser", MainForm.NastaveniAplikace);
                }
                catch
                {
                    MessageBox.Show("Nepodařilo se uložit nastavení aplikace.", "Chyba prgramu...", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

        }

        private void pokladnaNulovatButton_Click(object sender, EventArgs e)
        {
            MainForm.Pokladna = 0;
            Program.hlavniOkno.pokladanLabel.Text = "Stav pokladny : " + MainForm.Pokladna.ToString() + " Kč";
            Program.hlavniOkno.RegisterKeyForAccount = Microsoft.Win32.Registry.CurrentUser.CreateSubKey("SOFTWARE\\RAS20ZP\\RAS20ZP");
            if (Program.hlavniOkno.RegisterKeyForAccount != null)
            {
                Program.hlavniOkno.RegisterKeyForAccount.SetValue("vaz", (int)0);
                Program.hlavniOkno.RegisterKeyForAccount.Close();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
         /*   for (int i = 0; i < 7; i++)
            {
                Program.ClassNamesList.Names[i] = dataGridView1.Rows[i].Cells[1].Value.ToString();
            } 
            
            Program.ClassNamesList.uloz(Program.ClassNamesList);*/
            //pokus o ulozeni souboru s nastavenim Api
            try
            {
                MainForm.NastaveniAplikace.preddefinovanyPort = readerPortComboBox.SelectedIndex + 1;
                MainForm.NastaveniAplikace.nastaveniZvukuCtecky = readerSoundCheckBox.Checked;
                MainForm.NastaveniAplikace.automaticConnect = readerAutomaticCheckBox.Checked;
                MainForm.Ctecka.SoundOnOff = MainForm.NastaveniAplikace.nastaveniZvukuCtecky;

                MainForm.NastaveniAplikace.terminalId = (int)terminalId.Value;

                //retezce pro pripojeni k DB serveru
                MainForm.NastaveniAplikace.MSSQLLogin = textBoxSqlLogin.Text;
                MainForm.NastaveniAplikace.MSSQLPassword = textBoxSqlPassword.Text;
                MainForm.NastaveniAplikace.MSSQLServer = textBoxSqlServerName.Text;

                //ulozeni nastavenych dat
                MainForm.NastaveniAplikace.uloz(Program.hlavniOkno.DirectoryAll + "apiSets.ser", MainForm.NastaveniAplikace);

                //ulozeni nastaveni uzivatelskych skupin
                ulozeniNastaveniSkupin();

            }
            catch
            {
                MessageBox.Show("Nepodařilo se uložit nastavení aplikace.", "Chyba prgramu...", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            this.Close();
        }


        /// <summary>
        /// Uklada nastaveni skupin pokud doslo ke zmene
        /// </summary>
        private void ulozeniNastaveniSkupin()
        {
            string connectionString = "Data source=" + MainForm.NastaveniAplikace.MSSQLServer + ";User ID=" + MainForm.NastaveniAplikace.MSSQLLogin + ";Password=" + MainForm.NastaveniAplikace.MSSQLPassword + ";Connect Timeout=1;Database=CMData";

            SqlConnection conn = new SqlConnection(connectionString);
            try
            {
                conn.Open();
                bool byloNastaveno = false; //indikace zda doslo ke zmene v nazvech trid
                int i;
                //kontrola schodnosti jednotlivych nazvu skupin a jejich pripadne ulozeni do DB
                for (i = 0; i < dataGridView1.Rows.Count; i++)
                {
                    if (dataGridView1.Rows[i].Cells[1].Value.ToString() != Program.hlavniOkno.classNames[i])
                    {
                        byloNastaveno = true;
                        SqlCommand CmdSelectPermanetka = new SqlCommand(@"UPDATE [CMData].[dbo].[classNames] SET [className] = '" + dataGridView1.Rows[i].Cells[1].Value.ToString() + "' WHERE [id] = " + (i+1).ToString(), conn);
                        CmdSelectPermanetka.ExecuteNonQuery(); //provedeni osdstraneni
                    }

                }

                //nastaveni priznaku posledni zmeny nastaveni aplikace
                if (byloNastaveno == true)
                {
                    SqlCommand CmdSelectPermanetka = new SqlCommand(@"SET LANGUAGE BRITISH; UPDATE [CMData].[dbo].[lastApChange] SET [lastApChange] = '"+DateTime.Now +"'", conn);
                    CmdSelectPermanetka.ExecuteNonQuery();
                }
            }
            catch 
            {
                MessageBox.Show("Během ukládání názvů skupin do databáze došlo k chybě. Zkontrolujte spojení s databází.", "Porucha komunikace s databází.", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                conn.Close();
            }
            Program.hlavniOkno.AplicationSettingRefresh();
        }

        public class ComboboxItem
        {
            public string Text { get; set; }
            public object Value { get; set; }

            public override string ToString()
            {
                return Text;
            }
        }

        /// <summary>
        /// FCE pro ulozeni nastaveni DB a nacteni loginu dle aktualniho nastaveni DB
        /// </summary>
        public void UlozeniNastaveniDBaNacteniLoginu()
        {
            //ulozeni aktualniho nastaveni DB            
            try
            {
                //retezce pro pripojeni k DB serveru
                MainForm.NastaveniAplikace.MSSQLLogin = textBoxSqlLogin.Text;
                MainForm.NastaveniAplikace.MSSQLPassword = textBoxSqlPassword.Text;
                MainForm.NastaveniAplikace.MSSQLServer = textBoxSqlServerName.Text;
                //ulozeni nastavenych dat
                MainForm.NastaveniAplikace.uloz(Program.hlavniOkno.DirectoryAll + "apiSets.ser", MainForm.NastaveniAplikace);
            }
            catch
            {
            }
            //overeni spojeni s DB a nacteni aktualniho seznamu loginu
            //nacteni seznamu uzivatelů z databáze
            string connectionString = "Data source=" + MainForm.NastaveniAplikace.MSSQLServer + ";User ID=" + MainForm.NastaveniAplikace.MSSQLLogin + ";Password=" + MainForm.NastaveniAplikace.MSSQLPassword + ";Connect Timeout=1;Database=CMData";
            SqlConnection conn = new SqlConnection(connectionString);
            try
            {
                conn.Open();
                SqlCommand CmdSelectPermanetka = new SqlCommand(@"SELECT [id],[nickname] FROM [CMData].[dbo].[users]", conn);
                // string dataReader = CmdAddARow.ExecuteScalar().ToString();
                SqlDataReader dataReader;
                dataReader = CmdSelectPermanetka.ExecuteReader();

                comboBoxLoginList.Items.Clear(); //vymazani stavajiciho seznamu

                while (dataReader.Read())
                {
                    ComboboxItem item = new ComboboxItem();
                    item.Text = dataReader.GetString(1);
                    item.Value = dataReader.GetInt32(0);
                    comboBoxLoginList.Items.Add(item);
                   
                }
                if (comboBoxLoginList.Items.Count > 0) comboBoxLoginList.SelectedIndex = 0;
                //dataGridView1.Enabled = true;
                dataReader.Close();
            }
            catch (Exception ex)
            {
                //dataGridView1.Enabled = false;
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }

        }

        /// <summary>
        /// SLouzi pro nacteni aktualnich nazvu terminalu po pripojeni k DB
        /// </summary>
        private void NacteniAktualnichNazvuSkupin()
        {
            dataGridView1.Enabled = (MainForm.HwKeyExpose == true) ? true : false;
            Program.hlavniOkno.AplicationSettingRefresh();
            //nacteni seznamu skupin pro editaci
            dataGridView1.Rows.Clear();
            for (int i = 0; i < 7; i++)
            {
                dataGridView1.Rows.Add();
                dataGridView1.Rows[dataGridView1.Rows.Count - 1].Cells[0].Value = (i + 1).ToString();
                dataGridView1.Rows[dataGridView1.Rows.Count - 1].Cells[1].Value = Program.hlavniOkno.classNames[i];
            }
            dataGridView1.Refresh();

        }

        private void button3_Click(object sender, EventArgs e)
        {
            string connectionString = "Data source=" + textBoxSqlServerName.Text + ";User ID=" + textBoxSqlLogin.Text + ";Password=" + textBoxSqlPassword.Text + "; Connect Timeout = 4;";
            SqlConnection conn = new SqlConnection(connectionString);
            SqlCommand commCheckDbExist = new SqlCommand("SELECT * from master.dbo.sysdatabases where name='CMData'", conn);
            try
            {
                conn.Open();
                SqlDataReader reader = commCheckDbExist.ExecuteReader();
                bool dbExist = reader.HasRows;
                reader.Close();
                if (dbExist)
                {
                    MessageBox.Show("Databaze nalezena.");
                    UlozeniNastaveniDBaNacteniLoginu(); //ulozni aktualni nastaveni spojeni na DB a nacte aktualni seznam uzivatelu pro jejich pripadnou upravu
                    NacteniAktualnichNazvuSkupin(); //nacteni aktualniho nastaveni aplikace
                }
                else
                {
                    SqlCommand CmdDbCreat = new SqlCommand("CREATE DATABASE CMData", conn);
                    CmdDbCreat.ExecuteNonQuery();

                    //Vybrat novou databazi
                    SqlCommand CmdDBSelect = new SqlCommand(@"USE [CMData]", conn);
                    CmdDBSelect.ExecuteNonQuery();

                    //Vytvoreni tabulky pro seznam uzivatelu
                    SqlCommand CmdTb1Creat = new SqlCommand(@"
                        USE [CMData]
                        SET ANSI_NULLS ON
                        SET QUOTED_IDENTIFIER ON
                        SET ANSI_PADDING ON
                        CREATE TABLE [dbo].[users](
	                        [id] [int] IDENTITY(1,1) NOT NULL,
	                        [nickname] [varchar](50) NOT NULL,
	                        [password] [varchar](50) NOT NULL,
	                        [cash] [int] NOT NULL,
	                        [lastActivity] [datetime] NOT NULL,
                         CONSTRAINT [PK_users] PRIMARY KEY CLUSTERED 
                        (
	                        [id] ASC
                        )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
                        ) ON [PRIMARY]
                        SET ANSI_PADDING OFF
                        ALTER TABLE [dbo].[users] ADD  CONSTRAINT [DF_users_cash]  DEFAULT ((0)) FOR [cash]
                        ALTER TABLE [dbo].[users] ADD  CONSTRAINT [DF_users_lastActivity]  DEFAULT (getdate()) FOR [lastActivity]
                    ", conn);
                    CmdTb1Creat.ExecuteNonQuery();

                    //Priznak posledni zmeny sdileneho aplikacniho nastaveni
                    SqlCommand CmdTb2Creat = new SqlCommand(@"
                        USE [CMData]
                        SET ANSI_NULLS ON
                        SET QUOTED_IDENTIFIER ON
                        CREATE TABLE [dbo].[lastApChange](
	                        [lastApChange] [datetime] NOT NULL
                        ) ON [PRIMARY]
                        ALTER TABLE [dbo].[lastApChange] ADD  CONSTRAINT [DF_lastApChange_lastApChange]  DEFAULT (getdate()) FOR [lastApChange]
                        INSERT INTO [CMData].[dbo].[lastApChange]
                                   ([lastApChange])
                             VALUES
                                   (GETDATE())
                    ", conn);
                    CmdTb2Creat.ExecuteNonQuery();

                    //Tabulka s názvy skupin
                    SqlCommand CmdTb3Creat = new SqlCommand(@"
                        USE [CMData]
                        SET ANSI_NULLS ON
                        SET QUOTED_IDENTIFIER ON
                        SET ANSI_PADDING ON
                        CREATE TABLE [dbo].[classNames](
	                        [id] [int] NOT NULL,
	                        [className] [varchar](50) NULL
                        ) ON [PRIMARY]
                        SET ANSI_PADDING OFF
                    ", conn);
                    CmdTb3Creat.ExecuteNonQuery();

                    //vložení názvů jednotlivých skupin
                    SqlCommand CmdTClassAdd = new SqlCommand(@"INSERT INTO [CMData].[dbo].[classNames] ([id],[className]) VALUES (1, 'Skupina 1')", conn);
                    CmdTClassAdd.ExecuteNonQuery();
                    CmdTClassAdd = new SqlCommand(@"INSERT INTO [CMData].[dbo].[classNames] ([id],[className]) VALUES (2, 'Skupina 2')", conn);
                    CmdTClassAdd.ExecuteNonQuery();
                    CmdTClassAdd = new SqlCommand(@"INSERT INTO [CMData].[dbo].[classNames] ([id],[className]) VALUES (3, 'Skupina 3')", conn);
                    CmdTClassAdd.ExecuteNonQuery();
                    CmdTClassAdd = new SqlCommand(@"INSERT INTO [CMData].[dbo].[classNames] ([id],[className]) VALUES (4, 'Skupina 4')", conn);
                    CmdTClassAdd.ExecuteNonQuery();
                    CmdTClassAdd = new SqlCommand(@"INSERT INTO [CMData].[dbo].[classNames] ([id],[className]) VALUES (5, 'Skupina 5')", conn);
                    CmdTClassAdd.ExecuteNonQuery();
                    CmdTClassAdd = new SqlCommand(@"INSERT INTO [CMData].[dbo].[classNames] ([id],[className]) VALUES (6, 'Skupina 6')", conn);
                    CmdTClassAdd.ExecuteNonQuery();
                    CmdTClassAdd = new SqlCommand(@"INSERT INTO [CMData].[dbo].[classNames] ([id],[className]) VALUES (7, 'Skupina 7')", conn);
                    CmdTClassAdd.ExecuteNonQuery();

                    //Tabulka s názvy terminálů
                    SqlCommand CmdTb4Creat = new SqlCommand(@"
                        USE [CMData]
                        SET ANSI_NULLS ON
                        SET QUOTED_IDENTIFIER ON
                        SET ANSI_PADDING ON
                        CREATE TABLE [dbo].[terminalNames](
	                        [aplikacniId] [int] NOT NULL,
	                        [terminalId] [int] NOT NULL,
	                        [terminalNick] [varchar](70) NOT NULL
                        ) ON [PRIMARY]
                        SET ANSI_PADDING OFF
                    ", conn);
                    CmdTb4Creat.ExecuteNonQuery();

                    //Tabulka seznamu vydanych karet
                    SqlCommand CmdTb5Creat = new SqlCommand(@"
                        USE [CMData]
                        SET ANSI_NULLS ON
                        SET QUOTED_IDENTIFIER ON
                        SET ANSI_PADDING ON
                        CREATE TABLE [dbo].[vendetCardList](
	                        [cardNum] [int] NOT NULL,
	                        [vendetTime] [datetime] NOT NULL,
	                        [userName] [varchar](50) NOT NULL,
	                        [note] [varchar](100) NOT NULL
                        ) ON [PRIMARY]
                        SET ANSI_PADDING OFF
                    ", conn);
                    CmdTb5Creat.ExecuteNonQuery();

                    //Tabulka seznamu udalost
                    SqlCommand CmdTb6Creat = new SqlCommand(@"
                        USE [CMData]
                        SET ANSI_NULLS ON
                        SET QUOTED_IDENTIFIER ON
                        CREATE TABLE [dbo].[eventsList](
	                        [id] [int] IDENTITY(1,1) NOT NULL,
	                        [eventTime] [datetime] NOT NULL,
	                        [cardNum] [int] NOT NULL,
	                        [userClass] [tinyint] NOT NULL,
	                        [userId] [smallint] NOT NULL,
	                        [eventType] [tinyint] NOT NULL,
	                        [cashChange] [int] NOT NULL,
                         CONSTRAINT [PK_eventsList] PRIMARY KEY CLUSTERED 
                        (
	                        [id] ASC
                        )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
                        ) ON [PRIMARY]
                    ", conn);
                    CmdTb6Creat.ExecuteNonQuery();

                    MessageBox.Show("Databaze byla úspěšně založena.");
                    UlozeniNastaveniDBaNacteniLoginu(); //ulozni aktualni nastaveni spojeni na DB a nacte aktualni seznam uzivatelu pro jejich pripadnou upravu
                    NacteniAktualnichNazvuSkupin(); //nacteni aktualniho nastaveni aplikace
                }

            }
            //catch (Exception ex)
            catch
            {
                //MessageBox.Show(ex.Message);
                MessageBox.Show("Nepodařilo se spojit s databázovým serverem.");
            }
            finally
            {
                conn.Close();
            }

        }

        private Form subOkno = null;

        private void buttonUserAdd_Click(object sender, EventArgs e)
        {
            //PRIDANI NOVEHO UZIVATELE
            if (subOkno == null || subOkno.IsDisposed)
            {
                subOkno = new UserForm(0);
                subOkno.Show();
            }
        }

        private void buttonUserDel_Click(object sender, EventArgs e)
        {
            //ODSTRANENI UZIVATELE
            string connectionString = "Data source=" + MainForm.NastaveniAplikace.MSSQLServer + ";User ID=" + MainForm.NastaveniAplikace.MSSQLLogin + ";Password=" + MainForm.NastaveniAplikace.MSSQLPassword + ";Connect Timeout=1;Database=CMData";

//!!! Zde doplnit kontrolu zda nejsou udalosti k danemu uzivateli

            SqlConnection conn = new SqlConnection(connectionString);
            try
            {
                conn.Open();

                SqlCommand CmdUserCheck = new SqlCommand(@"SELECT COUNT(userId) as pocet FROM [CMData].[dbo].[eventsList] WHERE userId = " + ((ComboboxItem)comboBoxLoginList.SelectedItem).Value.ToString(), conn);

                if ((int)CmdUserCheck.ExecuteScalar() == 0)
                {

                    SqlCommand CmdSelectPermanetka = new SqlCommand(@"DELETE FROM [CMData].[dbo].[users] WHERE [id] = " + ((ComboboxItem)comboBoxLoginList.SelectedItem).Value.ToString(), conn);
                    CmdSelectPermanetka.ExecuteNonQuery(); //provedeni osdstraneni
                    UlozeniNastaveniDBaNacteniLoginu(); //nacteni noveho seznamu 
                }
                else
                {
                    MessageBox.Show("Před odstraněním uživatelského účtu je třeba odstranit veškeré jeho záznamy ze statistiky.","Odstranění účtu",MessageBoxButtons.OK,MessageBoxIcon.Stop);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }

        private void buttonUserPassword_Click(object sender, EventArgs e)
        {
            //ZMENA HESLA UZIVATELE
            if (subOkno == null || subOkno.IsDisposed)
            {
                subOkno = new UserForm((int)((ComboboxItem)comboBoxLoginList.SelectedItem).Value);
                subOkno.Show();
            }
        }
    }
}
