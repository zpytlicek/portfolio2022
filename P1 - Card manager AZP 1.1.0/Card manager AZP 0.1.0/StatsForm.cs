﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Data.SqlClient;

namespace Card_manager_AZP
{
    public partial class StatsForm : Form
    {
        public class tUserList 
        {
            public tUserList(int id, string nick)
            {
                this.id = id;
                this.nick = nick;
            }

            public int id;
            public string nick;
        }
        
        List<cardEvent> EventsAll;
        List<tUserList> SeznamUzivatelu = new List<tUserList>();

        string getUserName(int id)
        {
            for (int i = 0; i < SeznamUzivatelu.Count; i++)
            {
                if (SeznamUzivatelu[i].id == id) return SeznamUzivatelu[i].nick;
            }
            

            return ("neznamý");

        }

        public StatsForm()
        {
            
                

                InitializeComponent();
                

                DateTime minDate = DateTime.Now, maxDate = DateTime.Now;
                //Urceni data od - do
                string connectionString = "Data source=" + MainForm.NastaveniAplikace.MSSQLServer + ";User ID=" + MainForm.NastaveniAplikace.MSSQLLogin + ";Password=" + MainForm.NastaveniAplikace.MSSQLPassword + ";Connect Timeout=1;Database=CMData";
                SqlConnection conn = new SqlConnection(connectionString);
                try
                {
                    conn.Open();
                    SqlDataReader dataReader;
                    


                    SqlCommand CmdSelectMinDate = new SqlCommand(@"SELECT MIN(eventTime) as MINTIME FROM [CMData].[dbo].[eventsList]", conn);
                    dataReader = CmdSelectMinDate.ExecuteReader();
                    if (dataReader.HasRows)
                    {
                        dataReader.Read();
                        minDate = dataReader.GetValue(0) != DBNull.Value ? dataReader.GetDateTime(0) : DateTime.Now;
                    }
                    dataReader.Close();

                    SqlCommand CmdSelectMaxDate = new SqlCommand(@"SELECT MAX(eventTime) as MAXTIME FROM [CMData].[dbo].[eventsList]", conn);
                    dataReader = CmdSelectMaxDate.ExecuteReader();
                    if (dataReader.HasRows)
                    {
                        dataReader.Read();
                        maxDate = dataReader.GetValue(0) != DBNull.Value ? dataReader.GetDateTime(0) : DateTime.Now;
                    }
                    dataReader.Close();
                }
                catch
                {
                    MessageBox.Show("Nepodařilo se navázat spojení s databází.", "Problém s databází.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Close();
                }
                finally
                {
                    conn.Close();
                }

                //vychozi nastaveni filtru - vsechna data
                platnostOdDate.MinDate = minDate;
                platnostOdDate.MaxDate = maxDate;
                platnostOdDate.Value = maxDate;
                platnostDoDate.MinDate = minDate;
                platnostDoDate.MaxDate = maxDate;
                platnostDoDate.Value = maxDate;

                //nacteni seznamu uzivatelů z databáze
                try
                {
                    conn.Open();
                    SqlCommand CmdSelectPermanetka = new SqlCommand(@"SELECT [id],[nickname] FROM [CMData].[dbo].[users]", conn);
                    // string dataReader = CmdAddARow.ExecuteScalar().ToString();
                    SqlDataReader dataReader;
                    dataReader = CmdSelectPermanetka.ExecuteReader();

                    comboBoxPerson.Items.Clear(); //vymazani stavajiciho seznamu
                    ComboboxItem item2 = new ComboboxItem();
                    item2.Text = "všichni";
                    item2.Value = 0;
                    comboBoxPerson.Items.Add(item2);
                    int selectedIndex = 0;
                    while (dataReader.Read())
                    {
                        ComboboxItem item = new ComboboxItem();
                        item.Text = dataReader.GetString(1);
                        item.Value = dataReader.GetInt32(0);
                        comboBoxPerson.Items.Add(item);
                        if (Program.hlavniOkno.UserId == dataReader.GetInt32(0)) selectedIndex = comboBoxPerson.Items.Count - 1; 
                        SeznamUzivatelu.Add(new tUserList(dataReader.GetInt32(0), dataReader.GetString(1)));
                    }
                    try
                    {
                        comboBoxPerson.SelectedIndex = selectedIndex;
                    }
                    catch { }
                    dataReader.Close();
                }
                catch
                {

                }
                finally
                {
                    conn.Close();
                }

                //načtení dat
                EventsAll = CardEventFile.ReadAllEvents(Program.hlavniOkno.UserId, new DateTime(maxDate.Year, maxDate.Month, maxDate.Day, 0, 0, 0), new DateTime(maxDate.Year, maxDate.Month, maxDate.Day, 23, 59, 59));

                zobrazVybranaData(EventsAll);
                //povoleni tlacitka nulovani pokladny a smazani dat
                if (MainForm.HwKeyExpose == true) pokladnaNulovatButton.Enabled = true;
                buttonDelAll.Enabled = (MainForm.HwKeyExpose == true) ? true : false;
        }

        public class ComboboxItem
        {
            public string Text { get; set; }
            public object Value { get; set; }

            public override string ToString()
            {
                return Text;
            }
        }

        private void buttonDelAll_Click(object sender, EventArgs e)
        {
            CardEventFile.DelAll((int)((ComboboxItem)comboBoxPerson.SelectedItem).Value);
            buttonFilter_Click(null, null);
        }

        /// <summary>
        /// Fce pro zobrazeni vybranych dat v data grid view
        /// </summary>
        public void zobrazVybranaData(List<cardEvent> EventsForShow)
        {
            //vymazani puvodnich dat
            dataGridView1.Rows.Clear();
            dataGridView2.Rows.Clear();
            //vypis udalosti do hlavniho prehledu
            cardEvent recordBuff;
            for (int zaznam = 0; zaznam < EventsForShow.Count; zaznam++)
            {
                recordBuff = EventsForShow[zaznam];
                dataGridView1.Rows.Add();
                dataGridView1.Rows[dataGridView1.Rows.Count - 1].Cells[0].Value = recordBuff.time.ToString("dd.MM.yyyy HH:mm:ss");
                dataGridView1.Rows[dataGridView1.Rows.Count - 1].Cells[1].Value = recordBuff.cardNum;
                dataGridView1.Rows[dataGridView1.Rows.Count - 1].Cells[2].Value = Program.hlavniOkno.classNames[recordBuff.userClass];
                  //  "skupina" + (recordBuff.userClass + 1).ToString();
                switch (recordBuff.typUdalosti)
                {
                    case t_typUdalosti.cashChange:
                        dataGridView1.Rows[dataGridView1.Rows.Count - 1].Cells[3].Value = "změna kreditu";
                        break;
                    case t_typUdalosti.distribution:
                        dataGridView1.Rows[dataGridView1.Rows.Count - 1].Cells[3].Value = "vydání karty";
                        break;
                    case t_typUdalosti.withdrawal:
                        dataGridView1.Rows[dataGridView1.Rows.Count - 1].Cells[3].Value = "stažení karty";
                        break;
                    default:
                        dataGridView1.Rows[dataGridView1.Rows.Count - 1].Cells[3].Value = "neznámá";
                        break;
                }
                
                dataGridView1.Rows[dataGridView1.Rows.Count - 1].Cells[4].Value = recordBuff.cashChange;

                dataGridView1.Rows[dataGridView1.Rows.Count - 1].Cells[5].Value = getUserName(recordBuff.user);
            }

            //vypis tabulky dle skupin

            int[,] pocty = new int[8, 3];

            //pocitani statistik
            for (int zaznam = 0; zaznam < EventsForShow.Count; zaznam++)
            {
                recordBuff = EventsForShow[zaznam];
                //pocitani vydanych karet
                if (recordBuff.typUdalosti == t_typUdalosti.distribution)
                {
                    pocty[recordBuff.userClass, 0] = pocty[recordBuff.userClass, 0] + 1;
                    pocty[7, 0] = pocty[7, 0] + 1;
                }
                //pocitani prijatych karet
                if (recordBuff.typUdalosti == t_typUdalosti.withdrawal)
                {
                    pocty[recordBuff.userClass, 1] = pocty[recordBuff.userClass, 1] + 1;
                    pocty[7, 1] = pocty[7, 1] + 1;
                }
                //pocitani zmeny pokladny - vzdy
                pocty[recordBuff.userClass, 2] = pocty[recordBuff.userClass, 2] + recordBuff.cashChange;
                pocty[7, 2] = pocty[7, 2] + recordBuff.cashChange;

            }

            if (dataGridView2.ColumnCount != 0)
            {
                for (int skupNum = 0; skupNum < 7; skupNum++)
                {
                    dataGridView2.Rows.Add();
                    dataGridView2.Rows[dataGridView2.Rows.Count - 1].Cells[0].Value = Program.hlavniOkno.classNames[skupNum];
                    //"skupina " + (skupNum + 1).ToString();
                    dataGridView2.Rows[dataGridView2.Rows.Count - 1].Cells[1].Value = pocty[skupNum, 0].ToString();
                    dataGridView2.Rows[dataGridView2.Rows.Count - 1].Cells[2].Value = pocty[skupNum, 1].ToString();
                    dataGridView2.Rows[dataGridView2.Rows.Count - 1].Cells[3].Value = pocty[skupNum, 2].ToString();
                }

                //celkovy souhrn za vybrane období
                dataGridView2.Rows.Add();
                dataGridView2.Rows[dataGridView2.Rows.Count - 1].Cells[0].Value = "CELKEM";
                dataGridView2.Rows[dataGridView2.Rows.Count - 1].Cells[1].Value = pocty[7, 0].ToString();
                dataGridView2.Rows[dataGridView2.Rows.Count - 1].Cells[2].Value = pocty[7, 1].ToString();
                dataGridView2.Rows[dataGridView2.Rows.Count - 1].Cells[3].Value = pocty[7, 2].ToString();
            }
        }

        private void buttonFilter_Click(object sender, EventArgs e)
        {
            List<cardEvent> EventsFiltring;
            DateTime dateOd = new DateTime(platnostOdDate.Value.Year, platnostOdDate.Value.Month, platnostOdDate.Value.Day, platnostOdTime.Value.Hour, platnostOdTime.Value.Minute, 0);
            DateTime dateDo = new DateTime(platnostDoDate.Value.Year, platnostDoDate.Value.Month, platnostDoDate.Value.Day, platnostDoTime.Value.Hour, platnostDoTime.Value.Minute, 0);

            EventsFiltring = CardEventFile.ReadAllEvents((int)((ComboboxItem)comboBoxPerson.SelectedItem).Value,dateOd, dateDo);

            zobrazVybranaData(EventsFiltring);
        }

        private void buttonExportData_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == saveFileDialog1.ShowDialog())
            {
                //uloženi datagriedview1 do formatu CSV
                FileStream fs = null;
                StreamWriter sw = null;

                try
                {
                    fs = new FileStream(saveFileDialog1.FileName, FileMode.Create);
                    sw = new StreamWriter(fs, Encoding.Default);
                    // Pokud soubor neexistuje, vytvoříme ho a zapíšeme do něj text:
                    sw.WriteLine(dataGridView1.Columns[0].HeaderText.ToString() + ";" + dataGridView1.Columns[1].HeaderText.ToString() + ";" + dataGridView1.Columns[2].HeaderText.ToString() + ";" + dataGridView1.Columns[3].HeaderText.ToString() + ";" + dataGridView1.Columns[4].HeaderText.ToString() + ";" + dataGridView1.Columns[5].HeaderText.ToString() + ";");
                    for (int rowsNum = 0; rowsNum < dataGridView1.RowCount; rowsNum++)
                    {
                        sw.WriteLine(dataGridView1.Rows[rowsNum].Cells[0].Value.ToString() + ";" + dataGridView1.Rows[rowsNum].Cells[1].Value.ToString() + ";" + dataGridView1.Rows[rowsNum].Cells[2].Value.ToString() + ";" + dataGridView1.Rows[rowsNum].Cells[3].Value.ToString() + ";" + dataGridView1.Rows[rowsNum].Cells[4].Value.ToString() + ";" + dataGridView1.Rows[rowsNum].Cells[5].Value.ToString() + ";");
                    }
                    MessageBox.Show("Export dat proběhl úspěšně.", "Export dat", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch
                {
                    MessageBox.Show("Export dat se nezdařil.", "Export dat", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    if (sw != null) sw.Close();
                    if (fs != null) fs.Close();
                }
            }

        }

        private void pokladnaNulovatButton_Click(object sender, EventArgs e)
        {
            SqlTransaction myTran = null; //vlakno transakce
            string connectionString = "Data source=" + MainForm.NastaveniAplikace.MSSQLServer + ";User ID=" + MainForm.NastaveniAplikace.MSSQLLogin + ";Password=" + MainForm.NastaveniAplikace.MSSQLPassword + ";Connect Timeout=1;Database=CMData";
            SqlConnection conn = new SqlConnection(connectionString);
            try
            {
                    //zacatek komunikace s databazi
                    conn.Open();
                    myTran = conn.BeginTransaction();
                    //aktualizace pokladny
                    SqlCommand CmdUserCreditUpdate = new SqlCommand("UPDATE [CMData].[dbo].[users] SET [cash] = 0 WHERE [id] = " + Program.hlavniOkno.UserId, conn);
                    CmdUserCreditUpdate.Transaction = myTran;
                    CmdUserCreditUpdate.ExecuteNonQuery();

                    myTran.Commit(); //transakce bude dokoncena 
            }
            catch
            {
                try
                {
                    myTran.Rollback();
                }
                catch { }
            }
            finally
            {
                conn.Close();
                Program.hlavniOkno.UserInfoRefresh(); //aktualizace informaci v hlavnim okne
            }

            
        }

        private void StatsForm_Load(object sender, EventArgs e)
        {

        }

    }
}
