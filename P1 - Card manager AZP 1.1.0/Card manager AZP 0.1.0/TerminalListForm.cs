﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Security.Principal;
namespace Card_manager_AZP
{
    public partial class TerminalListForm : Form
    {

        public DataTable gridViewTable1 = new DataTable("terminalt");

        public TerminalListForm()
        {
            InitializeComponent();

            dataGridView1.Rows.Clear();
        //    nacteniDataGridu();
            //DataRow radek = new DataRow();
            //Nastaveni tabulky s daty
            gridViewTable1.Columns.Add("Id.aplikace");
            gridViewTable1.Columns.Add("Id.terminálu");
            gridViewTable1.Columns.Add("Název terminálu");
            //dt.Columns.Add("id", System.Type.GetType("System.Int32"));

            //propojeni tabulky dat a gridView
            dataGridView1.DataSource = gridViewTable1;
            dataGridView1.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            dataGridView1.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            dataGridView1.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dataGridView1.Columns[0].ReadOnly = true;
            dataGridView1.Columns[1].ReadOnly = true;
            dataGridView1.Columns[2].ReadOnly = false;

            if (MainForm.HwKeyExpose == true) dataGridView1.Enabled = true;

            //zobrazeni sezanamu terminalu
            DataRow dr;
            for (int i = 0; i < MainForm.TerminalList.TerminalList.Count; i++)
            {
                dr = gridViewTable1.NewRow();
                dr[0] = MainForm.TerminalList.TerminalList[i].ApilikacniID;
                dr[1] = MainForm.TerminalList.TerminalList[i].TerminalId;
                dr[2] = MainForm.TerminalList.TerminalList[i].TerminalNick;
                gridViewTable1.Rows.Add(dr);
            }

            /*    DataRow dr = gridViewTable1.NewRow();
                dr[0] = cardId;
                dr[1] = vystaveno.ToString("dd.MM.yyyy HH:mm");
                string nick = "";
                for (int i = 0; i < UsersList.Count; i++)
                {
                    if (UsersList[i].Id == personalId) nick = UsersList[i].Nick;
                }

                dr[2] = nick;*/


        }

    /*    void nacteniDataGridu(){
            for (int i =0; i < MainForm.SeznamTerminalu.terminalList.Count; i++){
                        dataGridView1.Rows.Add();
                        dataGridView1.Rows[dataGridView1.Rows.Count - 1].Cells[0].Value = MainForm.SeznamTerminalu.terminalList[i].ApId;
                        dataGridView1.Rows[dataGridView1.Rows.Count - 1].Cells[1].Value = MainForm.SeznamTerminalu.terminalList[i].TId;
                        dataGridView1.Rows[dataGridView1.Rows.Count - 1].Cells[2].Value = MainForm.SeznamTerminalu.terminalList[i].Name;
            }

        } */

        private void TerminalListForm_Load(object sender, EventArgs e)
        {

        }

        private void dataGridView1_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            string name = sender.GetType().ToString();
        }

        public static bool IsAdministrator()
        {
            System.AppDomain.CurrentDomain.SetPrincipalPolicy(PrincipalPolicy.WindowsPrincipal); 
            WindowsIdentity identity = WindowsIdentity.GetCurrent();
            WindowsPrincipal principal = new WindowsPrincipal(identity);
            return principal.IsInRole(WindowsBuiltInRole.Administrator);
        }

        private void button3_Click(object sender, EventArgs e)
        {
        ///    dataGridView1
            

            //MessageBox.Show(
            MessageBox.Show(WindowsIdentity.GetCurrent().IsAuthenticated.ToString() + " " + IsAdministrator().ToString());
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MainForm.TerminalList.save(dataGridView1);
            this.Close();
        }

        private void dataGridView1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                dataGridView1.Rows.Remove(dataGridView1.SelectedRows[0]);
            }
        }
    }
}
