# Software portfolio by Zdeněk Pytlíček / 2022 <br> [zpytlicek@gmail.com](mailto:zpytlicek@gmail.com)

## Project #1 - Complex contactless payment system (2012)
The project was focused on developing a universal payment system using contactless smart cards (Mifare / DESFire). In the first phase, the project consisted of work on the creation of several peripherals for paid services (HW+FW). The HW was based mainly on the MCUs from PIC24F family. Several PC applications have been developed for card management and store transaction history. The demonstration software (located here in the directory ["/P1 - Card manager AZP 1.1.0"](/P1 - Card manager AZP 1.1.0) is an application for one or more reception desks, which enables card management and simple accounting linked via a shared database (MS SQL).
> **HW** (PCB design in Eagle), **FW** (MCU PIC24F / ***C***), **SW** (***VisualStudio 2013 / C# / MS SQL***)

![Project 1 - Main form](/P1 - Card manager AZP 1.1.0/project01-01.png "Main form")

**Fig.1.1: Main form**

![Project 1 - Setting form](/P1 - Card manager AZP 1.1.0/project01-02.png "Setting form")

**Fig.1.2: Setting form**

## Project #2 - Point-of-sale terminal for swimming pool (2013)
This project is based on the previous contactless payment system. The program manages contactless smart cards and chips within the swimming pool and wellness facility. The PC station included several other peripherals, including a cash register, cash terminal, and receipt printer.
> **SW** (VisualStudio 2013 / ***C#*** / ***MS SQL***)

![Project 2 - Main form - tab1](/P2 - Swimming pool/project02-01.png "Main form - tab1")

**Fig.2.1: Main form tab1**

![Project 2 - Main form - tab2](/P2 - Swimming pool/project02-02.png "Main form - tab2")

**Fig.2.2: Main form tab2**

## Project #3 - NextPerception - wearable sweat sensor system (2021)
HW, FW and SW development for sweating monitoring wearable sensor within the NextPerception project. The HW is based on low power components like STM32L and BLE (Bluetooth Low Energy) module from STMicroelectronics. The repository includes sample code of FW and SW from the first iteration of development.
The demonstration software 
>**HW** (PCB design and 3D visualisation in KiCad), **FW** (MCU STM32 / ***C,C++*** / ), **SW** (VisualStudio 2019 / ***C#***)
The sample code is located in this directory : ["/P3 - NextPerception"](/P3 - NextPerception)

![Project 3 - PCB visualisation](/P3 - NextPerception/project03-01.png "PCB visualisation")

**Fig.3.1: PCB visualisation**

![Project 3 - wearable sweat sensor](/P3 - NextPerception/project03-02.png "Wearable sweat sensor")

**Fig.3.2: Wearable sweat sensor**

![Project 3 - Simple data visualisation app](/P3 - NextPerception/project03-03.png "Simple data visualisation app")

**Fig.3.3: Simple data visualisation app**


## Project #4 - Application for anodizing (2020)

One of many applications for controlling laboratory equipement via the GPIB interface and VISA. In this case, it is an application designed for the controlled fabrication of nanostructures using the anodization method.
>**SW**(VisualStudio 2019 / ***C#***)

![Project 4 - Main form](/P4 - Anodization utility/project04-01.png "Main form")

**Fig.4.1: Main form - Anodization unit**

## Project #5 - Application for memristors characterization (2021)
One of many applications for controlling laboratory equipment via GPIB and VISA interfaces. In this case, an application to facilitate memristor characterization.
>**SW** (VisualStudio 2019 / ***C#***)

![Project 5 - Main form](/P5 - Memristor/project05-01.png "Main form")

**Fig.5.1: Main form - Memristor characterization utility**

## Project #6 - LabSensNano Website (2011)

The website serves to present the results and equipment of the LabSensNano research group. The  backend part was written in C#/ASP.NET using stylization templates. A simple editorial system with wysiwyg input item was created to allow easy editing of all content. An external upload plugin was implemented for easy insertion of images and files directly into the MS SQL database.
>**SW** (VisualStudio 2010 / ***C#,ASP.NET, HTML, CSS, JavaScript***) 
The sample code is located in this directory : ["/P6 - LabSensNano web"](/P6 - LabSensNano web)

![Project 6 - LabSensnano - homepage](/P6 - LabSensNano web/project06-01.jpg "LabSensnano - homepage")

**Fig.6.1: LabSensnano - homepage**

![Project 6 - RS LabSensNano](/P6 - LabSensNano web/project06-02.png "LabSensnano - RS")

**Fig.6.2: RS LabSensNano**