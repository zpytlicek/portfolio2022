﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

//pro úpravu obrázků
using System.IO;
using System.Drawing;
using System.Configuration;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;


public partial class Default6 : System.Web.UI.Page
{
    
    string category = "all";
    string type = "files";
    string redirectPath = "fileList.aspx";
    string HomeUrl = Global.HomeUrl();
    protected void Page_Load(object sender, EventArgs e)
    {
        // nastavení typu souboru a volba kategorie
        
        try
        {
            category = Request.QueryString["cat"].ToString();
        }
        catch
        {
            category = "all";
        }
        try
        {
            type = Request.QueryString["type"].ToString();
        }
        catch
        {
            type = "files";
        }
        if (type == "image")
        {
            MultiView1.SetActiveView(View1);
        }
        else
        {
            MultiView1.SetActiveView(View2);
        }
        redirectPath = redirectPath + "?cat=" + category + "&type=" + type;

        /*SqlDataSource1.SelectParameters["@cat"].DefaultValue = category.ToString();
        SqlDataSource2.SelectParameters["@cat"].DefaultValue = category.ToString();*/
    }

    /// <summary>
    /// Funkce na výpočet změny velikosti dle zadaných maxim
    /// </summary>
    /// <param name="sirka"></param>
    /// <param name="vyska"></param>
    /// <param name="sirkaMax"></param>
    /// <param name="vyskaMax"></param>
    /// <returns></returns>
    Size resize(int sirka, int vyska, int sirkaMax, int vyskaMax)
    {
        double konstantaMeritka = 0;
        if (sirka >= sirkaMax || vyska >= vyskaMax)
        {
            // zmensovani
            if (sirka / sirkaMax >= vyska / vyskaMax)
            {
                //sirka je smerodatna 
                konstantaMeritka = Convert.ToDouble(sirkaMax) / Convert.ToDouble(sirka);
            }
            else
            {
                //vyska je smerodatna
                konstantaMeritka = Convert.ToDouble(vyskaMax) / Convert.ToDouble(vyska);
            }
        }
        else
        {
            // zvetsovani
            if (sirkaMax / sirka >= vyskaMax / vyska)
            {
                //vyska je smerodatna
                konstantaMeritka = Convert.ToDouble(vyskaMax) / Convert.ToDouble(vyska);
            }
            else
            {
                //sirka je smerodatna
                konstantaMeritka = Convert.ToDouble(sirkaMax) / Convert.ToDouble(sirka);
            }
        }

        Size novavelikost;

        if (konstantaMeritka != 0)
        {
            novavelikost = new Size(Convert.ToUInt16(sirka * konstantaMeritka), Convert.ToUInt16(vyska * konstantaMeritka));
        }
        else {
            novavelikost = new Size(sirka, vyska);
        }

        return novavelikost;
    }

    private static ImageFormat GetFormat(string filename)
    {
        if (filename.EndsWith("jpg") || filename.EndsWith("jpeg") || filename.EndsWith("tiff"))
            return ImageFormat.Jpeg;

        return ImageFormat.Png;
    }

    private bool ThumbnailCallback()
    {
        return false;
    }
 
    protected void Button1_Click(object sender, EventArgs e)
    {
        string ContentType;
         ContentType=FileUpload1.PostedFile.ContentType;
         if ((ContentType == "image/jpeg" || ContentType == "image/gif" || ContentType == "image/pjpeg" || ContentType == "image/png" || ContentType == "image/x-png") && FileUpload1.PostedFile.ContentLength < 1000000)
         {
             // Label1.Text = FileUpload1.PostedFile.ContentType + " " + FileUpload1.PostedFile.FileName + " " + FileUpload1.PostedFile.ContentLength;
             //nacteni obrázku do pole
             byte[] Picture_content;
             Picture_content = new byte[FileUpload1.PostedFile.ContentLength];
             FileUpload1.PostedFile.InputStream.Read(Picture_content, 0, FileUpload1.PostedFile.ContentLength);

             // zjištění rozměrů obrázku. Lze však zjistit i více
             MemoryStream stream = new MemoryStream();
             stream.Write(Picture_content, 0, Picture_content.Length);
             Bitmap objBitmap = new Bitmap(stream);

             /* Změna velikosti obrázku */
             int width = 100;
             int height = 100;

             //  Image1. .ImageUrl = objBitmap.;
             Size SizePicture = objBitmap.Size;

             // výpočet nových rozměrů 
             Size nvo;
             nvo = resize(SizePicture.Width, SizePicture.Height, width, height);

             MemoryStream PreviewStream = new MemoryStream();
             System.Drawing.Image orig = System.Drawing.Image.FromStream(stream);
             System.Drawing.Image.GetThumbnailImageAbort callback = new System.Drawing.Image.GetThumbnailImageAbort(ThumbnailCallback);
             System.Drawing.Image img = orig.GetThumbnailImage(nvo.Width, nvo.Height, callback, IntPtr.Zero);
             switch (ContentType)
             {
                 case "image/gif":
                     img.Save(PreviewStream, ImageFormat.Png);
                     break;
                 case "image/png":
                 case "image/x-png":
                     img.Save(PreviewStream, ImageFormat.Png);
                     break;
                 default:
                     img.Save(PreviewStream, ImageFormat.Jpeg);
                     break;
             }
             
             

           
             
             // vytvoření obrázku s novými rozměry 
             //Bitmap objBitmapResize = new Bitmap(objBitmap, nvo);

             // načtení zmenšeného obrázku do pole

             byte[] PicturePreview_content = new byte[PreviewStream.Length];
             PreviewStream.Position = 0;
             PreviewStream.Read(PicturePreview_content, 0, PicturePreview_content.Length);

             // objBitmapResize.Save(Response.OutputStream, ImageFormat.Jpeg);
             // uvolnění paměti 
             objBitmap.Dispose();
             stream.Close();
             PreviewStream.Close();
             //objBitmapResize.Dispose();

             // vytvoření spojení do databáze
             //načtení počtu řádků z databáze (skalární výseledek na SQL dotaz)
             SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["labsensnanoConnectionString"].ToString());

             // vytvoření SQL příkazu
             SqlCommand cmd = new SqlCommand("ImageAdd", conn);
             // budeme používat uloženou proceduru
             cmd.CommandType = CommandType.StoredProcedure;

             
             // priprava parametru pro obrazek
             string jmenoSouboru;
             jmenoSouboru = FileUpload1.PostedFile.FileName;
             string[] castiJmena = jmenoSouboru.Split('\\');
             jmenoSouboru = castiJmena[castiJmena.Length - 1];


             SqlParameter prm_picture = new SqlParameter("@data", SqlDbType.Image);
             prm_picture.Value = Picture_content;
             cmd.Parameters.Add(prm_picture);

             SqlParameter prm_picture2 = new SqlParameter("@preview", SqlDbType.Image);
             prm_picture2.Value = PicturePreview_content;
             cmd.Parameters.Add(prm_picture2);

             SqlParameter prm_width = new SqlParameter("@width", SqlDbType.Int);
             prm_width.Value = SizePicture.Width;
             cmd.Parameters.Add(prm_width);

             SqlParameter prm_height = new SqlParameter("@height", SqlDbType.Int);
             prm_height.Value = SizePicture.Height;
             cmd.Parameters.Add(prm_height);

             SqlParameter prm_size = new SqlParameter("@size", SqlDbType.Int);
             prm_size.Value = FileUpload1.PostedFile.ContentLength;
             cmd.Parameters.Add(prm_size);

             SqlParameter prm_contenttype = new SqlParameter("@contenttype", SqlDbType.NChar, 60);
             prm_contenttype.Value = FileUpload1.PostedFile.ContentType;
             cmd.Parameters.Add(prm_contenttype);

             SqlParameter prm_category = new SqlParameter("@category", SqlDbType.NChar, 12);
             prm_category.Value = category;
             cmd.Parameters.Add(prm_category);

             SqlParameter prm_title = new SqlParameter("@title", SqlDbType.NChar, 60);
             prm_title.Value = title.Text;
             cmd.Parameters.Add(prm_title);

             SqlParameter prm_path = new SqlParameter("@datapath", SqlDbType.NChar, 250);
             prm_path.Value = jmenoSouboru;
             cmd.Parameters.Add(prm_path);


             //Otevřeme spojení do databáze a provedeme uloženou proceduru s parametry, které jsme si připravili.
             try
             {
                 conn.Open();
                 cmd.ExecuteNonQuery();
             }
             finally
             {
                 conn.Close();
             }



             Response.Redirect(redirectPath);

             //Error.Text = "Soubor byl uložen" + jmenoSouboru; 
         }
         else
         {
             //Error.Text = "Zvolený soubor není obrázek, nebo je příliš velký." + FileUpload1.PostedFile.ContentType + "  " + FileUpload1.PostedFile.ContentLength.ToString();
             Error.Text = "Zvolený soubor není obrázek, nebo je příliš velký.";
         }
         
 
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //prevod velikosti z B na kB (neni nezbytne)
            int velikost = int.Parse(((DataRowView)e.Row.DataItem)["size"].ToString());
            velikost = velikost / 1000;
            e.Row.Cells[2].Text = velikost.ToString();

            string id = e.Row.Cells[5].Text.ToString();
            string path = ((DataRowView)e.Row.DataItem)["datapath"].ToString();
            path = path.Trim();
            string img = "<img src='images/n/" + id + "/" + path + "' border='0' \\>";
            e.Row.Cells[4].Text = "<a href=\"javascript:FileBrowserDialogue.mySubmit('" + HomeUrl + "images/" + id + "/" + path + "')\">Select</a>";
            e.Row.Cells[5].Text = img;
        }
    }

    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {
      //  GridViewRow row = GridView1.SelectedRow;

      //  Label3.Text = "You selected " + row.Cells[5].Text + ".";
        //Label3.Text = GridView1.SelectedDataKey.ToString();
    }
    protected void Button2_Click(object sender, EventArgs e)
    {

            
            //nacteni souboru do pole
            byte[] blob = new byte[FileUpload2.PostedFile.ContentLength];
            FileUpload2.PostedFile.InputStream.Read(blob, 0, FileUpload2.PostedFile.ContentLength);

            // vytvoření spojení do databáze
            //načtení počtu řádků z databáze (skalární výseledek na SQL dotaz)
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["labsensnanoConnectionString"].ToString());

            // vytvoření SQL příkazu
            SqlCommand cmd = new SqlCommand("fileAdd", conn);
            // budeme používat uloženou proceduru
            cmd.CommandType = CommandType.StoredProcedure;


            // priprava parametru pro obrazek
            string jmenoSouboru;
            jmenoSouboru = FileUpload2.PostedFile.FileName;
            string[] castiJmena = jmenoSouboru.Split('\\');
            jmenoSouboru = castiJmena[castiJmena.Length - 1];
            
            SqlParameter prm_picture = new SqlParameter("@data", SqlDbType.VarBinary, blob.Length, ParameterDirection.Input, false, 0, 0, null, DataRowVersion.Current, blob);
            cmd.Parameters.Add(prm_picture);


            SqlParameter prm_size = new SqlParameter("@size", SqlDbType.Int);
            prm_size.Value = FileUpload2.PostedFile.ContentLength / 1000;
            cmd.Parameters.Add(prm_size);

            SqlParameter prm_contenttype = new SqlParameter("@contenttype", SqlDbType.NChar, 70);
            prm_contenttype.Value = FileUpload2.PostedFile.ContentType;
            cmd.Parameters.Add(prm_contenttype);

            SqlParameter prm_category = new SqlParameter("@category", SqlDbType.NChar, 12);
            prm_category.Value = category;
            cmd.Parameters.Add(prm_category);

            SqlParameter prm_title = new SqlParameter("@title", SqlDbType.NChar, 70);
            prm_title.Value = title.Text;
            cmd.Parameters.Add(prm_title);

            SqlParameter prm_path = new SqlParameter("@datapath", SqlDbType.NChar, 250);
            prm_path.Value = jmenoSouboru;
            cmd.Parameters.Add(prm_path);


            //Otevřeme spojení do databáze a provedeme uloženou proceduru s parametry, které jsme si připravili.
            try
            {
                conn.Open();
                cmd.ExecuteNonQuery();
            }
            finally
            {
                conn.Close();
            }
            Response.Redirect(redirectPath);
            //Error.Text = "Soubor byl uložen" + jmenoSouboru; 
    }

    protected void GridView2_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //unikatní id souboru
            string id = ((DataRowView)e.Row.DataItem)["id"].ToString();
            //název souboru
            string path = ((DataRowView)e.Row.DataItem)["datapath"].ToString();
            //odstranení přebytečných mezer
            path = path.Trim();
            e.Row.Cells[4].Text = "<a href=\"javascript:FileBrowserDialogue.mySubmit('" + HomeUrl + "download/" + id + "/" + path + "')\">Select</a>";
        }
    }


}