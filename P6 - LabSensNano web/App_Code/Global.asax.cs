﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
// pro culture
using System.Threading;
using System.Globalization;
// pro routing
using System.Web.Routing;

/// <summary>
/// Summary description for Global
/// </summary>
public class Global : System.Web.HttpApplication  
{
    
   
    /// <summary>
    /// Home URL webu
    /// </summary>
    /// <returns></returns>
    public static string HomeUrl()
    {
        //return "http://localhost/labsensnano/";
        return "";
    }

    //metoda pro routovaní obrázků
    public static void RegisterRoutes(RouteCollection routes)
    {
        

        routes.MapPageRoute("ImageLoadAdmin",
            "admin/images/{id}/{name}",
            "~/imgShow.aspx");
        routes.MapPageRoute("ImageLoad",
            "images/{id}/{name}",
            "~/imgShow.aspx");
        routes.MapPageRoute("ImagePreview",
            "images/n/{id}/{name}",
            "~/imgPreview.aspx");
        routes.MapPageRoute("ImageImgShow",
            "images/{db}/{id}/{name}",
            "~/imgImgShow.aspx?db=projects");
        routes.MapPageRoute("FileDownload",
            "download/{id}/{name}",
            "~/fileDownload.aspx");
    }

    // metoda pro registraci
    void Application_Start(object sender, EventArgs e)
    {
        RegisterRoutes(RouteTable.Routes);
    }

    private void Application_BeginRequest() {
        HttpCookie cookie = Request.Cookies["Culture"]; 
        if (cookie != null) { 
            CultureInfo culture = new CultureInfo(cookie.Value);
            Thread.CurrentThread.CurrentCulture = culture;
            Thread.CurrentThread.CurrentUICulture = culture; 
        }
    }

	public Global()
	{
		//
		// TODO: Add constructor logic here
		//
	}
}