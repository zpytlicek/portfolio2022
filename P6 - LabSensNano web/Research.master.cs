﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;

public partial class Research : System.Web.UI.MasterPage
{
    public string lang = "es";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (CultureInfo.CurrentUICulture.Name == "cs-CZ")
        {
            //nastavení sitemap hlavniho menu pro konkretni jazyk
            SiteMapDataSourceSubmenu.SiteMapProvider = "lsnMenuCs";
            SqlDataTeamy.SelectCommand = "SELECT titleCs, enable, id FROM teams WHERE (titleCs IS NOT NULL) AND (titleCs <> '\"\"') AND (enable = 1) ORDER BY titleCs";
            lang = "cs";
        }
        else
        {
            //nastavení sitemap hlavniho menu pro konkretni jazyk
            SiteMapDataSourceSubmenu.SiteMapProvider = "lsnMenuEn";
            SqlDataTeamy.SelectCommand = "SELECT titleEn, enable, id FROM teams WHERE (titleEn IS NOT NULL) AND (titleEn <> '\"\"') AND (enable = 1) ORDER BY titleEn";
            lang = "en";
        }
    }
    protected void DataList2_ItemDataBound(object sender, DataListItemEventArgs e)
    {

        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HyperLink link = (HyperLink)e.Item.FindControl("HyperLink3");
            if (lang == "cs")
            {
                link.Text = ((System.Data.DataRowView)e.Item.DataItem)["titleCs"].ToString();
                link.NavigateUrl = "Research.aspx?id=" + ((System.Data.DataRowView)e.Item.DataItem)["id"].ToString();
            } else {
                link.Text = ((System.Data.DataRowView)e.Item.DataItem)["titleEn"].ToString();
                link.NavigateUrl = "Research.aspx?id=" + ((System.Data.DataRowView)e.Item.DataItem)["id"].ToString();
            }
        }
    }
}
