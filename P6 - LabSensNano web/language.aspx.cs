﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Default4 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected override void OnInit(EventArgs e)
    {
        string culture = Request.QueryString["culture"];
        if ((culture != null) && (culture.Length > 0))
        {
            HttpCookie cookie = new HttpCookie("Culture");
            cookie.Value = culture; Response.Cookies.Add(cookie);
        }
        // Návrat na původní URL
        if (Request.UrlReferrer != null)
        { Response.Redirect(Request.UrlReferrer.ToString()); }
            else { Response.Redirect("~/"); } base.OnInit(e);
        }

        /*
        string returnUrl = Request.QueryString["returnUrl"]; 
        if ((returnUrl != null) && (returnUrl.Length > 0)) 
        { Response.Redirect(returnUrl); }
        else { Response.Redirect("~/"); } base.OnInit(e); }
        */
}