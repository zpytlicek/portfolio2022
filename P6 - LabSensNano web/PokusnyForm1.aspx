﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PokusnyForm1.aspx.cs" Inherits="PokusnyForm1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server" name="id">
    <div>
    
        <asp:Button ID="Button1" runat="server" onclick="Button1_Click" Text="Button" />
        <br />
        <asp:MultiView ID="MultiView1" runat="server">
            <asp:View ID="View1" runat="server">
                Krátký text
            </asp:View>
            <asp:View ID="View2" runat="server">
                Celý nějaký text
            </asp:View>
            <br />
            <br />
        </asp:MultiView>

        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:testDBConnectionString %>" 
            DeleteCommand="DELETE FROM Persony FROM Persony INNER JOIN knihovnik.knihovnici ON Persony.knihovnik = knihovnik.knihovnici.id WHERE (Persony.id = @id)" 
            InsertCommand="INSERT INTO Persony(enable, knihovnik, popis) VALUES (@enable, @knihovnik, @popis)" 
            SelectCommand="SELECT Persony.enable, Persony.popis, knihovnik.knihovnici.name, knihovnik.knihovnici.adresa, Persony.id, knihovnik.knihovnici.id AS nameId FROM Persony INNER JOIN knihovnik.knihovnici ON Persony.knihovnik = knihovnik.knihovnici.id" 
            
            UpdateCommand="UPDATE Persony SET enable = @enable, knihovnik = @knihovnik, popis = @popis FROM Persony INNER JOIN knihovnik.knihovnici ON Persony.knihovnik = knihovnik.knihovnici.id WHERE (Persony.id = @id)">
            <DeleteParameters>
                <asp:Parameter Name="id" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="enable" Type="Double" />
                <asp:Parameter Name="knihovnik" Type="Int32" />
                <asp:Parameter Name="popis" Type="String" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="enable" Type="Double" />
                <asp:Parameter Name="knihovnik" Type="Int32" />
                <asp:Parameter Name="popis" Type="String" />
                <asp:Parameter Name="id" Type="Int32" />
            </UpdateParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
            ConnectionString="<%$ ConnectionStrings:testDBConnectionString %>" 
            SelectCommand="SELECT [name] FROM [Knihovnici]"></asp:SqlDataSource>

        <asp:SqlDataSource ID="SqlDataSource4" runat="server" 
            ConnectionString="<%$ ConnectionStrings:testDBConnectionString %>" 
            DeleteCommand="DELETE FROM Persony FROM Persony INNER JOIN knihovnik.knihovnici ON Persony.knihovnik = knihovnik.knihovnici.id WHERE (Persony.id = @id)" 
            InsertCommand="INSERT INTO Persony(enable, knihovnik, popis) VALUES (@enable, @knihovnik, @popis)" 
            SelectCommand="SELECT COUNT(*) AS Expr1 FROM Persony WHERE (knihovnik = @idKnihovnika)" 
            
            
            UpdateCommand="UPDATE Persony SET enable = @enable, knihovnik = @knihovnik, popis = @popis FROM Persony INNER JOIN knihovnik.knihovnici ON Persony.knihovnik = knihovnik.knihovnici.id WHERE (Persony.id = @id)">
            <DeleteParameters>
                <asp:Parameter Name="id" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="enable" Type="Double" />
                <asp:Parameter Name="knihovnik" Type="Int32" />
                <asp:Parameter Name="popis" Type="String" />
            </InsertParameters>
            <SelectParameters>
                <asp:Parameter DefaultValue="0" Name="idKnihovnika" />
            </SelectParameters>
            <UpdateParameters>
                <asp:Parameter Name="enable" Type="Double" />
                <asp:Parameter Name="knihovnik" Type="Int32" />
                <asp:Parameter Name="popis" Type="String" />
                <asp:Parameter Name="id" Type="Int32" />
            </UpdateParameters>
        </asp:SqlDataSource>
        <br />
        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
            AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" 
            DataKeyNames="id,nameId" DataSourceID="SqlDataSource1" ForeColor="#333333" 
            GridLines="None" PageSize="5" 
            onselectedindexchanged="GridView1_SelectedIndexChanged">
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:CommandField ShowSelectButton="True" />
                <asp:BoundField DataField="enable" HeaderText="enable" 
                    SortExpression="enable" />
                <asp:BoundField DataField="popis" HeaderText="popis" SortExpression="popis" />
                <asp:BoundField DataField="name" HeaderText="name" SortExpression="name" />
                <asp:BoundField DataField="adresa" HeaderText="adresa" 
                    SortExpression="adresa" />
            </Columns>
            <EditRowStyle BackColor="#2461BF" />
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#EFF3FB" />
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#F5F7FB" />
            <SortedAscendingHeaderStyle BackColor="#6D95E1" />
            <SortedDescendingCellStyle BackColor="#E9EBEF" />
            <SortedDescendingHeaderStyle BackColor="#4870BE" />
        </asp:GridView>
        <br />
        <br />
        <br />
        <br />
    
    </div>
    <asp:TextBox ID="TextBox1" runat="server" AutoPostBack="True" 
        ontextchanged="TextBox1_TextChanged"></asp:TextBox>
    <asp:Button ID="Button2" runat="server" Text="Button" onclick="Button2_Click" />
    <br />
    <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
    <br />
    <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" 
        DataKeyNames="id" DataSourceID="SqlDataSource3" 
        EmptyDataText="There are no data records to display." AllowPaging="True" 
        AllowSorting="True" CellPadding="4" ForeColor="#333333" GridLines="None" 
        ShowFooter="True" onrowdeleting="GridView2_RowDeleting" PageSize="5">
        <AlternatingRowStyle BackColor="White" />
        <Columns>
            <asp:CommandField ShowEditButton="True" ShowSelectButton="True" 
                ShowDeleteButton="True" />
            <asp:TemplateField HeaderText="id" InsertVisible="False" SortExpression="id">
                <EditItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("id") %>'></asp:Label>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("id") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="name" SortExpression="name">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("name") %>'></asp:TextBox>
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="TextBoxName" runat="server" Width="194px"></asp:TextBox>
                </FooterTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("name") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="adresa" SortExpression="adresa">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("adresa") %>'></asp:TextBox>
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="TextBoxAdresa" runat="server"></asp:TextBox>
                    <asp:Button ID="Button3" runat="server" onclick="Button3_Click" Text="Add" />
                </FooterTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("adresa") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <EmptyDataTemplate>
            Žádná data
        </EmptyDataTemplate>
        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
        <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
        <SortedAscendingCellStyle BackColor="#FDF5AC" />
        <SortedAscendingHeaderStyle BackColor="#4D0000" />
        <SortedDescendingCellStyle BackColor="#FCF6C0" />
        <SortedDescendingHeaderStyle BackColor="#820000" />
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource3" runat="server" 
        ConnectionString="<%$ ConnectionStrings:testDBConnectionString %>" 
        DeleteCommand="DELETE FROM knihovnik.knihovnici WHERE [id] = @id" 
        InsertCommand="INSERT INTO knihovnik.knihovnici ([name], [adresa]) VALUES (@name, @adresa)" 
        ProviderName="<%$ ConnectionStrings:testDBConnectionString.ProviderName %>" 
        SelectCommand="SELECT id, name, adresa FROM knihovnik.knihovnici" 
        
        UpdateCommand="UPDATE knihovnik.knihovnici SET [name] = @name, [adresa] = @adresa WHERE [id] = @id">

        <DeleteParameters>
            <asp:Parameter Name="id" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="name" Type="String" />
            <asp:Parameter Name="adresa" Type="String" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="name" Type="String" />
            <asp:Parameter Name="adresa" Type="String" />
            <asp:Parameter Name="id" Type="Int32" />
        </UpdateParameters>

    </asp:SqlDataSource>
    <asp:DropDownList ID="DropDownList1" runat="server" 
        DataSourceID="SqlDataSource1" DataTextField="name" DataValueField="nameId">
        <asp:ListItem Value="0">all</asp:ListItem>
    </asp:DropDownList>
    <br />

    </form>
</body>
</html>
