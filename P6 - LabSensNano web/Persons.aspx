﻿<%@ Page Title="LabSensNano" Language="C#" MasterPageFile="~/introContents.master" AutoEventWireup="true" CodeFile="Persons.aspx.cs" Inherits="introTest" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SubMenu" Runat="Server">
    <table cellspacing='0' cellpadding='0' align='center' border='0' width='250'>
        <tr><td class='borderLeftTop' width='20'></td><td class='borderTop'></td><td class='borderRightTop' width='20'></td></tr>

        <tr>
        <td class='borderLeft' width='20'></td><td class='borderCenter submenu2' align="left">

            <asp:SiteMapDataSource ID="SiteMapDataSourceSubmenu" runat="server" 
                StartingNodeUrl="~/About.aspx" />
            <asp:Menu ID="Menu2" runat="server" DataSourceID="SiteMapDataSourceSubmenu" 
                MaximumDynamicDisplayLevels="0" StaticDisplayLevels="2" ItemWrap="True">
            </asp:Menu>


        </td><td class='borderRight' width='20'></td>
        </tr>
        <tr>
        <td class='borderLeftBottom' width='20'></td><td class='borderBottom'></td><td class='borderRightBottom' width='20'></td>
        </tr>
    </table>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="Main" Runat="Server">
    <table cellspacing='0' cellpadding='0' align='center' border='0' width='786'>
        <tr><td class='borderLeftTop' width='20'></td><td class='borderTop'></td><td class='borderRightTop' width='20'></td></tr>

        <tr><td class='borderLeft' width='20'></td>
        <td class='borderCenter' style='padding-left:10px;padding-right:10px;'>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                ConnectionString="<%$ ConnectionStrings:labsensnanoConnectionString %>" 
                SelectCommand="SELECT [name], [surname], [degreeBefor], [degreeAfter], [email], [enable], [class] FROM [persons] WHERE (([enable] = @enable) AND ([class] &lt;&gt; @class)) ORDER BY [class], [surname]">
                <SelectParameters>
                    <asp:Parameter DefaultValue="true" Name="enable" Type="Boolean" />
                    <asp:Parameter DefaultValue="1" Name="class" Type="Int32" />
                </SelectParameters>
            </asp:SqlDataSource>
            <br />
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
                CellPadding="4" DataSourceID="SqlDataSource1" ForeColor="#333333" 
                GridLines="None" Width="711px">
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                <Columns>
                    <asp:TemplateField HeaderText="Jméno">
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" 
                                
                                Text='<%# Eval("degreeBefor") + " " + Eval("name") + " " + Eval("surname") + "" + Eval("degreeAfter") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Email">
                        <ItemTemplate>
                            <asp:HyperLink ID="HyperLink1" runat="server" 
                                NavigateUrl='<%# "mailto:" + Eval("email") %>' Text='<%# Eval("email") %>'></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EditRowStyle BackColor="#999999" />
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                <SortedAscendingCellStyle BackColor="#E9E7E2" />
                <SortedAscendingHeaderStyle BackColor="#506C8C" />
                <SortedDescendingCellStyle BackColor="#FFFDF8" />
                <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
            </asp:GridView>
        </td>
        <td class='borderRight' width='20'></td></tr>
       

        <tr><td class='borderLeftBottom' width='20'></td><td class='borderBottom'></td><td class='borderRightBottom' width='20'></td></tr>
    </table>
    <asp:HiddenField ID="HiddenField1" runat="server" />
</asp:Content>

