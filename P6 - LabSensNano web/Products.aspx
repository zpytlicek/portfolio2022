﻿<%@ Page Title="LabSensNano" Language="C#" MasterPageFile="~/introContents.master" AutoEventWireup="true" CodeFile="Products.aspx.cs" Inherits="introTest" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SubMenu" Runat="Server">
    <table cellspacing='0' cellpadding='0' align='center' border='0' width='300'>
        <tr><td class='borderLeftTop' width='20'></td><td class='borderTop'></td><td class='borderRightTop' width='20'></td></tr>

        <tr>
        <td class='borderLeft' width='20'></td><td class='borderCenter submenu2'>

            <asp:SiteMapDataSource ID="SiteMapDataSourceSubmenu" runat="server" 
                StartingNodeUrl="~/Products.aspx" />
            <asp:Menu ID="Menu2" runat="server" DataSourceID="SiteMapDataSourceSubmenu" 
                MaximumDynamicDisplayLevels="1" StaticDisplayLevels="2">
            </asp:Menu>


        </td><td class='borderRight' width='20'></td>
        </tr>
        <tr>
        <td class='borderLeftBottom' width='20'></td><td class='borderBottom'></td><td class='borderRightBottom' width='20'></td>
        </tr>
    </table>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="Main" Runat="Server">
    <table cellspacing='0' cellpadding='0' align='center' border='0' width='736'>
        <tr><td class='borderLeftTop' width='20'></td><td class='borderTop'></td><td class='borderRightTop' width='20'></td></tr>

        <tr><td class='borderLeft' width='20'></td>
        <td class='borderCenter'>
            <asp:MultiView ID="MultiView1" runat="server">
                <asp:View ID="View1" runat="server">
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:labsensnanoConnectionString %>" 
                        
                        
                        SelectCommand="SELECT products.id, products.titleCs AS title, products.typProduktu, productTyps.typNameCs AS typName, products.enable, YEAR(products.publication) AS published FROM products INNER JOIN productTyps ON products.typProduktu = productTyps.id WHERE (products.enable = 1) ORDER BY published DESC, title">
                        <SelectParameters>
                            <asp:Parameter DefaultValue="true" Name="enable" Type="Boolean" />
                            <asp:Parameter DefaultValue="1" Name="class" Type="Int32" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <h2>
                        <asp:Label ID="Label3" runat="server" Text="<%$ Resources:labels, products %>"></asp:Label>
                    </h2>
                    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
                        AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" 
                        DataKeyNames="id" DataSourceID="SqlDataSource1" ForeColor="#333333" 
                        GridLines="None" Width="650px" PageSize="25">
                        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                        <Columns>


                            <asp:TemplateField HeaderText="Název produktu" SortExpression="title">
                                <HeaderTemplate>
                                    <asp:Label ID="Label4" runat="server" Text="<%$ Resources:labels, nazev %>"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" style="text-align: center" 
                                        Text='<%# Bind("published") %>'></asp:Label>
                                    &nbsp;<asp:HyperLink ID="HyperLink2" runat="server" 
                                        NavigateUrl='<%# "products.aspx?id="+Eval("id") %>' 
                                        Text='<%# Eval("title") %>'></asp:HyperLink>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("titleCs") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <FooterStyle BackColor="#0170A2" />
                                <HeaderStyle BackColor="#0170A2" HorizontalAlign="Left" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Typ produktu" SortExpression="typName">
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("typName") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("typName") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <HeaderTemplate>
                                    <asp:Label ID="Label5" runat="server" Text="<%$ Resources:labels, typ %>"></asp:Label>
                                </HeaderTemplate>
                                <FooterStyle BackColor="#0170A2" />
                                <HeaderStyle BackColor="#0170A2" />
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                            </asp:TemplateField>
                        </Columns>
                        <EditRowStyle BackColor="#999999" />
                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#F7F6F3" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#0170A2" ForeColor="White" HorizontalAlign="Center" />
                        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#E9E7E2" />
                        <SortedAscendingHeaderStyle BackColor="#506C8C" />
                        <SortedDescendingCellStyle BackColor="#FFFDF8" />
                        <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                    </asp:GridView>
                </asp:View>
                <asp:View ID="View2" runat="server">
                    <table style="width:100%;">
                        <tr>
                            <td align="left">
                                
                               <h2><asp:Label ID="titulek" runat="server" Text="titulek"></asp:Label></h2>
                                   <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
                                       ConnectionString="<%$ ConnectionStrings:labsensnanoConnectionString %>" 
                                       SelectCommand="SELECT [titleCs], [keyWordsCs], [typProduktu], [descriptionCs], [parametrsCs] FROM [products] WHERE ([id] = @id)">
                                       <SelectParameters>
                                           <asp:QueryStringParameter DefaultValue="0" Name="id" QueryStringField="id" 
                                               Type="Int32" />
                                       </SelectParameters>
                                   </asp:SqlDataSource>

                                
                                <asp:SqlDataSource ID="SqlDataSource3" runat="server" 
                                    ConnectionString="<%$ ConnectionStrings:labsensnanoConnectionString %>" 
                                    
                                    SelectCommand="SELECT rsPersonsProducts.productId, persons.name, persons.surname, persons.degreeBefor, persons.degreeAfter FROM rsPersonsProducts INNER JOIN persons ON rsPersonsProducts.personId = persons.id WHERE (rsPersonsProducts.productId = @id) ORDER BY rsPersonsProducts.id">
                                    <SelectParameters>
                                        <asp:QueryStringParameter DefaultValue="0" Name="id" QueryStringField="id" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                                <asp:SqlDataSource ID="SqlDataSource4" runat="server" 
                                    ConnectionString="<%$ ConnectionStrings:labsensnanoConnectionString %>" 
                                    SelectCommand="SELECT rsProjectsProducts.productId, projects.nick FROM rsProjectsProducts INNER JOIN projects ON rsProjectsProducts.projectId = projects.id WHERE (rsProjectsProducts.productId = @id)">
                                    <SelectParameters>
                                        <asp:QueryStringParameter DefaultValue="0" Name="id" QueryStringField="id" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                                <asp:Label ID="Label13" runat="server" style="font-weight: 700" 
                                    Text="<%$ Resources:labels, published %>"></asp:Label>
                                <b>:</b>
                                <asp:Label ID="published" runat="server" Text="Label"></asp:Label>
                                <br />
                                <asp:Label ID="Label1" runat="server" Text="<%$ Resources:labels, autori %>" 
                                    style="font-weight: 700"></asp:Label>
                                <asp:DataList ID="DataList1" runat="server" DataSourceID="SqlDataSource3" 
                                    RepeatDirection="Horizontal" RepeatLayout="Flow">
                                    <ItemTemplate>
                                        <asp:Label ID="Label2" runat="server" 
                                            
                                            Text='<%# Eval("degreeBefor") + " " + Eval("name") + " " + Eval("surname") + Eval("degreeAfter") %>'></asp:Label>
                                    </ItemTemplate>
                                    <SeparatorTemplate>
                                        ,
                                    </SeparatorTemplate>
                                </asp:DataList>

                                
                                <asp:Label ID="Label12" runat="server" Text="<%$ Resources:labels, keywords %>" 
                                    style="font-weight: 700"></asp:Label>
                                <asp:Label ID="keywords" runat="server" Text="Label"></asp:Label>

                                
                                <br />

                                
                                </td>

                        </tr>
                        <tr>
                            <td>
                                   
                                  <h4 style="padding-bottom:0px;padding-top:15px;">
                                      <asp:Label ID="Label6" runat="server" Text="<%$ Resources:labels, popis %>"></asp:Label>
                                  </h4>
                            </td>

                        </tr>
                        <tr>
                            <td style="padding:10px;padding-top:0px;">
                            
                                <asp:Label ID="Popis" runat="server" Text="Label"></asp:Label>
                            
                            </td>

                        </tr>
                                                <tr>
                            <td>
                                   
                                  <h4 style="padding-bottom:0px;padding-top:0px;">
                                      <asp:Label ID="Label9" runat="server" Text="<%$ Resources:labels, parametry %>"></asp:Label>
                                  </h4>
                            </td>

                        </tr>
                        <tr>
                            <td style="padding:10px;padding-top:0px;">
                            
                                <asp:Label ID="parametry" runat="server" Text="Label"></asp:Label>
                            
                            </td>

                        </tr>
                        <tr>
                            <td style="padding:0px;padding-top:0px;">
                            
                                <h4>
                                    <asp:Label ID="Label10" runat="server" Text="<%$ Resources:labels, projekty %>"></asp:Label>
                                </h4>
                                <asp:DataList ID="DataList2" runat="server" DataSourceID="SqlDataSource4" 
                                    RepeatDirection="Horizontal" RepeatLayout="Flow">
                                    <ItemTemplate>
                                        <asp:Label ID="Label11" runat="server" Text='<%# Eval("nick") %>'></asp:Label>
                                    </ItemTemplate>
                                    <SeparatorTemplate>
                                        ,
                                    </SeparatorTemplate>
                                </asp:DataList>
                            
                            </td>

                        </tr>

                    </table>

                    <center>
                        <h3>
                            &nbsp;</h3>
                        <h3>
                            <asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="~/Products.aspx" 
                                Text="<%$ Resources:labels, back %>"></asp:HyperLink>
                        </h3>
                    </center>
                    <br>
                    <br></br>
                    </br>
                </asp:View>
            </asp:MultiView>
        </td>
        <td class='borderRight' width='20'></td></tr>

        <tr><td class='borderLeftBottom' width='20'></td><td class='borderBottom'></td><td class='borderRightBottom' width='20'></td></tr>
    </table>
</asp:Content>

