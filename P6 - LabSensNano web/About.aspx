﻿<%@ Page Title="LabSensNano" Language="C#" MasterPageFile="~/introContents.master" AutoEventWireup="true" CodeFile="About.aspx.cs" Inherits="introTest" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SubMenu" Runat="Server">
    <table cellspacing='0' cellpadding='0' align='center' border='0' width='250'>
        <tr><td class='borderLeftTop' width='20'></td><td class='borderTop'></td><td class='borderRightTop' width='20'></td></tr>

        <tr>
        <td class='borderLeft' width='20'></td><td class='borderCenter submenu2' align="left">

            <asp:SiteMapDataSource ID="SiteMapDataSourceSubmenu" runat="server" 
                StartingNodeUrl="~/About.aspx" />
            <asp:Menu ID="Menu2" runat="server" DataSourceID="SiteMapDataSourceSubmenu" 
                MaximumDynamicDisplayLevels="0" StaticDisplayLevels="2" ItemWrap="True">
            </asp:Menu>


        </td><td class='borderRight' width='20'></td>
        </tr>
        <tr>
        <td class='borderLeftBottom' width='20'></td><td class='borderBottom'></td><td class='borderRightBottom' width='20'></td>
        </tr>
    </table>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="Main" Runat="Server">
    <table cellspacing='0' cellpadding='0' align='center' border='0' width='786'>
        <tr><td class='borderLeftTop' width='20'></td><td class='borderTop'></td><td class='borderRightTop' width='20'></td></tr>

        <tr><td class='borderLeft' width='20'></td>
        <td class='borderCenter' style='padding-left:10px;padding-right:10px;'>
            <asp:Label ID="contents" runat="server" Text="contents"></asp:Label>
        </td>
        <td class='borderRight' width='20'></td></tr>
       

        <tr><td class='borderLeftBottom' width='20'></td><td class='borderBottom'></td><td class='borderRightBottom' width='20'></td></tr>
    </table>
    <asp:HiddenField ID="HiddenField1" runat="server" />
</asp:Content>

