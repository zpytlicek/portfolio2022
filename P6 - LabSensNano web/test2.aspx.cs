﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class test2 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //Get Application Instance from Current Content
        HttpApplication httpApps = HttpContext.Current.ApplicationInstance;
        //Get List of modules in module collections
        HttpModuleCollection httpModuleCollections = httpApps.Modules;
        Response.Write("Total Number Active HttpModule : " + httpModuleCollections.Count.ToString() + "</br>");
        Response.Write("<b>List of Active Modules</b>" + "</br>");
        
        foreach (string activeModule in httpModuleCollections.AllKeys)
        {
            Response.Write(activeModule + "</br>");
        }
        Response.Write(Request.ServerVariables["SERVER_SOFTWARE"] + "</br>");
    }
}