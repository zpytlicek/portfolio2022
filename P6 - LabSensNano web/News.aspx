﻿<%@ Page Title="LabSensNano" Language="C#" MasterPageFile="~/introContents.master" AutoEventWireup="true" CodeFile="News.aspx.cs" Inherits="introTest" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SubMenu" Runat="Server">
    <table cellspacing='0' cellpadding='0' align='center' border='0' width='300'>
        <tr><td class='borderLeftTop' width='20'></td><td class='borderTop'></td><td class='borderRightTop' width='20'></td></tr>

        <tr>
        <td class='borderLeft' width='20'></td><td class='borderCenter submenu'>

            <asp:SiteMapDataSource ID="SiteMapDataSourceSubmenu" runat="server" 
                StartingNodeUrl="~/News.aspx" />
            <asp:Menu ID="Menu2" runat="server" DataSourceID="SiteMapDataSourceSubmenu" 
                MaximumDynamicDisplayLevels="0" StaticDisplayLevels="2">
            </asp:Menu>


        </td><td class='borderRight' width='20'></td>
        </tr>
        <tr>
        <td class='borderLeftBottom' width='20'></td><td class='borderBottom'></td><td class='borderRightBottom' width='20'></td>
        </tr>
    </table>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="Main" Runat="Server">
    <table cellspacing='0' cellpadding='0' align='center' border='0' width='736'>
        <tr><td class='borderLeftTop' width='20'></td><td class='borderTop'></td><td class='borderRightTop' width='20'></td></tr>

        <tr><td class='borderLeft' width='20'></td>
        <td class='borderCenter' height="1px" style="height: 1px">
            <asp:DataList ID="DataList1" runat="server" DataKeyField="id" 
                DataSourceID="SqlDataSource1" Width="679px">
                <ItemTemplate>
                    <asp:Label ID="titleLabel" runat="server" Font-Size="Small" ForeColor="#00A4E3" 
                        Text='<%# Eval("title") %>' Font-Bold="True" />
                    <table style="width:675px;padding:0px;margin:0px; ">
                        <tr>
                            <td style="padding-left: 15px;padding-right: 15px;">
                                <asp:Label ID="textLabel" runat="server" Text='<%# Eval("text") %>' />
                            </td>
                        </tr>
                    </table>
                   </ItemTemplate>
                <SeparatorTemplate>
                    <hr style="border: 1px solid #ddd;" />
                </SeparatorTemplate>
            </asp:DataList>
                        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                            ConnectionString="<%$ ConnectionStrings:labsensnanoConnectionString %>" 
                            ProviderName="<%$ ConnectionStrings:labsensnanoConnectionString.ProviderName %>" 
                            
                SelectCommand="SELECT TOP (5) id, title, lang, dateStart, dateEnd, text FROM news WHERE (enable = 1) AND (dateEnd &gt;= dateStart) AND (lang = @lang) AND (title &lt;&gt; '&quot;&quot;') AND (dateEnd &gt; GETDATE()) AND (dateStart &lt; GETDATE()) OR (dateStart = GETDATE()) ORDER BY dateStart DESC">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="HiddenField1" DefaultValue="cs" Name="lang" 
                                    PropertyName="Value" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                        <asp:HiddenField ID="HiddenField1" runat="server" />
        </td>
        <td class='borderRight' width='20'></td></tr>

        <tr><td class='borderLeftBottom' width='20'></td><td class='borderBottom'></td><td class='borderRightBottom' width='20'></td></tr>
    </table>
</asp:Content>

