﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

//kultura
using System.Globalization;
using System.Threading;
//pro zmenu SiteMapProvider
using System.Configuration;

using System.Globalization;
using System.Data.SqlClient;
using System.Data;

public partial class introTest : System.Web.UI.Page
{
    string page = "res";
    public string lang = "es";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (CultureInfo.CurrentUICulture.Name == "cs-CZ")
        {
            //nastavení sitemap hlavniho menu pro konkretni jazyk
            lang = "cs";
        }
        else
        {
            //nastavení sitemap hlavniho menu pro konkretni jazyk
            lang = "en";
        }
        string id = "";
        try
        {
            id = Request.QueryString["id"].ToString();
        }
        catch { }
        if (id != "")
        {
            //pokud je vybrán tým
            MultiView1.SetActiveView(View2);

            SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["labsensnanoConnectionString"].ToString());
            SqlCommand cmdSelect;
            if (lang == "cs"){
                cmdSelect = new SqlCommand("SELECT [id], [titleCs] AS title, [textCs] AS text, [leader], [enable] FROM [teams] WHERE (([id] = '" + id + "') AND ([enable] = 'true'))", dbConn);
            } else {
                cmdSelect = new SqlCommand("SELECT [id], [titleEn] AS title, [textEn] AS text, [leader], [enable] FROM [teams] WHERE (([id] = '" + id + "') AND ([enable] = 'true'))", dbConn);
            }

            if (!IsPostBack)
            {
                try
                {
               /*     cmdSelect.CommandType = System.Data.CommandType.Text;
                    SqlParameter prm_lang = new SqlParameter("@lang", SqlDbType.VarChar);
                    prm_lang.Value = lang;
                    cmdSelect.Parameters.Add(prm_lang);
                    SqlParameter prm_page = new SqlParameter("@page", SqlDbType.VarChar);
                    prm_page.Value = page;
                    cmdSelect.Parameters.Add(prm_page);*/

                    dbConn.Open();

                    //kontrolní výpis proměné o počtu výskytů 
                    SqlDataReader data = cmdSelect.ExecuteReader();
                    if (data.Read())
                    {
                        if (data["title"].ToString() == "")
                        {
                            Response.Redirect("Research.aspx");
                        }
                        else
                        {

                            // Vypis dat pro konkrétní stránku
                            Page.Title = "LabSensNano - " + data["title"].ToString();
                            TeamTitle.Text = data["title"].ToString();
                            content.Text = data["text"].ToString();
                            

                            // obsluha labelu členové
                            if (DataListManager.Items.Count == 0)
                            {
                                br1.Visible = false;
                                DataListManager.Visible = false;
                            }
                            else
                            {
                                br1.Text = "</br>";
                            }
                            if (DataListMembers.Items.Count == 0)
                            {
                                DataListMembers.Visible = false;

                            }
                        }
                    }
                    else
                    {
                        Response.Redirect("Research.aspx");
                    }

                }
                catch
                {
                    TeamTitle.Text = "Chyba komunikace s databází LabSensNano.";
                    dbConn.Close();
                    Response.Redirect("Research.aspx");
                }
            }

        }
        else
        {
            //pokud není vybrán tým
            MultiView1.SetActiveView(View1);

            if (!IsPostBack)
            {

                SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["labsensnanoConnectionString"].ToString());
                try
                {
                    SqlCommand cmdSelect = new SqlCommand("SELECT [contents], [title] FROM [pages] WHERE (([lang] = @lang) AND ([page] = @page))", dbConn);
                    cmdSelect.CommandType = System.Data.CommandType.Text;
                    SqlParameter prm_lang = new SqlParameter("@lang", SqlDbType.VarChar);
                    prm_lang.Value = lang;
                    cmdSelect.Parameters.Add(prm_lang);
                    SqlParameter prm_page = new SqlParameter("@page", SqlDbType.VarChar);
                    prm_page.Value = page;
                    cmdSelect.Parameters.Add(prm_page);

                    dbConn.Open();

                    //kontrolní výpis proměné o počtu výskytů 
                    SqlDataReader data = cmdSelect.ExecuteReader();
                    if (data.Read())
                    {
                        Page.Title = data["title"].ToString();
                        contents.Text = data["contents"].ToString();
                    }

                }
                catch
                {
                    contents.Text = "Chyba komunikace s databází LabSensNano.";
                    dbConn.Close();
                }
            }
        }

        
    }
}