﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ImageList.aspx.cs" Inherits="Default6" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>File Browser</title>
    <style type="text/css">
        .style1
        {
            width: 53px;
        }
    </style>
</head>

                                              


<body style="height: 167px">
    <form id="my_form" runat="server">
    <div>
    <script language="javascript" type="text/javascript" src="Scripts/tinymce/tiny_mce_popup.js"></script>
    <script language="javascript" type="text/javascript">
    var FileBrowserDialogue = {
        init: function () {
            // Here goes your code for setting your custom things onLoad.  
        },
        mySubmit: function () {
            var URL = document.my_form.my_field.value;
            var win = tinyMCEPopup.getWindowArg("window");
            // insert information now 
            win.document.getElementById(tinyMCEPopup.getWindowArg("input")).value = URL;
            // are we an image browser  
            if (typeof (win.ImageDialog) != "undefined") {
                // we are, so update image dimensions and preview if necessary  
                if (win.ImageDialog.getImageData) win.ImageDialog.getImageData();
                if (win.ImageDialog.showPreviewImage) win.ImageDialog.showPreviewImage(URL);
            }        // close popup window  
            tinyMCEPopup.close();
        }
    }

    tinyMCEPopup.onInit.add(FileBrowserDialogue.init, FileBrowserDialogue);
    </script>
                                            
         <br />

    <a href="javascript:FileBrowserDialogue.mySubmit()">test</a>
    <input type="button" value="Odeslat2" onClick="FileBrowserDialogue.mySubmit();" />
        <br />
        <br />
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
            DataKeyNames="id" DataSourceID="SqlDataSource1">
            <Columns>
                <asp:BoundField DataField="title" HeaderText="title" SortExpression="title" />
                <asp:BoundField DataField="size" HeaderText="size" SortExpression="size" />
                <asp:BoundField DataField="inserted" HeaderText="inserted" 
                    SortExpression="inserted" />
                <asp:BoundField DataField="datapath" HeaderText="datapath" 
                    SortExpression="datapath" />
                <asp:DynamicField DataField="preview" HeaderText="preview" />
            </Columns>
        </asp:GridView>
        <br />
        <hr />
        <table style="width:100%;">
            <tr>
                <td align=left class="style1">
                    <asp:Label ID="Label1" runat="server" Text="Soubor: "></asp:Label></td>
                <td>
                    <asp:FileUpload ID="FileUpload1" runat="server" Width="330px" />
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:labsensnanoConnectionString %>" 
                        DeleteCommand="ImageDel" DeleteCommandType="StoredProcedure" 
                        InsertCommand="ImageAdd" InsertCommandType="StoredProcedure" 
                        SelectCommand="SELECT title, size, inserted, preview, datapath, id FROM images">
                        <DeleteParameters>
                            <asp:Parameter Name="id" Type="Int32" />
                        </DeleteParameters>
                        <InsertParameters>
                            <asp:Parameter Name="title" Type="String" />
                            <asp:Parameter Name="contenttype" Type="String" />
                            <asp:Parameter Name="datapath" Type="String" />
                            <asp:Parameter Name="category" Type="String" />
                            <asp:Parameter Name="data" Type="Object" />
                            <asp:Parameter Name="preview" Type="Object" />
                            <asp:Parameter Name="width" Type="Int32" />
                            <asp:Parameter Name="height" Type="Int32" />
                            <asp:Parameter Name="size" Type="Int32" />
                        </InsertParameters>
                    </asp:SqlDataSource>
                </td>
            </tr>
            <tr>
                <td align=left class="style1">
                    <asp:Label ID="Label2" runat="server" Text="Popis: "></asp:Label></td>
                <td>
                    <asp:TextBox ID="title" runat="server" Width="330px"></asp:TextBox>
                    <asp:Button 
                        ID="Button1" runat="server" onclick="Button1_Click" Text="Odeslat" />
                    </td>
            </tr>
        </table>

        <asp:Label ID="Error" runat="server" ForeColor="Red"></asp:Label>
        <br />

    </div>
    </form>
</body>
</html>
