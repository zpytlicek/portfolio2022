﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

public partial class Admin_Products : System.Web.UI.Page
{
    string id="";
    string fileid = "";
    string puvodniDatum;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            id = Request.QueryString["id"].ToString();
            fileid = Request.QueryString["files"].ToString();
        }
        catch { }
                    // zobrazeni edit stránky
        if (id != "")
        {
            MultiView1.SetActiveView(View2);
            MultiView2.SetActiveView(View3);
            SqlDataSource2.SelectParameters["id"].DefaultValue = id;
             if (!IsPostBack)
             {
                SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["labsensnanoConnectionString"].ToString());
                try
                {
                    SqlCommand cmdSelect = new SqlCommand("SELECT titleCs, id, titleEn, keyWordsCs, publication, keyWordsEn, typProduktu, descriptionCs, descriptionEn, parametrsCs, parametrsEn, enable FROM products WHERE (id = @id)", dbConn);
                    cmdSelect.CommandType = System.Data.CommandType.Text;
                    SqlParameter prm_id = new SqlParameter("@id", SqlDbType.VarChar);
                    prm_id.Value = id;
                    cmdSelect.Parameters.Add(prm_id);

                    dbConn.Open();

                    //kontrolní výpis proměné o počtu výskytů 
                    SqlDataReader data = cmdSelect.ExecuteReader();
                    if (data.Read())
                    {
                        TextBox2.Text = data["titleCs"].ToString();
                        TextBox3.Text = data["titleEn"].ToString();
                        popisCs.Text = data["descriptionCs"].ToString();
                        popisEn.Text = data["descriptionEn"].ToString();
                        paramEn.Text = data["parametrsEn"].ToString();
                        paramCs.Text = data["parametrsCs"].ToString();
                        keyWordsCs.Text = data["keyWordsCs"].ToString();
                        keyWordsEn.Text = data["keyWordsEn"].ToString();
                        DropDownList1.SelectedValue = data["typProduktu"].ToString();

                        //nacteni data z databaze do editacniho pole
                        DateTime datumDate = DateTime.Parse(data["publication"].ToString());
                        datum.Text = datumDate.ToString("dd.MM.yyyy");

                        puvodniDatum = data["publication"].ToString();

                        //nacteni bin hodnoty do checked pole
                        publikovat.Checked = bool.Parse(data["enable"].ToString());
                    }
                    
                } catch {
                    dbConn.Close();
                }
            }
        }
        else
        {
            MultiView1.SetActiveView(View1);
        }
        try
        {
            Button10.Attributes.Add("onclick", "return ConfirmOnDelete()");
        }
        catch
        {
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        SqlDataSource1.InsertParameters["titleCs"].DefaultValue = TextBox1.Text;
        SqlDataSource1.Insert();
        SqlDataSource1.DataBind();
        GridView1.DataBind();
        TextBox1.Text = "";
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        MultiView2.SetActiveView(View3);
    }
    protected void Button3_Click(object sender, EventArgs e)
    {
        MultiView2.SetActiveView(View4);
    }
    protected void Button4_Click(object sender, EventArgs e)
    {
        MultiView2.SetActiveView(View5);
    }
    protected void Button5_Click(object sender, EventArgs e)
    {
        MultiView2.SetActiveView(View6);
    }
    protected void Button7_Click(object sender, EventArgs e)
    {
        Response.Redirect("editProducts.aspx");
    }
    protected void Button6_Click(object sender, EventArgs e)
    {
        SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["labsensnanoConnectionString"].ToString());
        try
        {

            SqlCommand cmdSet = new SqlCommand("UPDATE products SET modifed = @lastchange, publication = @publication, titleCs = @titleCs, titleEn = @titleEn, keyWordsCs = @keyWordsCs, keyWordsEn = @keyWordsEn, typProduktu = @typProduktu, descriptionCs = @descriptionCs, descriptionEn = @descriptionEn, parametrsCs = @parametrsCs, parametrsEn = @parametrsEn, enable = @enable WHERE (id = @id)", dbConn);
           
            cmdSet.CommandType = System.Data.CommandType.Text;

            SqlParameter prm_titleCs = new SqlParameter("@titleCs", SqlDbType.VarChar);
            prm_titleCs.Value = TextBox2.Text;
            cmdSet.Parameters.Add(prm_titleCs);



          
            SqlParameter prm_publication = new SqlParameter("@publication", SqlDbType.DateTime);
            //načtení data z položky
            try
            {
                string[] datumCasti = datum.Text.Split('.');
                DateTime datumPublication = new DateTime(Int32.Parse(datumCasti[2]), Int32.Parse(datumCasti[1]), Int32.Parse(datumCasti[0]));
                prm_publication.Value = datumPublication;
            }
            catch
            {
                prm_publication.Value = puvodniDatum;
            }
            cmdSet.Parameters.Add(prm_publication);

            SqlParameter prm_titleEn = new SqlParameter("@titleEn", SqlDbType.VarChar);
            prm_titleEn.Value = TextBox3.Text;
            cmdSet.Parameters.Add(prm_titleEn);

            SqlParameter prm_keyWordsCs = new SqlParameter("@keyWordsCs", SqlDbType.VarChar);
            prm_keyWordsCs.Value = keyWordsCs.Text;
            cmdSet.Parameters.Add(prm_keyWordsCs);

            SqlParameter prm_keyWordsEn = new SqlParameter("@keyWordsEn", SqlDbType.VarChar);
            prm_keyWordsEn.Value = keyWordsEn.Text;
            cmdSet.Parameters.Add(prm_keyWordsEn);

            SqlParameter prm_enable = new SqlParameter("@enable", SqlDbType.Bit);
            prm_enable.Value = publikovat.Checked;
            cmdSet.Parameters.Add(prm_enable);

            SqlParameter prm_typProduktu = new SqlParameter("@typProduktu", SqlDbType.Int);
            prm_typProduktu.Value = Int16.Parse(DropDownList1.SelectedValue.ToString());
            cmdSet.Parameters.Add(prm_typProduktu);

            SqlParameter prm_descriptionCs = new SqlParameter("@descriptionCs", SqlDbType.Text);
            prm_descriptionCs.Value = popisCs.Text;
            cmdSet.Parameters.Add(prm_descriptionCs);

            SqlParameter prm_descriptionEn = new SqlParameter("@descriptionEn", SqlDbType.Text);
            prm_descriptionEn.Value = popisEn.Text;
            cmdSet.Parameters.Add(prm_descriptionEn);

            SqlParameter prm_parametrsCs = new SqlParameter("@parametrsCs", SqlDbType.Text);
            prm_parametrsCs.Value = paramCs.Text;
            cmdSet.Parameters.Add(prm_parametrsCs);
            SqlParameter prm_parametrsEn = new SqlParameter("@parametrsEn", SqlDbType.Text);
            prm_parametrsEn.Value = paramEn.Text;
            cmdSet.Parameters.Add(prm_parametrsEn);

            SqlParameter prm_id = new SqlParameter("@id", SqlDbType.Int);
            prm_id.Value = Int16.Parse(id);
            cmdSet.Parameters.Add(prm_id);
/**************************************/
/**************************************/
            
            //nastavení data poslední zmeny
            SqlParameter prm_lastchange = new SqlParameter("@lastchange", SqlDbType.DateTime);
            prm_lastchange.Value = DateTime.Now;
            cmdSet.Parameters.Add(prm_lastchange);
            

            dbConn.Open();

            //kontrolní výpis proměné o počtu výskytů 
            cmdSet.ExecuteNonQuery().ToString();

            Response.Redirect("editProducts.aspx?id=" + id + "&files=" + fileid);

        }
        catch
        {
            Label1.Text = "Chyba komunikace s databází";
            dbConn.Close();
        }
    }

    protected void Button8_Click(object sender, EventArgs e)
    {
        SqlDataSource5.InsertParameters["personId"].DefaultValue = PersonId.SelectedValue;
        SqlDataSource5.InsertParameters["productId"].DefaultValue = id;
        SqlDataSource5.Insert();
        SqlDataSource5.DataBind();
        GridView2.DataBind();
    }

    protected void Button9_Click(object sender, EventArgs e)
    {
        SqlDataSource7.Insert();
        SqlDataSource7.DataBind();
        GridView3.DataBind();
    }

    protected void Button10_Click1(object sender, EventArgs e)
    {
        SqlDataSource2.Delete();
        Response.Redirect("editProducts.aspx");
    }

}