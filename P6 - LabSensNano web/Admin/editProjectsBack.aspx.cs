﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_editProjects : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        DetailsView1.DefaultMode = DetailsViewMode.ReadOnly;

        if (!IsPostBack)
        {
            DetailsView1.DefaultMode = DetailsViewMode.Insert;
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {

    }
    protected void DetailsView1_ItemInserted(object sender, DetailsViewInsertedEventArgs e)
    {
        SqlDataSource1.DataBind();
        GridView1.DataBind();
    }
    
    
    protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        DetailsView1.DefaultMode = DetailsViewMode.Edit;
    }
    protected void DetailsView1_ItemInserting(object sender, DetailsViewInsertEventArgs e)
    {
    }
    protected void Calendar2_Load(object sender, EventArgs e)
    {
  
    }
}