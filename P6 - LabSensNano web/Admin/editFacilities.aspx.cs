﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

public partial class Admin_Products : System.Web.UI.Page
{
    string id="";
    string fileid = "";
    string puvodniDatum;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            id = Request.QueryString["id"].ToString();
        }
        catch { }
                    // zobrazeni edit stránky
        if (id != "")
        {
            MultiView1.SetActiveView(View2);
            MultiView2.SetActiveView(View3);

            if (!IsPostBack)
            {

                try
                {
                    DataView dv = (DataView)SqlDataDetail.Select(DataSourceSelectArguments.Empty);
                    if (dv.Count > 0)
                    {
                        //SELECT enable, textEn, textCs, typCs, nameEn, nameCs, typEn FROM facilities WHERE (id = @id)
                        DataRowView data = dv[0];
                        EditNameCs.Text = data["nameCs"].ToString();
                        EditNameEn.Text = data["nameEn"].ToString();
                        EditTypCs.Text = data["typCs"].ToString();
                        EditTypEn.Text = data["typEn"].ToString();
                        EditTextCs.Text = data["textCs"].ToString();
                        EditTextEn.Text = data["textEn"].ToString();
                        publikovat.Checked = bool.Parse(data["enable"].ToString());
                        owner.SelectedValue = data["owner"].ToString();
                    }

                    
                } catch {
                }
            }
        }
        else
        {
            MultiView1.SetActiveView(View1);
        }

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        SqlDataSeznam.Insert();
        SqlDataSeznam.DataBind();
        GridViewSeznam.DataBind();
        NewName.Text = "";

    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        MultiView2.SetActiveView(View3);
    }
    protected void Button3_Click(object sender, EventArgs e)
    {
        MultiView2.SetActiveView(View4);
    }

    protected void Button7_Click(object sender, EventArgs e)
    {
        Response.Redirect("editFacilities.aspx");
    }
    protected void Button6_Click(object sender, EventArgs e)
    {
        try
        {
            SqlDataDetail.Update(); 
            //Response.Redirect("editFacilities.aspx?id=" + id);

        }
        catch
        {
            Label1.Text = "Chyba komunikace s databází";
            Response.Redirect("editFacilities.aspx?id=" + id);
        }
    }

    protected void Button8_Click(object sender, EventArgs e)
    {
        SqlDataDetail.Delete();
        Response.Redirect("editFacilities.aspx");
    }
}