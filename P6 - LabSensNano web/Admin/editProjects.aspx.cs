﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

//pro úpravu obrázků
using System.IO;
using System.Drawing;
using System.Configuration;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;

public partial class Admin_Products : System.Web.UI.Page
{
    private static ImageFormat GetFormat(string filename)
    {
        if (filename.EndsWith("jpg") || filename.EndsWith("jpeg") || filename.EndsWith("tiff"))
            return ImageFormat.Jpeg;

        if (filename.EndsWith("gif"))
            return ImageFormat.Gif;

        return ImageFormat.Png;
    }
    private bool ThumbnailCallback()
    {
        return false;
    }

    string id="";
    string puvodniDatumKonec;
    string puvodniDatumStart;

    string imgName = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            id = Request.QueryString["id"].ToString();
        }
        catch { }
                    // zobrazeni edit stránky
        if (id != "")
        {
            MultiView1.SetActiveView(View2);
            MultiView2.SetActiveView(View3);
            SqlDataSource2.SelectParameters["id"].DefaultValue = id;
             if (!IsPostBack)
             {
                SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["labsensnanoConnectionString"].ToString());
                try
                {
                    SqlCommand cmdSelect = new SqlCommand("SELECT imgName, id, titleCs, titleEn, textCs, textEn, nick, enable, start, finish FROM projects WHERE (id = @id)", dbConn);
                    cmdSelect.CommandType = System.Data.CommandType.Text;
                    SqlParameter prm_id = new SqlParameter("@id", SqlDbType.VarChar);
                    prm_id.Value = id;
                    cmdSelect.Parameters.Add(prm_id);

                    dbConn.Open();

                    //kontrolní výpis proměné o počtu výskytů 
                    SqlDataReader data = cmdSelect.ExecuteReader();
                    if (data.Read())
                    {
                        if (data["imgName"] == DBNull.Value)
                        {
                            LogoImg.Visible = false;
                            ButtonDeletLogo.Visible = false;
                        }
                        else
                        {
                            LogoImg.ImageUrl = "~" + "/images/projects/" + id + "/" + data["imgName"].ToString();
                        }

                        titleCs.Text = data["titleCs"].ToString();
                        titleEn.Text = data["titleEn"].ToString();
                        popisCs.Text = data["textCs"].ToString();
                        popisEn.Text = data["textEn"].ToString();
                        nick.Text = data["nick"].ToString();

                        //nacteni data z databaze do editacniho pole
                        DateTime datumDate = DateTime.Parse(data["finish"].ToString());
                        DateKonec.Text = datumDate.ToString("dd.MM.yyyy");
                        puvodniDatumKonec = data["finish"].ToString();

                        DateTime datumDate2 = DateTime.Parse(data["start"].ToString());
                        DateStart.Text = datumDate2.ToString("dd.MM.yyyy");
                        puvodniDatumStart = data["start"].ToString();


                        //nacteni bin hodnoty do checked pole
                        publikovat.Checked = bool.Parse(data["enable"].ToString());
                    }
                    
                } catch {
                    DateKonec.Text = puvodniDatumKonec;
                    DateStart.Text = puvodniDatumStart;
                    dbConn.Close();
                }
            }
        }
        else
        {
            MultiView1.SetActiveView(View1);
        }

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        SqlDataSource1.InsertParameters["titleCs"].DefaultValue = TextBoxTitleCs.Text;
        SqlDataSource1.InsertParameters["nick"].DefaultValue = TextBoxID.Text;
        SqlDataSource1.Insert();
        SqlDataSource1.DataBind();
        GridView1.DataBind();

    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        MultiView2.SetActiveView(View3);
    }
    protected void Button3_Click(object sender, EventArgs e)
    {
        MultiView2.SetActiveView(View4);
    }

    protected void Button7_Click(object sender, EventArgs e)
    {
        Response.Redirect("editProjects.aspx");
    }
    protected void Button6_Click(object sender, EventArgs e)
    {
        SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["labsensnanoConnectionString"].ToString());
        try
        {

            SqlCommand cmdSet = new SqlCommand("UPDATE projects SET titleEn = @titleEn, titleCs = @titleCs, textCs = @textCs, textEn = @textEn, nick = @nick, enable = @enable, start = @start, finish = @finish, lastchange = @lastchange WHERE (id = @id)", dbConn);

            cmdSet.CommandType = System.Data.CommandType.Text;

            SqlParameter prm_nick = new SqlParameter("@nick", SqlDbType.VarChar);
            prm_nick.Value = nick.Text;
            cmdSet.Parameters.Add(prm_nick);

            SqlParameter prm_titleCs = new SqlParameter("@titleCs", SqlDbType.VarChar);
            prm_titleCs.Value = titleCs.Text;
            cmdSet.Parameters.Add(prm_titleCs);

            SqlParameter prm_titleEn = new SqlParameter("@titleEn", SqlDbType.VarChar);
            prm_titleEn.Value = titleEn.Text;
            cmdSet.Parameters.Add(prm_titleEn);

            SqlParameter prm_textCs = new SqlParameter("@textCs", SqlDbType.Text);
            prm_textCs.Value = popisCs.Text;
            cmdSet.Parameters.Add(prm_textCs);

            SqlParameter prm_textEn = new SqlParameter("@textEn", SqlDbType.Text);
            prm_textEn.Value = popisEn.Text;
            cmdSet.Parameters.Add(prm_textEn);

            SqlParameter prm_start = new SqlParameter("@start", SqlDbType.DateTime);
            //načtení data z položky
            try
            {
                string[] datumCastiS = DateStart.Text.Split('.');
                DateTime datumStart = new DateTime(Int32.Parse(datumCastiS[2]), Int32.Parse(datumCastiS[1]), Int32.Parse(datumCastiS[0]));
                prm_start.Value = datumStart;
            }
            catch
            {
                prm_start.Value = puvodniDatumStart;
            }
            cmdSet.Parameters.Add(prm_start);

            SqlParameter prm_finish = new SqlParameter("@finish", SqlDbType.DateTime);
            //načtení data z položky
            try
            {
                string[] datumCastiK = DateKonec.Text.Split('.');
                DateTime datumKonec = new DateTime(Int32.Parse(datumCastiK[2]), Int32.Parse(datumCastiK[1]), Int32.Parse(datumCastiK[0]));
                prm_finish.Value = datumKonec;
            }
            catch
            {
                prm_finish.Value = puvodniDatumKonec;
            }
            cmdSet.Parameters.Add(prm_finish);

            SqlParameter prm_enable = new SqlParameter("@enable", SqlDbType.Bit);
            prm_enable.Value = publikovat.Enabled;
            prm_enable.Value = publikovat.Checked;
            cmdSet.Parameters.Add(prm_enable);

            SqlParameter prm_id = new SqlParameter("@id", SqlDbType.Int);
            prm_id.Value = Int16.Parse(id);
            cmdSet.Parameters.Add(prm_id);
            /**************************************/
            /**************************************/

            //nastavení data poslední zmeny
            SqlParameter prm_lastchange = new SqlParameter("@lastchange", SqlDbType.DateTime);
            prm_lastchange.Value = DateTime.Now;
            cmdSet.Parameters.Add(prm_lastchange);


            dbConn.Open();
            cmdSet.ExecuteNonQuery().ToString();

        }
        catch
        {
            Label1.Text = "Chyba komunikace s databází";
            dbConn.Close();
        }
        finally
        {
            Response.Redirect("editProjects.aspx?id=" + id);
        }
            
    }

    protected void ButtonDeletLogo_Click(object sender, EventArgs e)
    {
        SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["labsensnanoConnectionString"].ToString());

        SqlCommand cmdSet = new SqlCommand("UPDATE projects SET imgName = @imgName, img = @img WHERE (id = @id)", dbConn);

            cmdSet.CommandType = System.Data.CommandType.Text;

            //vymazání obrázku
            SqlParameter prm_img = new SqlParameter("@img", SqlDbType.Image);
            prm_img.Value = DBNull.Value;
            cmdSet.Parameters.Add(prm_img);

            SqlParameter prm_imgName = new SqlParameter("@imgName", SqlDbType.VarChar);
            prm_imgName.Value = DBNull.Value;
            cmdSet.Parameters.Add(prm_imgName);

            SqlParameter prm_id = new SqlParameter("@id", SqlDbType.Int);
            prm_id.Value = Int16.Parse(id);
            cmdSet.Parameters.Add(prm_id);

            dbConn.Open();
            cmdSet.ExecuteNonQuery();
            try
            {
        }
        catch
        {
            dbConn.Close();
            Response.Redirect("editProjects.aspx?id=" + id);
        }
    }
    protected void ButtonAddLogo_Click(object sender, EventArgs e)
    {
         string ContentType;
         ContentType=FileUploadImg.PostedFile.ContentType;
         if ((ContentType == "image/jpeg" || ContentType == "image/gif" || ContentType == "image/pjpeg" || ContentType == "image/png" || ContentType == "image/x-png") && FileUploadImg.PostedFile.ContentLength < 1000000)
         {
             //nacteni obrázku do pole
             byte[] Picture_content;
             Picture_content = new byte[FileUploadImg.PostedFile.ContentLength];
             FileUploadImg.PostedFile.InputStream.Read(Picture_content, 0, FileUploadImg.PostedFile.ContentLength);

             // vytvoření spojení do databáze
             SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["labsensnanoConnectionString"].ToString());

             // vytvoření SQL příkazu
             SqlCommand cmd = new SqlCommand("ProjectLogoAdd", conn);
             // budeme používat uloženou proceduru
             cmd.CommandType = CommandType.StoredProcedure;


             // priprava parametru pro obrazek
             string jmenoSouboru;
             jmenoSouboru = FileUploadImg.PostedFile.FileName;
             string[] castiJmena = jmenoSouboru.Split('\\');
             jmenoSouboru = castiJmena[castiJmena.Length - 1];


             SqlParameter prm_name = new SqlParameter("@imgName", SqlDbType.VarChar, 350);
             prm_name.Value = jmenoSouboru;
             cmd.Parameters.Add(prm_name);

             SqlParameter prm_picture = new SqlParameter("@img", SqlDbType.Image);
             prm_picture.Value = Picture_content;
             cmd.Parameters.Add(prm_picture);

             SqlParameter prm_id = new SqlParameter("@id", SqlDbType.Int);
             prm_id.Value = Int16.Parse(id);
             cmd.Parameters.Add(prm_id);

                 conn.Open();
                 cmd.ExecuteNonQuery();
                 try
                 {
             }
             finally
             {
                 conn.Close();
                 Response.Redirect("editProjects.aspx?id=" + id);
             }

         }
    }
}