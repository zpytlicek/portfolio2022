﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

//pro úpravu obrázků
using System.IO;
using System.Drawing;
using System.Configuration;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;

public partial class Admin_Products : System.Web.UI.Page
{
    private static ImageFormat GetFormat(string filename)
    {
        if (filename.EndsWith("jpg") || filename.EndsWith("jpeg") || filename.EndsWith("tiff"))
            return ImageFormat.Jpeg;

        if (filename.EndsWith("gif"))
            return ImageFormat.Gif;

        return ImageFormat.Png;
    }
    private bool ThumbnailCallback()
    {
        return false;
    }

    string id="";
    string puvodniDatumKonec;
    string puvodniDatumStart;

    string imgName = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            id = Request.QueryString["id"].ToString();
        }
        catch { }
                    // zobrazeni edit stránky
        if (id != "")
        {
            MultiView1.SetActiveView(View2);
            MultiView2.SetActiveView(View3);



             SqlDataDetail.SelectParameters["id"].DefaultValue = id;
             if (!IsPostBack)
             {
                try
                {
                    //SELECT id, lang, title, dateStart, text, dateEnd, enable FROM news WHERE (id = @id)
                    DataView dv = (DataView)SqlDataDetail.Select(DataSourceSelectArguments.Empty);
                    if (dv.Count > 0){
                        DataRowView data = dv[0];
                        NewTitleCs.Text = data["titleCs"].ToString();
                        NewEnable.Checked = bool.Parse(data["enable"].ToString());
                        NewTitleEn.Text = data["titleEn"].ToString();
                        NewTextCs.Text = data["textCs"].ToString();
                        NewTextEn.Text = data["textEn"].ToString();
                       // NewTitleEn.Text = data["leader"].ToString();
                        if (data["leader"].ToString() == "")
                        {
                            DropDownListPersons.SelectedValue = "null";
                        }
                        else
                        {
                            DropDownListPersons.SelectedValue = data["leader"].ToString();
                        }
                        
                        //parsovaní data
                    /*    DateTime datumDate = DateTime.Parse(data["dateEnd"].ToString());
                        NewEnd.Text = datumDate.ToString("dd.MM.yyyy");
                        puvodniDatumKonec = data["dateEnd"].ToString();*/
                       
                    }
                } catch {
                }
            }
        }
        else
        {
            MultiView1.SetActiveView(View1);
        }

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        //SqlDataSourceSeznam.InsertParameters["titleCs"].DefaultValue = titleCs1.Text;
        SqlDataSourceSeznam.Insert();
        SqlDataSourceSeznam.DataBind();
        GridView1.DataBind();

    }

    protected void Button7_Click(object sender, EventArgs e)
    {
        Response.Redirect("editTeams.aspx");
    }
    protected void Button6_Click(object sender, EventArgs e)
    {
      
        try
        {
            SqlDataDetail.UpdateParameters["lastchange"].DbType = DbType.DateTime;
            SqlDataDetail.UpdateParameters["lastchange"].DefaultValue = DateTime.Now.ToString();
            SqlDataDetail.UpdateParameters["leader"].DbType = DbType.Int32;
            if (DropDownListPersons.SelectedValue == "null")
            {
                SqlDataDetail.UpdateParameters["leader"].DefaultValue = DBNull.Value.ToString();
            }
            else
            {
                SqlDataDetail.UpdateParameters["leader"].DefaultValue = DropDownListPersons.SelectedValue.ToString();
            }


            SqlDataDetail.Update();
                       //načtení data z položky
            /*  SqlDataDetail.UpdateParameters["dateStart"].DbType = DbType.DateTime;
              try
              {
                  string[] datumCastiS = NewStart.Text.Split('.');
                  DateTime datumStart = new DateTime(Int32.Parse(datumCastiS[2]), Int32.Parse(datumCastiS[1]), Int32.Parse(datumCastiS[0]));
                  SqlDataDetail.UpdateParameters["dateStart"].DefaultValue = datumStart.ToString();
              }
              catch
              {
                  SqlDataDetail.UpdateParameters["dateStart"].DefaultValue = puvodniDatumStart;
              }*/

        }
        finally
        {
            Response.Redirect("editTeams.aspx?id=" + id);
        }
            
    }


    protected void Button8_Click(object sender, EventArgs e)
    {
        SqlDataDetail.Delete();
        Response.Redirect("editTeams.aspx");
    }
    protected void Button9_Click(object sender, EventArgs e)
    {
        MultiView2.SetActiveView(View3);
    }
    protected void Button10_Click(object sender, EventArgs e)
    {
        MultiView2.SetActiveView(View4);
    }
    protected void DropDownList1_DataBound(object sender, EventArgs e)
    {

    }
    protected void Button11_Click(object sender, EventArgs e)
    {
       // SqlDataSource5.InsertParameters["personId"].DefaultValue = PersonId.SelectedValue;
       // SqlDataSource5.InsertParameters["teamId"].DefaultValue = id;
        SqlDataSource5.Insert();
        SqlDataSource5.DataBind();
        GridView2.DataBind();
    }
}