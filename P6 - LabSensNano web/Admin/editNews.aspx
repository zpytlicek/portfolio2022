﻿<%@ Page Title="" Language="C#" validateRequest="false" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeFile="editNews.aspx.cs" Inherits="Admin_Products" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <!-- DATA PRO VLOŽENÍ KALENDÁŘE -->
  <link rel="stylesheet" href="datedit/datedit.css" type="text/css" />
  <script type="text/javascript" charset="iso-8859-1" src="datedit/datedit.js"></script>
  <script type="text/javascript" charset="utf-8" src="datedit/lang/cz.js"></script>
  <script type="text/javascript">
      datedit("MainContent_NewEnd", "dd.mm.yyyy", false);
      datedit("MainContent_NewStart", "dd.mm.yyyy", false);
      datedit_VALIDATE_OUTPUT = false;    
  </script>

<!-- TinyMCE -->
<script type="text/javascript" src="../Scripts/tinymce/tiny_mce.js"></script>
<script type="text/javascript">

    function myFileBrowser(field_name, url, type, win) {
        // alert("Field_Name: " + field_name + "nURL: " + url + "nType: " + type + "nWin: " + win);
        // debug/testing    
        /* If you work with sessions in PHP and your client doesn't accept cookies you might need to carry  
        the session name and session ID in the request string (can look like this: "?PHPSESSID=88p0n70s9dsknra96qhuk6etm5"). 
        These lines of code extract the necessary parameters and add them back to the filebrowser URL again. */

        var cmsURL = 'fileList.aspx?cat=<%= "news" + Request.QueryString["id"] %>';
        // var cmsURL = window.location.toString();    // script URL - use an absolute path!    
        if (cmsURL.indexOf("?") < 0) {
            //add the type as the only query parameter    
            cmsURL = cmsURL + "?type=" + type;
        } else {
            //add the type as an additional query parameter     
            // (PHP session ID is now included if there is one at all)       
            cmsURL = cmsURL + "&type=" + type;
        }
        tinyMCE.activeEditor.windowManager.open({
            file: cmsURL,
            title: "File Browser",
            width: 900,  // Your dimensions may differ - toy around with them!
            height: 400,
            resizable: "yes",
            inline: "yes",  // This parameter only has an effect if you use the inlinepopups plugin!  
            close_previous: "yes"
        },
     { window: win,
         input: field_name
     });
        return false;
    }

    tinyMCE.init({
        // General options
        mode: "exact",
        // MUSI BYT VCETNE SPECIFIKACE OKNA RODICE
        elements: "MainContent_NewText",
        theme: "advanced",
        plugins: "imagemanager,autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave",

        //vypnuti zmeny URL na relativni
        convert_urls: false,
        // external_image_list_url : "image.php5?x=",
        //  init_instance_callback : "init",

        // Theme options
        theme_advanced_buttons1: "newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
        theme_advanced_buttons2: "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
        theme_advanced_buttons3: "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,advhr,|,print,|,ltr,rtl,|,fullscreen",
        theme_advanced_buttons4: "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,pagebreak,restoredraft",
        theme_advanced_toolbar_location: "top",
        theme_advanced_toolbar_align: "left",
        theme_advanced_statusbar_location: "bottom",
        theme_advanced_resizing: true,


        // Example content CSS (should be your site CSS)
        content_css: "../layout/layoutContent.css",

        //extjsfilemanager_handlerurl: 'BrowserHandler.ashx',
        //Optional - Any extra parameters you want to have passed to your server-side handler on each request from the FileManager.
        //extjsfilemanager_extraparams: { param1: 'value1', param2: 'value2' }

        // Style formats
        /*   style_formats: [
        { title: 'Bold text', inline: 'b' },
        { title: 'Red text', inline: 'span', styles: { color: '#ff0000'} },
        { title: 'Red header', block: 'h1', styles: { color: '#ff0000'} },
        { title: 'Example 1', inline: 'span', classes: 'example1' },
        { title: 'Example 2', inline: 'span', classes: 'example2' },
        { title: 'Table styles' },
        { title: 'Table row 1', selector: 'tr', classes: 'tablerow1' }
        ],*/
        file_browser_callback: "myFileBrowser"
    });


</script>


<!-- /TinyMCE -->




    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    
        <h2><asp:Label ID="Label1" runat="server" Text="Správa novinek"></asp:Label></h2>
        <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="1">
            <asp:View ID="View1" runat="server">
                <h3>
                    <asp:Label ID="Label2" runat="server" Text="Přidat novinku :"></asp:Label>
                </h3>
                <p>
                    <asp:Label ID="Label11" runat="server" 
                        Text="Titulek novinky:"></asp:Label>
                    <asp:TextBox ID="Title1" runat="server" Width="682px"></asp:TextBox>
                    <asp:DropDownList ID="Lang1" runat="server">
                        <asp:ListItem Value="cs">Česká</asp:ListItem>
                        <asp:ListItem Value="en">Anglická</asp:ListItem>
                    </asp:DropDownList>
                    <asp:Button ID="Button1" runat="server" onclick="Button1_Click" Text="Přidat" />
                </p>
                <br />
                <br />
                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
                    CellPadding="4" DataKeyNames="id" DataSourceID="SqlDataSourceSeznam" 
                    ForeColor="#333333" GridLines="None" AllowSorting="True" 
                    AllowPaging="True">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField HeaderText="Titulek" SortExpression="title">
                            <ItemTemplate>
                                <asp:HyperLink ID="HyperLink1" runat="server" 
                                    NavigateUrl='<%# "editNews.aspx?id=" + Eval("id") %>' 
                                    Text='<%# Eval("title") %>'></asp:HyperLink>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("title") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="lang" HeaderText="Mutace" SortExpression="lang">
                        </asp:BoundField>
                        <asp:BoundField DataField="dateStart" DataFormatString="{0:d}" 
                            HeaderText="Začátek" SortExpression="dateStart" />
                        <asp:BoundField DataField="dateEnd" DataFormatString="{0:d}" HeaderText="Konec" 
                            SortExpression="dateEnd" />
                        <asp:CheckBoxField DataField="enable" HeaderText="Publikovat na WWW" 
                            SortExpression="enable" />
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
                <asp:SqlDataSource ID="SqlDataSourceSeznam" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:labsensnanoConnectionString %>" 
                    InsertCommand="INSERT INTO news(title, lang) VALUES (@title, @lang)" 
                    
                    SelectCommand="SELECT title, lang, id, dateStart, dateEnd, enable FROM news ORDER BY dateStart DESC, dateEnd DESC" 
                    
                    
                    
                    
                    UpdateCommand="UPDATE news SET title = @title, lang = @lang, dateStart = @dateStart, text = @text, dateEnd = @dateEnd, enable = @enable WHERE (id = @id)">
                    <InsertParameters>
                        <asp:Parameter Name="title" />
                        <asp:Parameter Name="lang" />
                    </InsertParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="title" />
                        <asp:Parameter Name="lang" />
                        <asp:Parameter Name="dateStart" />
                        <asp:Parameter Name="text" />
                        <asp:Parameter Name="dateEnd" />
                        <asp:Parameter Name="enable" />
                        <asp:Parameter Name="id" />
                    </UpdateParameters>
                </asp:SqlDataSource>
            </asp:View>
            <asp:View ID="View2" runat="server">
                <asp:SqlDataSource ID="SqlDataDetail" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:labsensnanoConnectionString %>" 
                    ProviderName="<%$ ConnectionStrings:labsensnanoConnectionString.ProviderName %>" 
                    
                    SelectCommand="SELECT id, lang, title, dateStart, text, dateEnd, enable FROM news WHERE (id = @id)" 
                    
                    
                    UpdateCommand="UPDATE news SET title = @title, lang = @lang, dateStart = @dateStart, dateEnd = @dateEnd, enable = @enable, text = @text WHERE (id = @id)" 
                    DeleteCommand="NewsDel" DeleteCommandType="StoredProcedure">
                    <DeleteParameters>
                        <asp:QueryStringParameter DefaultValue="0" Name="id" QueryStringField="id" 
                            Type="Int32" />
                        <asp:Parameter Direction="ReturnValue" Name="RETURN_VALUE" Type="Int32" />
                    </DeleteParameters>
                    <SelectParameters>
                        <asp:QueryStringParameter DefaultValue="0" Name="id" QueryStringField="id" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:ControlParameter ControlID="NewTitle" Name="title" PropertyName="Text" />
                        <asp:ControlParameter ControlID="NewLang" DefaultValue="cs" Name="lang" 
                            PropertyName="SelectedValue" />
                        <asp:Parameter Name="dateStart" />
                        <asp:Parameter Name="dateEnd" />
                        <asp:ControlParameter ControlID="NewEnable" DefaultValue="0" Name="enable" 
                            PropertyName="Checked" />
                        <asp:ControlParameter ControlID="NewText" DefaultValue="" Name="text" 
                            PropertyName="Text" />
                        <asp:QueryStringParameter DefaultValue="0" Name="id" QueryStringField="id" />
                    </UpdateParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="SqlDataSource3" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:labsensnanoConnectionString %>" 
                    ProviderName="<%$ ConnectionStrings:labsensnanoConnectionString.ProviderName %>" 
                    SelectCommand="SELECT [typNameCs], [id] FROM [productTyps]">
                </asp:SqlDataSource>
                <asp:Label ID="Label3" runat="server" Text="Titulek novinky: "></asp:Label>
                <asp:TextBox ID="NewTitle" runat="server" Width="696px"></asp:TextBox>
                <asp:DropDownList ID="NewLang" runat="server">
                    <asp:ListItem Value="cs">Česká</asp:ListItem>
                    <asp:ListItem Value="en">Anglická</asp:ListItem>
                </asp:DropDownList>
                <br />
                <br/>
                <asp:Label ID="Label13" runat="server" Text="Zaháhejní:"></asp:Label>
&nbsp;<asp:TextBox ID="NewStart" runat="server"></asp:TextBox>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Label ID="Label8" runat="server" Text="Ukončení:"></asp:Label>
                &nbsp;<asp:TextBox ID="NewEnd" runat="server"></asp:TextBox>
                &nbsp;&nbsp;&nbsp;
                <asp:CheckBox ID="NewEnable" runat="server" Text="Puvlikovat na WWW" />
                <br />
                <br />
                <asp:Label ID="Label16" runat="server" Text="Text novinky:"></asp:Label>
                <br />
                <asp:TextBox ID="NewText" runat="server" Height="287px" Width="900px"></asp:TextBox>
                <br />
                <br />
                <table style="width:100%;" align="center">
                    <tr>
                        <td align="center">
                            &nbsp;
                            <asp:Button ID="Button7" runat="server" onclick="Button7_Click" 
                                style="text-align: center" Text="Zpět na seznam" Width="159px" />
                            &nbsp;&nbsp;
                            <asp:Button ID="Button8" runat="server" onclick="Button8_Click" 
                                onclientclick="return confirm('Opravdu smazat?')" Text="Smazat novinku" />
                            &nbsp;
                            <asp:Button ID="Button6" runat="server" onclick="Button6_Click" 
                                style="text-align: center" Text="Uložit změny" Width="121px" />
                            &nbsp;&nbsp; </td>
                    </tr>

                </table>
                <hr />
                <br />

            </asp:View>
        </asp:MultiView>
    
    <br />
    </asp:Content>

