﻿<%@ Page Title="" Language="C#" validateRequest="false" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeFile="editProjects.aspx.cs" Inherits="Admin_Products" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <!-- DATA PRO VLOŽENÍ KALENDÁŘE -->
  <link rel="stylesheet" href="datedit/datedit.css" type="text/css" />
  <script type="text/javascript" charset="iso-8859-1" src="datedit/datedit.js"></script>
  <script type="text/javascript" charset="utf-8" src="datedit/lang/cz.js"></script>
  <script type="text/javascript">
      datedit("MainContent_DateKonec", "dd.mm.yyyy", false);
      datedit("MainContent_DateStart", "dd.mm.yyyy", false);
      datedit_VALIDATE_OUTPUT = false;    
  </script>

<!-- TinyMCE -->
<script type="text/javascript" src="../Scripts/tinymce/tiny_mce.js"></script>
<script type="text/javascript">

    function myFileBrowser(field_name, url, type, win) {
        // alert("Field_Name: " + field_name + "nURL: " + url + "nType: " + type + "nWin: " + win);
        // debug/testing    
        /* If you work with sessions in PHP and your client doesn't accept cookies you might need to carry  
        the session name and session ID in the request string (can look like this: "?PHPSESSID=88p0n70s9dsknra96qhuk6etm5"). 
        These lines of code extract the necessary parameters and add them back to the filebrowser URL again. */

        var cmsURL = 'fileList.aspx?cat=<%= "proj" + Request.QueryString["id"] %>';
        // var cmsURL = window.location.toString();    // script URL - use an absolute path!    
        if (cmsURL.indexOf("?") < 0) {
            //add the type as the only query parameter    
            cmsURL = cmsURL + "?type=" + type;
        } else {
            //add the type as an additional query parameter     
            // (PHP session ID is now included if there is one at all)       
            cmsURL = cmsURL + "&type=" + type;
        }
        tinyMCE.activeEditor.windowManager.open({
            file: cmsURL,
            title: "File Browser",
            width: 900,  // Your dimensions may differ - toy around with them!
            height: 400,
            resizable: "yes",
            inline: "yes",  // This parameter only has an effect if you use the inlinepopups plugin!  
            close_previous: "yes"
        },
     { window: win,
         input: field_name
     });
        return false;
    }

    tinyMCE.init({
        // General options
        mode: "exact",
        // MUSI BYT VCETNE SPECIFIKACE OKNA RODICE
        elements: "MainContent_popisCs,MainContent_popisEn",
        theme: "advanced",
        plugins: "imagemanager,autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave",

        //vypnuti zmeny URL na relativni
        convert_urls: false,
        // external_image_list_url : "image.php5?x=",
        //  init_instance_callback : "init",

        // Theme options
        theme_advanced_buttons1: "newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
        theme_advanced_buttons2: "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
        theme_advanced_buttons3: "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,advhr,|,print,|,ltr,rtl,|,fullscreen",
        theme_advanced_buttons4: "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,pagebreak,restoredraft",
        theme_advanced_toolbar_location: "top",
        theme_advanced_toolbar_align: "left",
        theme_advanced_statusbar_location: "bottom",
        theme_advanced_resizing: true,


        // Example content CSS (should be your site CSS)
        content_css: "../layout/layoutContent.css",

        //extjsfilemanager_handlerurl: 'BrowserHandler.ashx',
        //Optional - Any extra parameters you want to have passed to your server-side handler on each request from the FileManager.
        //extjsfilemanager_extraparams: { param1: 'value1', param2: 'value2' }

        // Style formats
        /*   style_formats: [
        { title: 'Bold text', inline: 'b' },
        { title: 'Red text', inline: 'span', styles: { color: '#ff0000'} },
        { title: 'Red header', block: 'h1', styles: { color: '#ff0000'} },
        { title: 'Example 1', inline: 'span', classes: 'example1' },
        { title: 'Example 2', inline: 'span', classes: 'example2' },
        { title: 'Table styles' },
        { title: 'Table row 1', selector: 'tr', classes: 'tablerow1' }
        ],*/
        file_browser_callback: "myFileBrowser"
    });


</script>


<!-- /TinyMCE -->




    <style type="text/css">
        .style1
        {
            height: 25px;
        }
    </style>




</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    
        <h2><asp:Label ID="Label1" runat="server" Text="Správa projektů"></asp:Label></h2>
        <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="1">
            <asp:View ID="View1" runat="server">
                <h3>
                    <asp:Label ID="Label2" runat="server" Text="Přidat nový projekt :"></asp:Label>
                </h3>
                <p>
                    <asp:Label ID="Label11" runat="server" 
                        Text="ID:"></asp:Label>
                    <asp:TextBox ID="TextBoxID" runat="server" Width="285px"></asp:TextBox>
                </p>
                <p>
                    <asp:Label ID="Label12" runat="server" 
                        Text="Název česky:"></asp:Label>
                <asp:TextBox ID="TextBoxTitleCs" runat="server" Width="610px"></asp:TextBox>
                <asp:Button ID="Button1" runat="server" onclick="Button1_Click" Text="Přidat" />
                </p>
                <br />
                <br />
                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
                    CellPadding="4" DataKeyNames="id" DataSourceID="SqlDataSource1" 
                    ForeColor="#333333" GridLines="None" AllowSorting="True" 
                    AllowPaging="True">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField HeaderText="ID" SortExpression="nick">
                            <ItemTemplate>
                                <asp:HyperLink ID="HyperLink2" runat="server" 
                                    NavigateUrl='<%# "editProjects.aspx?id=" + Eval("id") %>' 
                                    Text='<%# Eval("nick") %>'></asp:HyperLink>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("nick") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Název CS" SortExpression="titleCs">
                            <ItemTemplate>
                                <asp:HyperLink ID="HyperLink1" runat="server" 
                                    NavigateUrl='<%# "editProjects.aspx?id=" + Eval("id") %>' 
                                    Text='<%# Eval("titleCs") %>'></asp:HyperLink>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("titleCs") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:CheckBoxField DataField="enable" HeaderText="Zobrazit na WWW" 
                            SortExpression="enable" >
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:CheckBoxField>
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:labsensnanoConnectionString %>" 
                    InsertCommand="INSERT INTO projects(titleCs, nick) VALUES (@titleCs, @nick)" 
                    
                    SelectCommand="SELECT id, titleCs, nick, enable FROM projects" 
                    
                    
                    
                    UpdateCommand="UPDATE projects SET titleCs = @titleCs, nick = @nick, enable = @enable WHERE (id = @id)">
                    <InsertParameters>
                        <asp:ControlParameter ControlID="TextBoxTitleCs" Name="titleCs" 
                            PropertyName="Text" />
                        <asp:ControlParameter ControlID="TextBoxID" Name="nick" PropertyName="Text" />
                    </InsertParameters>
                </asp:SqlDataSource>
            </asp:View>
            <asp:View ID="View2" runat="server">
                <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:labsensnanoConnectionString %>" 
                    InsertCommand="INSERT INTO products(titleCs) VALUES (@titleCs)" 
                    ProviderName="<%$ ConnectionStrings:labsensnanoConnectionString.ProviderName %>" 
                    
                    SelectCommand="SELECT titleCs, id, titleEn, keyWordsCs, keyWordsEn, typProduktu, descriptionCs, descriptionEn, parametrsCs, parametrsEn, enable FROM products WHERE (id = @id)" 
                    UpdateCommand="UPDATE products SET titleCs = @titleCs, titleEn = @titleEn, keyWordsCs = @keyWordsCs, keyWordsEn = @keyWordsEn, typProduktu = @typProduktu, descriptionCs = @descriptionCs, descriptionEn = @descriptionEn, parametrsCs = @parametrsCs, parametrsEn = @parametrsEn WHERE (id = @id)">
                    <InsertParameters>
                        <asp:Parameter Name="titleCs" />
                    </InsertParameters>
                    <SelectParameters>
                        <asp:QueryStringParameter DefaultValue="0" Name="id" QueryStringField="id" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="titleCs" />
                        <asp:Parameter Name="titleEn" />
                        <asp:Parameter Name="keyWordsCs" />
                        <asp:Parameter Name="keyWordsEn" />
                        <asp:Parameter Name="typProduktu" />
                        <asp:Parameter Name="descriptionCs" />
                        <asp:Parameter Name="descriptionEn" />
                        <asp:Parameter Name="parametrsCs" />
                        <asp:Parameter Name="parametrsEn" />
                        <asp:Parameter Name="id" />
                    </UpdateParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="SqlDataSource3" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:labsensnanoConnectionString %>" 
                    ProviderName="<%$ ConnectionStrings:labsensnanoConnectionString.ProviderName %>" 
                    SelectCommand="SELECT [typNameCs], [id] FROM [productTyps]">
                </asp:SqlDataSource>
                <asp:Label ID="Label3" runat="server" Text="CS Název: "></asp:Label>
                <asp:TextBox ID="titleCs" runat="server" Width="842px"></asp:TextBox>
                <br />
                <br/>
                <asp:Label ID="Label4" runat="server" Text="EN Název: "></asp:Label>
                <asp:TextBox ID="titleEn" runat="server" Width="840px"></asp:TextBox>
                <br />
                <br />
                <asp:Label ID="Label14" runat="server" Text="ID: "></asp:Label>
                <asp:TextBox ID="nick" runat="server" Width="324px"></asp:TextBox>
                <br />
                <br />
                <asp:Label ID="Label13" runat="server" Text="Zaháhejní:"></asp:Label>
&nbsp;<asp:TextBox ID="DateStart" runat="server"></asp:TextBox>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Label ID="Label8" runat="server" Text="Ukončení:"></asp:Label>
                &nbsp;<asp:TextBox ID="DateKonec" runat="server"></asp:TextBox>
                &nbsp;&nbsp;&nbsp;
                <asp:CheckBox ID="publikovat" runat="server" Text="Puvlikovat na WWW" />
                <br />
                <br />
                <asp:Button ID="Button2" runat="server" onclick="Button2_Click" 
                    Text="CS Popis" />
                <asp:Button ID="Button3" runat="server" onclick="Button3_Click" 
                    Text="EN Popis" />
                <asp:MultiView ID="MultiView2" runat="server" >
                    <asp:View ID="View3" runat="server">
                        <asp:TextBox ID="popisCs" runat="server" Height="287px" Width="900px"></asp:TextBox>
                    </asp:View>
                    <asp:View ID="View4" runat="server">
                        <asp:TextBox ID="popisEn" runat="server" Height="287px" Width="900px"></asp:TextBox>
                    </asp:View>
                </asp:MultiView>
                <br />
                <table style="width:100%;" align="center">
                    <tr>
                        <td align="center">
                            &nbsp;
                            <asp:Button ID="Button7" runat="server" onclick="Button7_Click" 
                                style="text-align: center" Text="Zpět na seznam" Width="159px" />
                            &nbsp;&nbsp;
                            <asp:Button ID="Button6" runat="server" style="text-align: center" 
                                Text="Uložit změny" Width="121px" onclick="Button6_Click" />
                            &nbsp;&nbsp; </td>
                    </tr>

                </table>
                <hr />
                <br />
                <table style="width:100%;">
                    <tr>
                        <td class="style1">
                            <asp:Label ID="Label9" runat="server" Text="Vazby na lidi:" Visible="False"></asp:Label>
                        </td>
                        <td class="style1">
                            <asp:Label ID="Label10" runat="server" Text="Vazby na projekty:" 
                                Visible="False"></asp:Label>
                        </td>
                    </tr>
                </table>
                <br />
                <asp:Label ID="Label15" runat="server" 
                    Text="Logo projektu (max. šířka 250 px):" Visible="False"></asp:Label>
                <table style="width:100%;">
                    <tr>
                        <td class="style1">
                            <asp:Image ID="LogoImg" runat="server" />
                        </td>
                        <td class="style1">
                            <asp:FileUpload ID="FileUploadImg" runat="server" />
                            <asp:Button ID="ButtonAddLogo" runat="server" onclick="ButtonAddLogo_Click" 
                                Text="Vložit logo" />
                            <asp:Button ID="ButtonDeletLogo" runat="server" onclick="ButtonDeletLogo_Click" 
                                Text="Odstranit" />
                        </td>
                    </tr>
                </table>
                <br />
                <br />

            </asp:View>
        </asp:MultiView>
    
    <br />
    </asp:Content>

