﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeFile="editProjectsBack.aspx.cs" Inherits="Admin_editProjects" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
       <asp:Label ID="Label1" runat="server" 
            style="font-weight: 700; font-size: large" Text="Seznam projektů"></asp:Label>

        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:labsensnanoConnectionString %>" 
            InsertCommand="INSERT INTO projects(titleCs) VALUES (@titleCs)" 
            SelectCommand="SELECT id, titleCs FROM projects ORDER BY titleCs">
            <InsertParameters>
                <asp:Parameter Name="titleCs" />
            </InsertParameters>
        </asp:SqlDataSource>
        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
            AutoGenerateColumns="False" CellPadding="4" 
            DataKeyNames="id" DataSourceID="SqlDataSource1" ForeColor="#333333" 
            GridLines="None" 
            style="margin-right: 306px" Width="900px" PageSize="8" 
           onselectedindexchanging="GridView1_SelectedIndexChanging">
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            <Columns>
                <asp:TemplateField HeaderText="Přehled všech projektů" SortExpression="titleCs">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("titleCs") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="Label2" runat="server" Text="Název projektu (CS)"></asp:Label>
                        &nbsp;
                        <asp:TextBox ID="TextBox2" runat="server" Width="446px"></asp:TextBox>
                    </FooterTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("titleCs") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ShowHeader="False">
                    <FooterTemplate>
                        <asp:Button ID="Button1" runat="server" onclick="Button1_Click" Text="Přidej" />
                    </FooterTemplate>
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" 
                            CommandName="Select" Text="Select"></asp:LinkButton>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Right" Width="100px" />
                </asp:TemplateField>
            </Columns>
            <EditRowStyle BackColor="#999999" />
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#E9E7E2" />
            <SortedAscendingHeaderStyle BackColor="#506C8C" />
            <SortedDescendingCellStyle BackColor="#FFFDF8" />
            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
        </asp:GridView>
        <br />
        <asp:DetailsView ID="DetailsView1" runat="server" AutoGenerateRows="False" 
            DataKeyNames="id" DataSourceID="SqlDataSource2" Height="50px" 
           Width="900px" oniteminserted="DetailsView1_ItemInserted" 
    oniteminserting="DetailsView1_ItemInserting">
            <Fields>
                <asp:BoundField DataField="titleCs" HeaderText="Název (CS)" 
                    SortExpression="titleCs" >
                <HeaderStyle Width="150px" />
                </asp:BoundField>
                <asp:BoundField DataField="titleEn" HeaderText="Název (EN)" 
                    SortExpression="titleEn" />
                <asp:BoundField DataField="nick" HeaderText="Zkratka (CS+EN)" 
                    SortExpression="nick" />
                <asp:TemplateField HeaderText="Popis (CS)" SortExpression="textCs">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("textCs") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("textCs") %>'></asp:TextBox>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("textCs") %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle Width="150px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Popis (EN)" SortExpression="textEn">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("textEn") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("textEn") %>'></asp:TextBox>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("textEn") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Nastavení projektu" SortExpression="enable">
                    <EditItemTemplate>
                        <table style="width:100%;">
                            <tr>
                                <td valign="middle">
                                    <asp:Calendar ID="Calendar1" runat="server" Caption="Začátek projektu:" 
                                        EnableTheming="True" SelectedDate='<%# Bind("start") %>'>
                                    </asp:Calendar>
                                </td>
                                <td>
                                    <asp:Calendar ID="Calendar2" runat="server" Caption="Konec projektu:" 
                                        EnableTheming="True" SelectedDate='<%# Bind("finish") %>'>
                                    </asp:Calendar>
                                </td>
                                <td>
                                    <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("enable") %>' 
                                        Text="Viditelnost projektu na WWW" />
                                </td>
                            </tr>
                        </table>
                        <br />
                        &nbsp;
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <table style="width:100%;">
                            <tr>
                                <td valign="middle">
                                    <asp:Calendar ID="Calendar1" runat="server" Caption="Začátek projektu:" 
                                        EnableTheming="True" SelectedDate='<%# Bind("start") %>'>
                                    </asp:Calendar>
                                </td>
                                <td>
                                    <asp:Calendar ID="Calendar2" runat="server" Caption="Konec projektu:" 
                                        EnableTheming="True" SelectedDate='<%# Bind("finish") %>' 
                                        onload="Calendar2_Load">
                                    </asp:Calendar>
                                </td>
                                <td>
                                    <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("enable") %>' 
                                        Text="Viditelnost projektu na WWW" />
                                </td>
                            </tr>
                        </table>
                        <br />
                        &nbsp;
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <table style="width:100%;">
                            <tr>
                                <td valign="middle">
                                    <asp:Calendar ID="Calendar1" runat="server" Caption="Začátek projektu:" 
                                        EnableTheming="True" SelectedDate='<%# Bind("start") %>' SelectionMode="None">
                                    </asp:Calendar>
                                </td>
                                <td>
                                    <asp:Calendar ID="Calendar2" runat="server" Caption="Konec projektu:" 
                                        EnableTheming="True" SelectedDate='<%# Bind("finish") %>' SelectionMode="None">
                                    </asp:Calendar>
                                </td>
                                <td>
                                    <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("enable") %>' 
                                        Enabled="false" Text="Viditelnost projektu na WWW" />
                                </td>
                            </tr>
                        </table>
                        <br />
                        &nbsp;
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:CommandField ShowInsertButton="True" ShowEditButton="True" />
            </Fields>
        </asp:DetailsView>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
            ConnectionString="<%$ ConnectionStrings:labsensnanoConnectionString %>" 
            InsertCommand="INSERT INTO projects(titleCs, titleEn, textCs, textEn, nick, enable, start, finish, lastchange) VALUES (@titleCs, @titleEn, @textCs, @textEn, @nick, @enable, @start, @finish, @lastchange)" 
            
           
    SelectCommand="SELECT id, titleCs, titleEn, textCs, textEn, nick, enable, start, finish, lastchange, logo FROM projects WHERE (id = @id) ORDER BY titleCs" 
    UpdateCommand="UPDATE projects SET titleCs = @titleCs, titleEn = @titleEn, textCs = @textCs, textEn = @textEn, nick = @nick, enable = @enable, start = @start, finish = @finish, lastchange = @lastchange WHERE (id = @id)">
            <InsertParameters>
                <asp:Parameter Name="titleCs" />
                <asp:Parameter Name="titleEn" />
                <asp:Parameter Name="textCs" />
                <asp:Parameter Name="textEn" />
                <asp:Parameter Name="nick" />
                <asp:Parameter Name="enable" />
                <asp:Parameter Name="start" />
                <asp:Parameter Name="finish" />
                <asp:Parameter Name="lastchange" />
            </InsertParameters>
            <SelectParameters>
                <asp:ControlParameter ControlID="GridView1" DefaultValue="0" Name="id" 
                    PropertyName="SelectedValue" />
            </SelectParameters>
            <UpdateParameters>
                <asp:Parameter Name="titleCs" />
                <asp:Parameter Name="titleEn" />
                <asp:Parameter Name="textCs" />
                <asp:Parameter Name="textEn" />
                <asp:Parameter Name="nick" />
                <asp:Parameter Name="enable" />
                <asp:Parameter Name="start" />
                <asp:Parameter Name="finish" />
                <asp:Parameter Name="lastchange" />
                <asp:Parameter Name="id" />
            </UpdateParameters>
        </asp:SqlDataSource>

</asp:Content>

