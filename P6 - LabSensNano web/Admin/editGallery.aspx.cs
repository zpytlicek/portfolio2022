﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

public partial class Admin_Products : System.Web.UI.Page
{
    string id="";
    string fileid = "";
    string puvodniDatum;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            id = Request.QueryString["id"].ToString();
            fileid = Request.QueryString["files"].ToString();
        }
        catch { }
                    // zobrazeni edit stránky
        if (id != "")
        {
            MultiView1.SetActiveView(View2);
            MultiView2.SetActiveView(View3);
            SqlDataSource2.SelectParameters["id"].DefaultValue = id;
             if (!IsPostBack)
             {
                SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["labsensnanoConnectionString"].ToString());
                try
                {
                    SqlCommand cmdSelect = new SqlCommand("SELECT titleCs, id, titleEn, textCs, textEn, enable, eventDate FROM gallery WHERE (id = @id)", dbConn);
                    cmdSelect.CommandType = System.Data.CommandType.Text;
                    SqlParameter prm_id = new SqlParameter("@id", SqlDbType.VarChar);
                    prm_id.Value = id;
                    cmdSelect.Parameters.Add(prm_id);

                    dbConn.Open();

                    //kontrolní výpis proměné o počtu výskytů 
                    SqlDataReader data = cmdSelect.ExecuteReader();
                    if (data.Read())
                    {
                        TextBox2.Text = data["titleCs"].ToString();
                        TextBox3.Text = data["titleEn"].ToString();
                        popisCs.Text = data["textCs"].ToString();
                        popisEn.Text = data["textEn"].ToString();

                        //nacteni data z databaze do editacniho pole
                        DateTime datumDate = DateTime.Parse(data["eventDate"].ToString());
                        datum.Text = datumDate.ToString("dd.MM.yyyy");

                        puvodniDatum = data["eventDate"].ToString();

                        //nacteni bin hodnoty do checked pole
                        publikovat.Checked = bool.Parse(data["enable"].ToString());
                    }
                    
                } catch {
                    dbConn.Close();
                }
            }
        }
        else
        {
            MultiView1.SetActiveView(View1);
        }

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        SqlDataSource1.InsertParameters["titleCs"].DefaultValue = TextBox1.Text;
        SqlDataSource1.Insert();
        SqlDataSource1.DataBind();
        GridView1.DataBind();

    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        MultiView2.SetActiveView(View3);
    }
    protected void Button3_Click(object sender, EventArgs e)
    {
        MultiView2.SetActiveView(View4);
    }
    protected void Button7_Click(object sender, EventArgs e)
    {
        Response.Redirect("editGallery.aspx");
    }
    protected void Button6_Click(object sender, EventArgs e)
    {
        SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["labsensnanoConnectionString"].ToString());
        try
        {

            SqlCommand cmdSet = new SqlCommand("UPDATE gallery SET eventDate = @eventDate, titleCs = @titleCs, titleEn = @titleEn, textCs = @textCs, textEn = @textEn, enable = @enable WHERE (id = @id)", dbConn);
           
            cmdSet.CommandType = System.Data.CommandType.Text;

            SqlParameter prm_titleCs = new SqlParameter("@titleCs", SqlDbType.VarChar);
            prm_titleCs.Value = TextBox2.Text;
            cmdSet.Parameters.Add(prm_titleCs);



          
            SqlParameter prm_eventDate = new SqlParameter("@eventDate", SqlDbType.DateTime);
            //načtení data z položky
            try
            {
                string[] datumCasti = datum.Text.Split('.');
                DateTime datumPublication = new DateTime(Int32.Parse(datumCasti[2]), Int32.Parse(datumCasti[1]), Int32.Parse(datumCasti[0]));
                prm_eventDate.Value = datumPublication;
            }
            catch
            {
                prm_eventDate.Value = puvodniDatum;
            }
            cmdSet.Parameters.Add(prm_eventDate);

            SqlParameter prm_titleEn = new SqlParameter("@titleEn", SqlDbType.VarChar);
            prm_titleEn.Value = TextBox3.Text;
            cmdSet.Parameters.Add(prm_titleEn);

            SqlParameter prm_enable = new SqlParameter("@enable", SqlDbType.Bit);
            prm_enable.Value = publikovat.Checked;
            cmdSet.Parameters.Add(prm_enable);

            SqlParameter prm_descriptionCs = new SqlParameter("@textCs", SqlDbType.Text);
            prm_descriptionCs.Value = popisCs.Text;
            cmdSet.Parameters.Add(prm_descriptionCs);

            SqlParameter prm_descriptionEn = new SqlParameter("@textEn", SqlDbType.Text);
            prm_descriptionEn.Value = popisEn.Text;
            cmdSet.Parameters.Add(prm_descriptionEn);

            SqlParameter prm_id = new SqlParameter("@id", SqlDbType.Int);
            prm_id.Value = Int16.Parse(id);
            cmdSet.Parameters.Add(prm_id);
/**************************************/
/**************************************/

            dbConn.Open();

            //kontrolní výpis proměné o počtu výskytů 
            cmdSet.ExecuteNonQuery().ToString();

            Response.Redirect("editGallery.aspx?id=" + id + "&files=" + fileid);

        }
        catch
        {
            Label1.Text = "Chyba komunikace s databází";
            dbConn.Close();
        }
    }

}