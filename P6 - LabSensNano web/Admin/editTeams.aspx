﻿<%@ Page Title="" Language="C#" validateRequest="false" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeFile="editTeams.aspx.cs" Inherits="Admin_Products" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <!-- DATA PRO VLOŽENÍ KALENDÁŘE -->
  <link rel="stylesheet" href="datedit/datedit.css" type="text/css" />
  <script type="text/javascript" charset="iso-8859-1" src="datedit/datedit.js"></script>
  <script type="text/javascript" charset="utf-8" src="datedit/lang/cz.js"></script>
  <script type="text/javascript">
      datedit("MainContent_NewEnd", "dd.mm.yyyy", false);
      datedit("MainContent_NewStart", "dd.mm.yyyy", false);
      datedit_VALIDATE_OUTPUT = false;    
  </script>

<!-- TinyMCE -->
<script type="text/javascript" src="../Scripts/tinymce/tiny_mce.js"></script>
<script type="text/javascript">

    function myFileBrowser(field_name, url, type, win) {
        // alert("Field_Name: " + field_name + "nURL: " + url + "nType: " + type + "nWin: " + win);
        // debug/testing    
        /* If you work with sessions in PHP and your client doesn't accept cookies you might need to carry  
        the session name and session ID in the request string (can look like this: "?PHPSESSID=88p0n70s9dsknra96qhuk6etm5"). 
        These lines of code extract the necessary parameters and add them back to the filebrowser URL again. */

        var cmsURL = 'fileList.aspx?cat=<%= "team" + Request.QueryString["id"] %>';
        // var cmsURL = window.location.toString();    // script URL - use an absolute path!    
        if (cmsURL.indexOf("?") < 0) {
            //add the type as the only query parameter    
            cmsURL = cmsURL + "?type=" + type;
        } else {
            //add the type as an additional query parameter     
            // (PHP session ID is now included if there is one at all)       
            cmsURL = cmsURL + "&type=" + type;
        }
        tinyMCE.activeEditor.windowManager.open({
            file: cmsURL,
            title: "File Browser",
            width: 900,  // Your dimensions may differ - toy around with them!
            height: 400,
            resizable: "yes",
            inline: "yes",  // This parameter only has an effect if you use the inlinepopups plugin!  
            close_previous: "yes"
        },
     { window: win,
         input: field_name
     });
        return false;
    }

    tinyMCE.init({
        // General options
        mode: "exact",
        // MUSI BYT VCETNE SPECIFIKACE OKNA RODICE
        elements: "MainContent_NewTextCs,MainContent_NewTextEn",
        theme: "advanced",
        plugins: "imagemanager,autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave",

        //vypnuti zmeny URL na relativni
        convert_urls: false,
        // external_image_list_url : "image.php5?x=",
        //  init_instance_callback : "init",

        // Theme options
        theme_advanced_buttons1: "newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
        theme_advanced_buttons2: "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
        theme_advanced_buttons3: "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,advhr,|,print,|,ltr,rtl,|,fullscreen",
        theme_advanced_buttons4: "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,pagebreak,restoredraft",
        theme_advanced_toolbar_location: "top",
        theme_advanced_toolbar_align: "left",
        theme_advanced_statusbar_location: "bottom",
        theme_advanced_resizing: true,


        // Example content CSS (should be your site CSS)
        content_css: "../layout/layoutContent.css",

        //extjsfilemanager_handlerurl: 'BrowserHandler.ashx',
        //Optional - Any extra parameters you want to have passed to your server-side handler on each request from the FileManager.
        //extjsfilemanager_extraparams: { param1: 'value1', param2: 'value2' }

        // Style formats
        /*   style_formats: [
        { title: 'Bold text', inline: 'b' },
        { title: 'Red text', inline: 'span', styles: { color: '#ff0000'} },
        { title: 'Red header', block: 'h1', styles: { color: '#ff0000'} },
        { title: 'Example 1', inline: 'span', classes: 'example1' },
        { title: 'Example 2', inline: 'span', classes: 'example2' },
        { title: 'Table styles' },
        { title: 'Table row 1', selector: 'tr', classes: 'tablerow1' }
        ],*/
        file_browser_callback: "myFileBrowser"
    });


</script>


<!-- /TinyMCE -->




    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    
        <h2><asp:Label ID="Label1" runat="server" Text="Správa výzkumných týmů"></asp:Label></h2>
        <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="1">
            <asp:View ID="View1" runat="server">
                <h3>
                    <asp:Label ID="Label2" runat="server" Text="Přidat oblast výzkumu :"></asp:Label>
                </h3>
                    <asp:TextBox ID="titleCs1" runat="server" Width="682px"></asp:TextBox>
                    <asp:Button ID="Button1" runat="server" onclick="Button1_Click" Text="Přidat" />
                <br />
                <br />
                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
                    CellPadding="4" DataKeyNames="id" DataSourceID="SqlDataSourceSeznam" 
                    ForeColor="#333333" GridLines="None" AllowSorting="True" 
                    AllowPaging="True">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField HeaderText="Oblast výzkumu" SortExpression="titleCs">
                            <ItemTemplate>
                                <asp:HyperLink ID="HyperLink2" runat="server" 
                                    NavigateUrl='<%# "editTeams.aspx?id=" + Eval("id") %>' 
                                    Text='<%# Eval("titleCs") %>'></asp:HyperLink>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("titleCs") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:CheckBoxField DataField="enable" HeaderText="Publikováno na www" 
                            SortExpression="enable" />
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
                <asp:SqlDataSource ID="SqlDataSourceSeznam" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:labsensnanoConnectionString %>" 
                    InsertCommand="INSERT INTO teams(titleCs) VALUES (@title)" 
                    
                    SelectCommand="SELECT titleCs, id, enable FROM teams" 
                    
                    
                    
                    
                    UpdateCommand="UPDATE teams SET titleCs = @title">
                    <InsertParameters>
                        <asp:ControlParameter ControlID="titleCs1" DefaultValue="-" Name="title" 
                            PropertyName="Text" />
                    </InsertParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="title" />
                    </UpdateParameters>
                </asp:SqlDataSource>
            </asp:View>
            <asp:View ID="View2" runat="server">
                <asp:SqlDataSource ID="SqlDataPersons" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:labsensnanoConnectionString %>" 
                    SelectCommand="SELECT id, degreeBefor + ' ' + name + ' ' + surname + degreeAfter AS person FROM persons ORDER BY surname">
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="SqlDataDetail" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:labsensnanoConnectionString %>" 
                    ProviderName="<%$ ConnectionStrings:labsensnanoConnectionString.ProviderName %>" 
                    
                    SelectCommand="SELECT id, titleCs, titleEn, leader, textCs, textEn, enable FROM teams WHERE (id = @id)" 
                    
                    
                    UpdateCommand="UPDATE teams SET titleCs = @titleCs, titleEn = @titleEn, leader = @leader, textCs = @textCs, textEn = @textEn, enable = @enable, lastChange = @lastChange WHERE (id = @id)" 
                    DeleteCommand="TeamDel" DeleteCommandType="StoredProcedure">
                    <DeleteParameters>
                        <asp:Parameter Direction="ReturnValue" Name="RETURN_VALUE" Type="Int32" />
                        <asp:QueryStringParameter DefaultValue="0" Name="id" QueryStringField="id" 
                            Type="Int32" />
                    </DeleteParameters>
                    <SelectParameters>
                        <asp:QueryStringParameter DefaultValue="0" Name="id" QueryStringField="id" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:ControlParameter ControlID="NewTitleCs" DefaultValue="" Name="titleCs" 
                            PropertyName="Text" />
                        <asp:ControlParameter ControlID="NewTitleEn" Name="titleEn" 
                            PropertyName="Text" />
                        <asp:Parameter DefaultValue="null" Name="leader" />
                        <asp:ControlParameter ControlID="NewTextCs" DefaultValue="" Name="textCs" 
                            PropertyName="Text" />
                        <asp:ControlParameter ControlID="NewTextEn" Name="textEn" PropertyName="Text" />
                        <asp:ControlParameter ControlID="NewEnable" DefaultValue="0" Name="enable" 
                            PropertyName="Checked" />
                        <asp:Parameter Name="lastChange" />
                        <asp:QueryStringParameter DefaultValue="0" Name="id" QueryStringField="id" />
                    </UpdateParameters>
                </asp:SqlDataSource>
                <asp:Label ID="Label3" runat="server" Text="Oblas výzkumu CS: "></asp:Label>
                <asp:TextBox ID="NewTitleCs" runat="server" Width="696px"></asp:TextBox>
                <br />
                <asp:Label ID="Label17" runat="server" Text="Oblas výzkumu EN: "></asp:Label>
                <asp:TextBox ID="NewTitleEn" runat="server" Width="696px"></asp:TextBox>
                <br />
                <br/>
                <asp:Label ID="Label18" runat="server" Text="Vedoucí osoba: "></asp:Label>
                <asp:DropDownList ID="DropDownListPersons" runat="server" 
                    AppendDataBoundItems="True" DataSourceID="SqlDataPersons" 
                    DataTextField="person" DataValueField="id" 
                    ondatabound="DropDownList1_DataBound">
                    <asp:ListItem Value="null">NURČENO</asp:ListItem>
                </asp:DropDownList>
                &nbsp;&nbsp;&nbsp;
                <asp:CheckBox ID="NewEnable" runat="server" Text="Puvlikovat na WWW" />
                <br />
                <br />
                <asp:Label ID="Label16" runat="server" Text="Popis:"></asp:Label>
                <br />
                <asp:Button ID="Button9" runat="server" onclick="Button9_Click" 
                    Text="Popis CS" />
                <asp:Button ID="Button10" runat="server" onclick="Button10_Click" 
                    Text="Popis EN" />
                <asp:MultiView ID="MultiView2" runat="server">
                    <asp:View ID="View3" runat="server">
                        <asp:TextBox ID="NewTextCs" runat="server" Height="287px" Width="900px"></asp:TextBox>
                    </asp:View>
                    <asp:View ID="View4" runat="server">
                        <asp:TextBox ID="NewTextEn" runat="server" Height="287px" Width="900px"></asp:TextBox>
                    </asp:View>
                </asp:MultiView>
                <br />
                <table style="width:100%;" align="center">
                    <tr>
                        <td align="center">
                            &nbsp;
                            <asp:Button ID="Button7" runat="server" onclick="Button7_Click" 
                                style="text-align: center" Text="Zpět na seznam" Width="159px" />
                            &nbsp;&nbsp;
                            <asp:Button ID="Button8" runat="server" onclick="Button8_Click" 
                                onclientclick="return confirm('Opravdu smazat?')" Text="Smazat" />
                            &nbsp;
                            <asp:Button ID="Button6" runat="server" onclick="Button6_Click" 
                                style="text-align: center" Text="Uložit změny" Width="121px" />
                            &nbsp;&nbsp; </td>
                    </tr>

                </table>
                <hr />
                <table style="width: 100%;">
                    <tr>
                        <td>
                            <asp:Label ID="Label9" runat="server" Text="Ostatní členové týmu:"></asp:Label>
                            <br />
                            <asp:SqlDataSource ID="SqlDataSource4" runat="server" 
                                ConnectionString="<%$ ConnectionStrings:labsensnanoConnectionString %>" 
                                SelectCommand="SELECT ([surname] + ' ' + [name]) AS name, [id] FROM [persons]">
                            </asp:SqlDataSource>
                            <asp:SqlDataSource ID="SqlDataSource5" runat="server" 
                                ConnectionString="<%$ ConnectionStrings:labsensnanoConnectionString %>" 
                                DeleteCommand="DELETE FROM rsTeamsPersons WHERE (id = @id)" 
                                InsertCommand="INSERT INTO rsTeamsPersons(personId, teamId) VALUES (@personId, @teamId)" 
                                ProviderName="<%$ ConnectionStrings:labsensnanoConnectionString.ProviderName %>" 
                                SelectCommand="SELECT persons.name, persons.surname, persons.degreeBefor, persons.degreeAfter, rsTeamsPersons.id, rsTeamsPersons.teamId FROM persons INNER JOIN rsTeamsPersons ON persons.id = rsTeamsPersons.personId WHERE (rsTeamsPersons.teamId = @id)">
                                <DeleteParameters>
                                    <asp:Parameter Name="id" />
                                </DeleteParameters>
                                <InsertParameters>
                                    <asp:ControlParameter ControlID="PersonId" DefaultValue="0" Name="personId" 
                                        PropertyName="SelectedValue" />
                                    <asp:QueryStringParameter DefaultValue="0" Name="teamId" 
                                        QueryStringField="id" />
                                </InsertParameters>
                                <SelectParameters>
                                    <asp:QueryStringParameter DefaultValue="0" Name="id" QueryStringField="id" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                            <asp:DropDownList ID="PersonId" runat="server" DataSourceID="SqlDataSource4" 
                                DataTextField="name" DataValueField="id" Width="322px">
                            </asp:DropDownList>
                            <asp:Button ID="Button11" runat="server" onclick="Button11_Click" Text="+" />
                            <br />
                            <br />
                            <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" 
                                DataKeyNames="id" DataSourceID="SqlDataSource5" ShowHeader="False">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Label ID="Label11" runat="server" 
                                                Text='<%# Eval("degreeBefor") + " " + Eval("surname") + " " + Eval("name") + "" + Eval("degreeAfter") + "&nbsp;&nbsp;&nbsp;" %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:CommandField ShowDeleteButton="True" />
                                </Columns>
                            </asp:GridView>
                            <br />
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                </table>
                <br />

            </asp:View>
        </asp:MultiView>
    
    <br />
    </asp:Content>

