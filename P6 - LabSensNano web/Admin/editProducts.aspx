﻿<%@ Page Title="" Language="C#" validateRequest="false" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeFile="editProducts.aspx.cs" Inherits="Admin_Products" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
  <!-- DATA PRO VLOŽENÍ KALENDÁŘE -->
  <link rel="stylesheet" href="datedit/datedit.css" type="text/css" />
  <script type="text/javascript" charset="iso-8859-1" src="datedit/datedit.js"></script>
  <script type="text/javascript" charset="utf-8" src="datedit/lang/cz.js"></script>
  <script type="text/javascript">
      datedit("MainContent_datum", "dd.mm.yyyy", false);
      datedit_VALIDATE_OUTPUT = false;    
  </script>

<!-- TinyMCE -->
<script type="text/javascript" src="../Scripts/tinymce/tiny_mce.js"></script>
<script type="text/javascript">

    function myFileBrowser(field_name, url, type, win) {
        // alert("Field_Name: " + field_name + "nURL: " + url + "nType: " + type + "nWin: " + win);
        // debug/testing    
        /* If you work with sessions in PHP and your client doesn't accept cookies you might need to carry  
        the session name and session ID in the request string (can look like this: "?PHPSESSID=88p0n70s9dsknra96qhuk6etm5"). 
        These lines of code extract the necessary parameters and add them back to the filebrowser URL again. */

        var cmsURL = 'fileList.aspx?cat=<%= Request.QueryString["files"] %>';
        // var cmsURL = window.location.toString();    // script URL - use an absolute path!    
        if (cmsURL.indexOf("?") < 0) {
            //add the type as the only query parameter    
            cmsURL = cmsURL + "?type=" + type;
        } else {
            //add the type as an additional query parameter     
            // (PHP session ID is now included if there is one at all)       
            cmsURL = cmsURL + "&type=" + type;
        }
        tinyMCE.activeEditor.windowManager.open({
            file: cmsURL,
            title: "File Browser",
            width: 900,  // Your dimensions may differ - toy around with them!
            height: 400,
            resizable: "yes",
            inline: "yes",  // This parameter only has an effect if you use the inlinepopups plugin!  
            close_previous: "yes"
        },
     { window: win,
         input: field_name
     });
        return false;
    }

    tinyMCE.init({
        // General options
        mode: "exact",
        // MUSI BYT VCETNE SPECIFIKACE OKNA RODICE
        elements: "MainContent_paramCs,MainContent_paramEn,MainContent_popisCs,MainContent_popisEn",
        theme: "advanced",
        plugins: "imagemanager,autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave",

        //vypnuti zmeny URL na relativni
        convert_urls: false,
        // external_image_list_url : "image.php5?x=",
        //  init_instance_callback : "init",

        // Theme options
        theme_advanced_buttons1: "newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
        theme_advanced_buttons2: "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
        theme_advanced_buttons3: "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,advhr,|,print,|,ltr,rtl,|,fullscreen",
        theme_advanced_buttons4: "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,pagebreak,restoredraft",
        theme_advanced_toolbar_location: "top",
        theme_advanced_toolbar_align: "left",
        theme_advanced_statusbar_location: "bottom",
        theme_advanced_resizing: true,


        // Example content CSS (should be your site CSS)
        content_css: "../layout/layoutContent.css",

        //extjsfilemanager_handlerurl: 'BrowserHandler.ashx',
        //Optional - Any extra parameters you want to have passed to your server-side handler on each request from the FileManager.
        //extjsfilemanager_extraparams: { param1: 'value1', param2: 'value2' }

        // Style formats
        /*   style_formats: [
        { title: 'Bold text', inline: 'b' },
        { title: 'Red text', inline: 'span', styles: { color: '#ff0000'} },
        { title: 'Red header', block: 'h1', styles: { color: '#ff0000'} },
        { title: 'Example 1', inline: 'span', classes: 'example1' },
        { title: 'Example 2', inline: 'span', classes: 'example2' },
        { title: 'Table styles' },
        { title: 'Table row 1', selector: 'tr', classes: 'tablerow1' }
        ],*/
        file_browser_callback: "myFileBrowser"
    });


</script>


<!-- /TinyMCE -->

    <script type="text/javascript" language="javascript">
        function ConfirmOnDelete() {
            if (confirm("Opravdu odstranit produkt?") == true)
                return true;
            else
                return false;
        }
    </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    
        <h2><asp:Label ID="Label1" runat="server" Text="Správa produktů"></asp:Label></h2>
        <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="1">
            <asp:View ID="View1" runat="server">
                <h3>
                    <asp:Label ID="Label2" runat="server" Text="Přidat nový produkt (název česky):"></asp:Label>
                </h3>
                <asp:TextBox ID="TextBox1" runat="server" Width="529px"></asp:TextBox>
                <asp:Button ID="Button1" runat="server" onclick="Button1_Click" Text="Přidat" />
                <br />
                <br />
                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
                    CellPadding="4" DataKeyNames="id" DataSourceID="SqlDataSource1" 
                    ForeColor="#333333" GridLines="None">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField HeaderText="Seznam všech produktů">
                            <ItemTemplate>
                                <asp:HyperLink ID="HyperLink1" runat="server" 
                                    NavigateUrl='<%# "editProducts.aspx?id=" + Eval("id", "{0}") + "&files=pro" + Eval("id", "{0}") %>' 
                                    Text='<%# Eval("titleCs") %>'></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Rok">
                            <ItemTemplate>
                                <asp:HyperLink ID="HyperLink2" runat="server" 
                                    NavigateUrl='<%# "editProducts.aspx?id=" + Eval("id", "{0}") + "&files=pro" + Eval("id", "{0}") %>' 
                                    Text='<%# Eval("publication") %>'></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:labsensnanoConnectionString %>" 
                    InsertCommand="INSERT INTO products(titleCs) VALUES (@titleCs)" 
                    
                    SelectCommand="SELECT [titleCs], [id], YEAR([publication]) as [publication] FROM [products] ORDER BY [publication]">
                    <InsertParameters>
                        <asp:Parameter Name="titleCs" />
                    </InsertParameters>
                </asp:SqlDataSource>
            </asp:View>
            <asp:View ID="View2" runat="server">
                <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:labsensnanoConnectionString %>" 
                    InsertCommand="INSERT INTO products(titleCs) VALUES (@titleCs)" 
                    ProviderName="<%$ ConnectionStrings:labsensnanoConnectionString.ProviderName %>" 
                    
                    SelectCommand="SELECT titleCs, id, titleEn, keyWordsCs, keyWordsEn, typProduktu, descriptionCs, descriptionEn, parametrsCs, parametrsEn, enable FROM products WHERE (id = @id)" 
                    
                    UpdateCommand="UPDATE products SET titleCs = @titleCs, titleEn = @titleEn, keyWordsCs = @keyWordsCs, keyWordsEn = @keyWordsEn, typProduktu = @typProduktu, descriptionCs = @descriptionCs, descriptionEn = @descriptionEn, parametrsCs = @parametrsCs, parametrsEn = @parametrsEn WHERE (id = @id)" 
                    DeleteCommand="ProductDel" DeleteCommandType="StoredProcedure">
                    <DeleteParameters>
                        <asp:Parameter Direction="ReturnValue" Name="RETURN_VALUE" Type="Int32" />
                        <asp:QueryStringParameter DefaultValue="0" Name="id" QueryStringField="id" 
                            Type="Int32" />
                    </DeleteParameters>
                    <InsertParameters>
                        <asp:Parameter Name="titleCs" />
                    </InsertParameters>
                    <SelectParameters>
                        <asp:QueryStringParameter DefaultValue="0" Name="id" QueryStringField="id" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="titleCs" />
                        <asp:Parameter Name="titleEn" />
                        <asp:Parameter Name="keyWordsCs" />
                        <asp:Parameter Name="keyWordsEn" />
                        <asp:Parameter Name="typProduktu" />
                        <asp:Parameter Name="descriptionCs" />
                        <asp:Parameter Name="descriptionEn" />
                        <asp:Parameter Name="parametrsCs" />
                        <asp:Parameter Name="parametrsEn" />
                        <asp:Parameter Name="id" />
                    </UpdateParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="SqlDataSource3" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:labsensnanoConnectionString %>" 
                    ProviderName="<%$ ConnectionStrings:labsensnanoConnectionString.ProviderName %>" 
                    SelectCommand="SELECT [typNameCs], [id] FROM [productTyps]">
                </asp:SqlDataSource>
                <asp:Label ID="Label3" runat="server" Text="CS Název: "></asp:Label>
                <asp:TextBox ID="TextBox2" runat="server" Width="842px"></asp:TextBox>
                <br />
                <br/>
                <asp:Label ID="Label4" runat="server" Text="EN Název: "></asp:Label>
                <asp:TextBox ID="TextBox3" runat="server" Width="840px"></asp:TextBox>
                <br />
                <br />
                <asp:Label ID="Label7" runat="server" Text="Typ produktu: "></asp:Label>
                <asp:DropDownList ID="DropDownList1" runat="server" 
                    DataSourceID="SqlDataSource3" DataTextField="typNameCs" DataValueField="id">
                </asp:DropDownList>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Label ID="Label8" runat="server" Text="Produk vytvořen:"></asp:Label>
                <asp:TextBox ID="datum" runat="server"></asp:TextBox>
                &nbsp;&nbsp;&nbsp;
                <asp:CheckBox ID="publikovat" runat="server" Text="Puvlikovat na WWW" />
                <br />
                <br />
                <asp:Button ID="Button2" runat="server" onclick="Button2_Click" 
                    Text="CS Popis" />
                <asp:Button ID="Button3" runat="server" onclick="Button3_Click" 
                    Text="EN Popis" />
                <asp:Button ID="Button4" runat="server" onclick="Button4_Click" 
                    Text="CS Parametry" />
                <asp:Button ID="Button5" runat="server" onclick="Button5_Click" 
                    Text="EN Parametry" />
                <asp:MultiView ID="MultiView2" runat="server" >
                    <asp:View ID="View3" runat="server">
                        <asp:TextBox ID="popisCs" runat="server" Height="287px" Width="900px"></asp:TextBox>
                    </asp:View>
                    <asp:View ID="View4" runat="server">
                        <asp:TextBox ID="popisEn" runat="server" Height="287px" Width="900px"></asp:TextBox>
                    </asp:View>
                    <asp:View ID="View5" runat="server">
                        <asp:TextBox ID="paramCs" runat="server" Height="287px" Width="900px"></asp:TextBox>
                    </asp:View>
                    <asp:View ID="View6" runat="server">
                        <asp:TextBox ID="paramEn" runat="server" Height="287px" Width="900px"></asp:TextBox>
                    </asp:View>
                </asp:MultiView>
                <br />
                <asp:Label ID="Label5" runat="server" Text=" CS key words: "></asp:Label>
                <asp:TextBox ID="keyWordsCs" runat="server" Width="650px"></asp:TextBox>
                <br />
                <br />
                <asp:Label ID="Label6" runat="server" Text="EN key words: "></asp:Label>
                <asp:TextBox ID="keyWordsEn" runat="server" Width="650px"></asp:TextBox>
                <br />
                <br />
                <table style="width:100%;" align="center">
                    <tr>
                        <td align="center">
                            &nbsp;
                            <asp:Button ID="Button7" runat="server" onclick="Button7_Click" 
                                style="text-align: center" Text="Zpět na seznam" Width="159px" />
                            &nbsp;&nbsp;
                            <asp:Button ID="Button6" runat="server" style="text-align: center" 
                                Text="Uložit změny" Width="121px" onclick="Button6_Click" />
                            &nbsp;&nbsp; </td>
                    </tr>

                </table>
                <hr />
                <br />
                <table style="width:100%;">
                    <tr>
                        <td valign="top">
                            <asp:Label ID="Label9" runat="server" Text="Vazby na lidi:"></asp:Label>
                            <br />
                            <asp:SqlDataSource ID="SqlDataSource4" runat="server" 
                                ConnectionString="<%$ ConnectionStrings:labsensnanoConnectionString %>" 
                                
                                SelectCommand="SELECT ([surname] + ' ' + [name]) AS name, [id] FROM [persons] ORDER BY [surname]">
                            </asp:SqlDataSource>
                            <asp:SqlDataSource ID="SqlDataSource5" runat="server" 
                                ConnectionString="<%$ ConnectionStrings:labsensnanoConnectionString %>" 
                                DeleteCommand="DELETE FROM rsPersonsProducts WHERE (id = @id)" 
                                InsertCommand="INSERT INTO rsPersonsProducts(personId, productId) VALUES (@personId, @productId)" 
                                SelectCommand="SELECT persons.name, persons.surname, rsPersonsProducts.id, persons.degreeBefor, persons.degreeAfter, rsPersonsProducts.productId FROM rsPersonsProducts INNER JOIN persons ON rsPersonsProducts.personId = persons.id WHERE (rsPersonsProducts.productId = @id)">
                                <DeleteParameters>
                                    <asp:Parameter Name="id" />
                                </DeleteParameters>
                                <InsertParameters>
                                    <asp:Parameter Name="personId" />
                                    <asp:Parameter Name="productId" />
                                </InsertParameters>
                                <SelectParameters>
                                    <asp:QueryStringParameter DefaultValue="0" Name="id" QueryStringField="id" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                            <asp:DropDownList ID="PersonId" runat="server" DataSourceID="SqlDataSource4" 
                                DataTextField="name" DataValueField="id" Width="322px">
                            </asp:DropDownList>
                            <asp:Button ID="Button8" runat="server" onclick="Button8_Click" Text="+" />
                            <br />
                            <br />
                            <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" 
                                DataKeyNames="id" DataSourceID="SqlDataSource5" ShowHeader="False">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Label ID="Label11" runat="server" 
                                                Text='<%# Eval("degreeBefor") + " " + Eval("surname") + " " + Eval("name") + "" + Eval("degreeAfter") + "&nbsp;&nbsp;&nbsp;" %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:CommandField ShowDeleteButton="True" />
                                </Columns>
                            </asp:GridView>
                            <br />
                        </td>
                        <td valign="top">
                            <asp:Label ID="Label10" runat="server" Text="Vazby na projekty:"></asp:Label>
                            <br />
                            <asp:SqlDataSource ID="SqlDataSource6" runat="server" 
                                ConnectionString="<%$ ConnectionStrings:labsensnanoConnectionString %>" 
                                SelectCommand="SELECT id, nick FROM projects ORDER BY nick">
                            </asp:SqlDataSource>
                            <asp:SqlDataSource ID="SqlDataSource7" runat="server" 
                                ConnectionString="<%$ ConnectionStrings:labsensnanoConnectionString %>" 
                                DeleteCommand="DELETE FROM rsProjectsProducts
WHERE (id = @id)" 
                                InsertCommand="INSERT INTO rsProjectsProducts(productId, projectId) VALUES (@productId, @projectId)" 
                                
                                SelectCommand="SELECT projects.nick, rsProjectsProducts.productId, rsProjectsProducts.id FROM rsProjectsProducts INNER JOIN projects ON rsProjectsProducts.projectId = projects.id WHERE (rsProjectsProducts.productId = @id)">
                                <DeleteParameters>
                                    <asp:ControlParameter ControlID="GridView3" Name="id" 
                                        PropertyName="SelectedValue" />
                                </DeleteParameters>
                                <InsertParameters>
                                    <asp:QueryStringParameter Name="productId" QueryStringField="id" />
                                    <asp:ControlParameter ControlID="ProjectId" Name="projectId" 
                                        PropertyName="SelectedValue" />
                                </InsertParameters>
                                <SelectParameters>
                                    <asp:QueryStringParameter DefaultValue="0" Name="id" QueryStringField="id" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                            <asp:DropDownList ID="ProjectId" runat="server" DataSourceID="SqlDataSource6" 
                                DataTextField="nick" DataValueField="id" Width="322px">
                            </asp:DropDownList>
                            <asp:Button ID="Button9" runat="server" onclick="Button9_Click" Text="+" />
                            <br /><br />
                            <asp:GridView ID="GridView3" runat="server" AutoGenerateColumns="False" 
                                DataKeyNames="id" DataSourceID="SqlDataSource7" ShowHeader="False">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Label ID="Label12" runat="server" Text='<%# Eval("nick") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:CommandField ShowDeleteButton="True" />
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
                <hr />
                <center />
                <asp:Button ID="Button10" runat="server" Text="Odstranit produkt" 
                        onclick="Button10_Click1"/>
                </center>
                <br />
                <br />
                <br />

            </asp:View>
        </asp:MultiView>
    
    <br />
    </asp:Content>

