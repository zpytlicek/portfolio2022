﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

//pro úpravu obrázků
using System.IO;
using System.Drawing;
using System.Configuration;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;

public partial class Admin_Products : System.Web.UI.Page
{
    private static ImageFormat GetFormat(string filename)
    {
        if (filename.EndsWith("jpg") || filename.EndsWith("jpeg") || filename.EndsWith("tiff"))
            return ImageFormat.Jpeg;

        if (filename.EndsWith("gif"))
            return ImageFormat.Gif;

        return ImageFormat.Png;
    }
    private bool ThumbnailCallback()
    {
        return false;
    }

    string id="";
    string puvodniDatumKonec;
    string puvodniDatumStart;

    string imgName = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            id = Request.QueryString["id"].ToString();
        }
        catch { }
                    // zobrazeni edit stránky
        if (id != "")
        {
            MultiView1.SetActiveView(View2);
            SqlDataSource2.SelectParameters["id"].DefaultValue = id;
             if (!IsPostBack)
             {
                SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["labsensnanoConnectionString"].ToString());
                try
                {
                    SqlCommand cmdSelect = new SqlCommand("SELECT name, id, surname, degreeBefor, email, degreeAfter, enable, class FROM persons WHERE (id = @id)", dbConn);
                    cmdSelect.CommandType = System.Data.CommandType.Text;
                    SqlParameter prm_id = new SqlParameter("@id", SqlDbType.VarChar);
                    prm_id.Value = id;
                    cmdSelect.Parameters.Add(prm_id);

                    dbConn.Open();

                    //kontrolní výpis proměné o počtu výskytů 
                    SqlDataReader data = cmdSelect.ExecuteReader();
                    if (data.Read())
                    {
                        degreeBefore.Text = data["degreeBefor"].ToString();
                        degreeAfter.Text = data["degreeAfter"].ToString();
                        email.Text = data["email"].ToString();
                        surname.Text = data["surname"].ToString();
                        name.Text = data["name"].ToString();
                        personClass.SelectedValue = data["class"].ToString();

                        /*
                        //nacteni data z databaze do editacniho pole
                        DateTime datumDate = DateTime.Parse(data["finish"].ToString());
                        DateKonec.Text = datumDate.ToString("dd.MM.yyyy");
                        puvodniDatumKonec = data["finish"].ToString();

                        DateTime datumDate2 = DateTime.Parse(data["start"].ToString());
                        DateStart.Text = datumDate2.ToString("dd.MM.yyyy");
                        puvodniDatumStart = data["start"].ToString();*/


                        //nacteni bin hodnoty do checked pole
                        publikovat.Checked = bool.Parse(data["enable"].ToString());
                    }

                }
                catch
                {
                    /* DateKonec.Text = puvodniDatumKonec;
                     DateStart.Text = puvodniDatumStart;*/

                }
                finally
                {
                    dbConn.Close();
                }
            }
        }
        else
        {
            MultiView1.SetActiveView(View1);
        }

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
       /* SqlDataSource1.InsertParameters["titleCs"].DefaultValue = TextBoxTitleCs.Text;
        SqlDataSource1.InsertParameters["nick"].DefaultValue = TextBoxID.Text;*/
        SqlDataSource1.Insert();
        SqlDataSource1.DataBind();
        GridView1.DataBind();
        Response.Redirect("editPersons.aspx");

    }

    protected void Button7_Click(object sender, EventArgs e)
    {
        Response.Redirect("editPersons.aspx");
    }
    protected void Button6_Click(object sender, EventArgs e)
    {
        SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["labsensnanoConnectionString"].ToString());
        try
        {

            SqlCommand cmdSet = new SqlCommand("UPDATE persons SET lastchange = @lastchange, name = @name, surname = @surname, degreeBefor = @degreeBefor, degreeAfter = @degreeAfter, email = @email, class = @class, enable = @enable WHERE (id = @id)", dbConn);

            cmdSet.CommandType = System.Data.CommandType.Text;

            SqlParameter prm_degreeBefore = new SqlParameter("@degreeBefor", SqlDbType.VarChar);
            prm_degreeBefore.Value = degreeBefore.Text;
            cmdSet.Parameters.Add(prm_degreeBefore);
            
            SqlParameter prm_degreeAfter = new SqlParameter("@degreeAfter", SqlDbType.VarChar);
            prm_degreeAfter.Value = degreeAfter.Text;
            cmdSet.Parameters.Add(prm_degreeAfter);

            SqlParameter prm_class = new SqlParameter("@class", SqlDbType.Int);
            prm_class.Value = personClass.SelectedValue.ToString();
            cmdSet.Parameters.Add(prm_class);

            SqlParameter prm_surname = new SqlParameter("@surname", SqlDbType.VarChar);
            prm_surname.Value = surname.Text;
            cmdSet.Parameters.Add(prm_surname);

            SqlParameter prm_email = new SqlParameter("@email", SqlDbType.VarChar);
            prm_email.Value = email.Text;
            cmdSet.Parameters.Add(prm_email);

            SqlParameter prm_name = new SqlParameter("@name", SqlDbType.VarChar);
            prm_name.Value = name.Text;
            cmdSet.Parameters.Add(prm_name);
/*
            SqlParameter prm_start = new SqlParameter("@start", SqlDbType.DateTime);
            //načtení data z položky
            try
            {
                string[] datumCastiS = DateStart.Text.Split('.');
                DateTime datumStart = new DateTime(Int32.Parse(datumCastiS[2]), Int32.Parse(datumCastiS[1]), Int32.Parse(datumCastiS[0]));
                prm_start.Value = datumStart;
            }
            catch
            {
                prm_start.Value = puvodniDatumStart;
            }
            cmdSet.Parameters.Add(prm_start);*/

            SqlParameter prm_enable = new SqlParameter("@enable", SqlDbType.Bit);
            //prm_enable.Value = publikovat.Enabled;
            prm_enable.Value = publikovat.Checked;
            cmdSet.Parameters.Add(prm_enable);
        
            SqlParameter prm_id = new SqlParameter("@id", SqlDbType.Int);
            prm_id.Value = Int16.Parse(id);
            cmdSet.Parameters.Add(prm_id);
            /**************************************/
            /**************************************/

            //nastavení data poslední zmeny
            SqlParameter prm_lastchange = new SqlParameter("@lastchange", SqlDbType.DateTime);
            prm_lastchange.Value = DateTime.Now;
            cmdSet.Parameters.Add(prm_lastchange);


            dbConn.Open();
            cmdSet.ExecuteNonQuery().ToString();

        }
        catch
        {
            Label1.Text = "Chyba komunikace s databází";

        }
        finally
        {
            dbConn.Close();
            Response.Redirect("editPersons.aspx?id=" + id);
        }
            
    }


   
    protected void Button8_Click(object sender, EventArgs e)
    {
        int pocetVyskytuProduct = 0, pocetVyskytuProject = 0;
        SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["labsensnanoConnectionString"].ToString());
        try
        {

            SqlCommand cmdSet = new SqlCommand("SELECT COUNT(*) FROM [LabSensNano].[dbo].[rsPersonsProducts] WHERE personId = @id", dbConn);
            cmdSet.CommandType = System.Data.CommandType.Text;
            SqlParameter prm_degreeBefore = new SqlParameter("@id", SqlDbType.Int);
            prm_degreeBefore.Value = Int16.Parse(id);
            cmdSet.Parameters.Add(prm_degreeBefore);
            
            dbConn.Open();
            pocetVyskytuProduct = Convert.ToInt32(cmdSet.ExecuteScalar());

            cmdSet.CommandText = "SELECT COUNT(*) FROM [LabSensNano].[dbo].[rsProjectsPersons] WHERE personId = @id)";
            pocetVyskytuProject = Convert.ToInt32(cmdSet.ExecuteScalar());
        }
        catch
        {
        } 
        finally {
           dbConn.Close();
        }

        if ((pocetVyskytuProject + pocetVyskytuProduct) > 0)
        {
        Response.Write("<script type=\"text/javascript\">alert('Tato osoba se vyskytuje u " + pocetVyskytuProject.ToString() + " projektů a " + pocetVyskytuProduct.ToString() + " produktů. Nelze tedy odstranit.');</script>");
    } else {
            
        try
        {
            //odstraneni osoby pomoci procedury 
            SqlCommand cmdSet = new SqlCommand("[LabSensNano].[dbo].[PersonDel] @id", dbConn);
            cmdSet.CommandType = System.Data.CommandType.Text;
            SqlParameter prm_degreeBefore = new SqlParameter("@id", SqlDbType.Int);
            prm_degreeBefore.Value = Int16.Parse(id);
            cmdSet.Parameters.Add(prm_degreeBefore);
            dbConn.Open();
            cmdSet.ExecuteNonQuery();
        }
        catch
        {
        } 
        finally {
           dbConn.Close();
        }

            Response.Redirect("editPersons.aspx");
        }

    }
}