﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="fileList.aspx.cs" Inherits="Default6" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>File Browser</title>
    <style type="text/css">
        .style1
        {
            width: 53px;
        }
    </style>
</head>

                                              


<body style="height: 167px">
    <form id="my_form" runat="server" enctype="multipart/form-data">
    <script language="javascript" type="text/javascript" src="../Scripts/tinymce/tiny_mce_popup.js"></script>
    <script language="javascript" type="text/javascript">
        var FileBrowserDialogue = {
            init: function () {
                // Here goes your code for setting your custom things onLoad.  
            },
            mySubmit: function (URL) {
                //hodnota vrácené URL je předáván jako parametr při volání fce
                //var URL = document.my_form.my_field.value;
                var win = tinyMCEPopup.getWindowArg("window");
                // insert information now 
                win.document.getElementById(tinyMCEPopup.getWindowArg("input")).value = URL;
                // are we an image browser  
                if (typeof (win.ImageDialog) != "undefined") {
                    // we are, so update image dimensions and preview if necessary  
                    if (win.ImageDialog.getImageData) win.ImageDialog.getImageData();
                    if (win.ImageDialog.showPreviewImage) win.ImageDialog.showPreviewImage(URL);
                }        // close popup window  
                tinyMCEPopup.close();
            }
        }

        tinyMCEPopup.onInit.add(FileBrowserDialogue.init, FileBrowserDialogue);
    </script>

    <div>
         <asp:MultiView ID="MultiView1" runat="server">
             <asp:View ID="View1" runat="server">
                 <asp:GridView ID="GridView1" runat="server" AllowSorting="True" 
                     AutoGenerateColumns="False" BorderWidth="1px" CellPadding="4" DataKeyNames="id" 
                     DataSourceID="SqlDataSource1" ForeColor="#333333" GridLines="None" 
                     onrowdatabound="GridView1_RowDataBound">
                     <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                     <Columns>
                         <asp:BoundField DataField="inserted" HeaderText="Vloženo" ReadOnly="True" 
                             SortExpression="inserted" />
                         <asp:BoundField DataField="title" HeaderText="Popis" SortExpression="title" />
                         <asp:BoundField DataField="size" HeaderText="Velikost [kB]" ReadOnly="True" 
                             SortExpression="size" />
                         <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
                         <asp:HyperLinkField />
                         <asp:BoundField DataField="id" HeaderText="Náhled" InsertVisible="False" 
                             ReadOnly="True" SortExpression="id" />
                     </Columns>
                     <EditRowStyle BackColor="#999999" />
                     <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                     <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                     <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                     <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                     <SortedAscendingCellStyle BackColor="#E9E7E2" />
                     <SortedAscendingHeaderStyle BackColor="#506C8C" />
                     <SortedDescendingCellStyle BackColor="#FFFDF8" />
                     <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                 </asp:GridView>
                 <hr />
                 <table style="width:100%;">
                     <tr>
                         <td align="left" class="style1">
                             <asp:Label ID="Label1" runat="server" Text="Soubor: "></asp:Label>
                         </td>
                         <td>
                             <asp:FileUpload ID="FileUpload1" runat="server" Width="330px" />
                             <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                                 ConnectionString="<%$ ConnectionStrings:labsensnanoConnectionString %>" 
                                 DeleteCommand="DELETE FROM images WHERE (id = @id)" InsertCommand="ImageAdd" 
                                 InsertCommandType="StoredProcedure" 
                                 SelectCommand="SELECT title, size, inserted, datapath, id, category FROM images WHERE (category = @cat)" 
                                 UpdateCommand="UPDATE images SET title = @title WHERE (id = @id)">
                                 <DeleteParameters>
                                     <asp:Parameter Name="id" Type="Int32" />
                                 </DeleteParameters>
                                 <InsertParameters>
                                     <asp:Parameter Name="title" Type="String" />
                                     <asp:Parameter Name="contenttype" Type="String" />
                                     <asp:Parameter Name="datapath" Type="String" />
                                     <asp:Parameter Name="category" Type="String" />
                                     <asp:Parameter Name="data" Type="Object" />
                                     <asp:Parameter Name="preview" Type="Object" />
                                     <asp:Parameter Name="width" Type="Int32" />
                                     <asp:Parameter Name="height" Type="Int32" />
                                     <asp:Parameter Name="size" Type="Int32" />
                                 </InsertParameters>
                                 <SelectParameters>
                                     <asp:QueryStringParameter DefaultValue="all" Name="cat" 
                                         QueryStringField="cat" />
                                 </SelectParameters>
                                 <UpdateParameters>
                                     <asp:Parameter Name="title" />
                                     <asp:Parameter Name="id" />
                                 </UpdateParameters>
                             </asp:SqlDataSource>
                         </td>
                     </tr>
                     <tr>
                         <td align="left" class="style1">
                             <asp:Label ID="Label2" runat="server" Text="Popis: "></asp:Label>
                         </td>
                         <td>
                             <asp:TextBox ID="title" runat="server" Width="330px"></asp:TextBox>
                             <asp:Button ID="Button1" runat="server" onclick="Button1_Click" 
                                 Text="Odeslat" />
                         </td>
                     </tr>
                 </table>
                 <asp:Label ID="Error" runat="server" ForeColor="Red"></asp:Label>
                 <br />
             </asp:View>
             <asp:View ID="View2" runat="server">
                 <asp:GridView ID="GridView2" runat="server" AllowSorting="True" 
                     AutoGenerateColumns="False" BorderWidth="1px" CellPadding="4" DataKeyNames="id" 
                     DataSourceID="SqlDataSource2" ForeColor="#333333" GridLines="None" 
                     onrowdatabound="GridView2_RowDataBound">
                     <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                     <Columns>
                         <asp:BoundField DataField="inserted" HeaderText="Vloženo" ReadOnly="True" 
                             SortExpression="inserted" />
                         <asp:BoundField DataField="title" HeaderText="Popis" SortExpression="title" />
                         <asp:BoundField DataField="size" HeaderText="Velikost [kB]" ReadOnly="True" 
                             SortExpression="size" />
                         <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
                         <asp:HyperLinkField />
                     </Columns>
                     <EditRowStyle BackColor="#999999" />
                     <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                     <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                     <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                     <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                     <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                     <SortedAscendingCellStyle BackColor="#E9E7E2" />
                     <SortedAscendingHeaderStyle BackColor="#506C8C" />
                     <SortedDescendingCellStyle BackColor="#FFFDF8" />
                     <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                 </asp:GridView>
                 <hr />
                 <table style="width:100%;">
                     <tr>
                         <td align="left" class="style1">
                             <asp:Label ID="Label3" runat="server" Text="Soubor: "></asp:Label>
                         </td>
                         <td>
                             <asp:FileUpload ID="FileUpload2" runat="server" Width="330px" />
                             <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
                                 ConnectionString="<%$ ConnectionStrings:labsensnanoConnectionString %>" 
                                 DeleteCommand="DELETE FROM files WHERE (id = @id)" InsertCommand="fileAdd" 
                                 InsertCommandType="StoredProcedure" 
                                 SelectCommand="SELECT title, size, inserted, datapath, id, category FROM files WHERE (category = @cat)" 
                                 UpdateCommand="UPDATE files SET title = @title WHERE (id = @id)">
                                 <DeleteParameters>
                                     <asp:Parameter Name="id" Type="Int32" />
                                 </DeleteParameters>
                                 <InsertParameters>
                                     <asp:Parameter Name="title" Type="String" />
                                     <asp:Parameter Name="contenttype" Type="String" />
                                     <asp:Parameter Name="datapath" Type="String" />
                                     <asp:Parameter Name="category" Type="String" />
                                     <asp:Parameter Name="data" Type="Object" />
                                     <asp:Parameter Name="size" Type="Int32" />
                                 </InsertParameters>
                                 <SelectParameters>
                                     <asp:QueryStringParameter DefaultValue="all" Name="cat" 
                                         QueryStringField="cat" />
                                 </SelectParameters>
                                 <UpdateParameters>
                                     <asp:Parameter Name="title" />
                                     <asp:Parameter Name="id" />
                                 </UpdateParameters>
                             </asp:SqlDataSource>
                         </td>
                     </tr>
                     <tr>
                         <td align="left" class="style1">
                             <asp:Label ID="Label4" runat="server" Text="Popis: "></asp:Label>
                         </td>
                         <td>
                             <asp:TextBox ID="title2" runat="server" Width="330px"></asp:TextBox>
                             <asp:Button ID="Button2" runat="server" onclick="Button2_Click" 
                                 Text="Odeslat" />
                         </td>
                     </tr>
                 </table>
             </asp:View>
         </asp:MultiView>

    </div>
    </form>
</body>
</html>
