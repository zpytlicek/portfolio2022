﻿<%@ Page Title="" Language="C#" validateRequest="false" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeFile="editPersons.aspx.cs" Inherits="Admin_Products" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <!-- DATA PRO VLOŽENÍ KALENDÁŘE -->
  <link rel="stylesheet" href="datedit/datedit.css" type="text/css" />
  <script type="text/javascript" charset="iso-8859-1" src="datedit/datedit.js"></script>
  <script type="text/javascript" charset="utf-8" src="datedit/lang/cz.js"></script>
  <script type="text/javascript">
      datedit("MainContent_DateKonec", "dd.mm.yyyy", false);
      datedit("MainContent_DateStart", "dd.mm.yyyy", false);
      datedit_VALIDATE_OUTPUT = false;    
  </script>

<!-- TinyMCE -->
<script type="text/javascript" src="../Scripts/tinymce/tiny_mce.js"></script>
<script type="text/javascript">

    function myFileBrowser(field_name, url, type, win) {
        // alert("Field_Name: " + field_name + "nURL: " + url + "nType: " + type + "nWin: " + win);
        // debug/testing    
        /* If you work with sessions in PHP and your client doesn't accept cookies you might need to carry  
        the session name and session ID in the request string (can look like this: "?PHPSESSID=88p0n70s9dsknra96qhuk6etm5"). 
        These lines of code extract the necessary parameters and add them back to the filebrowser URL again. */

        var cmsURL = 'fileList.aspx?cat=<%= "proj" + Request.QueryString["id"] %>';
        // var cmsURL = window.location.toString();    // script URL - use an absolute path!    
        if (cmsURL.indexOf("?") < 0) {
            //add the type as the only query parameter    
            cmsURL = cmsURL + "?type=" + type;
        } else {
            //add the type as an additional query parameter     
            // (PHP session ID is now included if there is one at all)       
            cmsURL = cmsURL + "&type=" + type;
        }
        tinyMCE.activeEditor.windowManager.open({
            file: cmsURL,
            title: "File Browser",
            width: 900,  // Your dimensions may differ - toy around with them!
            height: 400,
            resizable: "yes",
            inline: "yes",  // This parameter only has an effect if you use the inlinepopups plugin!  
            close_previous: "yes"
        },
     { window: win,
         input: field_name
     });
        return false;
    }

    tinyMCE.init({
        // General options
        mode: "exact",
        // MUSI BYT VCETNE SPECIFIKACE OKNA RODICE
        elements: "MainContent_popisCs,MainContent_popisEn",
        theme: "advanced",
        plugins: "imagemanager,autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave",

        //vypnuti zmeny URL na relativni
        convert_urls: false,
        // external_image_list_url : "image.php5?x=",
        //  init_instance_callback : "init",

        // Theme options
        theme_advanced_buttons1: "newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
        theme_advanced_buttons2: "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
        theme_advanced_buttons3: "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,advhr,|,print,|,ltr,rtl,|,fullscreen",
        theme_advanced_buttons4: "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,pagebreak,restoredraft",
        theme_advanced_toolbar_location: "top",
        theme_advanced_toolbar_align: "left",
        theme_advanced_statusbar_location: "bottom",
        theme_advanced_resizing: true,


        // Example content CSS (should be your site CSS)
        content_css: "../layout/layoutContent.css",

        //extjsfilemanager_handlerurl: 'BrowserHandler.ashx',
        //Optional - Any extra parameters you want to have passed to your server-side handler on each request from the FileManager.
        //extjsfilemanager_extraparams: { param1: 'value1', param2: 'value2' }

        // Style formats
        /*   style_formats: [
        { title: 'Bold text', inline: 'b' },
        { title: 'Red text', inline: 'span', styles: { color: '#ff0000'} },
        { title: 'Red header', block: 'h1', styles: { color: '#ff0000'} },
        { title: 'Example 1', inline: 'span', classes: 'example1' },
        { title: 'Example 2', inline: 'span', classes: 'example2' },
        { title: 'Table styles' },
        { title: 'Table row 1', selector: 'tr', classes: 'tablerow1' }
        ],*/
        file_browser_callback: "myFileBrowser"
    });


</script>


<!-- /TinyMCE -->




    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    
        <h2><asp:Label ID="Label1" runat="server" Text="Personál:"></asp:Label></h2>
        <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="1">
            <asp:View ID="View1" runat="server">
                <h3>
                    <asp:Label ID="Label2" runat="server" Text="Přidat nového člověka :"></asp:Label>
                </h3>
                <p>
                    <asp:Label ID="Label11" runat="server" Text="Titul před:"></asp:Label>
                    <asp:TextBox ID="titleBefore1" runat="server" Width="66px"></asp:TextBox>
                    &nbsp;&nbsp;
                    <asp:Label ID="Label12" runat="server" Text="Jméno:"></asp:Label>
                    <asp:TextBox ID="Name1" runat="server" Width="187px"></asp:TextBox>

                    <asp:Label ID="Label5" runat="server" Text="Příjmení:"></asp:Label>
                    <asp:TextBox ID="Surname1" runat="server" Width="211px"></asp:TextBox>
                    &nbsp;&nbsp;
                    <asp:Label ID="Label6" runat="server" Text="Titul za:"></asp:Label>
                    <asp:TextBox ID="titleAfter1" runat="server" Width="85px"></asp:TextBox>

                    <asp:Button ID="Button1" runat="server" onclick="Button1_Click" Text="Přidat" />
                </p>
                <br />
                <br />
                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
                    CellPadding="4" DataKeyNames="id" DataSourceID="SqlDataSource1" 
                    ForeColor="#333333" GridLines="None" AllowSorting="True">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField HeaderText="Jméno" SortExpression="surname">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("name") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:HyperLink ID="HyperLink1" runat="server" 
                                    NavigateUrl='<%# "editPersons.aspx?id=" + Eval("id") %>' 
                                    Text='<%# Eval("degreeBefor") + " " + Eval("surname") + " " + Eval("name") + "" + Eval("degreeAfter") %>'></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="nameCs" HeaderText="Zařazení" 
                            SortExpression="nameCs" />
                        <asp:CheckBoxField DataField="enable" HeaderText="Dostupný na www" 
                            SortExpression="enable" />
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:labsensnanoConnectionString %>" 
                    InsertCommand="INSERT INTO persons(name, surname, degreeBefor, degreeAfter) VALUES (@name, @surname, @degreeBefore, @degreeAfter)" 
                    
                    SelectCommand="SELECT persons.id, persons.name, persons.surname, persons.degreeAfter, persons.degreeBefor, persons.enable, personClass.nameCs FROM persons INNER JOIN personClass ON persons.class = personClass.id ORDER BY persons.surname" 
                    
                    
                    
                    
                    
                    UpdateCommand="UPDATE projects SET titleCs = @titleCs, nick = @nick, enable = @enable WHERE (id = @id)">
                    <InsertParameters>
                        <asp:ControlParameter ControlID="Name1" Name="name" 
                            PropertyName="Text" />
                        <asp:ControlParameter ControlID="Surname1" Name="surname" PropertyName="Text" />
                        <asp:ControlParameter ControlID="titleBefore1" Name="degreeBefore" 
                            PropertyName="Text" />
                        <asp:ControlParameter ControlID="titleAfter1" Name="degreeAfter" 
                            PropertyName="Text" />
                    </InsertParameters>
                </asp:SqlDataSource>
            </asp:View>
            <asp:View ID="View2" runat="server">
                <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:labsensnanoConnectionString %>" 
                    InsertCommand="INSERT INTO products(titleCs) VALUES (@titleCs)" 
                    ProviderName="<%$ ConnectionStrings:labsensnanoConnectionString.ProviderName %>" 
                    
                    SelectCommand="SELECT titleCs, id, titleEn, keyWordsCs, keyWordsEn, typProduktu, descriptionCs, descriptionEn, parametrsCs, parametrsEn, enable FROM products WHERE (id = @id)" 
                    UpdateCommand="UPDATE products SET titleCs = @titleCs, titleEn = @titleEn, keyWordsCs = @keyWordsCs, keyWordsEn = @keyWordsEn, typProduktu = @typProduktu, descriptionCs = @descriptionCs, descriptionEn = @descriptionEn, parametrsCs = @parametrsCs, parametrsEn = @parametrsEn WHERE (id = @id)">
                    <InsertParameters>
                        <asp:Parameter Name="titleCs" />
                    </InsertParameters>
                    <SelectParameters>
                        <asp:QueryStringParameter DefaultValue="0" Name="id" QueryStringField="id" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="titleCs" />
                        <asp:Parameter Name="titleEn" />
                        <asp:Parameter Name="keyWordsCs" />
                        <asp:Parameter Name="keyWordsEn" />
                        <asp:Parameter Name="typProduktu" />
                        <asp:Parameter Name="descriptionCs" />
                        <asp:Parameter Name="descriptionEn" />
                        <asp:Parameter Name="parametrsCs" />
                        <asp:Parameter Name="parametrsEn" />
                        <asp:Parameter Name="id" />
                    </UpdateParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="SqlDataSource3" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:labsensnanoConnectionString %>" 
                    ProviderName="<%$ ConnectionStrings:labsensnanoConnectionString.ProviderName %>" 
                    SelectCommand="SELECT [id], [nameCs] FROM [personClass] ORDER BY [id]">
                </asp:SqlDataSource>

                <p>
                    <asp:Label ID="Label7" runat="server" Text="Titul před:"></asp:Label>
                    <asp:TextBox ID="degreeBefore" runat="server" Width="66px"></asp:TextBox>
                    &nbsp;&nbsp;
                    <asp:Label ID="Label16" runat="server" Text="Jméno:"></asp:Label>
                    <asp:TextBox ID="name" runat="server" Width="187px"></asp:TextBox>

                    <asp:Label ID="Label17" runat="server" Text="Příjmení:"></asp:Label>
                    <asp:TextBox ID="surname" runat="server" Width="211px"></asp:TextBox>
                    &nbsp;&nbsp;
                    <asp:Label ID="Label18" runat="server" Text="Titul za:"></asp:Label>
                    <asp:TextBox ID="degreeAfter" runat="server" Width="85px"></asp:TextBox>

                </p>

                <br />
                <asp:Label ID="Label19" runat="server" Text="Zařazení: "></asp:Label>
                <asp:DropDownList ID="personClass" runat="server" DataSourceID="SqlDataSource3" 
                    DataTextField="nameCs" DataValueField="id">
                </asp:DropDownList>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:CheckBox ID="publikovat" runat="server" Text="Puvlikovat na WWW" />
                <br />
                <br />
                <asp:Label ID="Label4" runat="server" Text="Email: "></asp:Label>
                <asp:TextBox ID="email" runat="server" Width="281px"></asp:TextBox>
                <br />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <br />
                <br />
                <table style="width:100%;" align="center">
                    <tr>
                        <td align="center">
                            &nbsp;
                            <asp:Button ID="Button7" runat="server" onclick="Button7_Click" 
                                style="text-align: center" Text="Zpět na seznam" Width="159px" />
                            &nbsp;&nbsp;
                            <asp:Button ID="Button6" runat="server" style="text-align: center" 
                                Text="Uložit změny" Width="121px" onclick="Button6_Click" />
                            &nbsp;&nbsp; 
                            <asp:Button ID="Button8" runat="server" onclick="Button8_Click" 
                                style="text-align: center" Text="Smazat" Width="121px" />
                        </td>
                    </tr>

                </table>
                <hr />
                <br />

            </asp:View>
        </asp:MultiView>
    
    <br />
    </asp:Content>

