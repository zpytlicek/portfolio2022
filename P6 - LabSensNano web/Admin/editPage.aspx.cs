﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

//správa dat
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

public partial class Admin_editPage : System.Web.UI.Page
{
   
    string page = "home";
    string lang = "cs";
    protected void Page_Load(object sender, EventArgs e)
    {
        
        //parametry stránky z GET
        try
        {
            page = Request.QueryString["page"].ToString();
        }
        catch
        {
            page = "home";
        }
        try
        {
            lang = Request.QueryString["lang"].ToString();
        }
        catch
        {
            lang = "cs";
        }

        if (!IsPostBack)
        {
            SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["labsensnanoConnectionString"].ToString());
            try
            {
                SqlCommand cmdSelect = new SqlCommand("SELECT [contents], [title] FROM [pages] WHERE (([lang] = @lang) AND ([page] = @page))", dbConn);
                cmdSelect.CommandType = System.Data.CommandType.Text;
                SqlParameter prm_lang = new SqlParameter("@lang", SqlDbType.VarChar);
                prm_lang.Value = lang;
                cmdSelect.Parameters.Add(prm_lang);
                SqlParameter prm_page = new SqlParameter("@page", SqlDbType.VarChar);
                prm_page.Value = page;
                cmdSelect.Parameters.Add(prm_page);

                dbConn.Open();

                //kontrolní výpis proměné o počtu výskytů 
                SqlDataReader data = cmdSelect.ExecuteReader();
                if (data.Read())
                {
                    Label1.Text = data["title"].ToString();
                    TextBox1.Text = data["contents"].ToString();
                }

            }
            catch
            {
                Label1.Text = "Chyba komunikace s databází";
                dbConn.Close();
            }
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["labsensnanoConnectionString"].ToString());
        try
        {

            SqlCommand cmdSet = new SqlCommand("UPDATE pages SET contents = @contents, lastchange = @lastchange WHERE (lang = @lang) AND (page = @page)", dbConn);
            cmdSet.CommandType = System.Data.CommandType.Text;
            SqlParameter prm_lang = new SqlParameter("@lang", SqlDbType.VarChar);
            prm_lang.Value = lang;
            cmdSet.Parameters.Add(prm_lang);
            SqlParameter prm_page = new SqlParameter("@page", SqlDbType.VarChar);
            prm_page.Value = page;
            cmdSet.Parameters.Add(prm_page);

            SqlParameter prm_contents = new SqlParameter("@contents", SqlDbType.Text);
            prm_contents.Value = TextBox1.Text.ToString();
            cmdSet.Parameters.Add(prm_contents);

            //nastavení data poslední zmeny
            SqlParameter prm_lastchange = new SqlParameter("@lastchange", SqlDbType.DateTime);
            prm_lastchange.Value = DateTime.Now;
            cmdSet.Parameters.Add(prm_lastchange);

            dbConn.Open();

            //kontrolní výpis proměné o počtu výskytů 
            cmdSet.ExecuteNonQuery().ToString();

        }
        catch
        {
            Label1.Text = "Chyba komunikace s databází";
            dbConn.Close();
        }
        //Response.Redirect("editPage.aspx?page="+page+"&lang="+lang);
        
    }
}