﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

//pro úpravu obrázků
using System.IO;
using System.Drawing;
using System.Configuration;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;

public partial class Admin_Products : System.Web.UI.Page
{
    private static ImageFormat GetFormat(string filename)
    {
        if (filename.EndsWith("jpg") || filename.EndsWith("jpeg") || filename.EndsWith("tiff"))
            return ImageFormat.Jpeg;

        if (filename.EndsWith("gif"))
            return ImageFormat.Gif;

        return ImageFormat.Png;
    }
    private bool ThumbnailCallback()
    {
        return false;
    }

    string id="";
    string puvodniDatumKonec;
    string puvodniDatumStart;

    string imgName = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            id = Request.QueryString["id"].ToString();
        }
        catch { }
                    // zobrazeni edit stránky
        if (id != "")
        {
            MultiView1.SetActiveView(View2);




             SqlDataDetail.SelectParameters["id"].DefaultValue = id;
             if (!IsPostBack)
             {
                try
                {
                    //SELECT id, lang, title, dateStart, text, dateEnd, enable FROM news WHERE (id = @id)
                    DataView dv = (DataView)SqlDataDetail.Select(DataSourceSelectArguments.Empty);
                    if (dv.Count > 0){
                        DataRowView data = dv[0];
                        NewTitle.Text = data["title"].ToString();
                        NewEnable.Checked = bool.Parse(data["enable"].ToString());

                        //parsovaní data
                        DateTime datumDate = DateTime.Parse(data["dateEnd"].ToString());
                        NewEnd.Text = datumDate.ToString("dd.MM.yyyy");
                        puvodniDatumKonec = data["dateEnd"].ToString();

                        DateTime datumDate2 = DateTime.Parse(data["dateStart"].ToString());
                        NewStart.Text = datumDate2.ToString("dd.MM.yyyy");
                        puvodniDatumKonec = data["dateStart"].ToString();

                        NewLang.SelectedValue = data["lang"].ToString();
                        NewText.Text = data["text"].ToString();
                    }
                } catch {
                    NewEnd.Text = puvodniDatumKonec;
                    NewStart.Text = puvodniDatumStart;
                }
            }
        }
        else
        {
            MultiView1.SetActiveView(View1);
        }

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        SqlDataSourceSeznam.InsertParameters["title"].DefaultValue = Title1.Text;
        SqlDataSourceSeznam.InsertParameters["lang"].DefaultValue = Lang1.SelectedValue;
        SqlDataSourceSeznam.Insert();
        SqlDataSourceSeznam.DataBind();
        GridView1.DataBind();

    }

    protected void Button7_Click(object sender, EventArgs e)
    {
        Response.Redirect("editNews.aspx");
    }
    protected void Button6_Click(object sender, EventArgs e)
    {
      
        try

        {
                       //načtení data z položky
                       SqlDataDetail.UpdateParameters["dateStart"].DbType = DbType.DateTime;
                       try
                       {
                           string[] datumCastiS = NewStart.Text.Split('.');
                           DateTime datumStart = new DateTime(Int32.Parse(datumCastiS[2]), Int32.Parse(datumCastiS[1]), Int32.Parse(datumCastiS[0]));
                           SqlDataDetail.UpdateParameters["dateStart"].DefaultValue = datumStart.ToString();
                       }
                       catch
                       {
                           SqlDataDetail.UpdateParameters["dateStart"].DefaultValue = puvodniDatumStart;
                       }

                       SqlDataDetail.UpdateParameters["dateEnd"].DbType = DbType.DateTime;
                       try
                       {
                           string[] datumCastiE = NewEnd.Text.Split('.');
                           DateTime datumEnd = new DateTime(Int32.Parse(datumCastiE[2]), Int32.Parse(datumCastiE[1]), Int32.Parse(datumCastiE[0]));
                           SqlDataDetail.UpdateParameters["dateEnd"].DefaultValue = datumEnd.ToString();
                       }
                       catch
                       {
                           SqlDataDetail.UpdateParameters["dateEnd"].DefaultValue = puvodniDatumStart;
                       }
                       SqlDataDetail.Update();
        }
        finally
        {
            Response.Redirect("editNews.aspx?id=" + id);
        }
            
    }


    protected void Button8_Click(object sender, EventArgs e)
    {
        SqlDataDetail.Delete();
        Response.Redirect("editNews.aspx");
    }
}