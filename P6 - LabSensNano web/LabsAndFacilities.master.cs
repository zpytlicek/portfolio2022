﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;

public partial class Research : System.Web.UI.MasterPage
{
    public string lang = "es";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (CultureInfo.CurrentUICulture.Name == "cs-CZ")
        {
            //nastavení sitemap hlavniho menu pro konkretni jazyk
            SiteMapDataSourceSubmenu.SiteMapProvider = "lsnMenuCs";
            
            lang = "cs";
        }
        else
        {
            //nastavení sitemap hlavniho menu pro konkretni jazyk
            SiteMapDataSourceSubmenu.SiteMapProvider = "lsnMenuEn";
            
            lang = "en";
        }
    }

}
