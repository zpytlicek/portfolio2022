﻿<%@ Page Title="LabSensNano" Language="C#" MasterPageFile="~/introContents.master" AutoEventWireup="true" CodeFile="Photos.aspx.cs" Inherits="introTest" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SubMenu" Runat="Server">
    <table cellspacing='0' cellpadding='0' align='center' border='0' width='300'>
        <tr><td class='borderLeftTop' width='20'></td><td class='borderTop'></td><td class='borderRightTop' width='20'></td></tr>

        <tr>
        <td class='borderLeft' width='20'></td><td class='borderCenter submenu2'>

            <asp:SiteMapDataSource ID="SiteMapDataSourceSubmenu" runat="server" 
                StartingNodeUrl="~/About.aspx" />
                <asp:Menu ID="Menu2" runat="server" DataSourceID="SiteMapDataSourceSubmenu" 
                MaximumDynamicDisplayLevels="0" StaticDisplayLevels="2" ItemWrap="True">
            </asp:Menu>


        </td><td class='borderRight' width='20'></td>
        </tr>
        <tr>
        <td class='borderLeftBottom' width='20'></td><td class='borderBottom'></td><td class='borderRightBottom' width='20'></td>
        </tr>
    </table>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="Main" Runat="Server">
    <table cellspacing='0' cellpadding='0' align='center' border='0' width='736'>
        <tr><td class='borderLeftTop' width='20'></td><td class='borderTop'></td><td class='borderRightTop' width='20'></td></tr>

        <tr><td class='borderLeft' width='20'></td>
        <td class='borderCenter' height="1px" style="height: 1px">
            <asp:DataList ID="DataList1" runat="server" DataKeyField="id" 
                DataSourceID="SqlDataSource1" Width="679px">
                <ItemTemplate>
                    <asp:Label ID="titleLabel" runat="server" Font-Size="Small" ForeColor="#00A4E3" 
                        Text='<%# Eval("title") %>' Font-Bold="True" />
                    <table style="width:675px;padding:0px;margin:0px; ">
                        <tr>
                            <td style="padding-left: 15px;padding-right: 15px;">
                                <asp:Label ID="textLabel" runat="server" Text='<%# Eval("text") %>' />
                            </td>
                        </tr>
                    </table>
                   </ItemTemplate>
                <SeparatorTemplate>
                    <hr style="border: 1px solid #ddd;" />
                </SeparatorTemplate>
            </asp:DataList>
                        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                            ConnectionString="<%$ ConnectionStrings:testDBConnectionString %>" 
                            ProviderName="<%$ ConnectionStrings:testDBConnectionString.ProviderName %>" 
                            
                SelectCommand="SELECT [titleCs], [textCs] FROM [gallery]">
                        </asp:SqlDataSource>
                        <asp:HiddenField ID="HiddenField1" runat="server" />
        </td>
        <td class='borderRight' width='20'></td></tr>

        <tr><td class='borderLeftBottom' width='20'></td><td class='borderBottom'></td><td class='borderRightBottom' width='20'></td></tr>
    </table>
</asp:Content>

