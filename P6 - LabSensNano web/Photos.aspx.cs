﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

//kultura
using System.Globalization;
using System.Threading;
//pro zmenu SiteMapProvider
using System.Configuration;
public partial class introTest : System.Web.UI.Page
{

    string lang = "cs";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (CultureInfo.CurrentUICulture.Name == "cs-CZ")
        {
            //nastavení sitemap hlavniho menu pro konkretni jazyk
            SiteMapDataSourceSubmenu.SiteMapProvider = "lsnMenuCs";
            SqlDataSource1.SelectCommand = "SELECT id AS id, titleCs AS title, textCs AS text, enable FROM gallery WHERE (enable = 1) AND (titleCs IS NOT NULL) AND (textCs IS NOT NULL) ORDER BY eventDate";
            HiddenField1.Value = "cs";
        }
        else
        {
            //nastavení sitemap hlavniho menu pro konkretni jazyk
            SiteMapDataSourceSubmenu.SiteMapProvider = "lsnMenuEn";
            SqlDataSource1.SelectCommand = "SELECT id AS id, titleEn AS title, textEn AS text, enable FROM gallery WHERE (enable = 1) AND (titleCs IS NOT NULL) AND (textCs IS NOT NULL) ORDER BY eventDate";
            HiddenField1.Value = "en";
        }
    }
}