﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

//kultura
using System.Globalization;
using System.Threading;
//pro zmenu SiteMapProvider
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
public partial class introTest : System.Web.UI.Page
{
    string lang = "cs";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (CultureInfo.CurrentUICulture.Name == "cs-CZ")
        {
            //nastavení sitemap hlavniho menu pro konkretni jazyk
            lang = "cs";
            //SqlDataSource1.SelectCommand = "SELECT products.id, products.titleCs AS title, products.typProduktu, productTyps.typNameCs AS typName, products.enable FROM products INNER JOIN productTyps ON products.typProduktu = productTyps.id WHERE (products.enable = 1 AND products.titleCs <> '') ORDER BY products.titleCs";
            SqlDataSource1.SelectCommand = "SELECT YEAR(products.publication) AS published, products.id, products.titleCs AS title, products.typProduktu, productTyps.typNameCs AS typName, products.enable FROM products INNER JOIN productTyps ON products.typProduktu = productTyps.id WHERE (products.enable = 1 AND products.titleCs <> '') ORDER BY published DESC, titleCs";
            
            
        }
        else
        {
            //nastavení sitemap hlavniho menu pro konkretni jazyk
            lang = "en";
            //SqlDataSource1.SelectCommand = "SELECT products.id, products.titleEn AS title, products.typProduktu, productTyps.typNameEn AS typName, products.enable FROM products INNER JOIN productTyps ON products.typProduktu = productTyps.id WHERE (products.enable = 1 AND products.titleEn <> '') ORDER BY products.titleEn";
            SqlDataSource1.SelectCommand = "SELECT YEAR(products.publication) AS published, products.id, products.titleEn AS title, products.typProduktu, productTyps.typNameEn AS typName, products.enable FROM products INNER JOIN productTyps ON products.typProduktu = productTyps.id WHERE (products.enable = 1 AND products.titleEn <> '') ORDER BY published DESC, titleEn";
        }

        string id = "";
        try
        {
            id = Request.QueryString["id"].ToString();
        }
        catch { }

        if (id != "")
        {
            //zobrazení obsahu podle zadaného produktu
            MultiView1.SetActiveView(View2);
                SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["labsensnanoConnectionString"].ToString());
                try
                {
                    SqlCommand cmdSelect;
                    if (lang == "cs")
                    {
                        cmdSelect = new SqlCommand("SELECT [publication] AS publication, [titleCs] AS title, [keyWordsCs] AS keyWords, [typProduktu], [descriptionCs] AS description, [parametrsCs] AS parametrs FROM [products] WHERE ([id] = @id)", dbConn);
                    }
                    else
                    {
                        cmdSelect = new SqlCommand("SELECT [publication] AS publication, [titleEn] AS title, [keyWordsEn] AS keyWords, [typProduktu], [descriptionEn] AS description, [parametrsEn] AS parametrs FROM [products] WHERE ([id] = @id)", dbConn);
                    }



                    cmdSelect.CommandType = System.Data.CommandType.Text;
                    SqlParameter prm_id = new SqlParameter("@id", SqlDbType.VarChar);
                    prm_id.Value = id;
                    cmdSelect.Parameters.Add(prm_id);

                    dbConn.Open();

                    //kontrolní výpis proměné o počtu výskytů 
                    SqlDataReader data = cmdSelect.ExecuteReader();
                    if (data.Read())
                    {
                        titulek.Text = data["title"].ToString();
                        Popis.Text = data["description"].ToString();
                        if (data["parametrs"].ToString() == "")
                        {
                            Label9.Visible = false;
                            parametry.Visible = false;
                        }
                        else
                        {

                            parametry.Text = data["parametrs"].ToString();
                        }

                        if (data["keyWords"].ToString() != "")
                        {
                            Label12.Text = "<br><br>" + Label12.Text;
                            keywords.Text = data["keyWords"].ToString();
                        }
                        else
                        {
                            Label12.Visible = false;
                            keywords.Visible = false;
                        }


                        DateTime datumDate = DateTime.Parse(data["publication"].ToString());

                        published.Text = datumDate.ToString("yyyy");
                        
                        //published.Text = data["keyWords"].ToString();
                        /*
                        TextBox3.Text = data["titleEn"].ToString();
                        popisCs.Text = data["descriptionCs"].ToString();
                        popisEn.Text = data["descriptionEn"].ToString();
                        paramEn.Text = data["parametrsEn"].ToString();
                        paramCs.Text = data["parametrsCs"].ToString();
                        keyWordsCs.Text = data["keyWordsCs"].ToString();
                        keyWordsEn.Text = data["keyWordsEn"].ToString();
                        DropDownList1.SelectedValue = data["typProduktu"].ToString();

                        //nacteni data z databaze do editacniho pole
                        DateTime datumDate = DateTime.Parse(data["publication"].ToString());
                        datum.Text = datumDate.ToString("dd.MM.yyyy");

                        puvodniDatum = data["publication"].ToString();

                        //nacteni bin hodnoty do checked pole
                        publikovat.Checked = bool.Parse(data["enable"].ToString());*/
                    }
                    else
                    {
                        Response.Redirect("Products.aspx");
                    }
                    
                } catch {
                    dbConn.Close();
                }
            }


        else {
            MultiView1.SetActiveView(View1);
        }
            

        if (CultureInfo.CurrentUICulture.Name == "cs-CZ")
        {
            //nastavení sitemap hlavniho menu pro konkretni jazyk
            SiteMapDataSourceSubmenu.SiteMapProvider = "lsnMenuCs";
        }
        else
        {
            //nastavení sitemap hlavniho menu pro konkretni jazyk
            SiteMapDataSourceSubmenu.SiteMapProvider = "lsnMenuEn";
        }
    }
}