﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

//kultura
using System.Globalization;
using System.Threading;
//pro zmenu SiteMapProvider
using System.Configuration;

using System.Globalization;
using System.Data.SqlClient;
using System.Data;

public partial class introTest : System.Web.UI.Page
{
    string page = "res";
    public string lang = "es";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (CultureInfo.CurrentUICulture.Name == "cs-CZ")
        {
            //nastavení sitemap hlavniho menu pro konkretni jazyk
            lang = "cs";
        }
        else
        {
            //nastavení sitemap hlavniho menu pro konkretni jazyk
            lang = "en";
        }
        string id = "";
        try
        {
            id = Request.QueryString["id"].ToString();
        }
        catch { }
        if (id != "")
        {
            //pokud je vybrán tým

            SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["labsensnanoConnectionString"].ToString());
            SqlCommand cmdSelect;
            if (lang == "cs")
            {
                cmdSelect = new SqlCommand("SELECT [titleCs] AS title, [textCs] AS text, [id], [nick], convert(varchar, [start], 104) AS [start], convert(varchar, [finish], 104) AS [finish] FROM [projects] WHERE ([id] = '" + id + "' AND [titleCs] <> '')", dbConn);
            }
            else
            {
                cmdSelect = new SqlCommand("SELECT [titleEn] AS title, [textEn] AS text, [id], [nick], convert(varchar, [start], 102) AS [start], convert(varchar, [finish], 102) AS [finish] FROM [projects] WHERE ([id] = '" + id + "' AND [titleEn] <> '')", dbConn);
            }

            if (!IsPostBack)
            {
                try
                {
                    dbConn.Open();

                    //kontrolní výpis proměné o počtu výskytů 
                    SqlDataReader data = cmdSelect.ExecuteReader();
                    if (data.Read())
                    {
                         // Vypis dat pro konkrétní stránku
                         Page.Title = "LabSensNano - " + data["title"].ToString();
                         ProjectTitle.Text = data["title"].ToString();
                         nick.Text = data["nick"].ToString();
                        // string datum;
                        // datum = data["start"].ToString();
                        
                         start.Text = data["start"].ToString();
                         end.Text = data["finish"].ToString();
                         content.Text = data["text"].ToString();


                    }
                    else
                    {
                        Response.Redirect("Research.aspx");
                    }

                }
                catch
                {
                    ProjectTitle.Text = "Chyba komunikace s databází LabSensNano.";
                    dbConn.Close();
                    Response.Redirect("Research.aspx");
                }
            }

        }
        else
        {
            Response.Redirect("Research.aspx");
        }
       
    }
}