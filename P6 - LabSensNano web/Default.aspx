﻿<%@ Page Title="LabSensNano" Language="C#" MasterPageFile="~/introContents.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="introTest" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SubMenu" Runat="Server">
    <table cellspacing='0' cellpadding='0' align='center' border='0' width='300'>
        <tr><td class='borderLeftTop' width='20'></td><td class='borderTop'></td><td class='borderRightTop' width='20'></td></tr>

        <tr>
        <td class='borderLeft' width='20'></td><td class='borderCenter submenu'>
                <h5>
                    <asp:Label ID="Label1" runat="server" Text="<%$ Resources:labels, newsTitle %>"></asp:Label>
                </h5>
                <table cellspacing='0' cellpadding='0' align='left' border='0' >
                    <tr><td valign=top>

                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
                            BorderStyle="None" DataSourceID="SqlDataSource1" GridLines="None" 
                            ShowHeader="False" Width="250px">
                            <Columns>
                                <asp:TemplateField HeaderText="title" SortExpression="title">
                                    <ItemTemplate>
                                        <table style="width:100%;">
                                            <tr>
                                                <td align="right" style="height: 19px" valign="top" width="10">
                                                    &bull;</td>
                                                <td style="height: 19px" valign="top">
                                                    <asp:HyperLink ID="HyperLink1" runat="server" BorderStyle="None" 
                                                        CssClass="linka" NavigateUrl='<%# "news.aspx#" + Eval("id") %>' 
                                                        Text='<%# Eval("title", "{0}") %>'></asp:HyperLink>
                                                </td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Eval("title") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemStyle Width="195px" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>

                        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                            ConnectionString="<%$ ConnectionStrings:labsensnanoConnectionString %>" 
                            ProviderName="<%$ ConnectionStrings:labsensnanoConnectionString.ProviderName %>" 
                            SelectCommand="SELECT TOP (5) id, title, lang, dateStart, dateEnd, enable FROM news WHERE (enable = 1) AND (dateEnd &gt;= dateStart) AND (lang = @lang) AND (title &lt;&gt; '&quot;&quot;') AND (dateEnd &gt; GETDATE()) AND (dateStart &lt; GETDATE()) OR (dateStart = GETDATE()) ORDER BY dateStart DESC">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="HiddenField1" DefaultValue="cs" Name="lang" 
                                    PropertyName="Value" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                        <asp:HiddenField ID="HiddenField1" runat="server" />
                    </td></tr>
                </table>
        </td><td class='borderRight' width='20'></td>
        </tr>

        <tr>
        <td class='borderMiddleLeft' width='20'></td><td class='borderMiddle'></td><td class='borderMiddleRight' width='20'></td>
        </tr>

        <tr>
        <td class='borderLeft' width='20'></td><td class='borderCenter submenu'>
                <h5>
                    <asp:Label ID="Label2" runat="server" 
                        Text="<%$ Resources:labels, projektyTitle %>"></asp:Label>
                    <asp:DataList ID="DataList1" runat="server" DataKeyField="id" 
                        DataSourceID="SqlDataSource2">
                        <ItemTemplate>
                            <table style="width:255px;">
                                <tr>
                                    <td align="center" style="padding:5px">
                                        <asp:HyperLink ID="HyperLink2" runat="server" 
                                            ImageUrl='<%# "images/projects/" + Eval("id") + "/" + Eval("imgName") %>' 
                                            NavigateUrl='<%# "projects.aspx?id=" + Eval("id") %>' 
                                            Text='<%# Eval("nick") %>'></asp:HyperLink>
                                    </td>

                                </tr>
                            </table>
                        </ItemTemplate>
                    </asp:DataList>
                    <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:labsensnanoConnectionString %>" 
                        SelectCommand="SELECT id, nick, imgName FROM projects WHERE (NOT (imgName IS NULL)) AND (enable = 1) ORDER BY start">
                    </asp:SqlDataSource>
                </h5>
        </td><td class='borderRight' width='20'></td>
        </tr>

        <tr>
        <td class='borderLeftBottom' width='20'></td><td class='borderBottom'></td><td class='borderRightBottom' width='20'></td>
        </tr>
    </table>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="Main" Runat="Server">
    <table cellspacing='0' cellpadding='0' align='center' border='0' width='736'>
        <tr><td class='borderLeftTop' width='20'></td><td class='borderTop'></td><td class='borderRightTop' width='20'></td></tr>

        <tr><td class='borderLeft' width='20'></td>
        <td class='borderCenter' style='padding-left:10px;padding-right:10px;'>
            <asp:Label ID="contents" runat="server" Text="contents"></asp:Label>
        </td>
        <td class='borderRight' width='20'></td></tr>

        <tr><td class='borderLeftBottom' width='20'></td><td class='borderBottom'></td><td class='borderRightBottom' width='20'></td></tr>
    </table>
</asp:Content>

