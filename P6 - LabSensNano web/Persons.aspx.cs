﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

//kultura
using System.Globalization;
using System.Threading;
//pro zmenu SiteMapProvider
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
public partial class introTest : System.Web.UI.Page
{
    string page = "about";
    protected void Page_Load(object sender, EventArgs e)
    {
        //ovládání obrázku na masterpage dle zvoleného country
        string lang = "en";

        if (CultureInfo.CurrentUICulture.Name == "cs-CZ")
        {
            lang = "cs";
            HiddenField1.Value = "cs";
            //nastavení sitemap hlavniho menu pro konkretni jazyk
            SiteMapDataSourceSubmenu.SiteMapProvider = "lsnMenuCs";
        }
        else
        {
            //nastavení sitemap hlavniho menu pro konkretni jazyk
            SiteMapDataSourceSubmenu.SiteMapProvider = "lsnMenuEn";
            HiddenField1.Value = "en";
        }

        if (!IsPostBack)
        {

       /*     SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["labsensnanoConnectionString"].ToString());
            try
            {
                SqlCommand cmdSelect = new SqlCommand("SELECT [contents], [title] FROM [pages] WHERE (([lang] = @lang) AND ([page] = @page))", dbConn);
                cmdSelect.CommandType = System.Data.CommandType.Text;
                SqlParameter prm_lang = new SqlParameter("@lang", SqlDbType.VarChar);
                prm_lang.Value = lang;
                cmdSelect.Parameters.Add(prm_lang);
                SqlParameter prm_page = new SqlParameter("@page", SqlDbType.VarChar);
                prm_page.Value = page;
                cmdSelect.Parameters.Add(prm_page);

                dbConn.Open();

                //kontrolní výpis proměné o počtu výskytů 
                SqlDataReader data = cmdSelect.ExecuteReader();
                if (data.Read())
                {
                    Page.Title = data["title"].ToString();
                    contents.Text = data["contents"].ToString();
                }

            }
            catch
            {
                contents.Text = "Chyba komunikace s databází LabSensNano.";
                dbConn.Close();
            }*/
        }
    }
}