﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

//pro úpravu obrázků
using System.IO;
using System.Drawing;
using System.Configuration;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data;


public partial class Default6 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        string ContentType;
         ContentType=FileUpload1.PostedFile.ContentType;
         if ((ContentType == "image/jpeg" || ContentType == "image/gif" || ContentType == "image/pjpeg" || ContentType == "image/png") && FileUpload1.PostedFile.ContentLength < 1000000)
         {
            // Label1.Text = FileUpload1.PostedFile.ContentType + " " + FileUpload1.PostedFile.FileName + " " + FileUpload1.PostedFile.ContentLength;
             //nacteni obrázku do pole
             byte[] Picture_content;
             Picture_content = new byte[FileUpload1.PostedFile.ContentLength];
             FileUpload1.PostedFile.InputStream.Read(Picture_content, 0, FileUpload1.PostedFile.ContentLength);

             // zjištění rozměrů obrázku. Lze však zjistit i více
             MemoryStream stream = new MemoryStream();
             stream.Write(Picture_content, 0, Picture_content.Length);
             Bitmap objBitmap = new Bitmap(stream);
             Size SizePicture = objBitmap.Size;
             objBitmap.Dispose();
             stream.Close();

             // vytvoření spojení do databáze
             //načtení počtu řádků z databáze (skalární výseledek na SQL dotaz)
             SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["labsensnanoConnectionString"].ToString());

             // vytvoření SQL příkazu
             SqlCommand cmd = new SqlCommand("ImageAdd", conn);
             // budeme používat uloženou proceduru
             cmd.CommandType = CommandType.StoredProcedure;

             /*
              *      <asp:Parameter Name="datapath" Type="String" />
                            
              */

             // priprava parametru pro obrazek
             SqlParameter prm_picture=new SqlParameter("@data",SqlDbType.Image);
             prm_picture.Value=Picture_content;
             cmd.Parameters.Add(prm_picture);

             SqlParameter prm_picture2=new SqlParameter("@preview",SqlDbType.Image);
             prm_picture2.Value=Picture_content;
             cmd.Parameters.Add(prm_picture2);

              SqlParameter prm_width=new SqlParameter("@width",SqlDbType.Int);
              prm_width.Value = SizePicture.Width;
              cmd.Parameters.Add(prm_width);

              SqlParameter prm_height=new SqlParameter("@height",SqlDbType.Int);
              prm_height.Value = SizePicture.Height;
              cmd.Parameters.Add(prm_height);

              SqlParameter prm_size=new SqlParameter("@size",SqlDbType.Int);
              prm_size.Value = FileUpload1.PostedFile.ContentLength;
              cmd.Parameters.Add(prm_size);

              SqlParameter prm_contenttype=new SqlParameter("@contenttype",SqlDbType.NChar,60);
              prm_contenttype.Value = FileUpload1.PostedFile.ContentType;
              cmd.Parameters.Add(prm_contenttype);

              SqlParameter prm_category=new SqlParameter("@category",SqlDbType.NChar,12);
              prm_category.Value = "all";
              cmd.Parameters.Add(prm_category);

              SqlParameter prm_title=new SqlParameter("@title",SqlDbType.NChar,60);
              prm_title.Value = title.Text;
              cmd.Parameters.Add(prm_title);
             
              //Otevřeme spojení do databáze a provedeme uloženou proceduru s parametry, které jsme si připravili.
              try {
                  conn.Open();
                  cmd.ExecuteNonQuery(); 
              }
              finally {
                  conn.Close();
              }

             Error.Text = "Soubor byl uložen"; 
         }
         else
         {
             Error.Text = "Zvolený soubor není obrázek, nebo je příliš velký.";
         }
         
 
    }
}