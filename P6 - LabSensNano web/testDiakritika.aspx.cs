﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

// prostory pouzite pro osdstraneni diaktritiky
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;


public partial class testDiakritika : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public static string RemoveWhitespace(string str) {
        try {
            return new Regex(@"\s{2,}").Replace(str, " "); 
        } catch (Exception) {
            return str; 
        } 
    }


    // using System.Globalization
    public static string RemoveDiacritics(string s)
    {
        s = s.Normalize(NormalizationForm.FormD);
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < s.Length; i++)
        {
            if (CharUnicodeInfo.GetUnicodeCategory(s[i]) != UnicodeCategory.NonSpacingMark) sb.Append(s[i]);
        }
        string output = sb.ToString();
        output = output.Trim();
        output = RemoveWhitespace(output);

        return output;
    }

    public string createSEOTitle(string m_strText)
    {
    //m_strURL = m_strURL.Replace("&", "and");
        m_strText = Regex.Replace(m_strText, @"[^\w^\.]", "_");
            do
            {
               m_strText = m_strText.Replace("__", "_");
            } while (m_strText.Contains("__"));
            if (m_strText.EndsWith("_"))
                m_strText = m_strText.Substring(0, m_strText.Length - 1);
            if (m_strText.StartsWith("_"))
                m_strText = m_strText.Substring(1, m_strText.Length - 1);
            return m_strText.ToLower();
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        TextBox1.Text = RemoveDiacritics(TextBox1.Text);
        TextBox1.Text = createSEOTitle(TextBox1.Text);
    }
}