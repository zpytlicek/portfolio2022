﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

//kultura
using System.Globalization;
using System.Threading;
//pro zmenu SiteMapProvider
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
public partial class introTest : System.Web.UI.Page
{
    string lang = "cs";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (CultureInfo.CurrentUICulture.Name == "cs-CZ")
        {
            //nastavení sitemap hlavniho menu pro konkretni jazyk
            lang = "cs";
            SqlDataSource1.SelectCommand = "SELECT nameCs AS name, id, typCs AS typ, enable FROM facilities WHERE (enable = 1) AND (nameCs <> '') AND (owner = 0) ORDER BY nameCs";
            SqlDataSource2.SelectCommand = "SELECT nameCs AS name, id, typCs AS typ, enable FROM facilities WHERE (enable = 1) AND (nameCs <> '') AND (owner = 1) ORDER BY nameCs";
            
        }
        else
        {
            //nastavení sitemap hlavniho menu pro konkretni jazyk
            lang = "en";
            SqlDataSource1.SelectCommand = "SELECT nameEn AS name, id, typEn AS typ, enable FROM facilities WHERE (enable = 1) AND (nameEn <> '') AND (owner = 0) ORDER BY nameEn";
            SqlDataSource2.SelectCommand = "SELECT nameEn AS name, id, typEn AS typ, enable FROM facilities WHERE (enable = 1) AND (nameEn <> '') AND (owner = 1) ORDER BY nameEn";

        }

        string id = "";
        try
        {
            id = Request.QueryString["id"].ToString();
        }
        catch { }

        if (id != "")
        {
            //zobrazení obsahu podle zadaného produktu
            MultiView1.SetActiveView(View2);
                SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["labsensnanoConnectionString"].ToString());
                try
                {
                    SqlCommand cmdSelect;
                    if (lang == "cs")
                    {
                        cmdSelect = new SqlCommand("SELECT nameCs AS name, typCs AS typ, textCs AS text FROM facilities WHERE (id = '"+ id +"')", dbConn);
                    }
                    else
                    {
                        cmdSelect = new SqlCommand("SELECT nameEn AS name, typEn AS typ, textEn AS text FROM facilities WHERE (id = '" + id + "')", dbConn);
                    }


                    dbConn.Open();

                    //kontrolní výpis proměné o počtu výskytů 
                    SqlDataReader data = cmdSelect.ExecuteReader();
                    if (data.Read())
                    {
                        Name.Text = data["name"].ToString();
                        type.Text = data["typ"].ToString();
                        Contents.Text = data["text"].ToString();

                    }
                    else
                    {
                        Response.Redirect("Facilities.aspx");
                    }
                    
                } catch {
                    dbConn.Close();
                }
            }


        else {
            MultiView1.SetActiveView(View1);
        }
            

    }
}