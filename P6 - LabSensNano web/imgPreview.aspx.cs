﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Drawing.Imaging;
using System.IO;
using System.Drawing;
using System.Data;

public partial class imgPreview : System.Web.UI.Page
{
    public byte[] FileToArray(string sFilePath) {
        System.IO.FileStream fs = new System.IO.FileStream(sFilePath, System.IO.FileMode.Open, System.IO.FileAccess.Read);
        System.IO.BinaryReader br = new System.IO.BinaryReader(fs); 
        Byte[] bytes = br.ReadBytes((Int32)fs.Length); 
        br.Close();
        fs.Close(); 
        return bytes; 
    }

    protected void Page_Load(object sender, EventArgs e)
    {
       /* Label1.Text = (Convert.ToInt32(Page.RouteData.Values["id"])).ToString();
        Label2.Text = Page.RouteData.Values["name"].ToString(); */

        // Put user code to initialize the page here 
            byte[] data;
            SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["labsensnanoConnectionString"].ToString());
            int id = 0;
            string typImg = "image/png";
            data = null;
            try
            {
                id = Convert.ToInt32(Page.RouteData.Values["id"]);
                // vytvoření spojení do databáze
                //načtení počtu řádků z databáze (skalární výseledek na SQL dotaz)
                
                    //sql dotaz na pocet sloupcu kde idKnihovnika = id vybraneho knihovnika (e.Keys[0] je kličový parametr v dané tabulce)
                    SqlCommand cmdCount = new SqlCommand("SELECT COUNT(*) FROM images WHERE (id=@id)", dbConn);
                    cmdCount.CommandType = System.Data.CommandType.Text;
                    SqlParameter prm_picture2 = new SqlParameter("@id", SqlDbType.Int);
                    prm_picture2.Value = id;
                    cmdCount.Parameters.Add(prm_picture2);

                    SqlCommand cmd = new SqlCommand("select preview,contenttype from images where id=@id", dbConn);
                    cmd.Parameters.AddWithValue("@id",id);

                    //otevreni spojeni s databazi
                    dbConn.Open();                
                    //kontrolní výpis proměné o počtu výskytů 
                    int pocetVyskytu = (int)cmdCount.ExecuteScalar();
                    if (pocetVyskytu > 0)
                    {

                        SqlDataReader rdr = cmd.ExecuteReader();
                        rdr.Read();
                        data = (byte[])rdr["preview"];
                        typImg = (string)rdr["contenttype"];
                        rdr.Close();
                        dbConn.Close();
                        // zjištění rozměrů obrázku 

                    }
                    else
                    {
                        data = FileToArray(Page.MapPath("Images/noImage.png"));
                    }
                    //Bitmap objBitmap = new Bitmap(stream);

            }
            catch {
                typImg = "image/png";
                data = FileToArray(Page.MapPath("Images/noImage.png"));
            }
            finally
            {
                    // odeslání výsledku na klienta 
                    MemoryStream stream = new MemoryStream();
                    stream.Write(data, 0, data.Length);
                    Bitmap objBitmap = new Bitmap(stream);
                    // odeslání výsledku na klienta 
                    //Label1.Text = typImg;
                    Response.Clear();
                    Response.ContentType = typImg;
                    switch (typImg)
                    {
                        case "image/gif":
                            objBitmap.Save(stream, System.Drawing.Imaging.ImageFormat.Gif);
                            stream.WriteTo(Response.OutputStream);
                            //Label1.Text = "image/gif";
                            break;
                        case "image/png":
                        case "image/x-png":
                            objBitmap.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                            stream.WriteTo(Response.OutputStream);
                            break;
                        default:
                            objBitmap.Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg);
                            stream.WriteTo(Response.OutputStream);
                            break;
                    }
                   Response.Flush();
                   Response.End();

                    //uzavreni spojeni s databazi
                    dbConn.Close();    
                    // uvolnění paměti 
                    stream.Close();
                    objBitmap.Dispose();

            }
    }
}