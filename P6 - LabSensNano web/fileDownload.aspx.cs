﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Drawing.Imaging;
using System.IO;
using System.Drawing;
using System.Data;

public partial class imgPreview : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        int id = 0;
        SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["labsensnanoConnectionString"].ToString());
        try
        {
            //cmd = new SqlCommand("SELECT ID, data, datapath, contenttype FROM files WHERE ID=@id", conn);
            id = Convert.ToInt32(Page.RouteData.Values["id"]);
            // vytvoření spojení do databáze
            //načtení počtu řádků z databáze (skalární výseledek na SQL dotaz)

            //sql dotaz na pocet sloupcu kde idKnihovnika = id vybraneho knihovnika (e.Keys[0] je kličový parametr v dané tabulce)
            SqlCommand cmdCount = new SqlCommand("SELECT COUNT(*) FROM files WHERE (id=@id)", dbConn);
            cmdCount.CommandType = System.Data.CommandType.Text;
            SqlParameter prm_id = new SqlParameter("@id", SqlDbType.Int);
            prm_id.Value = id;
            cmdCount.Parameters.Add(prm_id);

            SqlCommand cmd = new SqlCommand("SELECT ID, data, datapath, contenttype FROM files WHERE ID=@id", dbConn);
            cmd.Parameters.AddWithValue("@id", id);

            //otevreni spojeni s databazi
            dbConn.Open();
            //kontrolní výpis proměné o počtu výskytů 
            int pocetVyskytu = (int)cmdCount.ExecuteScalar();
            if (pocetVyskytu > 0)
            {
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.Read()) //mam soubor
                {
                    Response.ContentType = dr["contenttype"].ToString();
                    Response.AppendHeader("Content-Disposition", "attachment; filename=" + dr["datapath"].ToString());
                    Response.BinaryWrite((byte[])dr["data"]);
                }
            }
        }
        catch (SqlException ex)
        {

        }
        finally
        {
            dbConn.Close();
        }

    }
}