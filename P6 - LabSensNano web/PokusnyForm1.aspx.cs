﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

//pro načtení SqlConnectionStrings 
using System.Configuration;
using System.Data.Sql;
using System.Data.SqlClient;

public partial class PokusnyForm1 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (MultiView1.ActiveViewIndex == -1) MultiView1.ActiveViewIndex = 0;
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        switch (MultiView1.ActiveViewIndex){
            case 0:
                MultiView1.ActiveViewIndex = 1;
                break;
            case 1:
                MultiView1.ActiveViewIndex = 0;
                break;
            default:
                MultiView1.ActiveViewIndex = 0;
                break;
        }
    }
    protected void Button2_Click(object sender, EventArgs e)
    {

    }
    protected void TextBox1_TextChanged(object sender, EventArgs e)
    {
        Label1.Text = SqlDataSource3.SelectCommand;
    }
    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {
        Label1.Text = GridView1.SelectedIndex.ToString();
      //  Label1.Text = GridView1.SelectedDataKey.Value.ToString();
        Label1.Text = GridView1.SelectedValue.ToString();
    }
    protected void Button3_Click(object sender, EventArgs e)
    {
        Label1.Text = SqlDataSource3.InsertParameters.Count.ToString();
        //SqlDataSource3.InsertParameters["@name"] = "test";
        //Label1.Text = SqlDataSource3.InsertParameters["@name"].DefaultValue.ToString();
        /*SqlParameter insertedKey = new SqlParameter("@PK_New", SqlDbType.Int);
    insertedKey.Direction    = ParameterDirection.Output;        
    e.Command.Parameters.Add(insertedKey);*/
        
        //SqlDataSource3.InsertParameters["name"].DefaultValue.ToString();

        // nalezení a vytvoření instance komponenty v tamplate footer
        TextBox tbName = GridView2.FooterRow.FindControl("TextBoxName") as TextBox;
        TextBox tbAdresa = GridView2.FooterRow.FindControl("TextBoxAdresa") as TextBox;
        // nastavení parametru pro insert
        SqlDataSource3.InsertParameters["name"].DefaultValue = tbName.Text.ToString();
        SqlDataSource3.InsertParameters["adresa"].DefaultValue = tbAdresa.Text.ToString();
        try
        {
            SqlDataSource3.Insert();
        }
        finally { }
        //Label1.Text = SqlDataSource3.InsertParameters["name"].DefaultValue.ToString();

        
    }
    protected void GridView2_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        //(toto lze také řešit triggerem v SQL nebo spuštěním procedůry která před samotným smazání zrůší vazby, popřípadě je nutno vypsat hlášku

        //načtení počtu řádků z databáze (skalární výseledek na SQL dotaz)
        SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["testDBConnectionString"].ToString());
        try
        {
            //otevreni spojeni s databazi
            dbConn.Open();
            //sql dotaz na pocet sloupcu kde idKnihovnika = id vybraneho knihovnika (e.Keys[0] je kličový parametr v dané tabulce)
            SqlCommand cmdCount = new SqlCommand("SELECT COUNT(*) FROM Persony WHERE (knihovnik = " + e.Keys[0].ToString() + ")", dbConn);
            //kontrolní výpis proměné o počtu výskytů 
            Label1.Text = cmdCount.ExecuteScalar().ToString();
        }
        finally 
        {
            //uzavreni spojeni s databazi
            dbConn.Close();
        }
        if (Label1.Text != "0")
        {   
            //zruseni provadeni akce tlacitka delete
            e.Cancel = true;
        }
    }
}