﻿<%@ Page Title="LabSensNano" Language="C#" MasterPageFile="~/LabsAndFacilities.master" AutoEventWireup="true" CodeFile="Labs.aspx.cs" Inherits="introTest" %>



<asp:Content ID="Content2" ContentPlaceHolderID="Content" Runat="Server">
            <asp:MultiView ID="MultiView1" runat="server">
                <asp:View ID="View1" runat="server">
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:testDBConnectionString %>" 
                        
                        
                        
                        
                        
                        SelectCommand="SELECT nameCs AS name, id, enable FROM workPlace WHERE (enable = 1) ORDER BY name" 
                        ProviderName="<%$ ConnectionStrings:testDBConnectionString.ProviderName %>">
                    </asp:SqlDataSource>
                    <h3>
                        <asp:Label ID="Label3" runat="server" 
                            Text="<%$ Resources:labels, ourWorkPlaces %>"></asp:Label>
                    </h3>
                    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
                        AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" 
                        DataKeyNames="id" DataSourceID="SqlDataSource1" ForeColor="#333333" 
                        GridLines="None" Width="765px" PageSize="25">
                        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                        <Columns>
                            <asp:TemplateField HeaderText="<%$ Resources:labels, nazev %>" SortExpression="name">
                                <ItemTemplate>
                                    &nbsp;
                                    <asp:HyperLink ID="HyperLink4" runat="server" 
                                        NavigateUrl='<%# "Labs.aspx?id=" + Eval("id") %>' 
                                        Text='<%# Eval("name") %>'></asp:HyperLink>
                                    &nbsp;&nbsp;&nbsp;
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("name") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <HeaderStyle BackColor="#0170A2" HorizontalAlign="Left" />
                            </asp:TemplateField>
                        </Columns>
                        <EditRowStyle BackColor="#999999" />
                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#F7F6F3" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#0170A2" ForeColor="White" HorizontalAlign="Center" />
                        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#E9E7E2" />
                        <SortedAscendingHeaderStyle BackColor="#506C8C" />
                        <SortedDescendingCellStyle BackColor="#FFFDF8" />
                        <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                    </asp:GridView>
                </asp:View>
                <asp:View ID="View2" runat="server">
                         <table aligne="center" border="0" cellpadding="0" cellspacing="0" width="750">
                              <tr>
                              <td style="padding:10px; padding-left:10px;">
                                   <asp:SqlDataSource ID="SqlDataDetail" runat="server" 
                                       ConnectionString="<%$ ConnectionStrings:labsensnanoConnectionString %>" 
                                       
                                       SelectCommand="SELECT nameCs AS name, typCs AS typ, textCs AS text FROM workPlace WHERE (id = @id)">
                                       <SelectParameters>
                                           <asp:QueryStringParameter DefaultValue="0" Name="id" QueryStringField="id" 
                                               Type="Int32" />
                                       </SelectParameters>
                                   </asp:SqlDataSource>
                                   <h3>
                                       <asp:Label ID="Name" runat="server" Text="titulek"></asp:Label>
                                   </h3>

                                               <hr noshade="noshade" style="border-style: 1; background-color: #FFFFFF; color: #666666; border-width: 1px; border-color: #666666" />

                                               <asp:Label ID="Contents" runat="server" Text="Contents"></asp:Label>

                                               <center>
                                                   <h3>
                                                       <asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="~/Labs.aspx" 
                                                           Text="<%$ Resources:labels, back %>"></asp:HyperLink>
                                                   </h3>
                                               </center>

                                           </td>
                                       </tr>
                                   </table>
                </asp:View>
            </asp:MultiView>
</asp:Content>

