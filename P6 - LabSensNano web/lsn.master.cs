﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

//kultura
using System.Globalization;
using System.Threading;
//pro zmenu SiteMapProvider
using System.Configuration;

public partial class lsn : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //ovládání obrázku na masterpage dle zvoleného country
        if (CultureInfo.CurrentUICulture.Name == "cs-CZ"){
            Image1.ImageUrl = "~/layout/langCsOn.png";
            Image2.ImageUrl = "~/layout/langEnOff.png";
            //nastavení sitemap hlavniho menu pro konkretni jazyk
            SiteMapDataSourceLSN.SiteMapProvider = "lsnMenuCs";
        } else {
            Image1.ImageUrl = "~/layout/langCsOff.png";
            Image2.ImageUrl = "~/layout/langEnOn.png";
            //nastavení sitemap hlavniho menu pro konkretni jazyk
            SiteMapDataSourceLSN.SiteMapProvider = "lsnMenuEn";
        }
        
    }
}
