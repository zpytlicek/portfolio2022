﻿<%@ Page Title="LabSensNano" Language="C#" MasterPageFile="~/Research.master" AutoEventWireup="true" CodeFile="Research.aspx.cs" Inherits="introTest" %>
<%@ MasterType virtualPath="~/Research.master"%> 


<asp:Content ID="Content1" ContentPlaceHolderID="Content" Runat="Server">

        

        <asp:MultiView ID="MultiView1" runat="server">
            <asp:View ID="View1" runat="server">
                <asp:Label ID="contents" runat="server" Text="Label"></asp:Label>
            </asp:View>
            <asp:View ID="View2" runat="server">
                <asp:SqlDataSource ID="SqlDataSource3" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:labsensnanoConnectionString %>" 
                    SelectCommand="SELECT [id], [titleCs], [titleEn], [textCs], [leader], [textEn], [enable] FROM [teams] WHERE (([id] = @id) AND ([enable] = @enable2))">
                    <SelectParameters>
                        <asp:QueryStringParameter DefaultValue="0" Name="id" QueryStringField="id" 
                            Type="Int32" />
                        <asp:Parameter DefaultValue="true" Name="enable2" Type="Boolean" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <h2>
                    <asp:Label ID="TeamTitle" runat="server" Text="Label"></asp:Label>
                    <asp:SqlDataSource ID="SqlDataSource5" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:labsensnanoConnectionString %>" 
                        SelectCommand="SELECT persons.degreeBefor + ' ' + persons.surname + ' ' + persons.name + '' + persons.degreeAfter AS name FROM persons INNER JOIN teams ON persons.id = teams.leader WHERE (teams.id = @id) ORDER BY persons.surname">
                        <SelectParameters>
                            <asp:QueryStringParameter DefaultValue="0" Name="id" QueryStringField="id" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlDataSource4" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:labsensnanoConnectionString %>" 
                        SelectCommand="SELECT persons.name, persons.surname, persons.degreeBefor, persons.degreeAfter, rsTeamsPersons.teamId FROM persons INNER JOIN rsTeamsPersons ON persons.id = rsTeamsPersons.personId WHERE (rsTeamsPersons.teamId = @id) ORDER BY persons.surname">
                        <SelectParameters>
                            <asp:QueryStringParameter DefaultValue="0" Name="id" QueryStringField="id" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </h2>

                    <asp:DataList ID="DataListManager" runat="server" DataSourceID="SqlDataSource5" 
                        RepeatDirection="Horizontal" RepeatLayout="Flow">
                        <HeaderTemplate>
                            <asp:Label ID="Label3" runat="server" style="font-weight: 700" 
                                Text="<%$ Resources:labels, manager %>"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label2" runat="server" Text='<%# Eval("name") %>'></asp:Label>
                        </ItemTemplate>
                        <SeparatorTemplate>
                            ,
                        </SeparatorTemplate>
                    </asp:DataList>
                    <asp:Label ID="br1" runat="server" Text="Label"></asp:Label>
                    <asp:DataList ID="DataListMembers" runat="server" DataSourceID="SqlDataSource4" 
                        RepeatDirection="Horizontal" RepeatLayout="Flow">
                        <HeaderTemplate>
                            <asp:Label ID="Label3" runat="server" style="font-weight: 700" 
                                Text="<%$ Resources:labels, members %>"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label2" runat="server" 
                                Text='<%# Eval("degreeBefor") + " " + Eval("surname") + " " + Eval("name") + Eval("degreeAfter")   %>'></asp:Label>
                        </ItemTemplate>
                        <SeparatorTemplate>
                            ,
                        </SeparatorTemplate>
                    </asp:DataList>
                    <asp:Label ID="content" runat="server" Text="Label"></asp:Label>
            </asp:View>
        </asp:MultiView>

                                
                                </asp:Content>

