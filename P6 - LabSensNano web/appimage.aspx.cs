﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

// vše pro práci s obrázky
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
public partial class appimage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // vytvoření spojení do databáze
        SqlConnection conn=new SqlConnection(SqlDataSource1.ConnectionString);
        // vytvoření SQL příkazu
        conn.Open();
        SqlCommand cmd = new SqlCommand("select img from images1 where id=@id", conn);
        cmd.Parameters.Add("@id", 2);
        SqlDataReader rdr = cmd.ExecuteReader();
        rdr.Read();
        byte[] data = (byte[])rdr["img"];
        rdr.Close();
        conn.Close();

        // zjištění rozměrů obrázku 
        MemoryStream stream = new MemoryStream();
        stream.Write(data, 0, data.Length);
        Bitmap objBitmap = new Bitmap(stream);
        objBitmap.Save(Response.OutputStream, ImageFormat.Jpeg);

/*        Size SizePicture = objBitmap.Size;

        // výpočet nových rozměrů 
        Size nvo = new Size(100,100);
        
       // nvo = velikost(SizePicture.Width, SizePicture.Height, width, height);
        // vytvoření obrázku s novými rozměry 
        Bitmap objBitmapResize = new Bitmap(objBitmap, nvo);

        // odeslání výsledku na klienta 
        objBitmapResize.Save(Response.OutputStream, ImageFormat.Jpeg);
             */
        // uvolnění paměti 
        objBitmap.Dispose();
        stream.Close();
        //objBitmapResize.Dispose(); 
    }
    protected void Button1_Click(object sender, EventArgs e)
    {   
        
        string ContentType;
        ContentType=  FileUpload1.PostedFile.ContentType;
        if (ContentType == "image/jpeg" || ContentType == "image/gif" || ContentType == "image/pjpeg")
        {
            //nacteni obrázku do pole
             byte[] Picture_content;
             Picture_content = new byte[FileUpload1.PostedFile.ContentLength];
             FileUpload1.PostedFile.InputStream.Read(Picture_content,0,FileUpload1.PostedFile.ContentLength); 
            // vytvoření spojení do databáze
            SqlConnection conn=new SqlConnection(SqlDataSource1.ConnectionString);
             // vytvoření SQL příkazu
             SqlCommand cmd=new SqlCommand("Add_picture",conn);
             // budeme používat uloženou proceduru
             cmd.CommandType= CommandType.StoredProcedure; 

                // priprava parametru pro obrazek
                 SqlParameter prm_picture=new SqlParameter("@Picture", SqlDbType.Image);
                 prm_picture.Value=Picture_content;
                 cmd.Parameters.Add(prm_picture);

                 conn.Open();
                 cmd.ExecuteNonQuery();
                 conn.Close();
        }

    }
}