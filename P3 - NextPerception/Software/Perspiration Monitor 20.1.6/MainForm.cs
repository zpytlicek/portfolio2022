﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using System.IO;
using ZedGraph;
using System.Text.RegularExpressions;

namespace PerspirationMonitor
{
    public partial class MainForm : Form
    {
        public decimal ActulaDeviceId = 0; //promenna s ID senzoru - pouzito pro export dat
        public SensorSettings FormSettings = null; // Formular pro nastaveni senzoru

        /// <summary>
        /// Trida - bod mereni
        /// </summary>
        public class tMeasurePoint
        {
            public DateTime timeStemp; //casova znacka
            public double tempPCB = 0; //teplota senzor PCB
            public double humidityPCB = 0; //vlhkost senzor PCB
            public double tempTOP = 0; //teplota senzoru dale od kuze
            public double humidityTop = 0;  //vlhkost senzoru dale od kuze
            public double tempBottom = 0; //teplota senzoru tesne nad kuzi
            public double humidityBottom = 0;  //vlhkost senzoru tesne nad kuzi
            public double bateryVoltage = 0; //napeti baterie
            public double avTempPCB = 0; //avXXX - prepoctene hodnoty (klouzavy prumer)
            public double avTempTop = 0;
            public double avTempBottom = 0;
            public double avHumidityPCB = 0;
            public double avHumidityTop = 0;
            public double avHumidityBottom = 0;
            public double avBateryVoltage = 0;
            public double perspiration = 0; //hodnota perspirace
        }

        /// <summary>
        /// Trida - zaznam jednoho mereni
        /// </summary>
        public class tMeasurement
        {
            public DateTime startOfMeasurement; //cas zacatku mereni
            public DateTime stopOfMeasurement; //cas konce mereni
            public List<tMeasurePoint> measuredPoints = new List<tMeasurePoint>(); //list namerenych bodu - zaznam
            public char samplingRate; //rychlost odectu (promena ulozena v zarizeni - zatim nepouzito)
            public char caseVersion; //verze krabicky senzoru (promena ulozena v zarizeni - zatim nepouzito)
            public decimal sensorsDistance; //vzdalenost TOP a BOTTOM sensozru [mm]
        }

        /// <summary>
        /// Stazena mereni pro aktualne pripojeny senzor - aktualizovano vzdy po stazeni novych dat
        /// </summary>
        public List<tMeasurement> DownloadedMeasurements;

        public MainForm()
        {
            InitializeComponent();
            
            //nacteni seznamu aktivnich COM portu pro vyber
            string[] ports = SerialPort.GetPortNames(); 
            foreach (string port in ports)
            {
                comboBoxCOMPortList.Items.Add(port);
            }
            if (ports.Count() > 0) comboBoxCOMPortList.SelectedIndex = 0;

            //Inicializace grafu a datovych slozek po spusteni aplikace
            DownloadedMeasurements = new List<tMeasurement>();
            GraphIni();
        }

        //buffer hodnot aktualne zobrazovanych v grafu
        PointPairList listMeasuredBatteryVoltage = new PointPairList();
        PointPairList listMeasuredPerspiration = new PointPairList();

        /// <summary>
        /// Inicializace komponenty grafu - nastaveni os, pozadi, popisku atd..
        /// </summary>
        private void GraphIni()
        {
            //ini main titles and plot properties
            GraphPane myPane = zedGraphControl1.GraphPane;
            myPane.IsFontsScaled = false;
            myPane.Title.Text = "Perspiration measurement";
            myPane.Title.FontSpec.Size = 16;
            myPane.Legend.Position = LegendPos.BottomCenter;
            // Fill the axis background with a gradient
            myPane.Chart.Fill = new Fill(Color.White, Color.AliceBlue, 90f);

            //ini the axis
            myPane.XAxis.Title.Text = "Time [minute]";

            myPane.YAxis.Title.Text = "Perspiration rate [g.m-2.h-1]";
            myPane.YAxis.Color = Color.Blue;
            myPane.YAxis.Scale.FontSpec.FontColor = Color.Blue;
            myPane.YAxis.Title.FontSpec.FontColor = Color.Blue;
           // myPane.YAxis.Scale.Min = 0;


            Color LightRed = Color.FromArgb(190, Color.Red);
            myPane.Y2Axis.Title.Text = "Battery condition [V]";
            myPane.Y2Axis.IsVisible = true;
            myPane.Y2Axis.Color = LightRed;
            myPane.Y2Axis.Scale.FontSpec.FontColor = Color.Red;
            myPane.Y2Axis.Title.FontSpec.FontColor = Color.Red;
            myPane.Y2Axis.Scale.Min = 0;

            // Generate a blue curve 
            //Voltage
            Color LightBlue2 = Color.FromArgb(50, Color.Blue);
            LineItem myCurveMeasuredVoltage = myPane.AddCurve("Perspiration rate [g.m-2.h-1]", listMeasuredPerspiration, Color.Blue, SymbolType.None);
            myCurveMeasuredVoltage.Line.Width = 2;

            //Current measured/set
            LineItem myCurveMeasuredCurrent = myPane.AddCurve("Battery condition [V]", listMeasuredBatteryVoltage, LightRed, SymbolType.None);
            myCurveMeasuredCurrent.Line.Width = 2;
            myCurveMeasuredCurrent.IsY2Axis = true;

            // Show the x axis grid
            myPane.XAxis.MajorGrid.IsVisible = true;
            myPane.YAxis.MajorGrid.IsVisible = true;
            myPane.Y2Axis.MajorGrid.IsVisible = true;
        }

        /// <summary>
        /// Inicializace vybraneho COM portu - pripojeni/odpojeni vybraneho sezoru
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonConnect_Click(object sender, EventArgs e)
        {
            if (comboBoxCOMPortList.Items.Count > 0 && buttonConnect.Text == "Connect")
            {
                serialPort1.PortName = (string)comboBoxCOMPortList.SelectedItem;
                serialPort1.BaudRate = 2 * 512000;
                serialPort1.Parity = Parity.None;
                serialPort1.Open();
                buttonConnect.Text = "Disconnect";
            } else
            {
                serialPort1.Close();
                serialPort1.Dispose();
                buttonConnect.Text = "Connect";
            }
        }


        /// <summary>
        /// Obsluha pro prenos dat ze senzoru
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonDownloadNewData_Click(object sender, EventArgs e)
        {
            //reinicializace datovych slozek
            listMeasuredPerspiration.Clear();
            zedGraphControl1.Refresh();

            if (buttonConnect.Text == "Disconnect" && timerDataDownload.Enabled == false)
            {
                buttonDownloadData.Enabled = false;
                buttonExportData.Enabled = false;
                buttonConnect.Enabled = false;
                toolStripProgressBar1.Value = 0;
                toolStripProgressBar1.Visible = true;

                //get device ID
                serialPort1.DiscardInBuffer();
                serialPort1.Write("^id?\r");
                string deviceId = WaitForResponse(1,150); //ID=XX\r\n
                deviceId = Regex.Replace(deviceId, @"[^\d]", "");
                if (decimal.Parse(deviceId) <= 99)
                {
                    ActulaDeviceId = decimal.Parse(deviceId);
                }

                //get number of bytes in EEPROM
                serialPort1.DiscardInBuffer();
                serialPort1.Write("^dc?\r\n");
                string dataCountText = WaitForResponse(1, 400); 
                dataCountText = dataCountText.Replace("D#", "");
                dataCountText = dataCountText.Replace("\r\n", "");
                if (int.Parse(dataCountText) == 0)
                {
                    listBoxDownloadedMeas.Items.Clear();
                    toolStripProgressBar1.Visible = false;
                    buttonDownloadData.Enabled = true;
                    buttonExportData.Enabled = true;
                    buttonConnect.Enabled = true;
                    return;
                }

                //nastaveni poctu byte ktere jsou ocekavane v ramci prijmu 
                bytesToRecive = (int.Parse(dataCountText) * 14 + 2);
                toolStripProgressBar1.Maximum = bytesToRecive;
                recivedByteStreamBuffer = new int[bytesToRecive];

                //start byte streaming - inicializace posilani dat ze senzoru v packetech po 210 bytech
                serialPort1.Write("^db?\r\n");
                System.Threading.Thread.Sleep(1);

                totalRecivedBytes = 0;
                dataTimeout = 0;

                timerDataDownload.Enabled = true; //spusteni backgroundWorkeru pro stahovani dat
            }
        }

        //pomocne promenne pro stahovani dat
        int bytesToRecive = 0; //konecny pocet prijatych dat v ramci transakce
        int totalRecivedBytes = 0; //pocet aktualne prijatych dat v ramci transakce
        int dataTimeout = 0; //slouzi pro casove omezeni transakce - ceka max 50 ms na odezvu od senzoru
        int[] recivedByteStreamBuffer; //promenna pro raw stazena data ze senzoru (byte stream)

        /// <summary>
        /// Casovac pro prenos dat mereni - kontrola novych dat kazdou 1 ms
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timerDataDownload_Tick(object sender, EventArgs e)
        {
            dataTimeout++; 

            //vypocet kolik bajtu je jeste treba prijmout
            int remainingBytes = 0;
            remainingBytes = (bytesToRecive - totalRecivedBytes) < 210 ? (bytesToRecive - totalRecivedBytes) : 210;

            //preneseni prijateho packetu dat na COM portu do recivedByteStreamBuffer a nasledny ACK poslanim "1" = inicializuje posladni dalsiho packetu
            try
            {
                if (serialPort1.BytesToRead == remainingBytes)
                {
                    for (int i = 0; i < remainingBytes; i++)
                    {
                        recivedByteStreamBuffer[totalRecivedBytes] = serialPort1.ReadByte();
                        totalRecivedBytes++;
                    }
                    serialPort1.Write("1");
                    toolStripProgressBar1.Value = totalRecivedBytes;
                    dataTimeout = 0;
                }
            }
            catch { 
                //ukonceni spojeni v pripade chyby komunikace s COM portem
                dataTimeout = 51;
                buttonConnect_Click(buttonConnect, null);
            }

            //ukonceni prijmani dat po preneseni vsech dat nebo vyprseni casoveho limitu (problem komunikace)
            //zpracovani prijatych dat
            if (totalRecivedBytes == bytesToRecive || dataTimeout > 50)
            {
                timerDataDownload.Enabled = false; //ukonceni background workeru pro prijem dat

                if (dataTimeout > 50) {
                    MessageBox.Show("Time limit expired.", "Communication Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                //zpracovani prijatych dat
                if (totalRecivedBytes == bytesToRecive)
                {
                    parseNewDownloadedData(recivedByteStreamBuffer); //zpracovani prijateho byte streamu na jednotliva mereni

                    //provedeni prumerovani pro kazdy zaznam mereni
                    for (int x = 0; x < DownloadedMeasurements.Count; x++)
                    {
                        movingAverage(DownloadedMeasurements[x]);
                    }

                    //provedeni vypoctu perspirace pro kazde mereni
                    for (int x = 0; x < DownloadedMeasurements.Count; x++)
                    {
                        for (int j = 0; j < DownloadedMeasurements[x].measuredPoints.Count; j++)
                        {
                            DownloadedMeasurements[x].measuredPoints[j].perspiration = calculatePerspirationValue(DownloadedMeasurements[x].measuredPoints[j], DownloadedMeasurements[x].sensorsDistance);
                        }
                    }

                    swhoNewData(); //zbrazeni senznamu mereni
                }

                //reaktivace ovladacich prvku umoznujici prijem dalsich dat
                toolStripProgressBar1.Visible = false;
                buttonDownloadData.Enabled = true;
                buttonExportData.Enabled = true;
                buttonConnect.Enabled = true;
                timerDataDownload.Enabled = false;
            }
        }

        /// <summary>
        /// Zobrazeni seznamu stazenych mereni
        /// </summary>
        private void swhoNewData()
        {
            //add new downloaded data at to list
            listBoxDownloadedMeas.Items.Clear();
            if (DownloadedMeasurements.Count != 0)
            {
                for (int i = 0; i < DownloadedMeasurements.Count; i++)
                {
                    DateTime tStart = DownloadedMeasurements[i].startOfMeasurement;
                    DateTime tStop = DownloadedMeasurements[i].stopOfMeasurement;
                    //vyvoreni labelu pro mereni
                    TimeSpan ts = tStop - tStart;
                    decimal min = (decimal)ts.TotalMinutes;
                    listBoxDownloadedMeas.Items.Add("Sensor("+ ActulaDeviceId.ToString() + "): " + DownloadedMeasurements[i].startOfMeasurement.ToString("dd.MM.yyyy HH:mm") + " / " + min.ToString("f2") + " min");
                }
            }
            else
            {
                MessageBox.Show("No data");
            }
        }

        /// <summary>
        /// zpracovani byte steamu prijateho ze zarizeni na jednotliva mereni
        /// </summary>
        /// <param name="downloadData">byte steram prijatych dat ze zarizeni</param>
        private void parseNewDownloadedData(int[] downloadData)
        {
            if (downloadData.Count() == 0) return;

            DownloadedMeasurements = new List<tMeasurement>();
            tMeasurePoint messPointBuff;
            byte[] packet;
            List<byte[]> dataPackets = new List<byte[]>();
            tMeasurement messBuffer;

            //rozdeleni prijateho datastreamu na jednotlive datove packety po 14B
            for (int i = 0; i < (downloadData.Count() -2) / 14; i++)
            {
                packet = new byte[14];
                for (int p = 0; p < 14; p++)
                {
                    packet[p] = (byte)downloadData[i * 14 + p];
                }
                dataPackets.Add(packet);
            }

            //parsovani kazdeho datoveho packet na data mereni
            for (int i = 0; i < dataPackets.Count; i++)
            {
                //zpracovani datoveho packetu podle druhu (hlavicka mereni / paticka mereni / data)
                if (dataPackets[i][0] == 0x5e && dataPackets[i][1] == 0x68 && dataPackets[i][2] == 0x73)
                {
                    //packet is head
                    messBuffer = new tMeasurement(); //vytvoreni noveho mereni
                    //parsovani prijatych dat hlavicky mereni
                    DateTime date = new DateTime(Convert.ToInt32(dataPackets[i][3]) + 2000, Convert.ToInt32(dataPackets[i][4]), Convert.ToInt32(dataPackets[i][5]), Convert.ToInt32(dataPackets[i][6]), Convert.ToInt32(dataPackets[i][7]), Convert.ToInt32(dataPackets[i][8]));
                    messBuffer.startOfMeasurement = date;
                    messBuffer.stopOfMeasurement = date;
                    uint distance = (uint)(dataPackets[i][10]) << 8 | (uint)(dataPackets[i][11]); //vzdalenost senzoru
                    messBuffer.sensorsDistance = (decimal)distance / 100;
                    DownloadedMeasurements.Add(messBuffer); // pridani noveho mereni

                } else if (dataPackets[i][0] == 0x5e && dataPackets[i][1] == 0x6d && dataPackets[i][2] == 0x74)
                {
                    //tail packet s casem ukonceni mereni
                    DateTime date = new DateTime(Convert.ToInt32(dataPackets[i][3]) + 2000, Convert.ToInt32(dataPackets[i][4]), Convert.ToInt32(dataPackets[i][5]), Convert.ToInt32(dataPackets[i][6]), Convert.ToInt32(dataPackets[i][7]), Convert.ToInt32(dataPackets[i][8]));
                    DownloadedMeasurements[DownloadedMeasurements.Count - 1].stopOfMeasurement = date;

                    //korekce casovych znacek prichozich dat (nemelo by mit vliv na namerena data - stale v reseni v souvislosti s HW...)
                    DateTime tStart = DownloadedMeasurements[DownloadedMeasurements.Count - 1].startOfMeasurement;
                    DateTime tStop = DownloadedMeasurements[DownloadedMeasurements.Count - 1].stopOfMeasurement;
                    TimeSpan ts = tStop - tStart;
                    decimal sec = (decimal)ts.TotalSeconds;
                    decimal tsDelta = (decimal)sec / DownloadedMeasurements[DownloadedMeasurements.Count - 1].measuredPoints.Count;
                    DateTime tActual = tStart;
                    for (int x=0; x < DownloadedMeasurements[DownloadedMeasurements.Count - 1].measuredPoints.Count; x++)
                    {
                        tActual = tActual.AddSeconds((double)tsDelta);
                        DownloadedMeasurements[DownloadedMeasurements.Count - 1].measuredPoints[x].timeStemp = tActual;
                    }
                } else
                {
                    //zpracovani packetu dat s namerenymi hodnotami 
                    DownloadedMeasurements[DownloadedMeasurements.Count - 1].stopOfMeasurement = DownloadedMeasurements[DownloadedMeasurements.Count - 1].stopOfMeasurement.AddSeconds(1);
                    messPointBuff = new tMeasurePoint();
                    messPointBuff.tempPCB = (double)((Convert.ToInt32(dataPackets[i][0]) << 8) | (Convert.ToInt32(dataPackets[i][1]))) / 100;
                    messPointBuff.humidityPCB = (double)((Convert.ToInt32(dataPackets[i][2]) << 8) | (Convert.ToInt32(dataPackets[i][3]))) / 100;
                    messPointBuff.tempBottom = (double)((Convert.ToInt32(dataPackets[i][4]) << 8) | (Convert.ToInt32(dataPackets[i][5]))) / 100;
                    messPointBuff.humidityBottom = (double)((Convert.ToInt32(dataPackets[i][6]) << 8) | (Convert.ToInt32(dataPackets[i][7]))) / 100;
                    messPointBuff.tempTOP = (double)((Convert.ToInt32(dataPackets[i][8]) << 8) | (Convert.ToInt32(dataPackets[i][9]))) / 100;
                    messPointBuff.humidityTop = (double)((Convert.ToInt32(dataPackets[i][10]) << 8) | (Convert.ToInt32(dataPackets[i][11]))) / 100;
                    messPointBuff.bateryVoltage = (double)((Convert.ToInt32(dataPackets[i][12]) << 8) | (Convert.ToInt32(dataPackets[i][13]))) / 100;
                    DownloadedMeasurements[DownloadedMeasurements.Count - 1].measuredPoints.Add(messPointBuff);
                }

            }
        }

        /// <summary>
        /// Zobrazeni formulare pro nastaveni senzoru
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void showSettingForm_Click(object sender, EventArgs e)
        {
            if (buttonConnect.Text == "Disconnect")
            {
                    FormSettings = new SensorSettings();
                    FormSettings.serialPortForDevice = serialPort1;
                    FormSettings.Show();
            }
        }


        /// <summary>
        /// Odeslani prikazu pro vymazani EEPROM pameti senzoru (pouze namerena data) 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void clearMemorzInDeviceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //clear device
            if (buttonConnect.Text == "Disconnect")
            {
                if (DialogResult.Yes == MessageBox.Show("Are you sure?", "Erase all stored data...", MessageBoxButtons.YesNo, MessageBoxIcon.Warning)) {
                    serialPort1.DiscardInBuffer();
                    serialPort1.Write("^de!\r\n");
                    System.Threading.Thread.Sleep(100);
                    serialPort1.DiscardInBuffer();
                }
            }
        }

        /// <summary>
        /// Obsluha tlacitka pro export dat
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonExportSelectedData_Click_1(object sender, EventArgs e)
        {
            if (listBoxDownloadedMeas.SelectedItems.Count > 0) exoprtSelectedMeasurement(DownloadedMeasurements[listBoxDownloadedMeas.SelectedIndex]);
        }

        /// <summary>
        /// Obsluha exportu jednoho mereni do CSV
        /// </summary>
        /// <param name="measurements">tMeasurement - zaznam jednoho mereni</param>
        private void exoprtSelectedMeasurement(tMeasurement measurements)  //S1-2020-12-09 20.49 Perspiration Record
        {
            //generovani nazvu ukladaneho soboru 
            saveFileDialog1.FileName = "S" + ActulaDeviceId.ToString() + "-" + measurements.startOfMeasurement.ToString("yyyy-MM-dd HH.mm") + " - " + "Perspiration Record.csv";

            //po vyberu umisteni soboru dojde na zaklade namerenych hodnot k vytvoreni textoveho souboru v CSV formatu
            if (DialogResult.OK == saveFileDialog1.ShowDialog())
            {
                FileStream fs = null;
                StreamWriter sw = null;
                try
                {
                    fs = new FileStream(saveFileDialog1.FileName, FileMode.Create);
                    sw = new StreamWriter(fs, Encoding.Default);
                    if (measurements.measuredPoints.Count == 0) return;

                    //vytvoreni hlavicky tabulky
                    sw.WriteLine("Date; Time; Temp_PCB; Temp_Top; Temp_Bottom; RH_PCB; RH_Top; RH_Bottom; Battery; Perspiration Rate;ID; Sensor distance");
                    sw.WriteLine(";;[°C];[°C];[°C];[%];[%];[%];[V];[g.m-2.h-1];-;[mm]");

                    //pridani radku namerenych dat
                    for (int i = 0; i < measurements.measuredPoints.Count; i++)
                    {
                        string datePart = measurements.measuredPoints[i].timeStemp.ToString("dd.MM.yyyy") + ";" + measurements.measuredPoints[i].timeStemp.ToString("HH:mm:ss") + ";";
                        string tempPart = measurements.measuredPoints[i].tempPCB.ToString("0.##") + ";" + measurements.measuredPoints[i].tempTOP.ToString("0.##") + ";" + measurements.measuredPoints[i].tempBottom.ToString("0.##") + ";";
                        string humidityPart = measurements.measuredPoints[i].humidityPCB.ToString("0.##") + ";" + measurements.measuredPoints[i].humidityTop.ToString("0.##") + ";" + measurements.measuredPoints[i].humidityBottom.ToString("0.##") + ";";
                        string batteryPart = measurements.measuredPoints[i].bateryVoltage.ToString("0.##") + ";";
                        string perspiRatePart = measurements.measuredPoints[i].perspiration.ToString();
                        if (i == 0)
                        {
                            string sensorSettings = ";" + ActulaDeviceId.ToString() + ";" + measurements.sensorsDistance.ToString("0.##");

                            sw.WriteLine(datePart + tempPart + humidityPart + batteryPart + perspiRatePart + sensorSettings);
                        } else
                        {
                            sw.WriteLine(datePart + tempPart + humidityPart + batteryPart + perspiRatePart);
                        }
                    }
                    MessageBox.Show("Export of the perspiration records was successful.", "Data export", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch
                {
                    MessageBox.Show("Export of the perspiration records failed.", "Data export", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    if (sw != null) sw.Close();
                    if (fs != null) fs.Close();
                }
            }
        }

        /// <summary>
        /// Zobrazeni namerenych dat v grafu po vyberu ze seznamu 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListBox ls = (ListBox)sender;
            if (ls.SelectedIndex >= 0) { 
                generatePlot(DownloadedMeasurements[ls.SelectedIndex]); //!! Calculate perspiration from downloaded packet
            }
        }

        /// <summary>
        /// Provedeni pohybliveho prumerovani z 20 hodnot nad namerenymi daty
        /// </summary>
        /// <param name="measurements"></param>
        private void movingAverage(tMeasurement measurements)
        {
            for (int i = 0; i < measurements.measuredPoints.Count; i++)
            {
                if (i == 0)
                {
                    measurements.measuredPoints[0].avHumidityBottom = measurements.measuredPoints[0].humidityBottom;
                    measurements.measuredPoints[0].avHumidityPCB = measurements.measuredPoints[0].humidityPCB;
                    measurements.measuredPoints[0].avHumidityTop = measurements.measuredPoints[0].humidityTop;
                    measurements.measuredPoints[0].avTempBottom = measurements.measuredPoints[0].tempBottom;
                    measurements.measuredPoints[0].avTempPCB = measurements.measuredPoints[0].tempPCB;
                    measurements.measuredPoints[0].avTempTop = measurements.measuredPoints[0].tempTOP;
                    measurements.measuredPoints[0].avBateryVoltage = measurements.measuredPoints[0].bateryVoltage;
                }
                else if (i < 20)
                {
                    for (int a = 0; a <= i; a++)
                    {
                        measurements.measuredPoints[i].avHumidityBottom += measurements.measuredPoints[a].humidityBottom;
                        measurements.measuredPoints[i].avHumidityPCB += measurements.measuredPoints[a].humidityPCB;
                        measurements.measuredPoints[i].avHumidityTop += measurements.measuredPoints[a].humidityTop;
                        measurements.measuredPoints[i].avTempBottom += measurements.measuredPoints[a].tempBottom;
                        measurements.measuredPoints[i].avTempPCB += measurements.measuredPoints[a].tempPCB;
                        measurements.measuredPoints[i].avTempTop += measurements.measuredPoints[a].tempTOP;
                        measurements.measuredPoints[i].avBateryVoltage += measurements.measuredPoints[a].bateryVoltage;
                    }
                    measurements.measuredPoints[i].avHumidityBottom = measurements.measuredPoints[i].avHumidityBottom /( i+1);
                    measurements.measuredPoints[i].avHumidityPCB = measurements.measuredPoints[i].avHumidityPCB/ (i + 1);
                    measurements.measuredPoints[i].avHumidityTop = measurements.measuredPoints[i].avHumidityTop/ (i + 1);
                    measurements.measuredPoints[i].avTempBottom = measurements.measuredPoints[i].avTempBottom/ (i + 1);
                    measurements.measuredPoints[i].avTempPCB = measurements.measuredPoints[i].avTempPCB/ (i + 1);
                    measurements.measuredPoints[i].avTempTop = measurements.measuredPoints[i].avTempTop/ (i + 1);
                    measurements.measuredPoints[i].avBateryVoltage = measurements.measuredPoints[i].avBateryVoltage/ (i + 1);
                } else
                {
                    //full movingaverage
                    for (int a = i-19; a <= i; a++)
                    {
                        measurements.measuredPoints[i].avHumidityBottom += measurements.measuredPoints[a].humidityBottom;
                        measurements.measuredPoints[i].avHumidityPCB += measurements.measuredPoints[a].humidityPCB;
                        measurements.measuredPoints[i].avHumidityTop += measurements.measuredPoints[a].humidityTop;
                        measurements.measuredPoints[i].avTempBottom += measurements.measuredPoints[a].tempBottom;
                        measurements.measuredPoints[i].avTempPCB += measurements.measuredPoints[a].tempPCB;
                        measurements.measuredPoints[i].avTempTop += measurements.measuredPoints[a].tempTOP;
                        measurements.measuredPoints[i].avBateryVoltage += measurements.measuredPoints[a].bateryVoltage;
                    }
                    measurements.measuredPoints[i].avHumidityBottom = measurements.measuredPoints[i].avHumidityBottom / 20;
                    measurements.measuredPoints[i].avHumidityPCB = measurements.measuredPoints[i].avHumidityPCB / 20;
                    measurements.measuredPoints[i].avHumidityTop = measurements.measuredPoints[i].avHumidityTop / 20;
                    measurements.measuredPoints[i].avTempBottom = measurements.measuredPoints[i].avTempBottom / 20;
                    measurements.measuredPoints[i].avTempPCB = measurements.measuredPoints[i].avTempPCB / 20;
                    measurements.measuredPoints[i].avTempTop = measurements.measuredPoints[i].avTempTop / 20;
                    measurements.measuredPoints[i].avBateryVoltage = measurements.measuredPoints[i].avBateryVoltage / 20;
                }
            }
        }

        /// <summary>
        /// Zpracovani vybranych dat pro zobrazeni v grafu a zobrazeni
        /// </summary>
        /// <param name="measurements"></param>
        private void generatePlot(tMeasurement measurements)
        {
            //mazani aktualne zobrazenych dat
            listMeasuredBatteryVoltage.Clear();
            listMeasuredPerspiration.Clear();

            //kalkulace casu mereni
            DateTime tStart = measurements.startOfMeasurement;
            DateTime tStop = measurements.stopOfMeasurement;
            TimeSpan ts = tStop - tStart;
            decimal sec = (decimal)ts.TotalSeconds;
            decimal tsDelta = (decimal)sec / measurements.measuredPoints.Count;

            //pridani dat do datovych slozek grafu
            for (int i = 0; i < measurements.measuredPoints.Count; i++)
            {
                  listMeasuredBatteryVoltage.Add((double)tsDelta * (double)i /60 , measurements.measuredPoints[i].avBateryVoltage);
                  listMeasuredPerspiration.Add((double)tsDelta * (double)i / 60, measurements.measuredPoints[i].perspiration);
            }
            GraphUpdate();
        }

        /// <summary>
        /// Vypocet perspirace !!!!
        /// </summary>
        /// <param name="measuredPoint">list of measured points</param>
        /// <param name="sensorDistance">sensor distance in [mm]</param>
        /// <returns></returns>
        private double calculatePerspirationValue(tMeasurePoint measuredPoint, decimal sensorDistance)
        {
            double pNasycenychParTop = Math.Exp(23.58 - (4044.2 / (235.6 + measuredPoint.avTempTop))); //tlak nasycenych par viz https://vetrani.tzb-info.cz/teorie-a-vypocty-vetrani-klimatizace/3323-teorie-vlhkeho-vzduchu-i
            double pNasycenychParBottom = Math.Exp(23.58 - (4044.2 / (235.6 + measuredPoint.avTempBottom))); //tlak nasycenych par senzor Bottom
            sensorDistance = sensorDistance / 1000; //prevod [mm] -> [m]
            double J = 0.00067 * (pNasycenychParBottom * measuredPoint.avHumidityBottom / 100 - pNasycenychParTop * measuredPoint.avHumidityTop / 100) / (double)sensorDistance; //vypocet perspirace [g/m2/h]
            //j - normal perspiration range: 10 - 40 helthy , 15-60 moderate
            return (J);
           // throw new NotImplementedException();
        }


        /// <summary>
        /// Aktualizace zobrazeni grafu
        /// </summary>
        private void GraphUpdate()
        {
            zedGraphControl1.AxisChange();
            zedGraphControl1.Invalidate();
        }

        /// <summary>
        /// Aktualizace seznamu COM portu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBoxPortList_DropDown(object sender, EventArgs e)
        {
            string[] ports = SerialPort.GetPortNames();
            comboBoxCOMPortList.Items.Clear();
            foreach (string port in ports)
            {
                comboBoxCOMPortList.Items.Add(port);
            }
            if (ports.Count() > 0) comboBoxCOMPortList.SelectedIndex = 0;
        }

        /// <summary>
        /// Waiting for response from UART - funkce pro rizeni toku dat
        /// </summary>
        /// <param name="nCount">Number of '\n' char in response</param>
        /// <param name="timeout">Maximum timeout in milliseconds</param>
        /// <returns></returns>
        private string WaitForResponse(int nCount, int timeout)
        {
            string response = "";
            int c = 0, n = 0;
            while (c < timeout && n < nCount)
            {
                if (serialPort1.BytesToRead > 0)
                {
                    response += (char)serialPort1.ReadChar();
                    if (response[response.Length - 1] == '\n') n++;
                }
                else
                {
                    c++;
                    System.Threading.Thread.Sleep(1);
                }
            }
            return (response);
        }

    }
}
