﻿namespace PerspirationMonitor
{
    partial class SensorSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.numericUpDownSensorDistance = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownDeviceId = new System.Windows.Forms.NumericUpDown();
            this.buttonSaveSettings = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.buttonSetCurrentTime = new System.Windows.Forms.Button();
            this.labelReadTime = new System.Windows.Forms.Label();
            this.labelFirmwareInfo = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSensorDistance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownDeviceId)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Actual sensor time:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(42, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Device ID";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 47);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Sensor distance";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.numericUpDownSensorDistance);
            this.groupBox1.Controls.Add(this.numericUpDownDeviceId);
            this.groupBox1.Controls.Add(this.buttonSaveSettings);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(12, 31);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(183, 100);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Device settings";
            // 
            // numericUpDownSensorDistance
            // 
            this.numericUpDownSensorDistance.DecimalPlaces = 2;
            this.numericUpDownSensorDistance.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numericUpDownSensorDistance.Location = new System.Drawing.Point(103, 45);
            this.numericUpDownSensorDistance.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            131072});
            this.numericUpDownSensorDistance.Name = "numericUpDownSensorDistance";
            this.numericUpDownSensorDistance.Size = new System.Drawing.Size(68, 20);
            this.numericUpDownSensorDistance.TabIndex = 5;
            // 
            // numericUpDownDeviceId
            // 
            this.numericUpDownDeviceId.Location = new System.Drawing.Point(103, 19);
            this.numericUpDownDeviceId.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.numericUpDownDeviceId.Name = "numericUpDownDeviceId";
            this.numericUpDownDeviceId.Size = new System.Drawing.Size(68, 20);
            this.numericUpDownDeviceId.TabIndex = 4;
            // 
            // buttonSaveSettings
            // 
            this.buttonSaveSettings.Location = new System.Drawing.Point(63, 71);
            this.buttonSaveSettings.Name = "buttonSaveSettings";
            this.buttonSaveSettings.Size = new System.Drawing.Size(108, 23);
            this.buttonSaveSettings.TabIndex = 3;
            this.buttonSaveSettings.Text = "Save settings";
            this.buttonSaveSettings.UseVisualStyleBackColor = true;
            this.buttonSaveSettings.Click += new System.EventHandler(this.SaveSettings_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.buttonSetCurrentTime);
            this.groupBox2.Controls.Add(this.labelReadTime);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(198, 31);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(213, 100);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Real time setting";
            // 
            // buttonSetCurrentTime
            // 
            this.buttonSetCurrentTime.Location = new System.Drawing.Point(97, 71);
            this.buttonSetCurrentTime.Name = "buttonSetCurrentTime";
            this.buttonSetCurrentTime.Size = new System.Drawing.Size(108, 23);
            this.buttonSetCurrentTime.TabIndex = 4;
            this.buttonSetCurrentTime.Text = "Set current time";
            this.buttonSetCurrentTime.UseVisualStyleBackColor = true;
            this.buttonSetCurrentTime.Click += new System.EventHandler(this.buttonSetCurrentTime_Click);
            // 
            // labelReadTime
            // 
            this.labelReadTime.AutoSize = true;
            this.labelReadTime.Location = new System.Drawing.Point(104, 21);
            this.labelReadTime.Name = "labelReadTime";
            this.labelReadTime.Size = new System.Drawing.Size(26, 13);
            this.labelReadTime.TabIndex = 1;
            this.labelReadTime.Text = "time";
            // 
            // labelFirmwareInfo
            // 
            this.labelFirmwareInfo.AutoSize = true;
            this.labelFirmwareInfo.Location = new System.Drawing.Point(12, 9);
            this.labelFirmwareInfo.Name = "labelFirmwareInfo";
            this.labelFirmwareInfo.Size = new System.Drawing.Size(35, 13);
            this.labelFirmwareInfo.TabIndex = 4;
            this.labelFirmwareInfo.Text = "label4";
            // 
            // timer1
            // 
            this.timer1.Interval = 400;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // SensorSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(423, 143);
            this.Controls.Add(this.labelFirmwareInfo);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(439, 160);
            this.Name = "SensorSettings";
            this.Text = "Sensor settings";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.SensorSettings_FormClosed);
            this.Shown += new System.EventHandler(this.SensorSettings_Shown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSensorDistance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownDeviceId)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.NumericUpDown numericUpDownSensorDistance;
        private System.Windows.Forms.NumericUpDown numericUpDownDeviceId;
        private System.Windows.Forms.Button buttonSaveSettings;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button buttonSetCurrentTime;
        private System.Windows.Forms.Label labelReadTime;
        private System.Windows.Forms.Label labelFirmwareInfo;
        private System.Windows.Forms.Timer timer1;
    }
}