﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PerspirationMonitor
{
    /// <summary>
    /// Formular pro zakladni nastaveni senzoru
    /// </summary>
    public partial class SensorSettings : Form
    {
        public System.IO.Ports.SerialPort serialPortForDevice;

        public SensorSettings()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Nacteni aktualnich informaci o senzoru po zobrazeni formulare (prenos informaci z pripojeneho zarizeni)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SensorSettings_Shown(object sender, EventArgs e)
        {
            //Nacteni casu z RTC
            serialPortForDevice.DiscardInBuffer();
            serialPortForDevice.Write("^t?\n\r");
            string time = WaitForResponse(1, 150); 
            CultureInfo provider = CultureInfo.InvariantCulture;
            string timeResult = "no connection";
            DateTime result = DateTime.Now;
            try
            {
                result = DateTime.ParseExact(time, "yyyy.MM.dd HH:mm:ss\r\n", provider);
                timeResult = result.ToString();
            }
            catch (FormatException)
            {
            }
            labelReadTime.Text = result.ToString();

            //Nacteni verze HW,FW (format: ‘Perspiration Sensor\r\nHW ver.xx.xx\r\nFW ver.xx.xx\r\n’)
            serialPortForDevice.DiscardInBuffer();
            serialPortForDevice.Write("^i?\r");
            string deviceInfo = WaitForResponse(3, 150); 
            deviceInfo = deviceInfo.Replace("\r\n", " ");
            labelFirmwareInfo.Text = deviceInfo;

            //Nacteni ID senzoru
            serialPortForDevice.DiscardInBuffer();
            serialPortForDevice.Write("^id?\r");
            string deviceId = WaitForResponse(1, 150); 
            deviceId = Regex.Replace(deviceId, @"[^\d]", "");
            if (decimal.Parse(deviceId) <= 99) { 
                numericUpDownDeviceId.Value = decimal.Parse(deviceId);
            } else
            {
                numericUpDownDeviceId.Value = 0;
            }

            //Nacteni nastavene vzdalenosti senzoru
            serialPortForDevice.DiscardInBuffer();
            serialPortForDevice.Write("^sd?\r");
            string sensorDistanceData = WaitForResponse(1, 150); 
            sensorDistanceData = Regex.Replace(sensorDistanceData, @"[^\d]", "");
            numericUpDownSensorDistance.Value = decimal.Parse(sensorDistanceData)/100;


            timer1.Enabled = true; //aktivace casovace pro aktualizaci zobrazeneho casu z RTC
        }

        /// <summary>
        /// Synchronizace RTC s casem PC
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonSetCurrentTime_Click(object sender, EventArgs e)
        {
            timer1.Enabled = false; //deaktivace casovace pro aktualizaci zobrazeneho casu z RTC

            //preneseni aktualniho casu PC do RTC 
            DateTime aktualDate = DateTime.Now;
            string dateTimeSettingString = "^t:" + aktualDate.Year.ToString() + "." + aktualDate.Month.ToString("D2") + "." + aktualDate.Day.ToString("D2") + ";" + aktualDate.Hour.ToString("D2") + ":" + aktualDate.Minute.ToString("D2") + ":" + aktualDate.Second.ToString("D2") + ";1\r\n";
            serialPortForDevice.Write(dateTimeSettingString);
            MessageBox.Show(aktualDate.ToString(), "New realtime setting", MessageBoxButtons.OK, MessageBoxIcon.Information);
            System.Threading.Thread.Sleep(400);
            serialPortForDevice.ReadExisting();

            timer1.Enabled = true; //aktivace casovace pro aktualizaci zobrazeneho casu z RTC
        }

        /// <summary>
        /// Preneseni nastavenych hodnot DeviceID a vzdalenosti senzoru do pameti senzoru
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SaveSettings_Click(object sender, EventArgs e)
        {
            timer1.Enabled = false; //deaktivace casovace pro aktualizaci zobrazeneho casu z RTC

            //priprava ASCII prikazu pro nastaveni
            string commandDeviceId;
            string commandSensorDistance;
            commandDeviceId = "^id:" +  ((Int32)(numericUpDownDeviceId.Value)).ToString("D2") + "\r";
            commandSensorDistance = "^sd:" + (((Int32)(numericUpDownSensorDistance.Value*100)) / 100).ToString("D2") + "." +(((Int32)(numericUpDownSensorDistance.Value*100)) % 100).ToString("D2") + "\r";

            //odeslani prikazu pres COM port
            serialPortForDevice.Write(commandDeviceId);
            System.Threading.Thread.Sleep(400);
            serialPortForDevice.ReadExisting();
            serialPortForDevice.Write(commandSensorDistance);
            System.Threading.Thread.Sleep(400);
            serialPortForDevice.ReadExisting();

            timer1.Enabled = true; //aktivace casovace pro aktualizaci zobrazeneho casu z RTC
        }

        /// <summary>
        /// Deaktivace opakovane COM komunikace pro aktualizaci casu z RTC pri ukonceni formulare
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SensorSettings_FormClosed(object sender, FormClosedEventArgs e)
        {
            MainForm form = (MainForm)this.Parent;
            timer1.Enabled = false;
        }

        /// <summary>
        /// Timer pro zobrazeni realneho casu - opakovane nacita RTC senzoru
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer1_Tick(object sender, EventArgs e)
        {
            serialPortForDevice.DiscardInBuffer();
            serialPortForDevice.Write("^t?\n\r");

            string time = WaitForResponse(1, 150); 

            CultureInfo provider = CultureInfo.InvariantCulture;
            string timeResult = "no connection";
            DateTime result = DateTime.Now;
            try
            {
                result = DateTime.ParseExact(time, "yyyy.MM.dd HH:mm:ss\r\n", provider);
                timeResult = result.ToString();
            }
            catch (FormatException)
            {
            }
            labelReadTime.Text = result.ToString();
        }

        /// <summary>
        /// Waiting for response from UART - funkce pro řízení toku dat
        /// </summary>
        /// <param name="nCount">Number of '\n' char in response</param>
        /// <param name="timeout">Maximum timeout in milliseconds</param>
        /// <returns></returns>
        private string WaitForResponse(int nCount, int timeout)
        {
            string response = "";
            int c = 0, n = 0;
            while (c < timeout && n < nCount)
            {
                if (serialPortForDevice.BytesToRead > 0)
                {
                    response += (char)serialPortForDevice.ReadChar();
                    if (response[response.Length - 1] == '\n') n++;
                }
                else
                {
                    c++;
                    System.Threading.Thread.Sleep(1);
                }
            }
            return (response);
        }
    }
}
