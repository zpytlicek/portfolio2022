﻿namespace PerspirationMonitor
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.comboBoxCOMPortList = new System.Windows.Forms.ComboBox();
            this.buttonConnect = new System.Windows.Forms.Button();
            this.buttonExportData = new System.Windows.Forms.Button();
            this.buttonDownloadData = new System.Windows.Forms.Button();
            this.timerDataDownload = new System.Windows.Forms.Timer(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.showRealiTimeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showRealiTimeToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.clearMemorzInDeviceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripProgressBar1 = new System.Windows.Forms.ToolStripProgressBar();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.zedGraphControl1 = new ZedGraph.ZedGraphControl();
            this.listBoxDownloadedMeas = new System.Windows.Forms.ListBox();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // serialPort1
            // 
            this.serialPort1.BaudRate = 256000;
            this.serialPort1.ParityReplace = ((byte)(0));
            this.serialPort1.ReadBufferSize = 250000;
            // 
            // comboBoxCOMPortList
            // 
            this.comboBoxCOMPortList.FormattingEnabled = true;
            this.comboBoxCOMPortList.Location = new System.Drawing.Point(3, 25);
            this.comboBoxCOMPortList.Name = "comboBoxCOMPortList";
            this.comboBoxCOMPortList.Size = new System.Drawing.Size(121, 21);
            this.comboBoxCOMPortList.TabIndex = 1;
            this.comboBoxCOMPortList.DropDown += new System.EventHandler(this.comboBoxPortList_DropDown);
            // 
            // buttonConnect
            // 
            this.buttonConnect.Location = new System.Drawing.Point(130, 25);
            this.buttonConnect.Name = "buttonConnect";
            this.buttonConnect.Size = new System.Drawing.Size(75, 23);
            this.buttonConnect.TabIndex = 2;
            this.buttonConnect.Text = "Connect";
            this.buttonConnect.UseVisualStyleBackColor = true;
            this.buttonConnect.Click += new System.EventHandler(this.buttonConnect_Click);
            // 
            // buttonExportData
            // 
            this.buttonExportData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonExportData.Location = new System.Drawing.Point(3, 480);
            this.buttonExportData.Name = "buttonExportData";
            this.buttonExportData.Size = new System.Drawing.Size(211, 23);
            this.buttonExportData.TabIndex = 3;
            this.buttonExportData.Text = "Export selected data";
            this.buttonExportData.UseVisualStyleBackColor = true;
            this.buttonExportData.Click += new System.EventHandler(this.buttonExportSelectedData_Click_1);
            // 
            // buttonDownloadData
            // 
            this.buttonDownloadData.Location = new System.Drawing.Point(211, 25);
            this.buttonDownloadData.Name = "buttonDownloadData";
            this.buttonDownloadData.Size = new System.Drawing.Size(142, 23);
            this.buttonDownloadData.TabIndex = 6;
            this.buttonDownloadData.Text = "Download new data";
            this.buttonDownloadData.UseVisualStyleBackColor = true;
            this.buttonDownloadData.Click += new System.EventHandler(this.buttonDownloadNewData_Click);
            // 
            // timerDataDownload
            // 
            this.timerDataDownload.Interval = 1;
            this.timerDataDownload.Tick += new System.EventHandler(this.timerDataDownload_Tick);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showRealiTimeToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(910, 24);
            this.menuStrip1.TabIndex = 8;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // showRealiTimeToolStripMenuItem
            // 
            this.showRealiTimeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showRealiTimeToolStripMenuItem1,
            this.clearMemorzInDeviceToolStripMenuItem});
            this.showRealiTimeToolStripMenuItem.Name = "showRealiTimeToolStripMenuItem";
            this.showRealiTimeToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.showRealiTimeToolStripMenuItem.Text = "Menu";
            // 
            // showRealiTimeToolStripMenuItem1
            // 
            this.showRealiTimeToolStripMenuItem1.Name = "showRealiTimeToolStripMenuItem1";
            this.showRealiTimeToolStripMenuItem1.Size = new System.Drawing.Size(199, 22);
            this.showRealiTimeToolStripMenuItem1.Text = "Device settings";
            this.showRealiTimeToolStripMenuItem1.Click += new System.EventHandler(this.showSettingForm_Click);
            // 
            // clearMemorzInDeviceToolStripMenuItem
            // 
            this.clearMemorzInDeviceToolStripMenuItem.Name = "clearMemorzInDeviceToolStripMenuItem";
            this.clearMemorzInDeviceToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.clearMemorzInDeviceToolStripMenuItem.Text = "Clear memory in device";
            this.clearMemorzInDeviceToolStripMenuItem.Click += new System.EventHandler(this.clearMemorzInDeviceToolStripMenuItem_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripProgressBar1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 510);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.statusStrip1.Size = new System.Drawing.Size(910, 22);
            this.statusStrip1.TabIndex = 9;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripProgressBar1
            // 
            this.toolStripProgressBar1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripProgressBar1.Name = "toolStripProgressBar1";
            this.toolStripProgressBar1.Size = new System.Drawing.Size(200, 16);
            this.toolStripProgressBar1.Step = 1;
            this.toolStripProgressBar1.Visible = false;
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.Filter = "Perspiration record (.csv)|*.csv";
            this.saveFileDialog1.Title = "Perspiratin data export";
            // 
            // zedGraphControl1
            // 
            this.zedGraphControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.zedGraphControl1.Location = new System.Drawing.Point(220, 55);
            this.zedGraphControl1.Name = "zedGraphControl1";
            this.zedGraphControl1.ScrollGrace = 0D;
            this.zedGraphControl1.ScrollMaxX = 0D;
            this.zedGraphControl1.ScrollMaxY = 0D;
            this.zedGraphControl1.ScrollMaxY2 = 0D;
            this.zedGraphControl1.ScrollMinX = 0D;
            this.zedGraphControl1.ScrollMinY = 0D;
            this.zedGraphControl1.ScrollMinY2 = 0D;
            this.zedGraphControl1.Size = new System.Drawing.Size(679, 452);
            this.zedGraphControl1.TabIndex = 11;
            // 
            // listBoxDownloadedMeas
            // 
            this.listBoxDownloadedMeas.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.listBoxDownloadedMeas.FormattingEnabled = true;
            this.listBoxDownloadedMeas.Location = new System.Drawing.Point(3, 54);
            this.listBoxDownloadedMeas.Name = "listBoxDownloadedMeas";
            this.listBoxDownloadedMeas.Size = new System.Drawing.Size(211, 420);
            this.listBoxDownloadedMeas.TabIndex = 12;
            this.listBoxDownloadedMeas.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(910, 532);
            this.Controls.Add(this.listBoxDownloadedMeas);
            this.Controls.Add(this.zedGraphControl1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.buttonDownloadData);
            this.Controls.Add(this.buttonExportData);
            this.Controls.Add(this.buttonConnect);
            this.Controls.Add(this.comboBoxCOMPortList);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(926, 571);
            this.Name = "MainForm";
            this.Text = "Perspiration Monitor 20.1.6";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.IO.Ports.SerialPort serialPort1;
        private System.Windows.Forms.ComboBox comboBoxCOMPortList;
        private System.Windows.Forms.Button buttonConnect;
        private System.Windows.Forms.Button buttonExportData;
        private System.Windows.Forms.Button buttonDownloadData;
        private System.Windows.Forms.Timer timerDataDownload;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar1;
        private System.Windows.Forms.ToolStripMenuItem showRealiTimeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showRealiTimeToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem clearMemorzInDeviceToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private ZedGraph.ZedGraphControl zedGraphControl1;
        private System.Windows.Forms.ListBox listBoxDownloadedMeas;
    }
}

