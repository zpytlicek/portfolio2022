/*
 * PerspirationAPI.c
 *
 *  Created on: 1. 12. 2020
 *      Author: Zdenek Pytlicek
 */

#include "PerspirationAPI.h"

//Ini system settings with default values
void PerAPI_DefaulSettingIni(void){
	PerApi_SystemSettings.samplingRate = DefaultSamplingRate;
	PerApi_SystemSettings.shtSensorsDistance = DefaultShtSensorDistance;
	PerApi_SystemSettings.caseVersion = DefaultCaseVersion;
	PerAPI_LoadSystemSetting();
}

//Save system setting to EEPROM (sensor ID, SHT distance)
void PerAPI_SaveSystemSetting(void){
	//write sensor ID
	M95M_WriteByte(0x00, (uint8_t)PerApi_SystemSettings.deviceId ,ID_Page);

	//write SHT sensor distance
	M95M_WriteByte(0x01, (uint8_t)(PerApi_SystemSettings.shtSensorsDistance >> 8) & 0xff, ID_Page);
	M95M_WriteByte(0x02, (uint8_t)PerApi_SystemSettings.shtSensorsDistance & 0xff, ID_Page);
}


//Load system setting from EEPROM (sensor ID, SHT distance)
void PerAPI_LoadSystemSetting(void){
	PerApi_SystemSettings.deviceId = M95M_ReadIdByte(0x00);
	uint16_t distanceBuff = 0;
	distanceBuff = M95M_ReadIdByte(0x01);
	distanceBuff = (distanceBuff << 8 & 0xff00) | M95M_ReadIdByte(0x02);
	PerApi_SystemSettings.shtSensorsDistance = distanceBuff;
}

//Prepare PerApi_EEPROM_Packet - HEAD
//Param: pHRTC - HAL RTC handler
void PerAPI_Prepare_MesHead(RTC_HandleTypeDef *pHrtc){
    //packet head
	PerApi_EEPROM_Packet[0] = '^';
	PerApi_EEPROM_Packet[1] = 'h';
	PerApi_EEPROM_Packet[2] = 's';
	//DateTime
	//buffer variables for dataTimeFunctions
	RTC_TimeTypeDef currentTimeBuff = {0};
	RTC_DateTypeDef currentDateBuff =  {0};

	HAL_RTC_GetTime(pHrtc, &currentTimeBuff, RTC_FORMAT_BIN);
	HAL_RTC_GetDate(pHrtc, &currentDateBuff, RTC_FORMAT_BIN);

	PerApi_EEPROM_Packet[3] = currentDateBuff.Year;
	PerApi_EEPROM_Packet[4] = currentDateBuff.Month;
	PerApi_EEPROM_Packet[5] = currentDateBuff.Date;
	PerApi_EEPROM_Packet[6] = currentTimeBuff.Hours;
	PerApi_EEPROM_Packet[7] = currentTimeBuff.Minutes;
	PerApi_EEPROM_Packet[8] = currentTimeBuff.Seconds;

	// Samling rate + case version
	PerApi_EEPROM_Packet[9] = (PerApi_SystemSettings.samplingRate << 4) & 0xf0;
	PerApi_EEPROM_Packet[9] = PerApi_EEPROM_Packet[9] | PerApi_SystemSettings.caseVersion;
	//SHT Distance
	PerApi_EEPROM_Packet[10] = (PerApi_SystemSettings.shtSensorsDistance>>8 & 0xff);
	PerApi_EEPROM_Packet[11] = PerApi_SystemSettings.shtSensorsDistance & 0xff;
	//NOT used / reserve
	PerApi_EEPROM_Packet[12] = 0xff;
	PerApi_EEPROM_Packet[13] = 0xff;
}

//Prepare PerApi_EEPROM_Packet - TAIL
//Param: pHRTC - HAL RTC handler
void PerAPI_Prepare_MesTail(RTC_HandleTypeDef *pHrtc){
    //packet head
	PerApi_EEPROM_Packet[0] = '^';
	PerApi_EEPROM_Packet[1] = 'm';
	PerApi_EEPROM_Packet[2] = 't';
	//DateTime
	//buffer variables for dataTimeFunctions
	RTC_TimeTypeDef currentTimeBuff = {0};
	RTC_DateTypeDef currentDateBuff =  {0};

	HAL_RTC_GetTime(pHrtc, &currentTimeBuff, RTC_FORMAT_BIN);
	HAL_RTC_GetDate(pHrtc, &currentDateBuff, RTC_FORMAT_BIN);

	PerApi_EEPROM_Packet[3] = currentDateBuff.Year;
	PerApi_EEPROM_Packet[4] = currentDateBuff.Month;
	PerApi_EEPROM_Packet[5] = currentDateBuff.Date;
	PerApi_EEPROM_Packet[6] = currentTimeBuff.Hours;
	PerApi_EEPROM_Packet[7] = currentTimeBuff.Minutes;
	PerApi_EEPROM_Packet[8] = currentTimeBuff.Seconds;

	//NOT used / reserve
	PerApi_EEPROM_Packet[9] = 0xff;
	PerApi_EEPROM_Packet[10] = 0xff;
	PerApi_EEPROM_Packet[11] = 0xff;
	PerApi_EEPROM_Packet[12] = 0xff;
	PerApi_EEPROM_Packet[13] = 0xff;
}

//Prepare PerApi_EEPROM_Packet
void PerAPI_Prepare_MesPoint(ADC_HandleTypeDef *pHadc){

	float vBat = 0; //doresit

	Sht35Data shtdataBuff;

	//Sensor PCB temp[2B], RH[2B]
	shtdataBuff = Sht35_read(PCBSensor);
	fto2charM100(shtdataBuff.temp, &PerApi_EEPROM_Packet[0]);
	fto2charM100(shtdataBuff.humidity, &PerApi_EEPROM_Packet[2]);

	//Sensor Bottom temp[2B], RH[2B]
	shtdataBuff = Sht35_read(MainSensorBottom);
	fto2charM100(shtdataBuff.temp, &PerApi_EEPROM_Packet[4]);
	fto2charM100(shtdataBuff.humidity, &PerApi_EEPROM_Packet[6]);

	//Sensor Top temp[2B], RH[2B]
	shtdataBuff = Sht35_read(MainSensorTop);
	fto2charM100(shtdataBuff.temp, &PerApi_EEPROM_Packet[8]);
	fto2charM100(shtdataBuff.humidity, &PerApi_EEPROM_Packet[10]);

	//Voltage - measured float value * 100
	//vBat = GetVbat(pHadc);
	vBat = PerApi_vBat;
	fto2charM100(vBat, &PerApi_EEPROM_Packet[12]);
}

//Get Vbat over internal ADC with external Vref
float GetVbat(ADC_HandleTypeDef *pHadc){
	  //ADC TEST
	  uint16_t AD_RES = 0;
	  HAL_ADC_Start(pHadc);
	  HAL_ADC_PollForConversion(pHadc, 1);
	  AD_RES = HAL_ADC_GetValue(pHadc);
	  float voltage = (2.8f / 4096) * AD_RES * 2.0f; //2.792V measured voltage at LDO output, Vbat at voltage divider 1:1 (2.13 cal correction)
      return(voltage);
};

//Setup time for sleeping mode
void PerAPI_LowPowerTimerIni(uint32_t msecTimer){
	//prvni verze
	//uint32_t _time =(((uint32_t)msecTimer)*2314)/1000;
	//HAL_RTCEx_SetWakeUpTimer_IT(&hrtc, _time, RTC_WAKEUPCLOCK_RTCCLK_DIV16);

	//druha verze

	__HAL_PWR_CLEAR_FLAG(PWR_FLAG_WU);
}

void PerAPI_LowPowerSetup(void){
/*	uint32_t _time =(((uint32_t)5000)*2314)/1000;
	HAL_RTCEx_SetWakeUpTimer_IT(&hrtc, _time, RTC_WAKEUPCLOCK_RTCCLK_DIV16);*/

	__HAL_PWR_CLEAR_FLAG(PWR_FLAG_WU);

	__HAL_RCC_PWR_CLK_ENABLE(); //Enable Power Control clock
	HAL_PWREx_EnableUltraLowPower(); // Ultra low power mode
	HAL_PWREx_EnableFastWakeUp(); // Fast wake-up for ultra low power mode

	CLEAR_BIT(SysTick->CTRL,SysTick_CTRL_TICKINT_Msk);
  //  __HAL_TIM_DISABLE_IT(&htim2, TIM_IT_UPDATE);

	/* Disable TIM2 clock */
    //__HAL_RCC_TIM2_CLK_DISABLE();
//	NVIC_DisableIRQ(TIM2_IRQn);

	HAL_SuspendTick(); // disable system tick


	// Disable Unused Gpios
	/*  __HAL_RCC_GPIOC_CLK_DISABLE();
	  __HAL_RCC_GPIOH_CLK_DISABLE();
	  __HAL_RCC_GPIOA_CLK_DISABLE();
	  __HAL_RCC_GPIOB_CLK_DISABLE();*/


	/* GPIO_InitTypeDef GPIO_InitStruct = {0};

	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	__HAL_RCC_GPIOA_CLK_DISABLE();
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	__HAL_RCC_GPIOB_CLK_DISABLE();
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
	__HAL_RCC_GPIOC_CLK_DISABLE();
	HAL_GPIO_Init(GPIOH, &GPIO_InitStruct);
	__HAL_RCC_GPIOH_CLK_DISABLE();
*/
	//HAL_PWR_EnterSTOPMode(PWR_LOWPOWERREGULATOR_ON, PWR_STOPENTRY_WFI); // Switch to stop mode
	 //HAL_PWR_EnterSTANDBYMode();
//	HAL_PWR_EnterSTOPMode(PWR_LOWPOWERREGULATOR_ON, PWR_STOPENTRY_WFI); // Switch to stop mode

	HAL_PWR_EnterSTOPMode(PWR_MAINREGULATOR_ON, PWR_STOPENTRY_WFI); // Switch to stop mode
}

void PerAPI_LowPowerResume(void){
	//Reinit clocks
	SystemClock_Config();

	/* Enable the TIM2 global Interrupt */
//	__HAL_RCC_TIM2_CLK_ENABLE();
//	HAL_NVIC_EnableIRQ(TIM2_IRQn);

	//  __HAL_TIM_ENABLE_IT(&htim2, TIM_IT_UPDATE);
	SET_BIT(SysTick->CTRL,SysTick_CTRL_TICKINT_Msk);
	HAL_ResumeTick();// disable system tick
	//Deactive RTC wakeUp
//	HAL_RTCEx_DeactivateWakeUpTimer(&hrtc);
	//Reinit GPIOs
	/*  __HAL_RCC_GPIOC_CLK_ENABLE();
	  __HAL_RCC_GPIOH_CLK_ENABLE();
	  __HAL_RCC_GPIOA_CLK_ENABLE();
	  __HAL_RCC_GPIOB_CLK_ENABLE();*/
}

//store number of measurements
void PerAPI_WriteMeasurementCount(uint32_t count){
	M95M_WriteByte(0xf0, (count >> 24) & 0xff , ID_Page);
	M95M_WriteByte(0xf1, (count >> 16) & 0xff , ID_Page);
	M95M_WriteByte(0xf2, (count >> 8) & 0xff, ID_Page);
	M95M_WriteByte(0xf3, count & 0xff, ID_Page);
}

//read number of measurements
uint32_t PerAPI_ReadMeasurementCount(void){
	uint32_t count = 0;
	uint8_t b0 = M95M_ReadIdByte(0xf0);
	uint8_t b1 = M95M_ReadIdByte(0xf1);
	uint8_t b2 = M95M_ReadIdByte(0xf2);
	uint8_t b3 = M95M_ReadIdByte(0xf3);
	count = (count | b0) << 8;
	count = (count | b1) << 8;
	count = (count | b2) << 8;
	count = (count | b3);
	return(count);
}

//write data from PerApi_EEPROM_Packet to EEPROM
void PerAPI_WritePreparedData(uint32_t acctualCounter){
	uint32_t startingAddress = acctualCounter * 14;
	for (int i=0; i<14; i++){
		M95M_WriteByte(startingAddress+i, PerApi_EEPROM_Packet[i], EEPROM_Array);
	}
}
