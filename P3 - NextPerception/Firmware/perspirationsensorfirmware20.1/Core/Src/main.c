/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"
#include "usb_device.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "usbd_cdc_if.h"
#include "sht35.h"
#include "m95m04.h"
#include "utilities.h"
#include <string.h>
#include <string.h>
#include "ASCIIComm.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc;

I2C_HandleTypeDef hi2c1;
I2C_HandleTypeDef hi2c2;

RTC_HandleTypeDef hrtc;

SPI_HandleTypeDef hspi2;

/* Definitions for defaultTask */
osThreadId_t defaultTaskHandle;
const osThreadAttr_t defaultTask_attributes = {
  .name = "defaultTask",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = 128 * 4
};
/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_I2C2_Init(void);
static void MX_RTC_Init(void);
static void MX_I2C1_Init(void);
static void MX_SPI2_Init(void);
static void MX_ADC_Init(void);
void StartDefaultTask(void *argument);

/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */
  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_I2C2_Init();
  MX_RTC_Init();
  MX_I2C1_Init();
  MX_SPI2_Init();
  MX_ADC_Init();
  /* USER CODE BEGIN 2 */


  ButtonState = 0; //indikace stavu stlaceni ovladaciho tlacitka
  USBIntState = 0; //indikace inicializace USB
  USBConnectionIndication = 0; //indikace uspesne enumerace spojeni Device-USB-PC
  API_Status = Idle; //indikace stavu API

  //HAL_Delay(7000); //prodleva pred usnutim MCU pro bezrpoblemove pripojeni programatoru

  //HW interface ini
  Sht35_I2C_Interface_Ini(&hi2c2, &hi2c1); //Inicializace knihovny pro SHT35 senzory / předání handleru pro I2C rozhrani
  M95M_Ini(&hspi2); //Ini EEPROM interface
  //osDelay(10);
  HAL_Delay(10);
  PerAPI_DefaulSettingIni(); //Ini system settings with default values // M95M_Ini(&hspi2) MUST BE BEFORE

  //Indikace spusteni senzoru (3 x blikne modre)
  for (int i = 0;i <3; i++){
   HAL_GPIO_WritePin(LED_B_GPIO_Port, LED_B_Pin, GPIO_PIN_SET);
   HAL_Delay(200);
   HAL_GPIO_WritePin(LED_B_GPIO_Port, LED_B_Pin, GPIO_PIN_RESET);
   HAL_Delay(200);
  }


 // for sleep mode testing only
 /*HAL_Delay(5000);
   for (;;){


	  PerAPI_LowPowerTimerIni(1000);
	  PerAPI_LowPowerSetup();
	  PerAPI_LowPowerResume();
	  HAL_GPIO_TogglePin(LED_B_GPIO_Port, LED_B_Pin);
	  //osDelay(1000);
  }*/

  //charging indication testing

  /* while (1){
	  if (HAL_GPIO_ReadPin(ChargingIndication_GPIO_Port, ChargingIndication_Pin)){ //charging if ChargingIndication_Pin is 0
		  //NO-charging
		  HAL_GPIO_WritePin(LED_R_GPIO_Port, LED_R_Pin, GPIO_PIN_SET);
		  HAL_GPIO_WritePin(LED_B_GPIO_Port, LED_B_Pin, GPIO_PIN_RESET);
		  HAL_Delay(300);
	  } else {
		  //Charging
		  HAL_GPIO_WritePin(LED_R_GPIO_Port, LED_R_Pin, GPIO_PIN_RESET);
		  HAL_GPIO_WritePin(LED_B_GPIO_Port, LED_B_Pin, GPIO_PIN_SET);
		  HAL_Delay(300);
	  }
  }*/


  /* USER CODE END 2 */

  /* Init scheduler */
  osKernelInitialize();

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* creation of defaultTask */
  defaultTaskHandle = osThreadNew(StartDefaultTask, NULL, &defaultTask_attributes);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* Start scheduler */
  osKernelStart();

  /* We should never get here as control is now taken by the scheduler */
  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {

	    /* Infinite loop */

    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_HSE
                              |RCC_OSCILLATORTYPE_LSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.LSEState = RCC_LSE_ON;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL12;
  RCC_OscInitStruct.PLL.PLLDIV = RCC_PLL_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV16;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_RTC;
  PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC_Init(void)
{

  /* USER CODE BEGIN ADC_Init 0 */

  /* USER CODE END ADC_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC_Init 1 */

  /* USER CODE END ADC_Init 1 */
  /** Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion)
  */
  hadc.Instance = ADC1;
  hadc.Init.ClockPrescaler = ADC_CLOCK_ASYNC_DIV4;
  hadc.Init.Resolution = ADC_RESOLUTION_12B;
  hadc.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc.Init.ScanConvMode = ADC_SCAN_DISABLE;
  hadc.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  hadc.Init.LowPowerAutoWait = ADC_AUTOWAIT_DISABLE;
  hadc.Init.LowPowerAutoPowerOff = ADC_AUTOPOWEROFF_IDLE_PHASE;
  hadc.Init.ChannelsBank = ADC_CHANNELS_BANK_A;
  hadc.Init.ContinuousConvMode = DISABLE;
  hadc.Init.NbrOfConversion = 1;
  hadc.Init.DiscontinuousConvMode = DISABLE;
  hadc.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc.Init.DMAContinuousRequests = DISABLE;
  if (HAL_ADC_Init(&hadc) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
  */
  sConfig.Channel = ADC_CHANNEL_18;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime = ADC_SAMPLETIME_4CYCLES;
  if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC_Init 2 */

  /* USER CODE END ADC_Init 2 */

}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 100000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief I2C2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C2_Init(void)
{

  /* USER CODE BEGIN I2C2_Init 0 */

  /* USER CODE END I2C2_Init 0 */

  /* USER CODE BEGIN I2C2_Init 1 */

  /* USER CODE END I2C2_Init 1 */
  hi2c2.Instance = I2C2;
  hi2c2.Init.ClockSpeed = 100000;
  hi2c2.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c2.Init.OwnAddress1 = 0;
  hi2c2.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c2.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c2.Init.OwnAddress2 = 0;
  hi2c2.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c2.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C2_Init 2 */

  /* USER CODE END I2C2_Init 2 */

}

/**
  * @brief RTC Initialization Function
  * @param None
  * @retval None
  */
static void MX_RTC_Init(void)
{

  /* USER CODE BEGIN RTC_Init 0 */

  /* USER CODE END RTC_Init 0 */

  RTC_TimeTypeDef sTime = {0};
  RTC_DateTypeDef sDate = {0};
  RTC_AlarmTypeDef sAlarm = {0};

  /* USER CODE BEGIN RTC_Init 1 */

  /* USER CODE END RTC_Init 1 */
  /** Initialize RTC Only
  */
  hrtc.Instance = RTC;
  hrtc.Init.HourFormat = RTC_HOURFORMAT_24;
  hrtc.Init.AsynchPrediv = 127;
  hrtc.Init.SynchPrediv = 255;
  hrtc.Init.OutPut = RTC_OUTPUT_DISABLE;
  hrtc.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
  hrtc.Init.OutPutType = RTC_OUTPUT_TYPE_OPENDRAIN;
  if (HAL_RTC_Init(&hrtc) != HAL_OK)
  {
    Error_Handler();
  }

  /* USER CODE BEGIN Check_RTC_BKUP */
  	  if (0) { //disable RTC reinitialize after RESET
  /* USER CODE END Check_RTC_BKUP */

  /** Initialize RTC and set the Time and Date
  */
  sTime.Hours = 0;
  sTime.Minutes = 0;
  sTime.Seconds = 0;
  sTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
  sTime.StoreOperation = RTC_STOREOPERATION_RESET;
  if (HAL_RTC_SetTime(&hrtc, &sTime, RTC_FORMAT_BIN) != HAL_OK)
  {
    Error_Handler();
  }
  sDate.WeekDay = RTC_WEEKDAY_SATURDAY;
  sDate.Month = RTC_MONTH_DECEMBER;
  sDate.Date = 5;
  sDate.Year = 20;

  if (HAL_RTC_SetDate(&hrtc, &sDate, RTC_FORMAT_BIN) != HAL_OK)
  {
    Error_Handler();
  }
  /** Enable the Alarm A
  */
  sAlarm.AlarmTime.Hours = 0;
  sAlarm.AlarmTime.Minutes = 0;
  sAlarm.AlarmTime.Seconds = 0;
  sAlarm.AlarmTime.SubSeconds = 0;
  sAlarm.AlarmTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
  sAlarm.AlarmTime.StoreOperation = RTC_STOREOPERATION_RESET;
  sAlarm.AlarmMask = RTC_ALARMMASK_ALL;
  sAlarm.AlarmSubSecondMask = RTC_ALARMSUBSECONDMASK_ALL;
  sAlarm.AlarmDateWeekDaySel = RTC_ALARMDATEWEEKDAYSEL_DATE;
  sAlarm.AlarmDateWeekDay = 1;
  sAlarm.Alarm = RTC_ALARM_A;
  if (HAL_RTC_SetAlarm_IT(&hrtc, &sAlarm, RTC_FORMAT_BIN) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN RTC_Init 2 */
	}

  	  /** Enable the Alarm A
  	  */
  	  sAlarm.AlarmTime.Hours = 0;
  	  sAlarm.AlarmTime.Minutes = 0;
  	  sAlarm.AlarmTime.Seconds = 0;
  	  sAlarm.AlarmTime.SubSeconds = 0;
  	  sAlarm.AlarmTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
  	  sAlarm.AlarmTime.StoreOperation = RTC_STOREOPERATION_RESET;
  	  sAlarm.AlarmMask = RTC_ALARMMASK_ALL;
  	  sAlarm.AlarmSubSecondMask = RTC_ALARMSUBSECONDMASK_ALL;
  	  sAlarm.AlarmDateWeekDaySel = RTC_ALARMDATEWEEKDAYSEL_DATE;
  	  sAlarm.AlarmDateWeekDay = 1;
  	  sAlarm.Alarm = RTC_ALARM_A;
  	  if (HAL_RTC_SetAlarm_IT(&hrtc, &sAlarm, RTC_FORMAT_BIN) != HAL_OK)
  	  {
  	    Error_Handler();
  	  }

  /** Enable the WakeUp
  */
  if (HAL_RTCEx_SetWakeUpTimer_IT(&hrtc, 0, RTC_WAKEUPCLOCK_RTCCLK_DIV16) != HAL_OK)
  {
    Error_Handler();
  }

  /* USER CODE END RTC_Init 2 */

}

/**
  * @brief SPI2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI2_Init(void)
{

  /* USER CODE BEGIN SPI2_Init 0 */

  /* USER CODE END SPI2_Init 0 */

  /* USER CODE BEGIN SPI2_Init 1 */

  /* USER CODE END SPI2_Init 1 */
  /* SPI2 parameter configuration*/
  hspi2.Instance = SPI2;
  hspi2.Init.Mode = SPI_MODE_MASTER;
  hspi2.Init.Direction = SPI_DIRECTION_2LINES;
  hspi2.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi2.Init.CLKPolarity = SPI_POLARITY_HIGH;
  hspi2.Init.CLKPhase = SPI_PHASE_2EDGE;
  hspi2.Init.NSS = SPI_NSS_SOFT;
  hspi2.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
  hspi2.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi2.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi2.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi2.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI2_Init 2 */

  /* USER CODE END SPI2_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, EEPROM_Write_Pin|EEPROM_CS_Pin|EEPROM_HOLD_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, LED_R_Pin|LED_B_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : EEPROM_Write_Pin EEPROM_CS_Pin EEPROM_HOLD_Pin */
  GPIO_InitStruct.Pin = EEPROM_Write_Pin|EEPROM_CS_Pin|EEPROM_HOLD_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : LED_R_Pin LED_B_Pin */
  GPIO_InitStruct.Pin = LED_R_Pin|LED_B_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : ChargingIndication_Pin */
  GPIO_InitStruct.Pin = ChargingIndication_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(ChargingIndication_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : Button_Pin */
  GPIO_InitStruct.Pin = Button_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(Button_GPIO_Port, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI9_5_IRQn, 1, 0);
  HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void *argument)
{
  /* init code for USB_DEVICE */
  MX_USB_DEVICE_Init();
  /* USER CODE BEGIN 5 */
  /* Infinite loop */

  PerApi_vBat =	GetVbat(&hadc);

  for(;;)
  {
  	  //auxiliary variables for PC communication
  	  uint32_t dataCount = 0;
  	  char preambule[30];

  	  //Check if USB is connected
  	  if (USBCheckConnectionState()) {

  		  HAL_GPIO_WritePin(LED_B_GPIO_Port, LED_B_Pin, GPIO_PIN_SET);

  		  if (HAL_GPIO_ReadPin(ChargingIndication_GPIO_Port, ChargingIndication_Pin)){ //charging if ChargingIndication_Pin is 0
  			  //NO-charging
  			  HAL_GPIO_WritePin(LED_R_GPIO_Port, LED_R_Pin, GPIO_PIN_RESET);
  			  //HAL_Delay(300);
  		  } else {
  			  //Charging
  			  HAL_GPIO_WritePin(LED_R_GPIO_Port, LED_R_Pin, GPIO_PIN_SET);
  			  //HAL_Delay(300);
  		  }

  	  	  //USB mode - no sleep
  		  //PC COMMAND RESPONSE
  		  //USBConnectionIndication = 1;
  	      switch(PRESP_PC_COMMAND()){
  	      case PERSP_GET_REALTIME: // ASCII command get RTC
  	   	      pPERSP_string_buff = PERSP_GetRTC_Time_String(&hrtc);
  	     	  CDC_Transmit_FS((uint8_t*)pPERSP_string_buff, strlen(pPERSP_string_buff)); //Send RTC time to PC
  	    	  break;
  	      case PERSP_SET_REALTIME: // ASCII command ser RTC
  	    	  pPERSP_string_buff = PERSP_SetRTC_Time_String(&hrtc);
  	    	  CDC_Transmit_FS((uint8_t*)pPERSP_string_buff, strlen(pPERSP_string_buff)); //ACK after RTC setting
  	    	  break;
  	      case PERSP_GET_SENSOR_VALUES:
  	    	  pPERSP_string_buff = PERSP_GetSHT_Sensor();
  	    	  CDC_Transmit_FS((uint8_t*)pPERSP_string_buff, strlen(pPERSP_string_buff)); //Send selected SHT actual data
  	    	  break;
  	      case PERSP_GET_HWFW_INFO:
  	    	  pPERSP_string_buff = PERSP_GetHWFW_Info();
  	    	  CDC_Transmit_FS((uint8_t*)pPERSP_string_buff, strlen(pPERSP_string_buff)); //Send HW/FW info
  	    	  break;
  	      case PERSP_GET_BATTERY_VOLTAGE:
  	    	  pPERSP_string_buff = PERSP_GetBattery_Voltage(&hadc);
  	    	  CDC_Transmit_FS((uint8_t*)pPERSP_string_buff, strlen(pPERSP_string_buff)); //Send HW/FW info
  	    	  break;
  	      case PERSP_GET_DATACOUNT: //get count of data packet stored in EEPROM
  	    	  dataCount = PerAPI_ReadMeasurementCount();
    	    	  // dataCount = 30000; //only for testing
  	    	  sprintf(&preambule[0],"D#%d\r\n",(int)dataCount);
  	    	  CDC_Transmit_FS((uint8_t*)&preambule[0], strlen(&preambule[0])); //Send data preambule
  	    	  break;
  	      case PERSP_GET_SENSOR_ID: //get sensor device ID
  	    	  sprintf(&preambule[0],"ID=%02d\r\n",(int)PerApi_SystemSettings.deviceId);
  	    	  CDC_Transmit_FS((uint8_t*)&preambule[0], strlen(&preambule[0])); //Send data preambule
  	    	  break;
  	      case PERSP_SET_SENSOR_ID: //set sensor device ID
  	    	  PERSP_SetSensor_ID();
  	    	  strcpy(&preambule[0],"ACK\r\n");
  	    	  CDC_Transmit_FS((uint8_t*)&preambule[0], strlen(&preambule[0])); //Send end of data
  	    	  break;
  	      case PERSP_SET_SENSOR_DISTANCE: //set SHT sensors distance
  	    	  PERSP_SetSensor_Distance();
  	    	  strcpy(&preambule[0],"ACK\r\n");
  	    	  CDC_Transmit_FS((uint8_t*)&preambule[0], strlen(&preambule[0])); //Send end of data
  	    	  break;
  	      case PERSP_GET_SENSOR_DISTANCE: //get SHT sensors distance
  	    	  if (PerApi_SystemSettings.shtSensorsDistance > 0) {
  		    	  //ftoa(((float)PerApi_SystemSettings.shtSensorsDistance)/100, &subStringBuff[0], 2);
  	    		  int a = PerApi_SystemSettings.shtSensorsDistance/100;
  	    		  int b = PerApi_SystemSettings.shtSensorsDistance%100;
  	    		  //double a = (float)PerApi_SystemSettings.shtSensorsDistance/(float)100;
  	    		  //ftoa(a, &subStringBuff[0], 2);
  		    	  //sprintf(&preambule[0],"Sensor distance: %s mm\r\n", subStringBuff);
  		    	  sprintf(&preambule[0],"Sensor distance: %02d.%02d mm\r\n", a,b);
  	    	  } else {
  	    		  sprintf(&preambule[0],"Sensor distance: 00.00 mm\r\n");
  	    	  }
  	    	  CDC_Transmit_FS((uint8_t*)&preambule[0], strlen(&preambule[0])); //Send data preambule
  	    	  break;

  	      case PERSP_GET_DATASTREAM: //get data stream of all data from EEPROM

  	    	  dataCount = PerAPI_ReadMeasurementCount();
  	    	  // dataCount = 30000; //only for testing
  	    	  uint32_t byteCount = dataCount * 14;


  	    	  M95M_StartReadArray(0); //START EEPROM array reading
  	    	  int timer = 0;

  	    	  while (byteCount > 0) {
  	    		  if (byteCount > 210) {

  	        		  uint8_t *readData = M95M_ReadArray(210);
  	        		  CDC_Transmit_FS(readData, 210); //Send end of data
  	        	      ASCIICommand_SwitchToFlowControl(); //only for data flow controlling purpose

  	        		  timer = 0;
  	        		  while(ReciveFlowControlCommand == 0x00 && timer < 250){
  	        			HAL_Delay(1);
  	        			timer++;
  	        		  }


        			  switch(ReciveFlowControlCommand){
        			  case 0x00:
        			  case '0': //cancel sending
        				  byteCount = 0;
        				  break;
        			  case '1': //ok, next data
  	        			  byteCount = byteCount - 210;
        				  break;
        			  case '2': //repeat sending data
        			  default:
        				  break;
        			  }
  	        		  ReciveFlowControlCommand = 0x00;

  	    		  } else {
  	        		  uint8_t *readData = M95M_ReadArray(byteCount);
  	        		  CDC_Transmit_FS(readData, byteCount); //Send end of data

  	        		  timer = 0;
  	        		  while(ReciveFlowControlCommand == 0x00 && timer < 250){
  	        			HAL_Delay(1);
  	        			timer++;
  	        		  }


        			  switch(ReciveFlowControlCommand){
        			  case 0x00:
        			  case '0': //cancel sending or OK
        			  case '1':
        				  byteCount = 0;
        				  break;
        			  case '2': //repeat sending data
        			  default:
        				  break;
        			  }
  	        		  ReciveFlowControlCommand = 0x00;

  	    		  }
  	    		 ASCIICommand_SwitchToReceiving(); //only for data flow controlling purpose -- back to ASCII command mode

  	    		// HAL_Delay(1);
  	    		 // osDelay(1); //9 ms 210 at baud rate 256kbps
  	    	  }

  	    	  HAL_Delay(10);
  	    	  M95M_StopReadArray(); //STOP EEPROM array reading

  	    	  strcpy(&preambule[0],"\r\n");
  	    	  CDC_Transmit_FS((uint8_t*)&preambule[0], strlen(&preambule[0])); //Send end of data
  	    	  break;
  	      case PERSP_CLEAR_ALLDATA: //clear all data
  	    	  PerAPI_WriteMeasurementCount(0);
  	    	  strcpy(&preambule[0],"ACK\r\n");
  	    	  CDC_Transmit_FS((uint8_t*)&preambule[0], strlen(&preambule[0])); //Send end of data
  	    	  break;
  	      default:
  	    	  break;
  	      }
  	      HAL_Delay(10);

  	  } else {
  		  //odpojeno od PC - uspany rezim + obsluha ovladaciho tlacitka

  		  	  	  HAL_GPIO_WritePin(LED_B_GPIO_Port, LED_B_Pin, GPIO_PIN_RESET);
  		  	      HAL_GPIO_WritePin(LED_R_GPIO_Port, LED_R_Pin, GPIO_PIN_RESET);

  		  		  if (HAL_GPIO_ReadPin(ChargingIndication_GPIO_Port, ChargingIndication_Pin)){ //charging if ChargingIndication_Pin is 0
  		  			  //NO-charging
  		  			  HAL_GPIO_WritePin(LED_R_GPIO_Port, LED_R_Pin, GPIO_PIN_RESET);
  		  		  } else {
  		  			  //Charging
  		  			  HAL_GPIO_WritePin(LED_R_GPIO_Port, LED_R_Pin, GPIO_PIN_SET);
  		  		  }

  		  	  	  //go to sleep each 10sec
  				  PerAPI_LowPowerSetup();
  				  PerAPI_LowPowerResume();

  				  //wake up by USB - dulezite pro enumeraci USB komunikace na PC
  				  if (USBIntState == 1){
  				      HAL_Delay(500);
  					  USBIntState = 0;
  					  HAL_Delay(500);
  				  }

  		  	  	  //osDelay(1000);
  				  // wake up by pressing control button
  				  if (ButtonState == 1){
  					  // 10 second's checking if button was press again
  					  HAL_Delay(200);
  					  ButtonState = 0;
  					  HAL_GPIO_WritePin(LED_B_GPIO_Port, LED_B_Pin, GPIO_PIN_SET);
  					  for (int i = 0; i<100; i++){
  						  HAL_Delay(100);
  						  if (ButtonState == 1) { //pokud ano - spusti se snimani
  							  ButtonState = 0;
  							  API_Status = Measuring;
  							  HAL_GPIO_WritePin(LED_B_GPIO_Port, LED_B_Pin, GPIO_PIN_RESET);
  							  HAL_Delay(200);
  							  PerApi_vBat =	GetVbat(&hadc);
  							  HAL_Delay(200);
  							  PerApi_vBat =	GetVbat(&hadc);
  							  break;
  						  }
  						  if (USBCheckConnectionState())  break;
  					  }
  					  HAL_GPIO_WritePin(LED_B_GPIO_Port, LED_B_Pin, GPIO_PIN_RESET);
  				  } else {
  					  HAL_GPIO_WritePin(LED_R_GPIO_Port, LED_R_Pin, GPIO_PIN_RESET);
  					  HAL_GPIO_WritePin(LED_B_GPIO_Port, LED_B_Pin, GPIO_PIN_RESET);
  				  }
  	  }

  	  int tick = 0; //cislo aktualniho bodu mereni
  	  uint32_t measurementCounter = 0; //indikuje aktualane mereny packet
  	  //PerApi_vBat = 4;

  	  // ************************************************************
  	  // Smycka mereni a ukladani dat do EEPROM po stisknuti tlacitka
  	  // ************************************************************

  	  if (API_Status == Measuring) {

  		  //ini measurement counter from EEPROM - nastaveni vychozi adresy EEPROM pro nadchazejici mereni
  		  measurementCounter = PerAPI_ReadMeasurementCount(); //nacteni aktualne ulozenych packetu mereni
  		  //add measurement head
  		  PerAPI_Prepare_MesHead(&hrtc);
  		  PerAPI_WritePreparedData(measurementCounter);
  		  measurementCounter++;
  		  HAL_GPIO_WritePin(LED_B_GPIO_Port, LED_B_Pin, GPIO_PIN_RESET);
  		  HAL_GPIO_WritePin(LED_R_GPIO_Port, LED_R_Pin, GPIO_PIN_RESET);


  		  //Indikace zahajeni mereni
  		  for (int g=0; g<3; g++){
  			  HAL_Delay(300);
  			  if (PerApi_vBat > 3.80){
  				  HAL_GPIO_WritePin(LED_B_GPIO_Port, LED_B_Pin, GPIO_PIN_SET);
  			  } else {
  				  HAL_GPIO_WritePin(LED_R_GPIO_Port, LED_R_Pin, GPIO_PIN_SET);
  			  }
  			  HAL_Delay(300);
  			  HAL_GPIO_WritePin(LED_R_GPIO_Port, LED_R_Pin, GPIO_PIN_RESET);
  			  HAL_GPIO_WritePin(LED_B_GPIO_Port, LED_B_Pin, GPIO_PIN_RESET);
  		  }

  		  PerAPI_LowPowerTimerIni(1000);

  		  //smycka provadeni mereni dokud neni stisknuto tlacitko pro ukonceni, nebo neni vyvolano ukonceni v ramci smycky
  		  while (ButtonState == 0){
  			  //!!!! UPGRADE - predelat na funkci casovanou pomoci 1 sec alarmu od RTC pro presnejsi casovani - v posledni verzi jiz hotovo

  			  //check if there is enough EEPROM space for next measurement

  			  if (measurementCounter < 37400){

  				  PerAPI_LowPowerSetup();
  				  PerAPI_LowPowerResume();
  				  //PerAPI_LowPowerTimerIni(1000);
  				  tick++;
   			      HAL_Delay(100);
				  PerApi_vBat =	GetVbat(&hadc);
  				  //running mode indication

  				  if ((tick % 3) == 0){

  					  HAL_Delay(50);
  					  if (PerApi_vBat > 3.8){
  						  HAL_GPIO_WritePin(LED_B_GPIO_Port, LED_B_Pin, GPIO_PIN_SET);
  					  } else {
  						  HAL_GPIO_WritePin(LED_R_GPIO_Port, LED_R_Pin, GPIO_PIN_SET);
  						  if (PerApi_vBat < 3.65) ButtonState = 1;
  					  }
  					  HAL_Delay(40);
  					  HAL_GPIO_WritePin(LED_R_GPIO_Port, LED_R_Pin, GPIO_PIN_RESET);
  					  HAL_GPIO_WritePin(LED_B_GPIO_Port, LED_B_Pin, GPIO_PIN_RESET);
  				  }

  				  //********************************************
  				  //measurement routine - namereni a ulozeni dat
  				  //********************************************

  				  PerAPI_Prepare_MesPoint(&hadc);
  				  PerAPI_WritePreparedData(measurementCounter);
  				  measurementCounter++;
  			  } else {
  				  //not enough space in EEPROM / then terminated measurement
  				  ButtonState = 1;
  				  HAL_GPIO_WritePin(LED_R_GPIO_Port, LED_R_Pin, GPIO_PIN_SET);
  				  HAL_GPIO_WritePin(LED_B_GPIO_Port, LED_B_Pin, GPIO_PIN_SET);
  				  HAL_Delay(1000);
  				  HAL_GPIO_WritePin(LED_R_GPIO_Port, LED_R_Pin, GPIO_PIN_RESET);
  				  HAL_GPIO_WritePin(LED_B_GPIO_Port, LED_B_Pin, GPIO_PIN_RESET);
  			  }
  		  }

  		  //indikace ukonceni mereni
  		  for (int g=0; g<5; g++){
  			  HAL_Delay(100);
  			  if (PerApi_vBat > 3.8){
  				  HAL_GPIO_WritePin(LED_B_GPIO_Port, LED_B_Pin, GPIO_PIN_SET);
  			  } else {
  				  HAL_GPIO_WritePin(LED_R_GPIO_Port, LED_R_Pin, GPIO_PIN_SET);
  			  }
  			  HAL_Delay(100);
  			  HAL_GPIO_WritePin(LED_B_GPIO_Port, LED_B_Pin, GPIO_PIN_RESET);
  			  HAL_GPIO_WritePin(LED_R_GPIO_Port, LED_R_Pin, GPIO_PIN_RESET);
  		  }

  		  //Add Measurement Tail - pridani ukoncovaciho packetu mereni, obsahuje cas ukonceni mereni odecteny z RTC
  		  PerAPI_Prepare_MesTail(&hrtc);
  		  PerAPI_WritePreparedData(measurementCounter);
  		  measurementCounter++;

  		  PerAPI_WriteMeasurementCount(measurementCounter);
  		  ButtonState = 0; //STOP measuring
  		  API_Status = Idle;
  	  }

  }
  /* USER CODE END 5 */
}

/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM6 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM6) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
