/*
 * ASCIIComm.c
 *
 *  Created on: Nov 28, 2020
 *      Author: Zdenek Pytlicek
 */

#include "ASCIIComm.h"
#include "main.h"





// ASCIICommandRegister store actual incoming command
struct ASCIICommandRegister {
	ASCIIComandState state;
	char Command[40];
	int incoming_command_len;
} ASCIIComnandStatusRegister = {READY_FOR_NEW_COMMAND, "", 0};

//only for data flow controlling purpose
void ASCIICommand_SwitchToFlowControl(void)
{
	ReciveFlowControlCommand = 0x00;
	ASCIIComnandStatusRegister.state = DATA_FLOW_CONTROLL;
}

void ASCIICommand_SwitchToReceiving(void){
	ASCIIComnandStatusRegister.state = READY_FOR_NEW_COMMAND;
}

char PERSP_string_buff[100]; //buffer for string response


//return actual PC command with data as a pointer
PERSP_CommandsList PRESP_PC_COMMAND(void){

	PERSP_CommandsList recivedCommand = PERSP_NOCOMMAND; //default value

	if (ASCIIComnandStatusRegister.state == COMMAND_READY)
	{
		switch(ASCIIComnandStatusRegister.Command[0])
		{
			case 't': //time command
				switch(ASCIIComnandStatusRegister.Command[1])
				{
					case '?': //get send back to PC acctual time form RTC
						recivedCommand = PERSP_GET_REALTIME;
						break;
					case ':': //set RTC time to received data
						recivedCommand = PERSP_SET_REALTIME;
						break;
					default:
						break;
					}
				break;
			case 'm': //sensor measurement command
				if (ASCIIComnandStatusRegister.Command[2] == '?'){
					recivedCommand = PERSP_GET_SENSOR_VALUES;
				} else {
					recivedCommand = PERSP_UNKNOWN_COMMAND;
				}
				break;
			case 's': //sensor distance
				if (ASCIIComnandStatusRegister.Command[1] == 'd'){
					switch(ASCIIComnandStatusRegister.Command[2]){
					case '?':
						recivedCommand = PERSP_GET_SENSOR_DISTANCE;
						break;
					case ':':
						recivedCommand = PERSP_SET_SENSOR_DISTANCE;
						break;
					default:
						recivedCommand = PERSP_UNKNOWN_COMMAND;
						break;
					}
				} else {
					recivedCommand = PERSP_UNKNOWN_COMMAND;
				}
				break;
			case 'i': //hw command
				switch (ASCIIComnandStatusRegister.Command[1])
				{
					case '?':
						recivedCommand = PERSP_GET_HWFW_INFO;
						break;
					case 'd':
						switch (ASCIIComnandStatusRegister.Command[2])
						{
						case '?':
							recivedCommand = PERSP_GET_SENSOR_ID;
							break;
						case ':':
							recivedCommand = PERSP_SET_SENSOR_ID;
							break;
						default:
							break;
						}
						break;
					default:
						break;
				}
				if (ASCIIComnandStatusRegister.Command[1] == '?'){
					recivedCommand = PERSP_GET_HWFW_INFO;
				}
				break;
			case 'b': //battery command
				if (ASCIIComnandStatusRegister.Command[1] == '?'){
					recivedCommand = PERSP_GET_BATTERY_VOLTAGE;
				}
				break;
			case 'd': //data command
				switch(ASCIIComnandStatusRegister.Command[1])
				{
					case 'c': //get count of data packet stored in EEPROM
						if (ASCIIComnandStatusRegister.Command[2] == '?') recivedCommand = PERSP_GET_DATACOUNT;
						break;
					case 'b': //send all data from EEPROM as binary data stream to PC
						if (ASCIIComnandStatusRegister.Command[2] == '?') recivedCommand = PERSP_GET_DATASTREAM;
						break;
					case 'e': //erase all stored data form EEPROM
						if (ASCIIComnandStatusRegister.Command[2] == '!') recivedCommand = PERSP_CLEAR_ALLDATA;
						break;
					default:
						break;
					}
				break;
			default:
				recivedCommand = PERSP_UNKNOWN_COMMAND;
				break;
		}
		//clear command after reading
		ASCIIComnandStatusRegister.state = READY_FOR_NEW_COMMAND;
		ASCIIComnandStatusRegister.incoming_command_len = 0;
	}
	return recivedCommand;
}



//get actual RTC time as string
char *PERSP_GetRTC_Time_String(RTC_HandleTypeDef *pHrtc)
{
	//buffer variables for dataTimeFunctions
	RTC_TimeTypeDef currentTimeBuff = {0};
	RTC_DateTypeDef currentDateBuff =  {0};

	HAL_RTC_GetTime(pHrtc, &currentTimeBuff, RTC_FORMAT_BIN);
	HAL_RTC_GetDate(pHrtc, &currentDateBuff, RTC_FORMAT_BIN);
    sprintf(PERSP_string_buff,"20%02d.%02d.%02d %02d:%02d:%02d\r\n",currentDateBuff.Year,currentDateBuff.Month, currentDateBuff.Date, currentTimeBuff.Hours, currentTimeBuff.Minutes, currentTimeBuff.Seconds);
	return (&PERSP_string_buff[0]);
}


//set RTC time - correct ASCII command necessary
char *PERSP_SetRTC_Time_String(RTC_HandleTypeDef *pHrtc)
{
	//buffer variables for dataTimeFunctions
	RTC_TimeTypeDef currentTimeBuff = {0};
	RTC_DateTypeDef currentDateBuff =  {0};
	//incoming data format "‘t:yyyy.mm.dd;hh:mm:ss;w’ (24-hour clock format, w – week day (0x01-MONDAY – 0x07 SUNDAY)) "

	strcpy(PERSP_string_buff,"NAK\r\n"); //ACK string

	if (strlen(ASCIIComnandStatusRegister.Command) == 23){ //check command length

		//Parse incoming command + date/time range check

		uint8_t year = (uint8_t)atoi(&ASCIIComnandStatusRegister.Command[4]);
		if (year > 0 && year < 100) currentDateBuff.Year = year; else return(&PERSP_string_buff[0]);

		uint8_t month = (uint8_t)atoi(&ASCIIComnandStatusRegister.Command[7]);
		if (month >= 1 && month <= 12) currentDateBuff.Month = month; else return(&PERSP_string_buff[0]);


		uint8_t date = (uint8_t)atoi(&ASCIIComnandStatusRegister.Command[10]);
		if (date >= 1 && date <= 31) currentDateBuff.Date = date; else return(&PERSP_string_buff[0]);

		uint8_t weekday = (uint8_t)atoi(&ASCIIComnandStatusRegister.Command[22]);
		if (weekday >= 1 && weekday <= 7) currentDateBuff.WeekDay = weekday; else return(&PERSP_string_buff[0]);

		currentTimeBuff.DayLightSaving = 0;


		uint8_t hours = (uint8_t)atoi(&ASCIIComnandStatusRegister.Command[13]);
		if (hours >= 0 && hours <= 23) currentTimeBuff.Hours = hours; else return(&PERSP_string_buff[0]);

		uint8_t minutes = (uint8_t)atoi(&ASCIIComnandStatusRegister.Command[16]);
		if (minutes >= 0 && minutes <= 59) currentTimeBuff.Minutes = minutes; else return(&PERSP_string_buff[0]);

		uint8_t seconds = (uint8_t)atoi(&ASCIIComnandStatusRegister.Command[19]);
		if (seconds >= 0 && seconds <= 59) currentTimeBuff.Seconds = seconds; else return(&PERSP_string_buff[0]);

		currentTimeBuff.TimeFormat = RTC_HOURFORMAT_24;

	    HAL_RTC_SetDate(pHrtc, &currentDateBuff, RTC_FORMAT_BIN);
	    HAL_RTC_SetTime(pHrtc, &currentTimeBuff, RTC_FORMAT_BIN);
		strcpy(PERSP_string_buff,"ACK\r\n"); //ACK string
	}

	return(&PERSP_string_buff[0]);
}

//set sensor distance - correct ASCII command necessary "sd:NN.NN"
char *PERSP_SetSensor_Distance(void){
	uint16_t distanceBuff = 0;
	distanceBuff = (uint8_t)atoi(&ASCIIComnandStatusRegister.Command[3]) * 100;
	distanceBuff +=  (uint8_t)atoi(&ASCIIComnandStatusRegister.Command[6]);
	PerApi_SystemSettings.shtSensorsDistance = distanceBuff;
	PerAPI_SaveSystemSetting();
	strcpy(PERSP_string_buff,"ACK\r\n"); //ACK string
	return(&PERSP_string_buff[0]);
}

//set device ID - correct ASCII command necessary "id:XX"
char *PERSP_SetSensor_ID(void){
	PerApi_SystemSettings.deviceId = (uint8_t)atoi(&ASCIIComnandStatusRegister.Command[3]);
	PerAPI_SaveSystemSetting();
	strcpy(PERSP_string_buff,"ACK\r\n"); //ACK string
	return(&PERSP_string_buff[0]);
}

//get acctual battery voltage PERSP_GET_BATTERY_VOLTAGE
char *PERSP_GetBattery_Voltage(ADC_HandleTypeDef *hadc){
	strcpy(PERSP_string_buff, "Vbat="); //ini string for sending
    float actualVbat = GetVbat(hadc);
    char valueBuf[10];
    ftoa(actualVbat, &valueBuf[0], 2);
    strcat(PERSP_string_buff, valueBuf);
	strcat(PERSP_string_buff, " V\r\n");
	return(&PERSP_string_buff[0]);
}

//get actual SHT sensor values
char *PERSP_GetSHT_Sensor(void){
	Sht35Data shtdataBuff;
	char textBuf[30];
	char tBuf[10];
	char hBuf[10];

	strcpy(PERSP_string_buff,""); //ini string for sending

	//get PCB sensor
	if (ASCIIComnandStatusRegister.Command[1] == 'p' || ASCIIComnandStatusRegister.Command[1] == 'a') {
		shtdataBuff = Sht35_read(PCBSensor);
		ftoa(shtdataBuff.temp, &tBuf[0], 2);
		ftoa(shtdataBuff.humidity, &hBuf[0], 2);
		sprintf(textBuf,"S.P.: %s`C, %s%%", tBuf, hBuf);
		strcat(PERSP_string_buff, textBuf);
	}

	//get bottom sensor
	if (ASCIIComnandStatusRegister.Command[1] == 'b' || ASCIIComnandStatusRegister.Command[1] == 'a') {
		if (ASCIIComnandStatusRegister.Command[1] == 'a') strcat(PERSP_string_buff,"; ");
		shtdataBuff = Sht35_read(MainSensorBottom);
		ftoa(shtdataBuff.temp, &tBuf[0], 2);
		ftoa(shtdataBuff.humidity, &hBuf[0], 2);
		sprintf(textBuf,"S.B.: %s`C, %s%%", tBuf, hBuf);
		strcat(PERSP_string_buff, textBuf);
	}

	//get top sensor
	if (ASCIIComnandStatusRegister.Command[1] == 't' || ASCIIComnandStatusRegister.Command[1] == 'a') {
		if (ASCIIComnandStatusRegister.Command[1] == 'a') strcat(PERSP_string_buff,"; ");
		shtdataBuff = Sht35_read(MainSensorTop);
		ftoa(shtdataBuff.temp, &tBuf[0], 2);
		ftoa(shtdataBuff.humidity, &hBuf[0], 2);
		sprintf(textBuf,"S.T.: %s`C, %s%%", tBuf, hBuf);
		strcat(PERSP_string_buff, textBuf);
	}

	strcat(PERSP_string_buff,"\r\n");
	return(&PERSP_string_buff[0]);

}

//get perspiracion HW/FW info
char *PERSP_GetHWFW_Info(void) {
	strcpy(PERSP_string_buff,"Perspiration Sensor\r\n"); //ini string for sending
	strcat(PERSP_string_buff, "HW ver.");
	strcat(PERSP_string_buff, PerspirationAPI_Hardware_version);
	strcat(PERSP_string_buff, "\r\nFW ver.");
	strcat(PERSP_string_buff, PerspirationAPI_Firmware_version);
	strcat(PERSP_string_buff, "\r\n");
	return(&PERSP_string_buff[0]);
}

//handling newly received char from USB
void recive_new_char(char incoming_char){
	switch(ASCIIComnandStatusRegister.state){
	case DATA_FLOW_CONTROLL: //no command is pending, ready for new command
		ReciveFlowControlCommand = incoming_char;
		break;
	case READY_FOR_NEW_COMMAND: //no command is pending, ready for new command
		if (incoming_char == '^') {
			ASCIIComnandStatusRegister.state = INCOMING_COMMAND;
			ASCIIComnandStatusRegister.incoming_command_len = 0;
		}
		break;
	case INCOMING_COMMAND: //no command is pending, ready for new command

		if (incoming_char != '\r' && ASCIIComnandStatusRegister.incoming_command_len < 40){
			ASCIIComnandStatusRegister.Command[ASCIIComnandStatusRegister.incoming_command_len] = incoming_char;
			ASCIIComnandStatusRegister.incoming_command_len++;
		} else {
			//termination of incoming command
			if (incoming_char == '\r'){
				ASCIIComnandStatusRegister.Command[ASCIIComnandStatusRegister.incoming_command_len] = '\0';
				ASCIIComnandStatusRegister.state = COMMAND_READY;
			} else {
				ASCIIComnandStatusRegister.state = READY_FOR_NEW_COMMAND;
				ASCIIComnandStatusRegister.incoming_command_len = 0;
			}
		}
		break;
	case COMMAND_READY:
	default:
		//ignore the newly incoming char until command is served
		break;
	}
}
