/*
 * m95m04.c
 *
 *  The M95M04 is a SPI-interface EEPROM chip with 4Mbit capacity
 *
 *  Author: Zdenek Pytlicek
 *  Created on: 23. 11. 2020
 *
 *  v1.0 - First release
 */

#include "m95m04.h"

#define M95M02_CMD_WREN								0x06   //Write enable
#define M95M02_CMD_WRDI								0x04   //Write disable
#define M95M02_CMD_RDSR								0x05   //Read Status register
#define M95M02_CMD_WRSR								0x01   //Write Status register
#define M95M02_CMD_READ								0x03   //Read from Memory array
#define M95M02_CMD_WRITE							0x02   //Write to Memory array
#define M95M02_CMD_READ_ID							0x83   //Read Identification page
#define M95M02_CMD_WRITE_ID							0x82   //Write Identification page
#define M95M02_CMD_READ_LOCK_STATUS					0x83   //Reads the Identification page lock status
#define M95M02_CMD_LOCK_ID							0x82   //Locks the Identification page in read-only mode


void commandWREN();	// Set the Write Enable Latch (WEL) bit
void SPI_DataWRITE(); //
void SPI_DataRead(); //

uint8_t recivedData[256];
uint8_t EEPROM_statusReg;

SPI_HandleTypeDef *halSpiInterface;

// Initialize interfaces
void M95M_Ini(SPI_HandleTypeDef *hspi){
	  halSpiInterface = hspi;
	  //reset EEPROM communication
	  HAL_GPIO_WritePin(EEPROM_CS_GPIO_Port, EEPROM_CS_Pin, GPIO_PIN_RESET);
	  //osDelay(1);
	  HAL_Delay(1);
	  HAL_GPIO_WritePin(EEPROM_HOLD_GPIO_Port, EEPROM_HOLD_Pin, GPIO_PIN_RESET);
	  //osDelay(1);
	  HAL_Delay(1);
	  HAL_GPIO_WritePin(EEPROM_CS_GPIO_Port, EEPROM_CS_Pin, GPIO_PIN_SET);
	  //osDelay(1);
	  HAL_Delay(1);
	  HAL_GPIO_WritePin(EEPROM_HOLD_GPIO_Port, EEPROM_HOLD_Pin, GPIO_PIN_SET);
}


// Read a byte from EEPROM Identification page
uint8_t M95M_ReadIdByte(uint32_t address){
	  //read data from address 1
	  HAL_GPIO_WritePin(EEPROM_CS_GPIO_Port, EEPROM_CS_Pin, GPIO_PIN_RESET); //activate EEPROM
	  uint8_t ReadAddress[4] = {M95M02_CMD_READ_ID,0x00,0x00,0x00};
	  ReadAddress[3] = address & 0xff;
	  ReadAddress[2] = address >> 8 & 0xff;
	  ReadAddress[1] = address >> 16 & 0xff;;
	  HAL_SPI_Transmit(halSpiInterface, &ReadAddress[0], 4, 100);
	  HAL_SPI_Receive(halSpiInterface, &recivedData[0], 1, 100); //read data (status register)
	  HAL_GPIO_WritePin(EEPROM_CS_GPIO_Port, EEPROM_CS_Pin, GPIO_PIN_SET); //deactivate EEPROM
	  //end read data from address 1
	  return(recivedData[0]);
}

// Read a byte from EEPROM array
uint8_t M95M_ReadByte(uint32_t address){
	  //read data from address 1
	  HAL_GPIO_WritePin(EEPROM_CS_GPIO_Port, EEPROM_CS_Pin, GPIO_PIN_RESET); //activate EEPROM
	  uint8_t ReadAddress[4] = {M95M02_CMD_READ,0x00,0x00,0x00};
	  ReadAddress[3] = address & 0xff;
	  ReadAddress[2] = address >> 8 & 0xff;
	  ReadAddress[1] = address >> 16 & 0xff;;
	  HAL_SPI_Transmit(halSpiInterface, &ReadAddress[0], 4, 100);
	  HAL_SPI_Receive(halSpiInterface, &recivedData[0], 1, 100); //read data (status register)
	  HAL_GPIO_WritePin(EEPROM_CS_GPIO_Port, EEPROM_CS_Pin, GPIO_PIN_SET); //deactivate EEPROM
	  //end read data from address 1
	  return(recivedData[0]);
}

// Initiate reading of bytes array from EEPROM
void M95M_StartReadArray(uint32_t address){
	  //read data from address 1
	  HAL_GPIO_WritePin(EEPROM_CS_GPIO_Port, EEPROM_CS_Pin, GPIO_PIN_RESET); //activate EEPROM
	  uint8_t ReadAddress[4] = {M95M02_CMD_READ,0x00,0x00,0x00};
	  ReadAddress[3] = address & 0xff;
	  ReadAddress[2] = address >> 8 & 0xff;
	  ReadAddress[1] = address >> 16 & 0xff;;
	  HAL_SPI_Transmit(halSpiInterface, &ReadAddress[0], 4, 100);
}

/* M95M_ReadArray
 * Read a byte by byte from EEPROM from starting address (M95M_StartReadArray necessary) (count - 1 - 256)
 * Param: count - number of reading at one invoke
 *
 * return: pointer uint_t8 array of read data
 */
uint8_t* M95M_ReadArray(int count){
	if (count > 256) count = 256;
	for (int i=0; i<count; i++){
		  HAL_SPI_Receive(halSpiInterface, &recivedData[i], 1, 100); //read data (status register)
	}
	return(&recivedData[0]);
}

// Terminate a reading of array from EEPROM
void M95M_StopReadArray(void){
	  HAL_GPIO_WritePin(EEPROM_CS_GPIO_Port, EEPROM_CS_Pin, GPIO_PIN_SET); //deactivate EEPROM
	  //end read data from address 1
}


int poolingCheckWriteInProgressBit(void){ // Check WriteInProgress Bit in mem status register
	//read enable bit from status register
	uint8_t commandData[1] = {M95M02_CMD_RDSR};
	uint8_t EEPROM_statusReg = 0x01;

	HAL_GPIO_WritePin(EEPROM_CS_GPIO_Port, EEPROM_CS_Pin, GPIO_PIN_RESET); //activate EEPROM
	HAL_SPI_Transmit(halSpiInterface, &commandData[0], 1, 100); // read status register

	int reading = 0;
	while ((EEPROM_statusReg & 0x01) != 0 && reading < 1000){
		reading++;
		HAL_SPI_Receive(halSpiInterface, &EEPROM_statusReg, 1, 100); //read data (status register)
	}
	HAL_GPIO_WritePin(EEPROM_CS_GPIO_Port, EEPROM_CS_Pin, GPIO_PIN_SET); //deactivate EEPROM

	if ((EEPROM_statusReg & 0x01) != 0) {
		return(1);
	} else {
		return(0);
	}
	return(0);
}

/******************************************************************************/
/*!
 *  @function		M95M04
 *  @parameters		address - byte address at EEPROM array
 *  				dataByte - byte which will be write
 *  				memoryArea - memory area for writing (eeprom array (512x1024 B) or ID page (512 B))
 *	@returns		None
 *	@description	Write a byte to EEPROM.
*/
/******************************************************************************/
void M95M_WriteByte(uint32_t address, uint8_t dataByte, EEPROM_memory_area memoryArea){
		  //set Write Enable bit in eeprom status register
		  uint8_t writeEnableCommand = M95M02_CMD_WREN;
		  HAL_GPIO_WritePin(EEPROM_CS_GPIO_Port, EEPROM_CS_Pin, GPIO_PIN_RESET); //activate EEPROM
		  HAL_SPI_Transmit(halSpiInterface, &writeEnableCommand, 1, 100); // write enable bit
		  HAL_GPIO_WritePin(EEPROM_CS_GPIO_Port, EEPROM_CS_Pin, GPIO_PIN_SET); //deactivate EEPROM

		  uint8_t writeCommandBuffer[5];

		  switch (memoryArea) {
			  case ID_Page:
				  writeCommandBuffer[0] = M95M02_CMD_WRITE_ID;
				  break;
			  case EEPROM_Array:
			  default:
				  writeCommandBuffer[0] = M95M02_CMD_WRITE;
				  break;
		  }

		  writeCommandBuffer[3] = address & 0xff;
		  writeCommandBuffer[2] = address >> 8 & 0xff;
		  writeCommandBuffer[1] = address >> 16 & 0xff;;

		  writeCommandBuffer[4] = dataByte;

		  //write data
		  HAL_GPIO_WritePin(EEPROM_CS_GPIO_Port, EEPROM_CS_Pin, GPIO_PIN_RESET); //activate EEPROM
		  HAL_SPI_Transmit(halSpiInterface, &writeCommandBuffer[0], 5, 100);
		  HAL_GPIO_WritePin(EEPROM_CS_GPIO_Port, EEPROM_CS_Pin, GPIO_PIN_SET); //deactivate EEPROM

		  poolingCheckWriteInProgressBit();

}




