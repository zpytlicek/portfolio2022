
//#include <stdint.h>
#include"sht35.h"



I2C_HandleTypeDef *p_hi2cPerspiration;
I2C_HandleTypeDef *p_hi2cPCB;

void Sht35_I2C_Interface_Ini(I2C_HandleTypeDef *hi2cPCB, I2C_HandleTypeDef *hi2cPerspiration){
	p_hi2cPerspiration = hi2cPerspiration;
	p_hi2cPCB = hi2cPCB;
}

Sht35Data Sht35_read(senzorPosition poziceSenzoru){

	HAL_StatusTypeDef transmitted;
	I2C_HandleTypeDef *hi2cx;
	Sht35Data data;
	data.humidity = 0;
	data.temp = 0;
	uint16_t address;
	uint8_t commandGetData[] = {0x24, 0x0b};
	uint8_t recivedData[6];

	//sensor selection
	switch(poziceSenzoru){
		case MainSensorTop:
			address = SHT35_I2C_ADDR0<<1;
			hi2cx = p_hi2cPerspiration;
			break;
		case MainSensorBottom:
			address = SHT35_I2C_ADDR1<<1;
			hi2cx = p_hi2cPerspiration;
			break;
		case PCBSensor:
		default:
			address = SHT35_I2C_ADDR0<<1;
			hi2cx = p_hi2cPCB;
			break;
	}

	uint8_t commandReset[] = {0xff, 0xff};

	uint8_t commandReInit[] = {0x30, 0xa2};

	//Interface Reset
    transmitted = HAL_I2C_Master_Transmit(hi2cx, address, &commandReset, 2, 100);
    HAL_Delay(2);
    //Reinit Interface
    transmitted = HAL_I2C_Master_Transmit(hi2cx, address, &commandReInit, 2, 100);
    HAL_Delay(1);

    transmitted = HAL_I2C_Master_Transmit(hi2cx, address, &commandGetData, 2, 100);
    HAL_Delay(15);
    HAL_I2C_Master_Receive(hi2cx, address, &recivedData, 6, 100);
    int32_t stemp = (int32_t)(((uint32_t)recivedData[0] << 8) | recivedData[1]);
	stemp = ((4375 * stemp) >> 14) - 4500;
	data.temp = (float)stemp / 100.0f;

	uint32_t shum = ((uint32_t)recivedData[3] << 8) | recivedData[4];
	// simplified (65536 instead of 65535) integer version of:
	// humidity = (shum * 100.0f) / 65535.0f;
	shum = (625 * shum) >> 12;
	data.humidity = (float)shum / 100.0f;


	//rounding
	data.humidity = (round(data.humidity * 100))/100;
	data.temp = (round(data.temp * 100))/100;

	return(data);

}


/**
 * Performs a CRC8 calculation on the supplied values.
 *
 * @param data  Pointer to the data to use when calculating the CRC8.
 * @param len   The number of bytes in 'data'.
 *
 * @return The computed CRC8 value.
 */
uint8_t crc8(uint8_t *data, int len) {
  /*
   *
   * CRC-8 formula from page 14 of SHT spec pdf
   *
   * Test data 0xBE, 0xEF should yield 0x92
   *
   * Initialization data 0xFF
   * Polynomial 0x31 (x8 + x5 +x4 +1)
   * Final XOR 0x00
   */

  uint8_t POLYNOMIAL = 0x31;
  uint8_t crc = 0xFF;

  for (int j = len; j; --j) {
    crc ^= *data++;

    for (int i = 8; i; --i) {
      crc = (crc & 0x80) ? (crc << 1) ^ POLYNOMIAL : (crc << 1);
    }
  }
  return crc;
}
