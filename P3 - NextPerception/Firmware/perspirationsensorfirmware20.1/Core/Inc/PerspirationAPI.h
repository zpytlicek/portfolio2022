/*
 * PerspirationAPI.h
 *
 *  Created on: 30. 11. 2020
 *      Author: Zdenek Pytlicek
 */

#ifndef INC_PERSPIRATIONAPI_H_
#define INC_PERSPIRATIONAPI_H_

#include "main.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "sht35.h"
#include "utilities.h"
#include "m95m04.h"

//Application FIRMWARE constants
#define PerspirationAPI_Hardware_version "20.01"
#define PerspirationAPI_Firmware_version "21.03"

//Default system settings
#define DefaultSamplingRate SamplingRate_60spm //see PerApi_SamplingRates
#define DefaultShtSensorDistance 1125 // mm / 100
#define DefaultCaseVersion 0 // 0 - 15

//zatim nevyuzito
typedef enum {
	  SamplingRate_60spm = 0x00U, //sampling rate 60spm = 1sps
	  SamplingRate_6spm = 0x01U, //sampling rate 6spm
	  SamplingRate_2spm = 0x02U, //sampling rate 2spm
	  SamplingRate_1spm = 0x03U //sampling rate 1spm
} PerApi_SamplingRates;

typedef enum {
	Idle = 0x00U,
	Measuring = 0x01U,
	USB_Mode = 0x02U //
} PerApi_APIRunningStatus;

struct systemSettings{
	char deviceId; // Device ID [1B]
	PerApi_SamplingRates samplingRate;
	short shtSensorsDistance; // value in [mm] / 100 -> (0 - 327.67 mm)
	char caseVersion;
};

PerApi_APIRunningStatus API_Status;

struct systemSettings PerApi_SystemSettings; //Actual API settings used for measurement

float PerApi_vBat; //Acctual battery voltage
char PerApi_EEPROM_Packet[14]; //Prepare data for EEPROM Storage

void PerAPI_DefaulSettingIni(void); //Ini system settings with default values
void PerAPI_Prepare_MesHead(RTC_HandleTypeDef *pHrtc); //Prepare PerApi_EEPROM_Packet - HEAD
void PerAPI_Prepare_MesPoint(ADC_HandleTypeDef *pHadc); //Prepare PerApi_EEPROM_Packet - Measurement point
void PerAPI_Prepare_MesTail(RTC_HandleTypeDef *pHrtc); //Prepare PerApi_EEPROM_Packet - TAIL

void PerAPI_SaveSystemSetting(void);
void PerAPI_LoadSystemSetting(void);
void PerAPI_WriteMeasurementCount(uint32_t count); //store number of measurements
uint32_t PerAPI_ReadMeasurementCount(void); //read number of measurements
void PerAPI_WritePreparedData(uint32_t acctualCounter); //write data from PerApi_EEPROM_Packet to EEPROM

void PerAPI_LowPowerTimerIni(uint32_t msecTimer); //Setup time for sleeping mode
void PerAPI_LowPowerSetup(void); //Go to sleep
void PerAPI_LowPowerResume(void); //WakeUp


//Axu internal fce
float GetVbat(ADC_HandleTypeDef *pHadc); //Get Vbat over internal ADC with external Vref

#endif /* INC_PERSPIRATIONAPI_H_ */
