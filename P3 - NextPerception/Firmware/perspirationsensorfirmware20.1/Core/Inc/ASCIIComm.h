/*
 * ASCIIComm.h
 *
 *  Created on: Nov 28, 2020
 *      Author: Zdenek Pytlicek
 */

#ifndef INC_ASCIICOMM_H_
#define INC_ASCIICOMM_H_

#include "main.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "sht35.h"
#include "utilities.h"
#include "PerspirationAPI.h"

//Perspiration sensor ASCII command set
typedef enum
{
  PERSP_NOCOMMAND = 0x00U, //no command is present
  PERSP_GET_REALTIME = 0x01U, //send back actual RTC time
  PERSP_SET_REALTIME = 0x02U, //set RTC time according to pData
  PERSP_GET_SENSOR_VALUES = 0x03U, //get SHT sensor values
  PERSP_GET_HWFW_INFO = 0x04U, //get hardware firmware info
  PERSP_GET_BATTERY_VOLTAGE = 0x05U, //get actual battery voltage
  PERSP_GET_DATACOUNT = 0x06U, //get count of data packet stored in EEPROM
  PERSP_GET_DATASTREAM = 0x07U, //get data stream of all data from EEPROM
  PERSP_CLEAR_ALLDATA = 0x08U, //clear all data stored in EEPROM
  PERSP_GET_SENSOR_DISTANCE = 0x09U, //get main sensors distance
  PERSP_SET_SENSOR_DISTANCE = 0x0AU, //set main sensors distance
  PERSP_GET_SENSOR_ID = 0x0BU, //get device id
  PERSP_SET_SENSOR_ID = 0x0CU, //set device id
  PERSP_UNKNOWN_COMMAND = 0xffU, //command wasn't recognized
} PERSP_CommandsList;

//status of command receiving
typedef enum
{
	READY_FOR_NEW_COMMAND = 0x00U, 	// READY - no command
	INCOMING_COMMAND = 0x01U,       // INCOMING_COMMAND - new command is incoming
	COMMAND_READY  = 0x02U,          // COMMAND_READY - has ready command
	DATA_FLOW_CONTROLL  = 0x03U
} ASCIIComandState;

char ReciveFlowControlCommand;

char *pPERSP_string_buff;

void ASCIICommand_SwitchToFlowControl(void); //only for data flow controlling purpose
void ASCIICommand_SwitchToReceiving(void);

void recive_new_char(char incoming_char); //handling newly received char from USB
PERSP_CommandsList PRESP_PC_COMMAND(void); //return actual PC command with data as a pointer
char *PERSP_GetRTC_Time_String(RTC_HandleTypeDef *pHrtc); //get actual RTC time as string
char *PERSP_SetRTC_Time_String(RTC_HandleTypeDef *pHrtc); //set RTC time - correct ASCII command necessary
char *PERSP_GetSHT_Sensor(void); //get actual SHT sensor values
char *PERSP_GetHWFW_Info(void); //get perspiration HW/FW info
char *PERSP_GetBattery_Voltage(ADC_HandleTypeDef *hadc); //get actual battery voltage PERSP_GET_BATTERY_VOLTAGE
char *PERSP_SetSensor_Distance(void); //set sensor distance
char *PERSP_SetSensor_ID(void); //set device ID
#endif /* INC_ASCIICOMM_H_ */

