/*
 *  m95m04.h
 *
 *  The M95M04 is a SPI-interface EEPROM chip with 4Mbit capacity
 *  This is the header file for the M95M04 STM32 library
 *
 *  Created on: 23. 11. 2020
 *  Author: Zdenek Pytlicek
 *
 *  v1.0a - First release
 */

#ifndef INC_M95M04_H_
#define INC_M95M04_H_

#include "main.h"

#define M95M04_SPI_PORT		hspi2 // SPI_HandleTypeDef where M95M04 is connected

#define M95M02_PAGE_SIZE			(512)
#define M95M02_NUM_PAGES			(1024)
#define M95M02_CAPACITY_BYTES		(M95M02_PAGE_SIZE * M95M02_NUM_PAGES) // 4 Mbit

void M95M_Ini(SPI_HandleTypeDef *hspi); // Initialize interfaces

typedef enum {
	EEPROM_Array = 0x00,
	ID_Page = 0x01
} EEPROM_memory_area;

uint8_t M95M_ReadByte(uint32_t address); // Read a byte from EEPROM
uint8_t M95M_ReadIdByte(uint32_t address); // Read a byte from EEPROM Identification page

void M95M_StartReadArray(uint32_t address); // Initiate reading of bytes array from EEPROM
uint8_t* M95M_ReadArray(int count); // Read a byte by byte from EEPROM from starting address (M95M_StartReadArray necessary) (count - 1 - 256)
void M95M_StopReadArray(void); // Terminate a reading of array from EEPROM

void M95M_WriteByte(uint32_t address, uint8_t dataByte, EEPROM_memory_area memoryArea); // Write a byte to EEPROM

//uint8_t M95M_getWIP(); // Get the Write In Progress (WIP) status flag from the status register

int poolingCheckWriteInProgressBit(void); // Check WriteInProgress Bit in mem status register


#endif /* INC_M95M04_H_ */
