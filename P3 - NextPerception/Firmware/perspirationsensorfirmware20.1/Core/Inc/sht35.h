
/*
 * This Library is written and optimized by Zdenek Pytlicek
 * for Stm32 Uc and HAL-i2c lib's.
 *
 * To use this library with SHT35 humidity sensor you will need to customize the defines below.
 *
 */

// Antirecursion
#ifndef sht35
#define sht35

#include "stm32l1xx_hal.h"
#include <math.h>

// SHT35 address
#define SHT35_I2C_ADDR0        0x44 //ADDR pin connected to logic low
#define SHT35_I2C_ADDR1        0x45 //ADDR pin connected to logic high

// sensor position
typedef enum {
	PCBSensor, MainSensorTop, MainSensorBottom
} senzorPosition;

// SHT35 datastructure
typedef struct {
	  float temp;
	  float humidity;
} Sht35Data;


void Sht35_I2C_Interface_Ini(I2C_HandleTypeDef *hi2cPCB, I2C_HandleTypeDef *hi2cPerspiration); //initialize I2C interface for SHT sensors
Sht35Data Sht35_read(senzorPosition poziceSenzoru); //perform SHT35 measurement


uint8_t crc8(uint8_t *data, int len); // kalkulace CRC8, zatim nevyužito

#endif
