/*
 * utilities.h
 *
 *  Created on: 24. 11. 2020
 *      Author: Zdenek Pytlicek
 */

#ifndef INC_UTILITIES_H_
#define INC_UTILITIES_H_


void ftoa(float n, char* res, int afterpoint); //float to string
void fto2charM100(float numb, char *pChar); //float to char[2] (float value * 100)

#endif /* INC_UTILITIES_H_ */
