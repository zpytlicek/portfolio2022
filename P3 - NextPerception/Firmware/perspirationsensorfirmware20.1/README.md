# Perspiration Sensor - Firmware 21.03 (20.2.2021)

### PERSPIRATION SENSOR HW and USED IDE:

Device name : STM32L100xC/***==STM32L15xxC==***/STM32L162xC
Flash size  : 256 KBytes
Device type : MCU
Device CPU  : Cortex-M3

STM32CubeProgrammer v2.5.0-RC1 
STM32CubeIDE Version: 1.4.0 (Build: 7511_20200720_0928)
Runtime library: Reduced C, reduced C++ (--specs=nano.specs)

Firmware for perspiration sensor NextPerception (PCB version 20.1)


* * * *
## Perspiration Sensor - ASCII communication protocol ver.20.1
### Command structure:
**Accepted message format:** `‘^’ + <message> + <‘:’> + <parameters>+ ‘\r’+<’\n’>`
- `‘^’` - starts of message (0x5e)
- `‚\r‘` – end of message (CR) (0x0d)
- `‘:’` – separator character (0x3)

Example: `‘^t?\r’ -> ‘2020.09.30 14:15:22’`*

ACK/NAK response message format: ‘ACK \r\n‘
‘ACK\r\n’ – acknowledgement response (ACK)
‘NAK\r\n’ – negative acknowledgement response (NAK)

#### Hardware/firmware info:
**Get hw/fw version info:** `‘^i?\r’`
Response: `‘Perspiration Sensor\r\nHW ver.xx.xx\r\nFW ver.xx.xx\r\n’`

**Get battery voltage:** `‘^b?\r’`
Response: `‘Vbat=XX.XX V\r\n’;`

**Get device ID:** `‘^id?\r’`
Response: `‘ID=XX\r\n’;`


**Set device ID:** `‘^id:XX\r’ ` 
*Where XX is sensor ID number 0 - 99 *
Response: `‘ACK\r’` //ACK

**Get sensor distance:** `‘^sd?\r’`
Response `‘Sensor distance: XX.XX mm\r\n’`

**Set sensor distance:** `‘^sd:NN.NN\r’`
*Where NN.NN is SHT sensor distance in [mm] (00.00 - 99.99 mm)*
Response: `‘ACK\r’` //ACK
Example: `‘^sd:11.25\r’ -> ‘ACK\r\n’`

#### Real time commands: 
**Set real time:** `‘^t:yyyy.mm.dd;hh:mm:ss;W\r’` 
*Where (‘yyyy.mm.dd;hh:mm:ss’ 24-hour clock format, W – week day (0x01-MONDAY – 0x07 SUNDAY)) *
Response: `‘ACK\r’` //ACK
Example: `‘^t:2020.07.23;15:30:00;1\r’ -> ‘ACK\r\n’`
**Get real time:** `‘^t?\r’`
Response `‘yyyy.mm.dd hh:mm:ss\r\n’`
Example: `‘^t?\r’ -> ‘2020.09.30 14:15:22\r\n’`

#### Sensor commands:
**Get actual sensor values (t,RH) - measurement:** `‘^mL? \r\n’` 
*Where L is sensor location (**b** – bottom sensor, **t** – top sensor, **p** – PCB sensor, **a** – all sensors)
Response: S.N: `TT.TT'C, RH.RH%\r\n` ... where TT.TT – temperature, RH.RH – relative humidity 

Example: `‘^mb?\r’` -> `‘S.B.: 24.00'C, 45.00%\r\n’`
Example: `‘^ma?\r’` -> `‘S.P.: 24.00'C, 45.00%; S.B.: 24.00'C, 45.00%; S.T.: 24.00'C, 45.00%\r\n’`

#### Data commands:
**Get count of data packets stored in EEPROM:** `‘^dc?\r’` 
Response: `‘D#nnnn\r\n‘` *... where nnnn is int number of data packets, if no data then 0*

**Get binar data stream from EEPROM:** `‘^db?\r’` 
Response: `‘BINARE DATASTREAM\r\n‘` *... where BINARE DATASTREAM: measured data in binare format 14B packets - see section "EEPROM DATA FORMAT (EEPROM - M95M04-DR"*

NOTE: The data stream is sent in packets of 210B. After each send, the device waits 250 ms for confirmation by sending character '1'. Then another 210B is sent. The character '2' means resending the last package of 210B. Char '0' means the end of the communication.

**Erase all stored data from EEPROM:** `‘^de!\r’` 
Response: `‘ACK\r\n’` //ACK

* * * *

### EEPROM DATA FORMAT (EEPROM - M95M04-DR)

524288 * 8bytes - main EEPROM memory array (measured data) + 512 byte Identification page - API DATA


###### **Measured data format:**

STORED DATA : [HEAD][POINTS][POINTS][POINTS]...[POINTS][TAIL][HEAD][POINTS][POINTS]....[TAIL]

**MEASUREMENT HEAD** [14 Byte]: HeadMARK, DateTime (Start of Measurement), Sampling Rate (60spm/6spm/2spm/1spm), DistanceOfSensorsInHead (X.X mm), case version (0x00)
- MSB[3B] HeadMARK: `'^','h','s'`
- [6B] DateTime : Year[1B],Month[1B],Day[1B],Hour[1B],Minute[1B],Sec[1B] 
- [MSB4b] Sampling rate [samples per minute]: 0 - 16 version / 0x0 - 60spm; 0x1 - 6spm; 0x2 - 2spm; 0x3 - 1spm; 0x4-0xf - not used // (NOT implemented in FW 2.03)
- [4bLSB] case version LSB: 0 - 15 / 0x0, 0xf - not specified  // (NOT implemented in FW 2.03)
- [2B] Distance Of SHT Sensors in chamber: short (0 - 32767) / value in [mm] / 100 -> (0 - 327.67 mm)
- [2B]LSB not used  - 0xff, 0xff

**MEASURED POINT** [14xByte]: Sensor PCB (T+H), Sensor Bottom(T+H),Sensor Top (T+H), Battery Voltage
- MSB[4B] Sensor PCB temp[2B], RH[2B] / 2 x short (-32768 - 32767) measured float value * 100
- [4B] Sensor Bottom temp[2B], RH[2B] / 2 x short (-32768 - 32767) measured float value * 100
- [4B] Sensor Top temp[2B], RH[2B] / 2 x short (-32768 - 32767) measured float value * 100
- [2B]LSB Voltage  / 1 x short (-32768 - 32767) measured float value * 100

**MEASUREMENT TAIL** [14 Byte]: TailMARK, DateTime (Start of Measurement) 
- MSB[3B] HeadMARK: `'^','m','t'`
- [6B] DateTime : Year[1B],Month[1B],Day[1B],Hour[1B],Minute[1B],Sec[1B] 
- [5B]LSB not used  - all 0xff

- - -

###### PERSPIRATION SENSOR API DATA stored in Identification page
-  address[0x00] - Device ID [1B]
-  address [0x01-x02] - Distance Of SHT Sensors in chamber: short (0 - 32767) / value in [mm] / 100 -> (0 - 327.67 mm)
-  address[0xf0-0xf3] - Number of measured and saved values in EEPROM memory array [4B]
